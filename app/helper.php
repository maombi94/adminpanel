<?php

function user_name($id,$schema){
    return DB::table($schema.'.user')->where('userID',$id)->first()->name;
}


function json_call($array = null) {
    if (isset($_GET['callback']) === TRUE) {
        header('Content-Type: text/javascript;');
        header('Access-Control-Allow-Origin: http://client');
        header('Access-Control-Max-Age: 3628800');
        header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');

        return request('callback') . '(' . (json_encode($array)) . ')';
    }
}

function form_error($errors, $tag) {
    if ($errors != null && $errors->has($tag)) {
        return $errors->first($tag);
    }
}

function createRoute() {
    $url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
    $url_param = explode('/', $url);

    $controller = isset($url_param[2]) && !empty($url_param[2]) ? $url_param[2] . '' : 'Dashboard';
    $method = isset($url_param[3]) && !empty($url_param[3]) ? $url_param[3] : 'index';
    $view = $method == 'view' ? 'show' : $method;

    return in_array($controller, array('public', 'storage')) ? NULL : ucfirst($controller) . '@' . $view;
}

function timeAgo($datetime, $full = false) {
    return \Carbon\Carbon::createFromTimeStamp(strtotime($datetime))->diffForHumans();
}

/**
 * Drop-down Menu
 *
 * @access  public
 * @param   string
 * @param   array
 * @param   string
 * @param   string
 * @return  string
 */
if (!function_exists('form_dropdown')) {

    function form_dropdown($name = '', $options = array(), $selected = array(), $extra = '') {
        if (!is_array($selected)) {
            $selected = array($selected);
        }

        // If no selected state was submitted we will attempt to set it automatically
        if (count($selected) === 0) {
            // If the form name appears in the $_POST array we have a winner!
            if (isset($_POST[$name])) {
                $selected = array($_POST[$name]);
            }
        }

        if ($extra != '')
            $extra = ' ' . $extra;

        $multiple = (count($selected) > 1 && strpos($extra, 'multiple') === FALSE) ? ' multiple="multiple"' : '';

        $form = '<select name="' . $name . '"' . $extra . $multiple . ">\n";

        foreach ($options as $key => $val) {
            $key = (string) $key;

            if (is_array($val) && !empty($val)) {
                $form .= '<optgroup label="' . $key . '">' . "\n";

                foreach ($val as $optgroup_key => $optgroup_val) {
                    $sel = (in_array($optgroup_key, $selected)) ? ' selected="selected"' : '';

                    $form .= '<option value="' . $optgroup_key . '"' . $sel . '>' . (string) $optgroup_val . "</option>\n";
                }

                $form .= '</optgroup>' . "\n";
            } else {
                $sel = (in_array($key, $selected)) ? ' selected="selected"' : '';

                $form .= '<option value="' . $key . '"' . $sel . '>' . (string) $val . "</option>\n";
            }
        }

        $form .= '</select>';

        return $form;
    }

}
 
 function default_school($id){
    //dd($id);
    if ((int) $id > 0) {
        return \App\Models\GroupUser::where('user_id', $id)->first()->group->clients()->first()->client()->first();
    }

}

function can_access($permission) {
    $user_id = \Auth::user()->id;
    if ((int) $user_id > 0) {
        $global = userAccessRole();
        return in_array($permission, $global) ? 1 : 0;
    }
}

function userAccessRole() {
    $user_id = \Auth::user()->id;

    if ((int) $user_id > 0) {
        $user = \App\Models\User::find($user_id);  
        $permission = \App\Models\PermissionRole::where('role_id', $user->role_id)->get();
      
        $objet = array();
      
        if (count($permission) > 0) {
            foreach ($permission as $perm) {
                array_push($objet, $perm->permission->name);
            }
        }
       
        return $objet;
    }
}


 function load_users(){
    $setting="'setting'";
    $id = \Auth::user()->id;
    if ((int) $id > 0) {
        $group = \App\Models\GroupUser::where('user_id', $id)->first();
        $schema = \App\Models\Client::whereIn('id', \App\Models\ClientGroup::where('group_id', \App\Models\GroupUser::where('user_id', $id)->first()->group_id)->get(['client_id']))->get(['username']);
        return \DB::SELECT('SELECT count(id),"table" from admin.all_users where status=1 and "table" != '.$setting.' and schema_name in (SELECT username from admin.clients where id in (select client_id from admin.client_groups where group_id='.$group->id.')) GROUP BY "table"');
    }else{
        return array();
    }
}

 function load_schemas(){
    $id = \Auth::user()->id;
    if ((int) $id > 0) {
        $group = \App\Models\GroupUser::where('user_id', $id)->first();
        return \App\Models\Client::whereIn('id', \App\Models\ClientGroup::where('group_id', $group->group_id)->get(['client_id']))->get(['username']);
    }else{
        return array();
    }
}

 function available_schemas(){
    $allschemas = array();
    $values = load_schemas();
        for ($i =0;$i < count($values); $i++){
          array_push($allschemas, $values[$i]->username);
     }
     return $allschemas;
 }


function get_avg_format($schema) {
    $avg_format = DB::table($schema.'.setting')->first();
    return $avg_format->exam_avg_format;
}

    function school($schema){
        return \App\Models\Client::where('username',$schema)->first();
    }

    function school_name($id){
        $id = \Auth::user()->id;
        if ((int) $id > 0) {
            $group = \App\Models\GroupUser::where('user_id', $id)->first();
            return  \App\Models\Client::whereIn('id', \App\Models\ClientGroup::where('group_id', $group->group_id)->get(['client_id']))->first()->name;
        }else{
            return 'School Name';
        }
    }
function total_students($schema){
        $schema_name = $schema.'.student' == null ? '' : 'shulesoft.student';
        return DB::table($schema_name)->where("status", '<>', 0)->count();
}

function total_boys($schema){
    $schema_name = $schema.'.student' == null ? '' : 'shulesoft.student';
    return DB::table($schema_name)->where("status", '<>', 0)->where('sex','LIKE','%M%')->count();
}

function total_girls($schema){
    $schema_name = $schema.'.student' == null ? '' : 'shulesoft.student';
    return DB::table($schema_name)->where("status", '<>', 0)->where('sex','LIKE','%F%')->count();
}


function customdate($date){
    return date("F j, Y"); 
}

function dobdate($date){
    return date("d F, Y"); 
}

function classname($schema,$id){
    return DB::table($schema.'.classes')->where('classesID',$id)->first()->classes;
}

function section_stream($schema,$c_id,$s_id){
    return DB::table($schema.'.section')->where('sectionID',$s_id)->where('classesID',$c_id)->first()->section;
}


function money($value){
    return number_format($value);
}

function invoice_number($schema,$std_id,$id){
    return DB::table($schema.'.invoices')->where('student_id',$std_id)->where('id',$id)->first()->reference;
}


function student_profile($schema,$student_id){
    return DB::table($schema.'.student')->where('student_id',$student_id)->first();
}


function get_position($student_id, $examID, $subject_id, $classesID, $sectionID = null, $schema) {

    if ($sectionID == NULL) {
        $sql = 'select subject, mark, rank FROM (
select a."examID", a."subject", a."classesID", a.student_id, a.mark, rank() over (partition by a."subjectID" order by a.mark desc) from ' . $schema . '.mark a where a."examID" = ' . $examID . ' and a."classesID" =' . $classesID . ' and "subjectID" = ' . $subject_id . '  and a.mark is not null 
) a where student_id=' . $student_id;
    } else {
        $sql = 'select subject, mark, rank FROM ( select subject, a."examID", a."classesID", a.student_id, a.mark, rank() over (partition by a."subjectID" order by a.mark desc) from ' . $schema . '.mark a where a."examID" = ' . $examID . ' and a."classesID" = ' . $classesID . ' and "subjectID" =' . $subject_id . ' and student_id in (select student_id from ' . $schema . '.student_archive where section_id = ' . $sectionID . ' ) and a.mark is not null) a where student_id=' . $student_id;
    }
    return \collect(\DB::SELECT($sql))->first();
}


function get_teacher_signature($sectionID, $schema){
    $sql='select a.signature, a.name,a.phone FROM '.   $schema.'.teacher a JOIN  '.   $schema.'.section b ON b."teacherID"=a."teacherID" where b."sectionID"='.$sectionID;
    return \collect(\DB::SELECT($sql))->first();
}


 function load_sexpersection($schema,$year_id,$section, $sex){
    $like = "'".$sex."%'";
    return \collect(\DB::SELECT('SELECT count(sex) as sex from '.$schema.'.student WHERE UPPER(sex) like '.$like.' and  student_id in (select student_id from '.$schema.'.student_archive where academic_year_id = '.$year_id.' AND section_id ='.$section.' AND status=1)'))->first()->sex;
}

function load_sexoverall($class_id,$sex,$schema_name){
    $like = '%'.$sex.'%';
    
     return DB::table('admin.all_student')
    ->join('admin.all_classes','admin.all_student.classesID','=','admin.all_classes.classesID')
    ->join('constant.refer_classes','constant.refer_classes.id','=','admin.all_classes.refer_class_id')
    ->select(DB::raw('count(admin.all_student.sex) as sex'))
    ->whereIn('admin.all_student.schema_name',available_schemas())
    ->where('admin.all_student.status','<>',0)
    ->where('admin.all_student.sex','LIKE', $like)
    ->where('constant.refer_classes.id','=', $class_id)
    ->where('admin.all_student.schema_name','=', $schema_name)
    ->first()->sex;
}

function parent_name($p_id,$schema){
    return $name = DB::table($schema.'.parent')->where('parentID',$p_id)->first()->name;
}

function teacher($t_id,$schema){
    return $name = DB::table($schema.'.teacher')->where('teacherID',$t_id)->first();
}

function to2decimal($foo){
  return  number_format((float)$foo, 2, '.', '');
}


if (!function_exists('img')) {

    function img($src = '', $index_page = FALSE) {
        if (!is_array($src)) {
            $src = array('src' => $src);
        }

        // If there is no alt attribute defined, set it to an empty string
        if (!isset($src['alt'])) {
            $src['alt'] = '';
        }

        $img = '<img';

        foreach ($src as $k => $v) {

            if ($k == 'src' AND strpos($v, '://') === FALSE) {

                $img .= ' src="' . url($v) . '"';
            } else {
                $img .= " $k=\"$v\"";
            }
        }

        $img .= '/>';

        return $img;
    }

}

    function clean_htmlentities($id) {
        return htmlentities($id, ENT_QUOTES, "UTF-8");
    }
    
    if (!function_exists('anchor')) {

        function anchor($uri = '', $title = '', $attributes = '') {
            $attr = '';
            if (is_array($attributes)) {
                foreach ($attributes as $key => $value) {
                    $attr .= $key . '="' . $value . '" ';
                }
            } else {
                $attr = $attributes;
            }
            return '<a href="' . url($uri) . '"' . $attr . '>' . $title . '</a>';
        }
    
    }
    
    
    function btn_add_pdf($uri, $name) {
        return anchor($uri, "" . $name, "class='btn btn-primary btn-sm' style='' role='button' target='_blank'");
    }
    
    function btn_sm_edit($uri, $name) {
        return anchor($uri, "<i class='fa fa-edit'></i> " . $name, "class='btn btn-info btn-sm' style='text-decoration: none;' role='button'");
    }
    
    function btn_sm_add($uri, $name) {
        return anchor($uri, "<i class='fa fa-plus'></i> " . $name, "class='btn btn-success btn-sm' style='text-decoration: none;' role='button'");
    }
    
    function btn_payment($uri, $name) {
        return anchor($uri, "<i class='fa fa-credit-card'></i> " . $name, "class='btn btn-success btn-sm' style='text-decoration: none;' id='menu_toggle' role='button'");
    }
    
    
    function btn_view($uri, $name) {
        return anchor($uri, "<i class='fa fa-check-square-o'></i> View", "class='btn btn-success btn-sm ' data-placement='top' target='_blank' data-toggle='tooltip' data-original-title='" . $name . "'");
    }

    
    function btn_edit($uri, $name) {
        return anchor($uri, "<i class='fa fa-edit'></i> Edit", "class='btn btn-warning btn-sm ' data-placement='top' data-toggle='tooltip' data-original-title='" . $name . "'");
    }

    function btn_delete($uri, $name, $title = null) {
        $confirm = $title == null ? 'you are about to delete a record. This cannot be undone. are you sure?' : $title;
        array(
            'onclick' => "return confirm('" . $confirm . "')",
            'class' => 'btn btn-danger btn-sm',
            'data-placement' => 'top',
            'data-toggle' => 'tooltip',
            'data-original-title' => $name
        );
        return anchor($uri, "<i class='fa fa-trash-o'></i> Delete", " class='btn btn-danger btn-sm ' data-placement='top' data-toggle='tooltip' data-original-title='{$name}' onclick='return confirm(\"$confirm\")' ");
    }
    /*
 * *  Function:   Convert number to string
 * *  Arguments:  int
 * *  Returns:    string
 * *  Description:
 * *      Converts a given integer (in range [0..1T-1], inclusive) into
 * *      alphabetical format ("one", "two", etc.).
 */

function number_to_words($number) {
    if (($number < 0) || ($number > 999999999)) {
        return "$number";
    }

    $Gn = floor($number / 1000000);  /* Millions (giga) */
    $number -= $Gn * 1000000;
    $kn = floor($number / 1000);     /* Thousands (kilo) */
    $number -= $kn * 1000;
    $Hn = floor($number / 100);      /* Hundreds (hecto) */
    $number -= $Hn * 100;
    $Dn = floor($number / 10);       /* Tens (deca) */
    $n = $number % 10; /* Ones */

    $res = "";

    if ($Gn) {
        $res .= number_to_words($Gn) . " Million";
    }

    if ($kn) {
        $res .= (empty($res) ? "" : " ") .
                number_to_words($kn) . " Thousand";
    }

    if ($Hn) {
        $res .= (empty($res) ? "" : " ") .
                number_to_words($Hn) . " Hundred";
    }

    $ones = array("", "One", "Two", "Three", "Four", "Five", "Six",
        "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen",
        "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen",
        "Nineteen");
    $tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty",
        "Seventy", "Eighty", "Ninety");

    if ($Dn || $n) {
        if (!empty($res)) {
            $res .= " and ";
        }

        if ($Dn < 2) {
            $res .= $ones[$Dn * 10 + $n];
        } else {
            $res .= $tens[$Dn];

            if ($n) {
                $res .= "-" . $ones[$n];
            }
        }
    }

    if (empty($res)) {
        $res = "zero";
    }

    return $res;
}

function monthly_revenues(){
    $current_month_total_revenues = 0;

        foreach(load_schemas() as $schema_name){
            $current_month_schemas_revenues = \DB::table($schema_name->username.'.revenues')->whereYear('created_at',date("Y"))->whereMonth('created_at',date('m'))->sum('amount');
            $current_month_total_revenues += $current_month_schemas_revenues;
        }
        return $current_month_total_revenues;
}

function monthly_expenses(){
    $current_month_total_expenses = 0;

        foreach(load_schemas() as $schema_name){
            $current_month_schemas_expenses = \DB::table($schema_name->username.'.salaries')->whereYear('created_at',date("Y"))->whereMonth('created_at',date('m'))->sum('basic_pay');
            $current_month_total_expenses += $current_month_schemas_expenses;
        }
        return $current_month_total_expenses;
}

function monthly_salary(){
    $total_month_salary = 0;
    foreach(load_schemas() as $schema_name){
        $current_month_salary = \DB::table($schema_name->username.'.salaries')->whereYear('created_at',date("Y"))->whereMonth('created_at', date('m'))->sum('basic_pay');
        $total_month_salary += $current_month_salary;
    }
    return $total_month_salary;
}

function all_schema_total_revenues(){
    $all_schema_total_revenue=0;
    foreach (load_schemas() as $schema_name) {
        $schema_revenues = \DB::table($schema_name->username.'.revenues')->whereDate('created_at', '>=', date('Y-01-01'))->sum('amount');
        $all_schema_total_revenue += $schema_revenues; 
    }
    return $all_schema_total_revenue;
}

function monthly_payments(){
    $total_month_salary = 0;

        foreach(load_schemas() as $schema_name){
            $current_month_payment = \DB::table($schema_name->username.'.payments')->whereYear('created_at',date("Y"))->whereMonth('created_at', date('m'))->sum('amount');
            $total_month_salary += $current_month_payment;
        }
        return $total_month_salary;
}

function all_schema_total_payments(){
    $total_revenue=0;
    foreach (load_schemas() as $schema_name) {
        $payment_total_revenue = \DB::table($schema_name->username.'.payments')->whereDate('created_at', '>=', date('Y-01-01'))->sum('amount');
        $total_revenue += $payment_total_revenue; 
    }
    return $total_revenue;
}


function all_total_salary(){
    $all_schema_total_revenue=0;
    foreach (load_schemas() as $schema_name) {
        $schema_revenues = \DB::table($schema_name->username.'.salaries')->whereDate('created_at', '>=', date('Y-01-01'))->sum('basic_pay');
        $all_schema_total_revenue += $schema_revenues; 
    }
    return $all_schema_total_revenue;
}

function all_schema_total_expenses(){
    $all_schema_total_expenses=0;
        foreach (load_schemas() as $schema_name) {
            $schema_expenses = \DB::table($schema_name->username.'.expense')->whereDate('created_at', '>=', date('Y-01-01'))->sum('amount');
            $all_schema_total_expenses += $schema_expenses;
         }
    return $all_schema_total_expenses;
}
// END ACCOUNTING

    function find_total_sex($schema,$class_id,$year_id, $type){
        $where = "'LIKE','%".$type."%'";
        if($class_id == null && $year_id != null){
            return  DB::table($schema.'.student')->where('academic_year_id',$year_id)->where("status", '<>', 1)->where('name', 'like', '%' . $type . '%')->count(); 
        }elseif($class_id != null && $year_id != null){
            return  DB::table($schema.'.student')->where('academic_year_id',$year_id)->where('classesID',$class_id)->where("status", '<>', 1)->where('name', 'like', '%' . $type . '%')->count(); 
        }else{
            return  DB::table($schema.'.student')->where("status", 1)->where('name', 'like', '%' . $type . '%')->count();     
        }
    }


function identical_values($arrayA, $arrayB) {

    sort($arrayA);
    sort($arrayB);

    return $arrayA == $arrayB;
}

if (!function_exists('return_grade')) {

    function return_grade($grades, $avg, $show_point) {
        $data = '';

        foreach ($grades as $grade) {
            if ($grade->gradefrom <= $avg && $grade->gradeupto >= $avg) {
                $data .= "<td>" .
                        $grade->grade .
                        "</td>";
                $data .= $show_point == 1 ? "<td>" .
                        $grade->point .
                        "</td>" : '';
                $data .= '<td>' . $grade->note . '</td>';
                return $data;
            }
        }
    }

}

function return_grade_point($grades, $avg, $level_format = NULL) {
    $data = array();

    foreach ($grades as $grade) {
        if ($grade->gradefrom <= $avg && $grade->gradeupto >= $avg) {
            $data = array('grade' => $grade->grade, 'point' => $grade->point);
            return $data;
        }
    }
    return $data;
}
