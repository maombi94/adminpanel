<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;

class Revenue extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {

        parent::__construct();
        // $this->lang->load('revenue');
    }

    public function index() {
            $id = clean_htmlentities((request()->segment(3)));
            $schema_name = clean_htmlentities((request()->segment(4)));
            if ((int) $id) {
                $this->data['id'] = $id;
                if ($_POST) {
                    $this->data['revenues'] = \DB::table($schema_name.'.revenues')->where('refer_expense_id', $id)->where('date', '>=', request('from_date'))->where('date', '<=', request('to_date'))->get();
                } else {

                    $this->data['revenues'] = \DB::table($schema_name.'.revenues')->where('refer_expense_id', $id)->get();
                }
                return view("account.revenues.index", $this->data);
            } else {
                $this->data['id'] = null;
                $this->data['revenues'] = \DB::table($schema_name.'.revenues')->get();

                $this->data['expenses'] = \DB::table($schema_name.'.refer_expense')->whereIn('financial_category_id', [1])->get();

                $this->data["subview"] = "revenue/index";
            }

            return view($this->data["subview"], $this->data);
    }

    public function view_expense() {
        $id = clean_htmlentities(($this->uri->segment(3)));
        if (can_access('view_expense')) {
            if ($_POST) {
                if (request("to_date")) {
                    $d1 = request("to_date");
                    $to_date = $this->createDate($d1);
                } else {
                    $to_date = request("to_date");
                }
                if (request("from_date")) {
                    $d2 = request("from_date");
                    $from_date = $this->createDate($d2);
                } else {
                    $from_date = request("from_date");
                }

                $this->data['expenses'] = $this->expense_m->get_join_expenses_by_time_interval($id, $from_date, $to_date);
            } else {
                $this->data['expenses'] = $this->expense_m->get_join_expenses($id);
            }
            $this->data['id'] = $id;
            $this->data["subview"] = "expense/expense_category";
            $this->load->view('_layout_main', $this->data);
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    protected function rules() {
        return $this->validate(request(), [
                    'payer_name' => 'required',
                    'payer_phone' => 'required|max:20',
                    'email' => 'email',
                    'amount' => 'required|numeric|min:1',
                    'payment_type_id' => 'required|min:1',
                    'note' => 'required|max:600'
                        ], $this->custom_validation_message);
    }

    protected function rulesWithUser() {
        return $this->validate(request(), [
                    'user_id' => 'required',
                    'amount' => 'required|numeric|min:1',
                    'payment_type_id' => 'required|min:1',
                    'note' => 'required|max:600'
                        ], $this->custom_validation_message);
    }

    function uniqueInvoice() {
        $invoiceNo = rand(1, 999) . substr(str_shuffle('ABCDEFGHJKLMNPQRSTUVWXYZ'), 0, 3) . rand(1, 999);
        $data = DB::table('revenues')->where('invoice_number', $invoiceNo)->first();
        if (!empty($data)) {
            return $this->uniqueInvoice();
        } else {
            return $invoiceNo;
        }
    }

    public function add() {
        if (can_access('add_revenue')) {
            if ($_POST) {
                    if(request('category') == 2 ){
                        $ref = \App\Model\ReferExpense::firstOrFail();
                        $refer_expense_id = $ref->id;
                    }elseif(request('category') == 3){
                        $ref = \App\Model\ReferExpense::where('name', 'Dispensary')->first();
                        $refer_expense_id = $ref->id;
                    }else{
                        $refer_expense_id = request('refer_expense_id');
                    }
                    $transaction_id = request('transaction_id');
                    if (request('user_in_shulesoft') == 1) {
                        $this->rulesWithUser();
                        $user_request = explode(',', request('user_id'));
                        $transaction = DB::table('revenues')->where('transaction_id', $transaction_id)->first();
                        if (!empty($transaction)) {
                            $this->session->set_flashdata('error', 'The Reference number already exist');
                            return redirect(base_url("revenue/add"));
                        }
                        $user = \App\Model\User::where('id', $user_request[0])->where('table', $user_request[1])->first();
                        $payer_name = $user->name;
                        $payer_phone = $user->phone;
                        $payer_email = $user->email;
                        $obj = array_merge(request()->except('refer_expense_id', 'date', 'transaction_id', 'amount'), [
                            'payer_name' => $user->name,
                            'transaction_id' => $transaction_id,
                            'amount' =>request('amount'),
                            'user_id' => $user->id,
                            'date' => date("Y-m-d", strtotime(request('date'))),
                            'user_table' => $user->table,
                            'payer_email' => $user->email,
                            'payer_phone' => $user->phone,
                            'refer_expense_id' => $refer_expense_id,
                            'invoice_number' => $this->uniqueInvoice()
                        ]);

                        $revenue = \App\Model\Revenue::create($obj);
                    } else {
                        $this->rules();
                        $transaction_id = DB::table('revenues')->where('transaction_id', request('transaction_id'))->first();
                        if (!empty($transaction_id) ) {

                            $this->session->set_flashdata('error', 'The Reference number already exist');
                            return redirect(base_url("revenue/add"));
                        }


                        $payer_name = request('payer_name');
                        $payer_phone = request('payer_phone');
                        $payer_email = request('payer_email');
                        $obj = array_merge(request()->except('refer_expense_id'), [
                            'refer_expense_id' => $refer_expense_id,
                            'invoice_number' => $this->uniqueInvoice()
                        ]);
                        $revenue = \App\Model\Revenue::create($obj);
                    }

                    if ((int) request('category') >= 2 || (int) request('category') >= 3) {
                        $products = request('product_alert_id');
                        if(count($products)){
                        foreach ($products as $key => $product) {
                            $item = \App\Model\ProductQuantity::find(request('product_alert_id')[$key]);
                            if(!empty($item)){
                            $cart = [
                                    'name' => $item->name,
                                    'product_alert_id' => request('product_alert_id')[$key],
                                    'quantity' => request('quantity')[$key],
                                    'revenue_id' => $revenue->id,
                                    'amount' => request('quantity')[$key] * request('amounts')[$key]
                                ];
                              //  print_r($cart); exit();
                             DB::table('product_cart')->insert($cart);
                                }
                            }   
                        }
                    //   return $this->addInventoryRevenue();
                    }

                if(request('sent_sms') == 1){

                    $ref_code = 'SSR.' . time() . '.' . date('Y') . $revenue->id;
                    $message = sprintf($this->message('add_revenue', $this->message_language()), $payer_name, $this->data['siteinfos']->currency_symbol . ' ' . remove_comma(request('amount')), $this->data['siteinfos']->sname, $revenue->invoice_number);

                    $this->send_sms($payer_phone, $message);
                    $this->send_email($payer_email, 'Payment Accepted', $message);
                }
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                return redirect(base_url("revenue/receipt/" . $revenue->id));
            } else {
                $this->data['banks'] = \App\Model\BankAccount::all();
                $this->data["payment_types"] = \App\Model\PaymentType::where('id', '<>', 6)->get();
                $this->data["category"] = \App\Model\ReferExpense::where('financial_category_id', 1)->get();
                $this->data["subview"] = "revenue/add";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    protected function category_rules($id) {
        $this->validate(request(), [
            "subcategory" => "required|regex:/(^([a-zA-Z ]+)(\d+)?$)/u",
            "code" => "regex:/(^[ A-Za-z0-9_@.#&+-]*$)/u|required|iunique:refer_expense,code," . $id,
            "financial_category_id" => "required|min:1"
                ], $this->custom_validation_message);
    }

    public function check_transaction_id() {
        return !empty(\App\Model\Revenue::where('transaction_id', request('trans_id'))->first()) ? 'This transaction ID is in use' : 'success';
    }

    public function createCode($last_code = 12345) {
        $number_part = substr($last_code, -3);
        return strtoupper(substr(set_schema_name(), 0, 2)) . '-' . ((int) $number_part + 1);
    }

    public function financial_expense($id, $from_date, $to_date) {

        return $this->expense_m->get_account_report($id, $from_date, $to_date);
    }

    public function edit() {
        if (can_access('edit_expense')) {
            $id = clean_htmlentities(($this->uri->segment(3)));
            if ((int) $id) {
                $this->data['revenue'] = \App\Model\Revenue::find($id);
                $this->data["payment_types"] = \App\Model\PaymentType::all();
                $this->data['banks'] = \App\Model\BankAccount::all();
                if ($this->data['revenue']) {
                    $this->data["category"] = \App\Model\ReferExpense::whereIn('financial_category_id', [1])->get();
                    if ($_POST) {

                        $this->rules();

                        $this->data['revenue']->update(request()->all());
                        $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                        return redirect(base_url("revenue/index"));
                    } else {
                        $this->data["subview"] = "revenue/edit";
                        $this->load->view('_layout_main', $this->data);
                    }
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function delete() {
        if (can_access('delete_expense')) {
            $id = clean_htmlentities(($this->uri->segment(3)));
            if ((int) $id) {
                $revenue = \App\Model\Revenue::find($id);
                $revenue->delete();
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                return redirect()->back();
            } else {
                $this->session->set_flashdata('error', $this->lang->line('menu_error'));
                return redirect(base_url("revenue/index"));
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function receipt() {
            $id = clean_htmlentities((request()->segment(3)));
            $this->data['schema_name'] = $schema_name = clean_htmlentities((request()->segment(4)));
            $this->data['siteinfos'] = \DB::table($schema_name.'.setting')->first();
            if ((int) $id) {
                $this->data['revenue'] = $a= \DB::table($schema_name.'.revenues')->where('id',$id)->first();
                $receipt_setting = \DB::table($schema_name.'.receipt_settings')->first();
                $template = $receipt_setting->template;
                $file = 'invoices.receipt_templates.' . $template;
                if ($_POST) {
                    $settings = DB::table($schema_name.'.receipt_settings')->first();
                    $vars = get_object_vars($settings);
                    $obj = array();
                    foreach ($vars as $key => $variable) {
                        if (!in_array($key, array('id', 'available_templates', 'created_at'))) {
                            $obj = array_merge($obj, array($key => request($key) == null ? 0 : request($key)));
                        }
                    }
                    !empty($obj) ? \App\Model\ReceiptSetting::first()->update($obj) : '';
                }
                $this->data['productcart'] = \DB::table($schema_name.'.product_cart')->where('revenue_id', $id)->get();
                return view('account.revenues.receipt', $this->data);
            } 
    }

    private function checkKeysExists($value) {
        $required = array('user_in_shulesoft', 'payer_name', 'payer_phone', 'payer_email', 'revenue_type',
            'amount', 'payment_method', 'reference_number', 'date', 'note');


        $data = array_change_key_case(array_shift($value), CASE_LOWER);
        $keys = str_replace(' ', '_', array_keys($data));
        $results = array_combine($keys, array_values($data));
        if (count(array_intersect_key(array_flip($required), $results)) === count($required)) {
            //All required keys exist!            
            $status = 1;
        } else {
            $missing = array_intersect_key(array_flip($required), $results);
            $data_miss = array_diff(array_flip($required), $missing);
            $status = ' Column with title <b>' . implode(', ', array_keys($data_miss)) . '</b>  miss from Excel file. Please make sure file is in the same format as a sample file';
        }

        return $status;
    }

    /**
     * This method is not yet complete, it does not work as expected
     * @param type $account_data=null
     */
    public function uploadByFile($account_data = null) {
        ini_set('max_execution_time', 300); //overwrite execution time, 5min
        $data = $account_data == null ? $this->uploadExcel() : $account_data;
        $status = $this->checkKeysExists($data);
        if ((int) $status == 1) {
            $status = '';
            foreach ($data as $value_array) {
                $call_array = new \App\Http\Controllers\Student();
                $value = $call_array->modify_keys_to_upper_and_underscore($value_array);
                $in_data = [
                    'created_by_id' => session('id'),
                    'amount' => $value['amount'],
                    'payment_method' => $value['payment_method'],
                    'transaction_id' => $value['reference_number'],
                    'date' => $value['date'],
                    'note' => $value['note']
                ];



                if ((int) $value['user_in_shulesoft'] == 1) {

                    $user = \App\Model\User::where('name', $value['payer_name'])->where('table', $value['type'])->first();
                    if (empty($user)) {
                        $status .= 'This user information ' . $value['payer_name'] . ' does not exists';
                    }
                    $data = array_merge($in_data, [
                        'payer_name' => $user->name,
                        'user_id' => $user->id,
                        'user_table' => $user->table,
                        'payer_email' => $user->email,
                        'payer_phone' => $user->phone
                    ]);
                } else {
                    $data = [
                        'payer_name' => $value['payer_name'],
                        'payer_phone' => $value['payer_phone'],
                        'payer_email' => $value['payer_email'],
                        'created_by_id' => session('id'),
                        'amount' => $value['amount'],
                        'payment_method' => $value['payment_method'],
                        'transaction_id' => $value['reference_number'],
                        'date' => $value['date'],
                        'note' => $value['note']
                    ];
                }

                $revenue = \App\Model\Revenue::create($data);
            }
        }
    }

}
