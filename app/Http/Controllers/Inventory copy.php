<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use \App\Model\ReferExpense;
use DB;
use Illuminate\Validation\Rule;

class Inventory extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
        parent::__construct();
        $language = $this->session->userdata('lang');
        $this->lang->load('inventory', $language);
    }

    public function vendor_add() {
        $this->data["subview"] = "inventory/add_vendor";
        $this->load->view('_layout_main', $this->data);
    }

    public function index() {
        if (can_access('view_inventory')) {
            $this->data['set'] = 0;
            $this->data['items'] = DB::select('SELECT * FROM ' . set_schema_name() . 'product_quantities');
            $this->data["subview"] = "inventory/index";
            $this->load->view('_layout_main', $this->data);
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function receipt() {
        if (can_access('view_revenue')) {
            $id = clean_htmlentities(($this->uri->segment(3)));
            $this->data['sale'] = \App\Model\ProductSale::find($id);
            $this->data['user'] = \App\Model\User::where('id', $this->data['sale']->revenue->user_id)->where('table', $this->data['sale']->revenue->user_table)->first();

            return view('inventory.sale.receipt', $this->data);
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    protected function purchase_rules() {
        return $this->validate(request(), [
                    'date' => 'required',
                    'note' => 'required|max:600'
                        ], $this->custom_validation_message);
    }

    protected function rulesWithUser() {
        return $this->validate(request(), [
                    'note' => 'required|max:600'
                        ], $this->custom_validation_message);
    }

    protected function rules() {
        $rules = array(
            array(
                'field' => 'note',
                'label' => $this->lang->line("comments"),
                'rules' => 'required|max:10'
            ),
            array(
                'field' => 'name',
                'label' => $this->lang->line("item_name"),
                'rules' => 'required'
            ),
        );
        return $rules;
    }

    public function add() {
        $setting = \App\Model\Setting::first();
        //$this->data['category']= \App\Model\AccountGroup::all();
        $this->data['set'] = 0;
        $this->data['metrics'] = DB::SELECT('select * from constant.metrics');
        $this->data['groups'] = \App\Model\ReferExpense::get();
        $this->data['products'] =  \App\Model\ProductRegister::where('schema_name', set_schema_name())->count() > 0 ?  \App\Model\ProductRegister::where('schema_name', set_schema_name())->get() : \App\Model\ProductRegister::all();

        if (can_access('add_inventory')) {
            $this->data['vendors'] = $this->vendor_m->get_vendor();

            if ($_POST) {
                $rules = $this->rules();
                $this->form_validation->set_rules($rules);

                    $array = [
                        'alert_quantity' => request('alert_quantity'),
                        'product_register_id' => request('product_register_id'),
                        'name' => request('name'),
                        'note' => request('note'),
                        'refer_expense_id' => request('refer_expense_id'),
                        'open_blance' => request('open_blance'),
                        'metric_id' => request('metric_id')
                    ];
                  DB::table('product_alert_quantity')->insert($array);

                    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                    return redirect(base_url("inventory/index"));

            } else {
                $this->data["subview"] = "inventory/add";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    private function checkKeysExists($value) {
        $required = array('name', 'alert_quantity', 'unit');
        return 1;
    }

    public function uploadByExcel() {
        $data = $this->uploadExcel();
        $status = $this->checkKeysExists($data);
        if ((int) $status == 1) {
            $status = '';
            foreach ($data as $key => $value_array) {
                $value = $value_array;
                $subject = \App\Model\ProductQuantity::where(DB::raw('lower(name)'), trim(strtolower($value['name'])))->first();
                
                //check if Chart of account Exists
                $inventory = DB::table('refer_expense')->where('name', "Inventory")->first();
                if(empty($inventory)){
                    $inventory =  DB::table('refer_expense')->insert(['name' => "Inventory", 'financial_category_id' => 2, 'predefined' => 1]);
                }
                //check if exists product_register
                $product_register = \App\Model\ProductRegister::where(DB::raw('lower(name)'), 'ilike', trim(strtolower($value['name'])))->first();
                
                //check if unit measure exists
                $unit = \App\Model\Metric::where(DB::raw('lower(name)'), 'ilike', trim(strtolower($value['unit'])))->first();
                if (!empty($subject)) {
                    unset($data[$key]);
                    $status .= '<p class="alert alert-info">Subject ' . trim($value['name']) . ' Exist. Skipped successfully</p>';
                } else {

                    \App\Model\ProductQuantity::insert([
                            'alert_quantity' => $value['alert_quantity'],
                            'product_register_id' => !empty($product_register) ? $product_register->id : 217,
                            'name' => $value['name'],
                            'note' => $value['note'],
                            'refer_expense_id' => $inventory->id,
                            'open_blance' => $value['open_balance'],
                            'metric_id' => !empty($unit) ? $unit->id : 3
                        ]);
                    $status .= '<p class="alert alert-success">Subject ' . trim($value['name']) . ' added successfully</p>';
                }
            }


            $this->data['status'] = $status;
            $this->data["subview"] = "mark/upload_status";
            $this->load->view('_layout_main', $this->data);
        } ELSE {
            $this->data['status'] = 'There is column renaming issue with your excel please check';
            $this->data["subview"] = "mark/upload_status";
            $this->load->view('_layout_main', $this->data);
        }
    }



    public function add_purchase() {
        if (can_access('add_inventory')) {
            $this->data['groups'] = \App\Model\ReferExpense::where('name', 'ilike', "%Inventory%")->orWhere('name', 'ilike', "%Dispensary%")->get();
            $this->data["payment_types"] = \App\Model\PaymentType::all();
            if ($_POST) {
              //  echo request('amount');
                $this->validate(request(), [
                    'note' => 'required|max:600',
                    'vendor_id' => 'required',
                    'date' => 'date|required',
                    'payment_type_id' => 'required'
                        ], $this->custom_validation_message);
                       $refer_id = \App\Model\ReferExpense::firstOrFail()->id;
                $array = array(
                    "create_date" => date("Y-m-d"),
                    "date" => date("Y-m-d", strtotime(request('date'))),
                    "note" => request("note"),
                    "ref_no" => request("transaction_id"),
                    "payment_type_id" => request("payment_type_id"),
                    "transaction_id" => request("transaction_id"),
                    "bank_account_id" => request("bank_account_id"),
                    "refer_expense_id" => (int)$refer_id>0 ? $refer_id : '',
                    "expenseyear" => date("Y"),
                    "expense" => request("note"),
                    'userID' => session('id'),
                    'uname' => session('username'),
                    'usertype' => session('usertype'),
                    'amount' => request('amount'),
                    'created_by' => $this->createdBy()
                );
                $insert_id = DB::table('expense')->insertGetId($array, "expenseID");
                if ((int) $insert_id > 0) {
                    $i=1;
                    $products = request('product_alert_id');
                    foreach ($products as $key => $product) {
                    $obj = array_merge(request()->all(), [
                        'created_by' => session('id'),
                        'product_alert_id' => request('product_alert_id')[$key],
                        'quantity' => request('quantity')[$key],
                        'amount' => request('quantity')[$key] * request('amounts')[$key],
                        'vendor_id' => request('vendor_id'),
                        'note' => request('note'),
                        'unit_price' => request('amounts')[$key],
                        'date' => date("Y-m-d", strtotime(request('date'))),
                        'expense_id' => $insert_id,
                        'created_by_table' => session('table'),
                        'status' => request('status'),
                    ]);
                    \App\Model\ProductPurchase::create($obj);
                    }
                    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                    return redirect(base_url("expense/voucher/$insert_id"));
                    //return redirect(base_url("inventory/purchase/"));
                } else {

                    return redirect()->back()->with('error', 'Data failed to be saved, Please try again later');
                }
            } else {
                $this->data['banks'] = \App\Model\BankAccount::all();
                $this->data["products"] = \App\Model\ProductQuantity::all();
                $schema = str_replace('.',null,set_schema_name());
                $this->data['vendors'] = DB::SELECT('select * from admin.vendors WHERE schema_name = \''. $schema. '\'');
                $this->data["subview"] = "inventory/purchase/add";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function add_sale() {
        if (can_access('add_inventory')) {
            $this->data["payment_types"] = \App\Model\PaymentType::all();


            if ($_POST) {
                $this->purchase_rules();
                $total_quantity = \App\Model\ProductPurchase::where('product_alert_id', request('product_alert_id'))->sum('quantity');
                $used_quantity = \App\Model\ProductSale::where('product_alert_id', request('product_alert_id'))->sum('quantity');
                $diff = $total_quantity - $used_quantity;
                $posted_quantity = request('quantity');
                $current_balance = \App\Model\ProductQuantity::where('id', request('product_alert_id'))->first();

                if ((int)($diff + $current_balance->open_blance) >= $posted_quantity || set_schema_name() == 'silverleafzanzibar' || set_schema_name() == 'silverleaf') {

                    \App\Model\ProductSale::insert([
                        'quantity' => $posted_quantity,
                        'product_alert_id' => request('product_alert_id'),
                        "date" => date("Y-m-d", strtotime(request('date'))),
                        "note" => request("note"),
                        'created_by' => session('id'),
                        'created_by_table' => session('table'),
                    ]);
                    //check alert quantity here and notify respective user
                    $this->checkQuantity(request('product_alert_id'));
                    \App\Model\ProductQuantity::where('id', request('product_alert_id'))->update(['open_blance' => $current_balance->open_blance - request('quantity')]);
                    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                    return redirect(base_url("inventory/sale"));
                } else {
                    return redirect()->back()->with('warning', 'Sorry, seems the quantity you specify is greater than whats available');
                }
            } else {
                $this->data['banks'] = \App\Model\BankAccount::all();
                $this->data["products"] = \App\Model\ProductQuantity::all();
                $this->data["category"] = \App\Model\ReferExpense::whereIn('financial_category_id', [1])->get();

                $this->data["subview"] = "inventory/sale/add";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function checkQuantity($product_alert_id) {
        $case = \collect(DB::select('select case when a.remain_quantity < b.alert_quantity then 1 else 0 end, a.remain_quantity,c.name from ' . set_schema_name() . 'product_quantities a join  ' . set_schema_name() . 'product_alert_quantity b on b.id=a.id join constant.product_registers c on c.id=b.product_register_id where b.id=' . $product_alert_id))->first();
        if ($case && $case->case == 1) {
            //send notifications
            $users = \App\Model\User::where(DB::raw('lower(usertype)'), 'admin')->get();
            foreach ($users as $user) {
                
                $message = sprintf($this->message('check_quantity_inventory', $this->message_language()),
                $user->name, $case->name,$case->remain_quantity);
                
                // 'Habari ' . $user->name . ' '
                //         . 'Kiwango cha ' . $case->name . ' Kimebaki ' . $case->remain_quantity . '. Kumbuka kufanya manunuzi mapema.'
                //         . ' Asante';
                $this->send_email($user->email, 'Kiwango cha matumizi ya ' . $case->name, $message);
                $this->send_sms($user->phone, $message);
            }
        }
    }

    public function addCategory() {
        if (can_access('edit_inventory')) {
                $data = [
                    'name' => request('name'),
                    'comment' => request('comment'),
                    'metric_id' => request('metric_id'),
                    'schema_name' => set_schema_name()
                ];
            $check = DB::table('constant.product_registers')->where(['name' => request('name'),'metric_id' => request('metric_id')])->first();
                if(empty($check)){
                 DB::table('constant.product_registers')->insert($data);
                 $this->session->set_flashdata('success', $this->lang->line('menu_success'));
         }else{
            return redirect()->back()->with('success', 'This Category Already Exists');
        }
    }else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function addMetric() {
        if (can_access('edit_inventory')) {
                $data = [
                    'name' => request('name'),
                    'abbreviation' => request('abbreviation')
                ];
            $check = DB::table('constant.metrics')->where(['name' => request('name'),'abbreviation' => request('abbreviation')])->first();
                if(empty($check)){
                 DB::table('constant.metrics')->insert($data);
                 $this->session->set_flashdata('success', $this->lang->line('menu_success'));
         }else{
        return redirect()->back()->with('success', 'This Metrics Already Exists');
        }
    }else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function edit_product() {
        if (can_access('edit_inventory')) {
            $id = clean_htmlentities(($this->uri->segment(3)));
            if ((int) $id) {                
                $this->data["product"] = \App\Model\ProductQuantity::find($id);
                $this->data['groups'] = \App\Model\ReferExpense::where('name', 'ilike', "%Inventory%")->orWhere('name', 'ilike', "%Dispensary%")->get();
                $this->data['metrics'] = DB::SELECT('select * from constant.metrics');
                $this->data['set'] = '';
                if ($this->data['product']) {
                    if ($_POST) {
                        DB::table('product_alert_quantity')->where('id', $id)->update(['name' => request('name'),'alert_quantity' => request('alert_quantity'),'metric_id' => request('metric_id'),'refer_expense_id' => request('refer_expense_id'), 'open_blance' => request('open_blance')]);

                        $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                        return redirect(base_url("inventory/index"));
                    } else {

                        $this->data["subview"] = "inventory/edit_inventory";
                        $this->load->view('_layout_main', $this->data);
                    }
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function edit_inventory() {

        if (can_access('edit_inventory')) {
            $id = clean_htmlentities(($this->uri->segment(3)));
            $this->data["payment_types"] = \App\Model\PaymentType::all();
            $this->data['vendors'] = \App\Model\Vendor::all();
            $this->data["category"] = \App\Model\ReferExpense::whereIn('financial_category_id', [1])->get();
            $this->data['banks'] = \App\Model\BankAccount::all();
            $this->data['products'] = \App\Model\ProductPurchase::where('expense_id', $id)->get();
            $this->data['vendor1'] = \App\Model\ProductPurchase::where('expense_id', $id)->first();
            $this->data['purchase'] = \App\Model\Expense::where('expenseID', $id)->first();
            //   $p = \App\Model\Expense::where('expenseID', $this->data['purchase']->expense_id);

            if ($_POST) {


                    $array = array(
                        "date" => date("Y-m-d", strtotime(request("date"))),
                        "amount" => request("amount"),
                        "transaction_id" => request("transaction_id"),
                        "note" => request("note"),
                        "payment_type_id" => request("payment_type_id"),
                        "ref_no" => request("transaction_id"),
                        'created_by' => $this->createdBy()
                    );

                   $update_expense =  \App\Model\Expense::where('expenseID', $id)->update($array);
                   if($update_expense){
                    $products = request('product_alert_id');
                    DB::table('product_purchases')->where('expense_id', $id)->delete();
                    foreach ($products as $key => $product) {

                        $purchases = [
                                'product_alert_id' => request('product_alert_id')[$key],
                                'expense_id' => $id,
                                'amount' => request('unit_price')[$key] * request('quantity')[$key],
                                'unit_price' => request('unit_price')[$key],
                                'vendor_id' => request('vendor_id'),
                                'note' =>request('note'),
                                'quantity' =>request('quantity')[$key],
                                'status' =>request('status'),
                                'date' =>request('date'),
                                'created_by_table' => session('table'),
                                'created_by' => session('id'),
                            ];

                          //  print_r($cart); exit();
                         DB::table('product_purchases')->insert($purchases);
                        }                               
                }
                    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                    return redirect(base_url("inventory/purchase"));

                    //return redirect()->back()->with('warning','You have maximum of '. $quantity->total_quantity . ' in the store ');  
              
            } else {
               
                $this->data["subview"] = "inventory/purchase/edit";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function edit_sale() {

        if (can_access('edit_inventory')) {
            $id = clean_htmlentities(($this->uri->segment(3)));

            $this->data["item"] = \App\Model\ProductSale::where('id', $id)->first();

            if ($_POST) {
                $id = $this->session->userdata("id");
                $user_table = $this->session->userdata("table");
                $user = \App\Model\User::where('id', $id)->where('table', $user_table)->first();
                $array = array(
                    'quantity' => request('quantity'),
                    'product_alert_id' => request('product_id'),
                    "date" => request('date'),
                    "note" => request("note"),
                    'created_by' => session('id'),
                    'created_by_table' => session('table'),
                );
                $this->data["item"]->update($array);
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                return redirect(base_url("inventory/sale"));
            } else {
                $this->data['banks'] = \App\Model\BankAccount::all();
                $this->data["products"] = \App\Model\ProductQuantity::all();
                $this->data["subview"] = "inventory/sale/edit";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function delete() {
        if (can_access('delete_inventory')) {
            $id = clean_htmlentities(($this->uri->segment(3)));
            if ((int) $id) {
               $on_sale = \App\Model\ProductSale::where('product_alert_id', $id)->count();
               $on_purchase =  \App\Model\ProductPurchase::where('product_alert_id', $id)->count();
               if(($on_sale > 0) || $on_purchase > 0){
                $this->session->set_flashdata('success', 'This Item Can Not be deleted because it contain information which has a relationship with other information');
                return redirect(base_url("inventory/index"));
               }else{
                \App\Model\ProductQuantity::find($id)->delete();
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                return redirect(base_url("inventory/index"));
               }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        }
    }

    public function delete_inventory() {
        if (can_access('delete_inventory')) {
            $id = clean_htmlentities(($this->uri->segment(3)));
            if ((int) $id) {

                $product = \App\Model\ProductPurchase::find($id);
                if (!empty($product) ) {
                    $expense = \App\Model\Expense::where('expense.expenseID', $product->expense_id)->first();
                    \App\Model\Expense::where('expense.expenseID', $product->expense_id)->delete();
                    $product->delete();
                }
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                return redirect(base_url("inventory/purchase"));
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        }
    }

    public function delete_sale() {
        if (can_access('delete_inventory')) {
            $id = clean_htmlentities(($this->uri->segment(3)));
            if ((int) $id) {

                $revenue = \App\Model\ProductSale::find($id);
                $revenue->delete();
                return redirect()->back()->with('success', 'Item Deleted Successfully');
                //$this->session->set_flashdata('success', $this->lang->line('menu_success'));
                // return redirect(base_url("inventory/sale"));
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        }
    }

    public function purchase_list() {
        $categoty_id = request('id');
        if (isset($categoty_id)) {
            $string = base_url("inventory/purchase/$categoty_id");
            echo $string;
        } else {
            $string = base_url("inventory/purchase");
            echo $string;
        }
    }

    public function purchase() {
        $id = clean_htmlentities(($this->uri->segment(3)));
        if (can_access('view_inventory')) {
            if ($_POST) {
                $to_date = request("to_date");
                $from_date = request("from_date");
            } else {
                $from_date = date("Y-01-01");
                $to_date = date("Y-m-d");
            }
            $this->data['from_date'] = $from_date;
            $this->data['to_date'] = $to_date;
            if((int)$id > 0 ){
                $this->data['product'] = \App\Model\ProductQuantity::where('id', $id)->first();
                $this->data['items'] = \App\Model\ProductPurchase::where('date', '>=', date("Y-m-d", strtotime($from_date)))->where('date', '<=', date("Y-m-d", strtotime($to_date)))->where('product_alert_id', $id)->orderBy("date", 'desc')->get();
                $this->data["subview"] = "inventory/purchase/purchase";
                $this->load->view('_layout_main', $this->data);
                exit;
            }else{
                $this->data['items'] = DB::select('SELECT sum(amount) as amount,b.name,product_alert_id,sum(quantity) as quantity, count(amount) as total from '.set_schema_name().'product_purchases a, '.set_schema_name().'product_alert_quantity b where a.product_alert_id=b.id and date >=\''. date("Y-m-d", strtotime($from_date)) .'\' and date <= \''. date("Y-m-d", strtotime($to_date)) .'\' group by product_alert_id, b.name');
                //\App\Model\ProductPurchase::where('date', '>=', $from_date)->where('date', '<=', $to_date)->groupBy('product_alert_id')->orderBy("date", 'desc')->get();
            }
            // $this->data['items'] = \App\Model\Expense::whereIn('expenseID',\App\Model\ProductPurchase::where('id','>',0)->get(['expense_id']))->where('date','>=',$from_date)->where('date','<=',$to_date)->orderBy("date",'desc')->get(); 

            $this->data["subview"] = "inventory/purchase/index";
            $this->load->view('_layout_main', $this->data);
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function sale() {
        if (can_access('view_inventory')) {
            if ($_POST) {
                $to_date = request("to_date");
                $from_date = request("from_date");
            } else {


                $academic_year = \App\Model\AcademicYear::whereYear('start_date', date('Y'))->first();


                $from_date = $academic_year->start_date;
                //$to_date = $academic_year->end_date;
                $to_date = date("Y-m-d");
            }
            $this->data['from_date'] = $from_date;
            $this->data['to_date'] = $to_date;

            $this->data['items'] = DB::SELECT('select x.product_alert_id,c.name,c.note,x.total_quantity,y.total_used,c.alert_quantity,(total_quantity-total_used) as remained_quantity from (SELECT  sum(b.quantity) as total_quantity, b.product_alert_id from ' . set_schema_name() . 'product_purchases b group by b.product_alert_id ) x join (
                SELECT  sum(a.quantity) as total_used, a.product_alert_id from ' . set_schema_name() . 'product_sales a group by a.product_alert_id ) y on x.product_alert_id=y.product_alert_id join ' . set_schema_name() . 'product_alert_quantity c on c.id=x.product_alert_id');


            $this->data["subview"] = "inventory/sale/index";
            $this->load->view('_layout_main', $this->data);
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function show() {
        if (can_access('view_inventory')) {
            $id = clean_htmlentities(($this->uri->segment(3)));
            if ($_POST) {
                $to_date = request("to_date");
                $from_date = request("from_date");
            } else {


                $academic_year = \App\Model\AcademicYear::whereYear('start_date', date('Y'))->first();


                $from_date = $academic_year->start_date;
                //$to_date = $academic_year->end_date;
                $to_date = date("Y-m-d");
            }
            $this->data['from_date'] = $from_date;
            $this->data['to_date'] = $to_date;


            $this->data['items'] = \App\Model\ProductSale::where('product_alert_id', $id)->where('date', '>=', $from_date)->where('date', '<=', $to_date)->get();

            //$this->data['items'] = DB::SELECT('SELECT sum(a.quantity) as used_quantity, sum(c.quantity)as total_quantity, a.product_id,b.name FROM  ' . set_schema_name() . ' product_sales a join  ' . set_schema_name() . 'product_registers b on b.id=a.product_id join product_purchases c on c.product_register_id=a.product_id where a.date >= \''.$from_date.'\' and a.date <= \''.$to_date.'\' group by a.product_id,b.name');



            $this->data['name'] = \App\Model\ProductQuantity::find($id)->name;


            //$this->data['items'] = DB::SELECT('SELECT sum(a.quantity) as used_quantity, sum(c.quantity)as total_quantity, a.product_id,b.name FROM  ' . set_schema_name() . ' product_sales a join  ' . set_schema_name() . 'product_registers b on b.id=a.product_id join product_purchases c on c.product_register_id=a.product_id where a.date >= \''.$from_date.'\' and a.date <= \''.$to_date.'\' group by a.product_id,b.name');

            $this->data["subview"] = "inventory/sale/view";
            $this->load->view('_layout_main', $this->data);
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    function uniqueInvoice() {
        $invoiceNo = rand(1, 999) . substr(str_shuffle('ABCDEFGHJKLMNPQRSTUVWXYZ'), 0, 3) . rand(1, 999);
        $data = DB::table('product_purchases')->where('invoice_number', $invoiceNo)->first();
        if (!empty($data)) {
            return $this->uniqueInvoice();
        } else {
            return $invoiceNo;
        }
    }

    /**
     * @accessed:  -Add single invoice : select class
     */
    function getVendor() {
        $product_register_id = request('product_register_id');
        $product = \App\Model\ProductQuantity::where('id', $product_register_id)->first();
        $vendors = (int) $product->product_register_id == 0 ? \App\Model\VendorProductRegister::all() : \App\Model\VendorProductRegister::where('product_register_id', $product->product_register_id)->get();
        if (!empty($vendors) ) {
            echo "<option value='0'>Select class</option>";
            //  echo "<option value='all'>All</option>";
            foreach ($vendors as $vendor) {
                echo '<option value=' . $vendor->vendor->id . '>' . $vendor->vendor->name . '</option>';
            }
        } else {
            echo "0";
        }
    }

    public function getItem() {

        $name = (int) request('type') == 2 ? 'inventory' : 'dispensary';
        $group = DB::table('account_groups')->where(DB::raw('lower(name)'), $name)->first();
       // $items = count($group) == 0 ? \App\Model\ReferExpense::whereNotNull('product_alert_id')->get() : \App\Model\ReferExpense::where('account_group_id', $group->id)->whereNotNull('product_alert_id')->get();
        if (!empty($group)) {
            $items = \App\Model\ProductQuantity::all();
            echo "<option value='0'>Select </option>";
            foreach ($items as $item) {
                echo '<option value=' . $item->id . '>' . $item->note . '</option>';
            }
        } else {
            echo "0";
        }
    }

    public function getProduct() {
        $product_register_id = request('type');
        $product = \App\Model\ProductQuantity::where('id', $product_register_id)->first();
        $vendors = (int) $product->product_register_id == 0 ? \App\Model\ProductRegister::all() : \App\Model\VendorProductRegister::where('product_register_id', $product->product_register_id)->get();
        if (!empty($vendors)) {
            echo "<option value='0'>Select class</option>";
            //  echo "<option value='all'>All</option>";
            foreach ($vendors as $vendor) {
                echo '<option value=' . $vendor->vendor->id . '>' . $vendor->vendor->name . '</option>';
            }
        } else {
            echo "0";
        }
    }

    public function getSellingPrice() {
        $product_alert_id = request('product_id');
        $price = \App\Model\ProductPurchase::where('product_alert_id', $product_alert_id)->first();
        echo $price->selling_price;
    }

}
