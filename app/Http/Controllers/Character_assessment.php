<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class General_character_assessment extends Admin_Controller
{

    /**
     * -----------------------------------------
     *
     * ******* Address***************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     *
     *
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     *
     *
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    public function __construct()
    {
        parent::__construct();
        $language = $this->session->userdata('lang');
        $this->lang->load('general_character', $language);
    }

    public function getCharactersAssessed($exam_id, $class_id)
    {
        $charactes = DB::select('select * from ' . set_schema_name() . 'general_character_assessment where student_id in (select student_id from ' . set_schema_name() . 'student_archive where section_id in (select "sectionID" from ' . set_schema_name() . 'section where "classesID"=' . $class_id . ')) and  exam_id=' . $exam_id . '');
        $list = array();

        if (!empty($charactes)) {
            foreach ($charactes as $value) {
                $list[$value->student_id] = array();
            }
            foreach ($charactes as $value) {
                array_push($list[$value->student_id], $value);
            }
        }
        return $list;
    }

    public function index()
    {
        if (can_access('general_character_assessment')) {
            $id = clean_htmlentities(($this->uri->segment(3)));
            $class_id = clean_htmlentities(($this->uri->segment(4)));
            $academic_year_id = clean_htmlentities(($this->uri->segment(5)));
            $exam_id = clean_htmlentities(($this->uri->segment(6)));
            $this->data['characters'] = array();
            $this->data['students'] = array();
            $this->data['exam'] = array();
            if ((int) $id > 0 && (int) $class_id > 0 && (int) $academic_year_id && (int) $exam_id) {
               // $classID = (new \App\Http\Controllers\Exam())->getCurrentClass($class_id, $academic_year_id);
                $this->data['student_info'] = $stud = \App\Model\StudentArchive::where('academic_year_id', $academic_year_id)->whereIn('section_id', \App\Model\Section::where("classesID", $class_id)->get(['sectionID']))->first();
                $classID = $stud->section->classesID;
               
                $this->data['exam'] = (int) $exam_id > 0 ? \App\Model\Exam::find($exam_id) : array();
                $this->data['characters'] = $this->getCharactersAssessed($exam_id, $classID);

                $this->data['students'] = $this->examreport_m->create_single_report($classID, $exam_id, 0);


                $this->data['sections'] = $sections = \App\Model\Section::where("classesID", $classID)->get();
                $this->data['subjects'] = $this->subject_m->get_order_by_subject(array("classesID" => $classID));
                foreach ($sections as $key => $section) {
                    $this->data['allsection'][$section->section] = $this->examreport_m->create_single_report($classID, $exam_id, $section->sectionID);
                    //DB::select('SELECT  a.student_id,c.name from ' . set_schema_name() . 'student_archive a JOIN ' . set_schema_name() . 'student c on a.student_id=c."student_id" WHERE c.status=1 and a.section_id=' . $section->sectionID . '  and a.academic_year_id=' . $academic_year_id);
                }
                $this->data['semester'] = \App\Model\Semester::find($id);
            }
            $this->data['semester_id'] = $id;
            $this->data['classlevels'] = $this->classlevel_m->get_classlevel();
            $this->data["subview"] = "general_character_assessment/index";
            $this->load->view('_layout_main', $this->data);
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    protected function rules()
    {
        $rules = array();
        return $rules;
    }

    public function assess()
    {
        $userid = $this->session->userdata("id");
        $usertype = $this->session->userdata("usertype");
        if (can_access('general_character_assessment') || can_access('assess_head_teacher')) {
            $this->data['classlevels'] = $this->classlevel_m->get_classlevel();

            if ($_POST) {

                //get students with general teacher comments
                $this->data['students_general_assessed'] = $this->general_character_assessment_m->getStudentAssessedByTeacher();

                $test = array();
                $semester_id = request("semester_id");
                $student_ids = request("student_assessed");
                $assessor_id = $this->session->userdata("id");
                if (can_access('general_character_assessment')) {
                    $teacher_comment = request("comment");
                    for ($i = 0; $i < count($student_ids); $i++) {
                        $student_id = $student_ids[$i];
                        $test[$i] = $this->general_character_assessment_m->addClassTeacherComment($student_id, $semester_id, $teacher_comment, $assessor_id);
                    }
                } elseif (can_access('assess_head_teacher')) {
                    $head_teacher_comment = request("comment");
                    for ($i = 0; $i < count($student_ids); $i++) {
                        $student_id = $student_ids[$i];
                        $test[$i] = $this->general_character_assessment_m->addHeadTeacherComment($student_id, $semester_id, $head_teacher_comment, $assessor_id);
                    }
                }
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                $this->data['classlevels'] = $this->classlevel_m->get_classlevel();
                return redirect(base_url("general_character_assessment/index/$semester_id"));
            } else {
                $this->session->set_flashdata('error', $this->lang->line('menu_error'));
                $this->data["subview"] = "general_character_assessment/assess";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function addClassTeacherGeneralComment()
    {
        $student_id = request('sid');
        $semester_id = request('exam_id');
        $sem_id = request('semester_id');
        $comment = request('comment');
        $class_id = DB::table('student')->where('student_id', $student_id)->value('classesID');

        $teacher_id = DB::table('classes')->where('classesID', $class_id)->value('teacherID');
        $insert = $this->general_character_assessment_m->addClassTeacherGeneralComment($sem_id, $student_id, $semester_id, $comment, $teacher_id);
        if ($insert) {
            $return = "Success";
        } else {
            $return = "Insertion error, please refresh";
        }
        return $return;
    }

    public function addHeadTeacherComment()
    {
        $student_id = request('sid');
        $semester_id = request('exam_id');
        $sem_id = request('semester_id');
        $head_teacher_comment = request('comment');
        $class_id = DB::table('student')->where('student_id', $student_id)->value('classesID');

        $teacher_id = DB::table('classes')->where('classesID', $class_id)->value('teacherID');
        $insert = $this->general_character_assessment_m->addHeadTeacherComment($sem_id, $student_id, $semester_id, $head_teacher_comment, $teacher_id);
        if ($insert) {
            $return = "Success";
        } else {
            $return = "Insertion error, please refresh";
        }
        return $return;
    }

    public function addComment()
    { 
        $student_id = request('sid');
        $semester_id = request('semester_id');
        $comment = request('comment');
        $exam_id = request('exam_id');
        $class_teacher_id = \App\Model\Student::find($student_id)->classes->teacherID;
        if (trim($comment) != '') {
            $updated_at = date('Y-m-d h:i:s.ms');
            if ((int) $exam_id == 0) {
                $obj = [
                    'student_id' => $student_id, 'semester_id' => $semester_id
                ];
            } else {
                $obj = [
                    'student_id' => $student_id, 'exam_id' => $exam_id
                ];
                
                $push_obj = [
                    'semester_id' => $semester_id,
                    request('type') == 2 ? 'head_teacher_comment' : 'class_teacher_comment' => $comment,
                    'class_teacher_id' => $class_teacher_id,
                    $class_teacher_id == session('id') ? '' :
                        'head_teacher_id' => session("id"), //we just assume the one who login will be headteacher
                    'head_teacher_created_at' => $updated_at,
                    'head_teacher_updated_at' => $updated_at,
                ];
                $check = DB::table('general_character_assessment')->where($obj);
                if (empty($check->first())) {
                    DB::table('general_character_assessment')->insert(array_merge($obj, $push_obj));
                    return 'Record saved successfully';
                } else {
                    $check->update($push_obj);
                    return 'Records updated successfully';
                }
            }
        }
    }

    public function classesToAssess()
    {
        $usertype = $this->session->userdata("usertype");
        if (can_access('assess_head_teacher')) {
            $academic_year_id = request('academic_year_id');
            $semester_id = request('semester_id');
            $sql = 'SELECT a."classesID" as classes_id, a.classes FROM ' . set_schema_name() . 'classes a JOIN ' . set_schema_name() . 'classlevel b'
                . ' ON a.classlevel_id = b.classlevel_id JOIN ' . set_schema_name() . 'semester c ON c.class_level_id = b.classlevel_id'
                . ' WHERE c.academic_year_id =' . $academic_year_id . ' AND c.semester_id =' . $semester_id;
            $classes = DB::select($sql);
        } elseif ($usertype == 'Teacher') {
            $teacher_id = $this->session->userdata('id');
            $academic_year_id = request('academic_year_id');
            $semester_id = request('semester_id');
            $sql = 'SELECT a."classesID" as classes_id, a.classes FROM ' . set_schema_name() . 'classes a JOIN ' . set_schema_name() . 'classlevel b'
                . ' ON a.classlevel_id = b.classlevel_id JOIN ' . set_schema_name() . 'semester c ON c.class_level_id = b.classlevel_id '
                . 'WHERE c.academic_year_id =' . $academic_year_id . ' AND a."teacherID" = ' . $teacher_id . ' AND c.semester_id =' . $semester_id;
            $classes = DB::select($sql);
        }
        foreach ($classes as $class) {
            echo '<option value ="" >Select Class</option>';
            echo '<option value ="' . $class->classes_id . '">' . $class->classes . '</option>';
        }
    }

    public function countStudentGeneralAssessment()
    {
        $classes_id = request('classes_id');
        $semester_id = request('semester_id');
        $total_student_complete_assessed = $this->student_characters_m->totalStudentGeneralAssessment($classes_id, $semester_id);
        $unassessed_students = $this->student_characters_m->studentsGeneralAssessment($classes_id, $semester_id);
        echo ' <label for="select student" class="col-sm-2 control-label">Message: </label><h4 style="margin-top: 2%; margin-bottom: 2%; color: #55B582"> You have assessed ' . count($total_student_complete_assessed) . ' Students and still have ' . count($unassessed_students) . ' Students to assess in this class</h4>';
    }

    public function generalCallStudentsToAssess()
    {
        $classes_id = request('classes_id');
        $semester_id = request('semester_id');
        $students = $this->student_characters_m->studentsGeneralAssessment($classes_id, $semester_id);
        foreach ($students as $value) {
            echo '<option value="' . $value->student_id . '">' . $value->name . '</option>';
        }
    }

    public function totalAssessedCharacterPerStudent($student_id, $semester_id)
    {
        return $this->student_characters_m->totalAssessedCharacterByStudentId($student_id, $semester_id);
    }

    public function assessed()
    {
        if (can_access('assess_character')) {
            $class_level_id = clean_htmlentities(($this->uri->segment(3)));
            $this->data['semester_id'] = $semester_id = clean_htmlentities(($this->uri->segment(4)));
            $academic_year_id = clean_htmlentities(($this->uri->segment(5)));
            $section_id = clean_htmlentities(($this->uri->segment(6)));
            $classes_id = clean_htmlentities(($this->uri->segment(7)));
            $this->data['exam_id'] = clean_htmlentities(($this->uri->segment(8)));
            if ($academic_year_id != '' && $semester_id != '') {
                $this->data['exam'] = \App\Model\Exam::find($this->data['exam_id']);
                $this->data['section'] = \App\Model\Section::find($section_id);
                $this->data['class_id'] = $this->data['section']->classesID;
                $this->data['character_gradings'] = DB::table('refer_character_grading_systems')->orderBy('points', 'asc')->get();
                $students = \App\Model\StudentArchive::where('section_id', $section_id)->where('academic_year_id', $academic_year_id)->where('status', 1);
                $this->data['active_section_students'] = $students->get(['student_id']);
                $this->data['assessed'] = \App\Model\StudentCharacter::where('exam_id', $this->data['exam_id'])->whereIn('student_id', $this->data['active_section_students'])->count(DB::raw('DISTINCT student_id'));
                $this->data['students'] = $students->get();
                $this->data['character_categories'] = \App\Model\CharacterCategory::orderBy('position', 'asc')->get();
                $this->data['total_characters'] = $this->characters_m->totalCharacters()->total;
                $this->data['classes_to_assess'] = $this->student_characters_m->get_classes_to_assess();
                $this->data['classlevels'] = $this->classlevel_m->get_classlevel();
            } else {
                $this->data['$student_complete_assessed'] = null;
                $this->data['classes_to_assess'] = $this->student_characters_m->get_classes_to_assess();
                $this->data['classlevels'] = $this->classlevel_m->get_classlevel();
            }
            $this->data["subview"] = "general_character_assessment/assessed";
            $this->load->view('_layout_main', $this->data);
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function create_report()
    {
        $classes_id = clean_htmlentities(($this->uri->segment(3)));
        $email_sent = request('email_sent') == 'on' ? '1' : '0';
        $sms_sent = request('sms_sent') == 'on' ? '1' : '0';
        $class_id = clean_htmlentities(($this->uri->segment(3)));
        $exam_id = clean_htmlentities(($this->uri->segment(4)));
        $students = DB::table('student')->where('classesID', $class_id)->get();

        $exam_report = DB::table('exam_report')->where('exam_id', $exam_id)->get();

        foreach ($students as $student) {
            $parent = \App\Model\StudentParent::where('student_id', $student->student_id)->first();
            $assessment_results = DB::table('student_characters')->where('classes_id', $class_id)->where('exam_id', $exam_id)->where('student_id', $student->student_id)->get();
            $result = '';
            if ($assessment_results) {
                foreach ($assessment_results as $assessment) {
                    $result = $result + $assessment->charachers->description + '=' + $assessment->refer_character_grading_systems->grade + ',';
                }

               // if ($parent && $result != '') {

                    #################################### checking if there is no report created and create Report ###################################################################

                    if (count($exam_report) < 1) {
                        $report_data = array(
                            'classes_id' => $classes_id,
                            'name' => request('name'),
                            'sms_sent' => $sms_sent,
                            'email_sent' => $email_sent,
                            'exam_id' => $exam_id,
                            'reporting_date' => date("Y-m-d", strtotime(request('reporting_date'))),
                            'show_reporting_date' => request('show_reporting_date')
                        );

                        $create_report = DB::table('exam_report')->insertGetId($report_data);
                    }

                    #################################### Then sending sms and email to parents ###################################################################

                    $message = sprintf($this->message('character_report', $this->message_language()), strtoupper($parent->parents->name), strtoupper($student->name), $result, request('name'), set_schema_name());
                    if (request('email_sent') == 'on') {
                        $this->send_email($parent->parents->email, 'Student General Character assessment... ' . strtoupper($student->name), $message);
                    }
                    if (request('sms_sent') == 'on') {
                        $this->send_sms($parent->parents->phone, $message);
                    }
                // } else {
                //     return redirect()->back()->with('error', 'Sorry Make sure all assessment are conplete!!');
                // }
            }
        }
        return redirect()->back()->with('success', 'Report has been generated');
    }

    public function getSectionByClass()
    {
        $classes_id = request('classes_id');
        $sections = $this->section_m->get_allsection($classes_id);
        echo '<option value="">Select section/Stream</option>';
        foreach ($sections as $value) {
            echo '<option value="' . $value->sectionID . '">' . $value->section . '</option>';
        }
    }

    public function fullAssessedStudent()
    {
        $semester_id = request('semester_id');
        $class_level_id = request('class_level_id');
        $academic_year_id = request('academic_year_id');
        $section_id = request('section_id');
        $classes_id = request('classes_id');
        $exam_id = request('exam_id');
        if (request('semester_id')) {
            $string = base_url("general_character_assessment/assessed/$class_level_id/$semester_id/$academic_year_id/$section_id/$classes_id/$exam_id");
            echo $string;
        } else {
            echo(base_url("general_character_assessment/assessed"));
        }
    }

    public function viewGeneralAssessment()
    {
        $semester_id = request('semester_id');
        $section_id = request('section_id');
        if (request('semester_id')) {
            $string = base_url("general_character_assessment/index/$semester_id/$section_id");
            echo $string;
        } else {
            echo(base_url("general_character_assessment/index"));
        }
    }

    public function report()
    {
        if (can_access('character_report')) {
            $exam_id = clean_htmlentities(($this->uri->segment(3)));
            $section_id = $this->data['section_id'] = clean_htmlentities(($this->uri->segment(4)));
            $student_id = $this->data['student_id'] = clean_htmlentities(($this->uri->segment(5)));
            $academic_year_id = clean_htmlentities(($this->uri->segment(6)));
            $this->data['character_categories'] = \App\Model\CharacterCategory::orderBy('position', 'asc')->get();

            if ($exam_id != '' && $section_id != '' && $student_id != '') { 
                $this->data['section'] = \App\Model\Section::find($section_id);
                $this->data['class_id'] = $this->data['section']->classesID;
                $this->data['allsections'] = \App\Model\Section::where('classesID', $this->data['class_id'])->get();
                $this->data['academic_year_id'] = $academic_year_id;
                $this->data['level'] = \App\Model\Classlevel::find($this->data['section']->classes->classlevel_id);

                $this->data['exam'] = \App\Model\Exam::find($exam_id);
                $this->data['exam_id'] = $exam_id;
                $this->data['character_gradings'] = \DB::table('refer_character_grading_systems')->orderBy('grade', 'asc')->get();
                $this->data['student'] = \App\Model\StudentArchive::where('student_id', $student_id)->where('academic_year_id', $academic_year_id)->first();
                $this->data['next_id'] = $this->nextStudentID($section_id, $student_id);
                $obj = [
                    'student_id' => $student_id, 'exam_id' => $exam_id
                ];
                $this->data['exam_report'] = $this->db->query('SELECT * from ' . set_schema_name() . 'exam_report WHERE classes_id=' . $this->data['class_id'] . '')->row();
                $this->data['assessment'] = DB::table('general_character_assessment')->where($obj)->first();
                $this->data['character_categories'] = \App\Model\CharacterCategory::orderBy('position', 'asc')->get();
               // $this->data["subview"] = "general_character_assessment/report";
                return view('general_character_assessment.new', $this->data);
            } else {
                $this->data["subview"] = "general_character_assessment/assessed";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }


    public function report_print_all()
    {
        if (can_access('character_report')) {
            $exam_id = clean_htmlentities(($this->uri->segment(3)));
            $section_id = $this->data['section_id'] = clean_htmlentities(($this->uri->segment(4)));
            $student_id = $this->data['student_id'] = clean_htmlentities(($this->uri->segment(5)));
            $academic_year_id = clean_htmlentities(($this->uri->segment(6)));
            $this->data['character_categories'] = \App\Model\CharacterCategory::orderBy('position', 'asc')->get();

            if ($exam_id != '' && $section_id != '' && $student_id != '') {
                $this->data['section'] = \App\Model\Section::find($section_id);
                $this->data['class_id'] = $this->data['section']->classesID;
                $this->data['allsections'] = \App\Model\Section::where('classesID', $this->data['class_id'])->get();

                $this->data['exam'] = \App\Model\Exam::find($exam_id);
                $this->data['exam_id'] = $exam_id;
                $this->data['character_gradings'] = \DB::table('refer_character_grading_systems')->orderBy('grade', 'asc')->get();
                $this->data['student'] = \App\Model\StudentArchive::where('student_id', $student_id)->where('academic_year_id', $academic_year_id)->first();
                $this->data['next_id'] = $this->nextStudentID($section_id, $student_id);
                $obj = [
                    'student_id' => $student_id, 'exam_id' => $exam_id
                ];
                $this->data['exam_report'] = $this->db->query('SELECT * from ' . set_schema_name() . 'exam_report WHERE classes_id=' . $this->data['class_id'] . '')->row();
                $this->data['assessment'] = DB::table('general_character_assessment')->where($obj)->first();
                $this->data['character_categories'] = \App\Model\CharacterCategory::orderBy('position', 'asc')->get();
                $this->data["subview"] = "general_character_assessment/report";
                return view('general_character_assessment.new', $this->data);
            } else {
                $this->data["subview"] = "general_character_assessment/assessed";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function nextStudentID($section_id, $student_id)
    {
        $student_array = \App\Model\StudentArchive::where('section_id', $section_id)->orderBy('student_id', 'asc')->get(['student_id']);
        $total_students = count($student_array);
        $ar = array();
        foreach ($student_array as $stud) {
            array_push($ar, $stud->student_id);
        }
        $key = array_search($student_id, $ar);
        $next_key = ($total_students - 1) == $key ? $key : $key + 1;
        return $ar[$next_key];
    }

    public function getRemark($student_id, $section_id, $semester_id, $character_category_id, $character_id)
    {
        return $this->general_character_assessment_m->assessedStudentCharactersByStudent($student_id, $section_id, $semester_id, $character_category_id, $character_id);
    }

    public function allCharacters($character_category_id)
    {
        return $this->general_character_assessment_m->allDefinedCharacters($character_category_id);
    }

    public function studentName($student_id)
    {
        return $this->general_character_assessment_m->studentName($student_id);
    }

    public function getTeacherComments($student_id)
    {
        return $this->general_character_assessment_m->getGeneralComment($student_id);
    }

    public function classesGeneralAssess()
    {
        $academic_year_id = request('academic_year_id');
        $class_level_id = request('class_level_id');

        $classes = $this->general_character_assessment_m->classesGeneralAssessed($academic_year_id, $class_level_id);
        echo '<option value="">select class</option>';
        foreach ($classes as $value) {
            echo '<option value="' . $value->classes_id . '">' . $value->classes . '</option>';
        }
    }


    public function printall()
    {
        if (can_access('character_report')) {
            $exam_id = clean_htmlentities(($this->uri->segment(3)));
            $section_id = $this->data['section_id'] = clean_htmlentities(($this->uri->segment(4)));
            $academic_year_id = clean_htmlentities(($this->uri->segment(5)));
            $this->data['character_categories'] = \App\Model\CharacterCategory::orderBy('position', 'asc')->get();

            if ($exam_id != '' && $section_id != '' && $academic_year_id != '') {
                $this->data['section'] = \App\Model\Section::find($section_id);
                $this->data['class_id'] = $this->data['section']->classesID;
                $this->data['academic_year_id'] = $academic_year_id;
                $this->data['allsections'] = \App\Model\Section::where('classesID', $this->data['class_id'])->get();
                $this->data['level'] = \App\Model\Classlevel::find($this->data['section']->classes->classlevel_id);

                $this->data['exam'] = \App\Model\Exam::find($exam_id);
                $this->data['exam_id'] = $exam_id;
                $this->data['character_gradings'] = \DB::table('refer_character_grading_systems')->orderBy('grade', 'asc')->get();
                $this->data['students'] = \App\Model\StudentArchive::where('section_id', $section_id)->where('academic_year_id', $academic_year_id)->get();

                $this->data['exam_report'] = $this->db->query('SELECT * from ' . set_schema_name() . 'exam_report WHERE classes_id=' . $this->data['class_id'] . '')->row();
                $this->data['character_categories'] = \App\Model\CharacterCategory::orderBy('position', 'asc')->get();
                return view('general_character_assessment.print', $this->data);
            } else {
                $this->data["subview"] = "general_character_assessment/assessed";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }



}
