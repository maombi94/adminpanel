<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use DB;
use Auth;


class Recruiments extends Admin_Controller
{ 


    public function __construct() {
         $this->middleware('auth', ['except' => ['application', 'positions', 'descriptions','feeback']]);
    }


    public function index()
    {   
        $group = \App\Models\GroupUser::where('user_id', Auth::User()->id)->first();
        $this->data['job_listings'] = DB::table('admin.job_listing')->select('admin.job_listing.*')->join('admin.group_job_listing','admin.group_job_listing.job_listing_id', '=', 'admin.job_listing.id')
        ->where('admin.group_job_listing.group_id', $group->group_id)->get();
         return view('recruiments.index',$this->data);
    }

     public function createjob()
    {    
        $group = \App\Models\GroupUser::where('user_id', Auth::User()->id)->first();
        
        if($_POST){
                $job_array = [
                        'name' => request('name_of_the_job'),
                        'deadline_date' => request('deadline_date'),
                        'address' => request('applicationaddress'),
                        'descriptions' => request('job_descriptions'),
                        'responsibilities' => request('responsibilities'),
                        'requirements' => request('requirements'),
                        'skills_and_experience' => request('skills_and_experience'),
                        'other_details' => request('other_details'),
                    ];

                $job_listing_id = DB::table('admin.job_listing')->insertGetId($job_array);
            
               $data = [ 
                  'job_listing_id' => $job_listing_id,
                  'group_id' => $group->group_id
                ];
             DB::table('admin.group_job_listing')->insert($data);  
            return redirect('recruiments/index')->with('success', 'Successfully');
        }

        return view('recruiments.create'); 
    }


    public function applicants()
    { 
        $this->data['job_id'] = $job_id = request()->segment(3);
        $this->data['job_data'] = DB::table('admin.job_listing')->where('id',$job_id)->first();
        $this->data['applicants'] = DB::table('admin.portal_applicant')->where('job_id',$job_id)->get();
        return view('recruiments.applicants',$this->data);
    }


      public function profile()
    {   
        $this->data['applicant_id'] = $applicant_id = request()->segment(3);
        $this->data['applicant'] = DB::table('admin.portal_applicant')->where('id',$applicant_id)->first();
        return view('recruiments.applicants_profile',$this->data);
    }


    public function positions()
    {
        $group = \App\Models\GroupUser::where('user_id', Auth::User()->id)->first();
        $this->data['job_listings'] = DB::table('admin.job_listing')->select('admin.job_listing.*')->join('admin.group_job_listing','admin.group_job_listing.job_listing_id', '=', 'admin.job_listing.id')
        ->where('admin.group_job_listing.group_id', $group->group_id)->get();
        return view('recruiments.positions',$this->data);
    }

    public function descriptions()
    {   
        $group = \App\Models\GroupUser::where('user_id', Auth::User()->id)->first();
        $job_id = request()->segment(3);
         $this->data['job_listing'] = DB::table('admin.job_listing')->select('admin.job_listing.*')->join('admin.group_job_listing','admin.group_job_listing.job_listing_id', '=', 'admin.job_listing.id')
        ->where('admin.group_job_listing.group_id', $group->group_id)->where('admin.job_listing.id', $job_id)->first();
        return view('recruiments.descriptions',$this->data);
    }


     public function application()
    {   
        $this->data['job_id'] = $job_id = request()->segment(3);
        if($_POST){
               $applicant_array = [
                    'name' => request('applicant_name'),
                    'email' => request('email'),
                    'phone_number' => request('phone'),
                    'about_applicant' => request('about_applicant'),
                    'education' => request('education_level'),
                    'specialization' => request('specialization'),
                    'experience' => request('experience'),
                    'find_us' => request('find_us'),
                    'others' => request('others'),
                    'job_id' => request('job_id'),
                    'gender' => request('gender')
                   ];

                 $file = request()->file('cv_letter');
                 $letter_cv = $file ? $this->saveFile($letter_cv, 'company/contracts') : 1;
                 $data = array_merge($applicant_array,['applicant_file'=>$letter_cv]);

                 DB::table('admin.portal_applicant')->insert($data);
              return redirect('recruiments/feeback'); 
        }

        return view('recruiments.application_form',$this->data);
    }

    public function feeback()
    {
        return view('recruiments.feedback');
    }
   
}
