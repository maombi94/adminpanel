<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class Character_grading extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
        parent::__construct();
        $this->lang->load('character_grading');
    }

    protected function rules() {
        return $this->validate(request(), [
                    'grade_remark' => 'required|max:255',
                    'grade' => 'required',
                    'points' => 'required|numeric|min:1',
                    'description' => 'required'
                        ], $this->custom_validation_message);
    }

    public function index() {
        if (can_access('assess_character')) {
            $this->data['character_gradings'] = DB::table('refer_character_grading_systems')->get();
            $this->data["subview"] = "character_grading/index";
            $this->load->view('_layout_main', $this->data);
        } else {
            $this->data["subview"] = "permission";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function add() {
        if (can_access('add_character')) {
            if ($_POST) {
                $this->rules();
                DB::table('refer_character_grading_systems')->insert(request()->except('_token'));
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                return redirect(base_url("character_grading/index"));
            } else {
                $this->data["subview"] = "character_grading/add";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function edit() {
        if (can_access('edit_character')) {
            $id = clean_htmlentities(($this->uri->segment(3)));
            if ((int) $id) {
                $grade=DB::table('refer_character_grading_systems')->where('id', $id);
                $this->data['grade'] =$grade->first() ;
                if (!empty($this->data['grade'])) {
                    if ($_POST) {
                        $this->rules();                  
                        $grade->update(request()->except('_token'));
                        $this->session->set_flashdata('success', $this->lang->line('menu_success'));

                        return redirect(base_url("character_grading/index"));
                    } else {
                        $this->data["subview"] = "character_grading/edit";
                        $this->load->view('_layout_main', $this->data);
                    }
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function delete() {
        if (can_access('delete_character')) {
            $id = clean_htmlentities(($this->uri->segment(3)));
            if ((int) $id) {

                DB::table('refer_character_grading_systems')->where('id', $id)->delete();
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                return redirect(base_url("character_grading/index"));
            } else {
                return redirect(base_url("character_grading/index"));
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

}
