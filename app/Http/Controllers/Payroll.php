<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use DB;
use PDF;
class Payroll extends Admin_Controller {

    function __construct() {
        parent::__construct();
    }

    public function taxes() {
        $this->data['taxes'] = DB::table('constant.paye')->get();
        return view('account.payroll.tax',$this->data);
    }

    
    public function taxes1() {
        $this->data['taxes'] = DB::table('constant.paye')->get();
        $id = '2020-01-22';
            $export = clean_htmlentities($this->uri->segment(4));
            $this->data['from'] = '2019-01-22'; 
            $this->data['to'] = '2021-01-22'; 
            $this->data['set'] = $id; 
            $this->data['salaries'] = \App\Model\Salary::where('payment_date', $id)->get();
            $this->data['subview'] = 'payroll/tra';
            $this->load->view('_layout_main', $this->data);
    }

    public function pension() {
        if($_POST){
            $this->data['schema_name'] = $schema_name = request('schema');
            $this->data['pensions'] = \DB::table($schema_name.'.pensions')->get();
            return view('account.payroll.overall_pension', $this->data);
        }else{ 
            $id = clean_htmlentities(request()->segment(3));
            $this->data['set'] = $id;
            if ((int) $id) {
                $this->data['schema_name'] = $schema_name = request()->segment(4);
                $this->data['type'] = 'pension';
                $this->data['users'] = $this->getUsers($schema_name);
                $subscriptions = DB::table($schema_name.'.user_pensions as a')->where('a.pension_id', $id)->get();
                $data = []; 
                foreach ($subscriptions as $value) {
                    $data = array_merge($data, array($value->user_id . $value->table));
                }
                $this->data['subscriptions'] = $data;
                return view('account.payroll.view_pension', $this->data);
            } else{
            return view('account.payroll.pension_index');
            }
        }
    }

    function addPension() {
        if (!empty($_POST)) {
            $this->validate(request(), [
                'name' => 'required|iunique:pensions,name',
                'employer_percentage' => 'required|min:1|numeric',
                // 'employee_percentage' => 'required|min:1|numeric',
                'refer_pension_id' => 'required',
                'address' => 'required',
                'status' =>'required|min:0|numeric',
                    ], $this->custom_validation_message);
            $pensions = request()->all();
            $pension = \App\Model\Pension::create($pensions);
            $group = \App\Model\AccountGroup::where('name', 'Employer Contributions')->first();
            $account_group_id = $group ? $group->id : \App\Model\AccountGroup::create(['name' => 'Employer Contributions', "financial_category_id" => 3, 'predefined' => 1])->id;
            // auto Add it to the chart of accounts
            DB::table('refer_expense')->insert([
                ["name" => request('name') . ' Contributions', "financial_category_id" => 3, "account_group_id" => $account_group_id, 'code' => 'EC-1001' . $pension->id, 'predefined' => $pension->id]]);

            $this->session->set_flashdata('success', $this->lang->line('menu_success'));
            return redirect(base_url('payroll/pension'));
        } else {
            $this->data['subview'] = 'payroll/add_pension';
            $this->load->view('_layout_main', $this->data);
        }
    }

    function deletePension() {
        $id = clean_htmlentities($this->uri->segment(3));
        $pension = \App\Model\Pension::find($id);

        if (!empty($pension)) {
            //check if there are members
            $user_pension = \App\Model\UserPension::where('pension_id', $id)->get();
            if (count($user_pension)<1) {
                $pension->delete();
                DB::table('refer_expense')->where("predefined", $pension->id)->where("financial_category_id", 3)->where('code', 'EC-1001' . $pension->id)->delete();

                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                return redirect(base_url('payroll/pension'));
            } else {
                $this->session->set_flashdata('error', 'You cannot delete this pension fund. There are members already subscribed');
                return redirect(base_url('payroll/pension'));
            }
        }
    }

    function editPension() {
        $id = $this->segment(3);
        $this->data['pension'] = \App\Model\Pension::find($id);
        if (!empty($_POST)) {
            $this->validate(request(), [
                'name' => ['required', Rule::unique('pensions')->ignore($id, 'id')],
                'employer_percentage' => 'required|min:1|numeric',
                'employee_percentage' => 'required|min:1|numeric',
                'address' => 'required',
                'status' =>'required|min:0|numeric',
                    ], $this->custom_validation_message);
            $pensions = request()->all();
            $this->data['pension']->update($pensions);
            DB::table('refer_expense')->where("predefined", $this->data['pension']->id)->where("financial_category_id", 3)->where('code', 'EC-1001' . $this->data['pension']->id)->update(['name' => request('name') . ' Contributions']);
            $this->session->set_flashdata('success', $this->lang->line('menu_success'));
            return redirect(base_url('payroll/pension'));
        } else {
            $this->data['subview'] = 'payroll/edit_pension';
            $this->load->view('_layout_main', $this->data);
        }
    }

    function pensionContribution() {
        $pension_id = request('id');
        $set = request('set');
        if (strlen($set) > 5 && (int) $pension_id > 0) {
            $salary = \App\Model\Salary::where('payment_date', $set)->get();
            $salary_ids = array();
            foreach ($salary as $value) {
                array_push($salary_ids, $value->id);
            }
            $this->data['pension'] = \App\Model\Pension::find($pension_id);
            $this->data['pensions'] = count($salary) > 0 ?
                    \App\Model\SalaryPension::where('pension_id', $pension_id)->whereIn('salary_id', $salary_ids)->get() : array();
            if((int)request('download')>0){
                $pdf = PDF::loadView('payroll/new_pensioncontribution', $this->data);
                return $pdf->download('new_pensioncontribution'.$set.'.pdf');
                }
            $this->data['subview'] = 'payroll/new_pensioncontribution';
           $this->load->view('_layout_main', $this->data);
  
       

        }
    }

    public function deleteSubscriber() {
        $user_id = request('user_id');
        $table = request('table');
        $type = request('type');
        $id = request('set');
        switch ($type) {
            case 'pension':
                DB::table('user_pensions')->where('user_id', $user_id)->where('pension_id', $id)->where('table', $table)->delete();
                $url = base_url("payroll/pension/" . $id);
                break;
            case 'allowance':
                DB::table('user_allowances')->where('user_id', $user_id)->where('allowance_id', $id)->where('table', $table)->delete();
                $url = base_url("allowance/subscribe/" . request('set'));
                break;
            case 'deduction':
                DB::table('user_deductions')->where('user_id', $user_id)->where('deduction_id', $id)->where('table', $table)->delete();
                $url = base_url("deduction/subscribe/" . request('set'));
                break;
            default:
                $url = base_url("allowance/index/");
                break;
        }
        return request()->ajax() == TRUE ? 'success' : redirect($url)->with('success', 'Successfully Unsubscribed');
    }

    public function subscribe() {
        //dd(request()->all());
        $type = request('datatype');
        if ($type == 'allowance') {
            $table = 'user_allowances';
            $table_id = 'allowance_id';
        } else if ($type == 'pension') {
            $table = 'user_pensions';
            $table_id = 'pension_id';
        } else if ($type == 'deduction') {
            $table = 'user_deductions';
            $table_id = 'deduction_id';
        }
        $insert_array = array(
            'user_id' => request('user_id'),
            'table' => request('table'),
            $table_id => request('tag_id'),
            'created_by' => '{' . session('id') . ',' . session('table') . '}'
        );
        $insert = $type == 'pension' ?
                array_merge(array('checknumber' => request('checknumber')), $insert_array) :
                array_merge($insert_array, ['amount' => (bool) request('is_percentage') == 0 ? (float) request('checknumber') : null,
                    'percent' => (bool) request('is_percentage') == 1 ? (float) request('checknumber') : null]);
        $final_array = $type == 'deduction' ? array_merge($insert, [
                    (bool) request('is_percentage') == 1 ? 'employer_percent' : 'employer_amount' => request('employer_amount')
                ]) : $insert;
                    $check = ['user_id' => request('user_id'), 'table' => request('table'), $table_id => request('tag_id')];
                    $validate = DB::table($table)->where($check)->first();
                    if(empty($validate)){
                       
                    $user_pensions = DB::table($table)->insertGetId($final_array);
                    echo (int) $user_pensions > 0 ? 'Successfully Added' : 'Error occurs on subscribe, please try again later';
                    }else{
                        echo 'This Staff Already Subscribed ';
                    }
                }

    public function checknumber() {
        $insert = array(
            'checknumber' => request('inputs'),
        );
        $user_pension = DB::table('user_pensions')->where('user_id', request('user_id'))->where('table', request('table'))->first();
        $pension_id = !empty($user_pension) ? $user_pension->id : request('pension_id');
        $user_pensions = DB::table('user_pensions')->where('id', $pension_id)->update($insert);
        echo (int) $user_pensions > 0 ? 'Success' : 'Error';
    }

    public function index() {
        if ($_POST) {
            $this->data['schema_name'] = $schema_name = request('schema');
            $this->data['set']  = $id = clean_htmlentities(request()->segment(3));
            $this->data['salaries'] = DB::select('select count(*) as total_users, sum(basic_pay) as basic_pay, sum(allowance) as allowance, sum(gross_pay) as gross_pay, sum(pension_fund) as pension, sum(deduction) as deduction, sum(tax) as tax, sum(paye) as paye, sum(net_pay) as net_pay, payment_date,reference FROM ' . $schema_name . '.salaries group by payment_date,reference order by payment_date');
            return view('account.payroll.custom_payroll', $this->data);
        } else {
            return view('account.payroll.index');
        }
    }

    public function show() {
            $id = clean_htmlentities(request()->segment(3));
            $check = clean_htmlentities(request()->segment(4));
            $this->data['set'] = $id; 
            if($check == "export"){
                return view('account.payroll.view', $this->data);
            }
            else{
                $this->data['schema_name'] = $schema_name = $check;
                $this->data['salaries'] = \DB::table($schema_name.'.salaries')->where('payment_date', $id)->get();
                return view('account.payroll.view',$this->data);
            }    
    }

    private function getSalaryCategory() {
        $id = \DB::table('refer_expense')->where('name', 'ilike', 'salary')->value('id');
        if ((int) $id > 0) {
            return $id;
        } else {
            $code = strtoupper(substr(set_schema_name(), 0, 2));
            $array = array(
                "name" => 'salary',
                "financial_category_id" => 2, //fixed category for OPEX
                "note" => 'Salary Expense',
                'code' => $code . '-OPEX-' . rand(1900, 582222),
                "status" => "",
                'predefined' => 1
            );
            return DB::table('refer_expense')->insertGetId($array);
        }
    }

    public function create() {
        if (can_access('manage_payroll')) {
            $this->data['create'] = 0;
            $this->data['special'] = (int) request()->segment(3) > 0 ? 1 : 0;
            if ($_POST) {
                $payroll_date = request('payroll_date');
                $refer_expense_id = $this->getSalaryCategory();
                if ((int) $refer_expense_id > 0) {
                    $this->data['create'] = 1;
                    $this->data['refer_expense_id'] = $refer_expense_id;
                    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                    $this->data['users'] = $this->getUsers();
                    $this->data["subview"] = "payroll/create";
                    $this->load->view('_layout_main', $this->data);
                } else {
                    //less likely to occur
                    $this->session->set_flashdata('error', 'Please define first expense category called salary in Account setting');
                    return redirect(base_url('payroll/create'));
                }
            } else {
                $this->data['users'] = $this->getUsers();
                $this->data["subview"] = "payroll/create";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function getUsers($schema_name) {
        return \DB::table($schema_name.'.users as a')->whereIn('a.table', ['user', 'teacher'])->where('a.status', 1)->where('a.payroll_status', 1)->get();
    }

    public function payslip() {
        $this->data['set'] = request('set');
        $this->data['salary'] = \App\Model\Salary::where('payment_date', request('set'))->where('user_id', request('id'))->where('table', request('table'))->first();
  
        $user = \App\Model\User::where('id', request('id'))->where('table', request('table'))->first();
        if ($_POST) {
            $settings = DB::table('payslip_settings')->first();
            $vars = get_object_vars($settings);
            $obj = array();
            foreach ($vars as $key => $variable) {
                if (!in_array($key, array('id', 'created_at'))) {
                    $obj = array_merge($obj, array($key => request($key) == null ? 0 : request($key)));
                }
            }
            !empty($obj) ? \App\Model\PayslipSetting::first()->update($obj) : '';
        }
        $this->data['user'] = !empty($user)? $user : die('User not found');
        $this->data["subview"] = "payroll/payslip";
        $this->load->view('_layout_main', $this->data);
    }

    public function salary() {

        $this->data['salaries'] = DB::select('select count(*) as total_users, sum(basic_pay) as basic_pay, sum(allowance) as allowance, sum(gross_pay) as gross_pay, sum(pension_fund) as pension, sum(deduction) as deduction, sum(tax) as tax, sum(paye) as paye, sum(net_pay) as net_pay, payment_date,reference,user_id,id FROM ' . set_schema_name() . 'salaries  where user_id=' . session('id') . ' and "table"=\'' . session('table') . '\' group by payment_date,reference,user_id,id');
        $this->data["subview"] = "payroll/salary";
        $this->load->view('_layout_main', $this->data);
    }

    public function payslipAll() {
        $salaries = \App\Model\Salary::where('payment_date', request('set'))->get();
        $this->data['salaries'] = count($salaries) > 0 ? $salaries : die('User not found');
        return view('payroll.payslip_print_all', $this->data);
    }

    public function summary() {
        if (request('set') != '' && strlen(request('set') > 8)) {
            $this->data['basic_payments'] = DB::select('select count(*), sum(basic_pay)  as amount, "table" from ' . set_schema_name() . 'salaries where payment_date=\'' . request('set') . '\' group by "table" ');
            $this->data['allowances'] = DB::select('select  sum(a.amount), a.allowance_id, b.name from ' . set_schema_name() . 'salary_allowances a join ' . set_schema_name() . 'allowances b on b.id=a.allowance_id  where salary_id IN (SELECT id FROM ' . set_schema_name() . 'salaries where payment_date=\'' . request('set') . '\')group by a.allowance_id,b.name');

            $this->data['deductions'] = DB::select('select  sum(a.amount), sum(a.employer_amount) as employer_amount,a.deduction_id, b.name from ' . set_schema_name() . 'salary_deductions a join ' . set_schema_name() . 'deductions b on b.id=a.deduction_id  where salary_id IN (SELECT id FROM ' . set_schema_name() . 'salaries where payment_date=\'' . request('set') . '\')group by a.deduction_id,b.name');


            $this->data['pensions'] = DB::select('select a.pension_id, sum(a.amount) as employee_contribution, sum(a.employer_amount) as employer_contribution, b.name from ' . set_schema_name() . 'salary_pensions a join ' . set_schema_name() . 'pensions b on b.id=a.pension_id  where salary_id IN (SELECT id FROM ' . set_schema_name() . 'salaries where payment_date=\'' . request('set') . '\')group by a.pension_id,b.name');

            $this->data["subview"] = "payroll/summary";
            $this->load->view('_layout_main', $this->data);
        } else {
            return redirect(base_url("payroll/index"));
        }
    }

    function delete($id, $x, $y) {
        DB::statement('delete FROM ' . set_schema_name() . 'salaries where reference=\'' . $y . '\'');
        DB::statement('delete from ' . set_schema_name() . 'expense where ref_no=\'' . $y . '\'');
        $this->session->set_flashdata('success', $this->lang->line('menu_success'));
        return redirect(base_url("payroll/index"));
    }

    public  function viewTaxSummary()
    { 
        if (request('set') != ''){
            $this->data['set'] = request('set');
        $this->data['salaries'] = \App\Model\Salary::where('payment_date', request('set'))->get();
      
        if (request('action') == 'export') {
            $this->data["subview"] = "payroll/export_tax_summary";
            echo view('payroll.export_tax_summary', $this->data);
            $file_name = set_schema_name() . '_payslip_'. $this->data['set'].'.xls';
         return response()->download('storage/app/' . $file_name, $file_name, array('Content-Type: application/excel'));
        }
            $this->data["subview"] = "payroll/tax_summary";
            $this->load->view('_layout_main', $this->data);
        } else {
            return redirect(base_url("payroll/index"));
        }
}
    public function summaryForm() {
        //$deduction_id = request('type');
        $set = request('set');
        $ded_id=request('ded_id');
        if (strlen($set) > 5) {
            $salary = \App\Model\Salary::where('payment_date', $set)->get();
            $salary_ids = array();
            foreach ($salary as $value) {
                array_push($salary_ids, $value->id);
            }

            $this->data['deductions'] =(int) $ded_id == 0 ? \App\Model\Deduction::all() :\App\Model\Deduction::where('id',$ded_id)->get();
            $this->data['users'] = count($salary) > 0 ? $salary : array();

            $this->data['subview'] = 'payroll/summaryform';
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function bankSubmission() {
        $id = request('set');
        $this->data['set'] = request('set');
        $this->data['month'] = request('set');
        $this->data['skip'] = (int) request('skip') == 0 ? 1 : 0;
        $this->data['deductions'] = DB::select('select  sum(a.amount), sum(a.employer_amount) as employer_amount, c.name as bank_name, a.deduction_id, b.account_number, b.name from ' . set_schema_name() . 'salary_deductions a join ' . set_schema_name() . 'deductions b on b.id=a.deduction_id  left join ' . set_schema_name() . 'bank_accounts c on c.id=b.bank_account_id  where salary_id IN (SELECT id FROM ' . set_schema_name() . 'salaries where payment_date=\'' . request('set') . '\')group by a.deduction_id,b.name,c.name, b.account_number');
        $this->data['salaries'] = \App\Model\Salary::where('payment_date', $id)->get();
        $this->data['subview'] = 'payroll/banksubmission';
        $this->load->view('_layout_main', $this->data);
    }

    public function payrollSetting() {  
        if ($_POST) {
           // dd(request()->except('_token'));
                $type = request()->except('_token');
                $settings = DB::table('payroll_setting')->first();
                if(!empty($settings)){
                $vars = get_object_vars(\DB::table('payroll_setting')->first());
                $obj = array();
                foreach ($vars as $key => $variable) {
                    if (!in_array($key, array('updated_at', 'created_at'))) {
                        $obj = array_merge($obj, array($key => request($key) == null ? 0 : request($key)));
                    }
                }
                    DB::table('payroll_setting')->update($obj);
                }else{
                    DB::table('payroll_setting')->create($type);
                }
            return redirect()->back()->with('success', 'Updated');
        }
    }
}
