<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use DB;
use Auth;
use Image;
class Users extends Admin_Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $group = \App\Models\GroupUser::where('user_id', Auth::User()->id)->first();
        $this->data['schools'] =  $this->load_school(Auth::User()->id);
        $this->data['users'] = \App\Models\User::get();
        $this->data['list_users'] = DB::table('portal_users')->get();
        return view('dashboard.user', $this->data);
        
    }

    public function create() {
        $users = User::where('id', Auth::user()->id)->get();
        $this->data['roles'] = DB::table('roles')->get();
        $this->data['countries'] = DB::table('constant.refer_countries')->get();
        if($_POST){
           
           $group = \App\Models\GroupUser::where('user_id', Auth::user()->id)->first();
           $user = User::create(array_merge(request()->all(), ['password' => bcrypt(request('email')), 'created_by' => Auth::user()->id]));
            if (!empty(request('photo'))) {
                $filename = 'portal'.time() . rand(11, 8844) . '.' . request()->file('photo')->guessExtension();
                Image::make(request()->file('photo'))->resize(232, 285)->save('storage/uploads/images/' . $filename);
                User::where('id', $user->id)->update(['photo' => $filename]);
            }
            \App\Models\GroupUser::create(['user_id' => $user->id, 'group_id' => $group->id, 'status' => 1]);

            $this->sendEmailAndSms($user);
            return redirect('users/index')->with('success', 'User ' . $user->name . ' created successfully');
        }
        return view('dashboard.add_user', $this->data);
    }

    
    public function sendEmailAndSms($requests, $content = null) {
        $request = (object) $requests;
        $message = $content == null ? 'Hello ' . $request->name . ' You have been added in ShuleSoft Administration Portal. You can login for Administration of schools with username ' . $request->email . ' and password ' . $request->email : $content;
        \DB::table('public.sms')->insert([
            'body' => $message,
            'user_id' => 1,
            'phone_number' => $request->phone,
            'table' => 'setting'
        ]);
        \DB::table('public.email')->insert([
            'body' => $message,
            'subject' => 'ShuleSoft Administration Credentials',
            'user_id' => 1,
            'email' => $request->email,
            'table' => 'setting'
        ]);
    }

    public function userinfo(){
        $id = request()->segment(3);
            $this->data['staff_id'] = (int)$id > 0 ? $id : Auth::user()->id;
            $this->data['group'] = \App\Models\GroupUser::where('user_id', $this->data['staff_id'])->first();
            $this->data['staff'] = \App\Models\User::where('id', $this->data['staff_id'])->first();
        return view('dashboard.profile', $this->data);
    }

    public function student() {
        $this->data['schema_name'] = $schema = request()->segment(3);
        $this->data['schools'] =  $this->load_school(Auth::User()->id);
        $academic_year_id = request()->segment(4);
        $class_id = request()->segment(5);
        $this->data['school_name']  =  school_name(Auth::User()->id);
        if($class_id > 0){
            $this->data['year'] = $this->get_year($schema,$academic_year_id);
            $this->data['class_name'] = $this->class_name($schema,$class_id);
            $this->data['students'] = $this->load_all_users($schema,$academic_year_id,$class_id,'Students');
            $this->data['all'] =      $this->count_students($schema,$class_id,$academic_year_id);
            $this->data['male'] =     $this->count_students($schema,$class_id,$academic_year_id,'Male');
            $this->data['female'] =   $this->count_students($schema,$class_id,$academic_year_id,'Female');
         }
        return view('users.students', $this->data);
    }

    public function teacher() {
        $this->data['home'] = 'Teachers';
        $this->data['schools'] =  $this->load_school(Auth::User()->id);
        $this->data['this_schema'] = $schema = request()->segment(3);
        if(!empty($schema)){
            $this->data['school_name']  = school_name(Auth::User()->id);
            $this->data['teachers'] = $this->load_all_users($schema,null,null,'Teachers');
            $this->data['all']   =  $this->count_teachers($schema);
            $this->data['male']  =  $this->count_teachers($schema,'Male');
            $this->data['female'] = $this->count_teachers($schema,'Female');
        }
        return view('users.teachers', $this->data);
    }

    public function parent() {
        $this->data['home'] = 'Parents';
        $this->data['schools'] =  $this->load_school(Auth::User()->id);
        $this->data['schema_name'] = $schema = request()->segment(3);
        $class_id = request()->segment(4);
        $this->data['school_name']  = school_name(Auth::User()->id);
        if($class_id > 0){
            $this->data['class_id'] = $class_id;
            $this->data['class_name'] = $this->class_name($schema,$class_id);
            $this->data['parents'] = $this->load_all_users($schema,null,$class_id,'Parents');
            $this->data['all'] = $this->count_parents($schema,$class_id);
            $this->data['male'] = $this->count_parents($schema,$class_id,'Male');
            $this->data['female'] = $this->count_parents($schema,$class_id,'Female');
        }
        if($class_id == 'all'){
            $this->data['class_name'] = 'All classes';
            $this->data['parents'] = $this->load_all_users($schema, null,null,'Parents');
            $this->data['all'] = $this->count_parents($schema,null);
            $this->data['male'] = $this->count_parents($schema,null,'Male');
            $this->data['female'] = $this->count_parents($schema,null,'Female');
          }
        return view('users.parents', $this->data);
    }


    public function user() {
        $this->data['home'] = 'Staffs';
        $this->data['schools'] =  $this->load_school(Auth::User()->id);
        $this->data['this_schema'] = $schema = request()->segment(3);
        if(isset($schema)){
            $this->data['staffs'] = $this->load_all_users($schema, null,null,'Staffs');
            $this->data['school_name']  = school_name(Auth::User()->id);
            $this->data['all']  =  $this->count_staff($schema);
            $this->data['male'] = $this->count_staff($schema,'Male');
            $this->data['female'] = $this->count_staff($schema,'Female'); 
        } 
        return view('users.staffs', $this->data);
    }

    public function report($id)
    {
        return view('user.profile', [
            'user' => User::findOrFail($id)
        ]);
    }

    public function getClasses() {
        $schema = request('schema');
        $classes = DB::table($schema.'.classes')->orderBy('classesID')->get();
        if (!empty($classes)) {
            echo '<option value="">select class</option>';
            foreach ($classes as $value) {
                echo '<option value="' . $value->classesID . '">' . $value->classes . '</option>';
            }
        } else {
            echo "0";
        }
    }


    public function getClassLevel() {
        $schema = request('schema');
        $classes = DB::table($schema.'.classlevel')->orderBy('name')->get();
        if (!empty($classes)) {
            echo '<option value="">select class</option>';
            foreach ($classes as $value) {
                echo '<option value="' . $value->classlevel_id . '">' . $value->name . '</option>';
            }
        } else {
            echo "0";
        }
    }


    public function get_academic_years(){
        $academic_years = DB::table(request('schema').'.academic_year')->distinct()->get();
            if (!empty($academic_years)) {
                echo '<option value="">select academic year</option>';
                foreach ($academic_years as $academic_year) {
                    echo '<option value="' . $academic_year->id . '">' . $academic_year->name . '</option>';
                }
            } else {
                echo "0";
        }
    }

    public function std_profile(){
        $this->data['schema'] = $schema = request()->segment(3);
        if(request()->segment(4) > 0){
            $this->data['schema'] = request()->segment(3);
            $this->data['student_id'] = request()->segment(4);
            $this->data['student'] = $this->get_single_student(request()->segment(3),request()->segment(4)); 
            $this->data['parent'] =  $this->get_parent_info_by_std_id(request()->segment(3),request()->segment(4)); 
            $this->data['student_archive'] = $this->student_archive(request()->segment(3),request()->segment(4));
        }
        return view('users.student_profile', $this->data);
    }


    public function prt_profile(){
        $this->data['schema'] = $schema = request()->segment(3);
        if(request()->segment(4) > 0){
            $this->data['parent'] =  $this->get_parent_info(request()->segment(3),request()->segment(4)); 

            $this->data['students'] =  $this->get_students_by_parent(request()->segment(3),request()->segment(4));
            $this->data['payments'] = $this->parent_payment_info(request()->segment(3),request()->segment(4)); 
            $this->data['messages'] = $this->user_sms($schema, request()->segment(4));
        }
        return view('users.parent_profile', $this->data);
    }

    public function teacherprofile(){
        $this->data['this_schema'] = $schema = request()->segment(3);
        if(request()->segment(4) > 0){
            $this->data['teacher'] = $this->teacher_info(request()->segment(3),request()->segment(4)); 
            //dd($this->data['teacher']);

            $this->data['messages'] = $this->user_sms(request()->segment(3), request()->segment(4));
            $this->data['logs'] = $this->activity_logs($schema,request()->segment(4)); 
            $this->data['subjects_allocated'] = $this->subjects_allocated($schema,request()->segment(4));
            $this->data['avg_perfomance'] = $this->avg_perfomance($schema,request()->segment(4));
        }
        return view('users.teacher_profile', $this->data);
    } 

    public function staff_profile(){
        $this->data['schema'] = $schema = request()->segment(3);
        $this->data['staff_id'] = request()->segment(4);
        if(request()->segment(4) > 0){
            $this->data['staff'] = $this->staff_info($schema, request()->segment(4));
            $this->data['messages'] = $this->user_sms($schema, request()->segment(4));
            $this->data['logs'] = $this->activity_logs($schema,request()->segment(4));
            $this->data['contracts'] = $this->staff_contracts($schema,request()->segment(4));
        }
        return view('users.staff_profile', $this->data);
    }
     
    public function profile() {
        $school = $this->data['schema'] = request()->segment(3);
        $id = request()->segment(4);
        $this->data['shulesoft_users'] = \App\Models\User::where('status', 1)->where('role_id', '<>', 7)->get();
        $client = \App\Models\Client::where('username', $school)->first();
        $is_client = 0;
        if ($school == 'school') {
            $id = request()->segment(4);
            $this->data['client_id'] = $id;
            $this->data['school'] = \collect(DB::select('select name as sname, name,schema_name, region , ward, district as address  from admin.schools where id=' . $id))->first();
        } else {
            $is_client = 1;
            $this->data['school'] = DB::table($school . '.setting')->first();
            $this->data['levels'] = DB::table($school . '.classlevel')->get();
            $client = \App\Models\Client::where('username', $school)->first();
            if (empty($client)) {
                $client = \App\Models\Client::create(['name' => $this->data['school']->sname, 'email' => $this->data['school']->email, 'phone' => $this->data['school']->phone, 'address' => $this->data['school']->address, 'username' => $school, 'created_at' => date('Y-m-d H:i:s')]);
            }
            $this->data['client_id'] = $client->id;

            $this->data['top_users'] = DB::select('select count(*), user_id,a."table",b.name,b.usertype from ' . $school . '.log a join ' . $school . '.users b on (a.user_id=b.id and a."table"=b."table") where user_id is not null group by user_id,a."table",b.name,b.usertype order by count desc limit 5');
        }
        $this->data['profile'] = \App\Models\ClientSchool::where('client_id', $client->id)->first();
        $setting = "'setting'";
        $this->data['list_users'] = DB::select('SELECT count(*) as total, a."table" from ' . $school . '.users a where status=1 AND "table" !='.$setting.' group by a."table" order by "table" desc limit 5');

        $this->data['is_client'] = $is_client;

        $year = \App\Models\AccountYear::where('name', date('Y'))->first();
        $this->data['invoices'] = \App\Models\Invoice::where('client_id', $client->id)->get();
        
            return view('users/profile', $this->data);
        }

    public function teacher_info($schema,$teacher_id){  
        return  DB::table($schema.'.teacher')->select('teacher.*','health_insurance.name as insurance_name','health_status.name as h_name','physical_condition.name as physical')
        ->join('constant.health_insurance','constant.health_insurance.id','=',$schema.'.teacher.health_insurance_id')
        ->join('constant.health_status','constant.health_status.id','=',$schema.'.teacher.health_status_id')
        ->join('constant.physical_condition','constant.physical_condition.id','=',$schema.'.teacher.physical_condition_id')
        ->where('teacherID',$teacher_id)->where('status','<>','0')->first();
       }
 
       public function staff_info($schema,$user_id){
           return  DB::table($schema.'.user')->where('userID',$user_id)->first();
       }

       public function staff_contracts($schema,$user_id){
        return DB::select('SELECT b.id,a.name, b.start_date, b.end_date,b.created_at, b.notify_date FROM ' . $schema . '.user_contract b,constant.employment_type a WHERE b.user_id =' . $user_id . '  AND b.employment_type_id = a.id ORDER BY b.created_at');
      }

      public function subjects_allocated($schema,$t_id){
        return  DB::select('select distinct B.name as year, C.subject,D.classes  from ' . $schema . '.section_subject_teacher A join ' . $schema . '.academic_year B on A.academic_year_id =B.id
        join ' . $schema . '.subject C on A.subject_id = C."subjectID" join ' . $schema . '.classes D on A."classesID" = D."classesID"
        where A."teacherID" =' . $t_id . ' order by year asc');
      }

      public function avg_perfomance($schema,$t_id){
        $subject_ids = DB::table($schema.'.section_subject_teacher')->where('teacherID', $t_id)->whereNotNull('subject_id')->first();
       
        if(!empty($subject_ids)){
            $sql = 'select avg(a.mark),a."examID",b.exam,a."subjectID",a.subject_name,a.academic_year,  max(a.mark), min(a.mark), c.classes from ' . $schema . '.mark_info a JOIN  ' . $schema . '.exam b on a."examID"=b."examID" join ' . $schema . '.classes c on c."classesID"=a."classesID"  where a.student_status=1 AND a.academic_year_id=' .$subject_ids->academic_year_id .' AND a."subjectID"=' . $subject_ids->subject_id . ' GROUP BY a."examID",a."subjectID",a.subject_name,b.exam, c.classes,b.date,a.academic_year order by b.date asc';
        return  DB::select($sql);
           
        }
      }
     
      public function parent_payment_info($schema,$parent_id){
        return DB::table($schema.'.payments')->select('payments.*')
        ->join($schema.'.student_parents', $schema.'.student_parents.student_id','=',$schema.'.payments.student_id')
        ->where([$schema.'.student_parents.parent_id' => $parent_id])->get();   
      }


      public function user_sms($schema,$user_id){
          return \DB::select('select sms_id,body, user_id, created_at, phone_number,1 as is_sent  from ' . $schema . 
          '.sms where user_id=' .$user_id . ' 
          UNION ALL 
          (select id as sms_id,message as body, device_id::integer as user_id, created_at,  
          "from" as phone_number,2 as is_sent from ' . $schema . '.reply_sms where user_id=' .
          $user_id . ') order by created_at desc');
      }

      public function activity_logs($schema,$user_id){
          return DB::table($schema.'.log')->where('user_id',$user_id)->orderBy('id', 'desc')->paginate(10);
      }

      public function get_parent_info_by_std_id($schema,$student_id){
        return DB::table($schema.'.parent')->select('parent.*')
          ->join($schema.'.student_parents', $schema.'.student_parents.parent_id','=',$schema.'.parent.parentID')
          ->where([$schema.'.student_parents.student_id' => $student_id])->first();   
      }
  
      public function get_payment_info($schema,$student_id){
          return DB::table($schema.'.payment')->select('payment.*')->where([$schema.'.payment.student_id' => $student_id])->get();   
        }
  
        public function get_parent_info($schema,$parent_id){
          return DB::table($schema.'.parent')->select('parent.*')
          ->where([$schema.'.parent.parentID' => $parent_id])->first();  
        }
  
        public function get_students_by_parent($schema,$parent_id){
          $this->data['parents'] = DB::table($schema.'.parent')->where('parentID',$parent_id)->first();
              if (!empty($this->data['parents'])) {
              $student_parents = DB::table($schema.'.student_parents')->where('parent_id', $parent_id)->select('student_id')->get();
              $array = array();
              foreach ($student_parents as $student) {
                  array_push($array, $student->student_id);
              } 
              return DB::table($schema.'.student')->select('student.*','classes.classes')->join($schema.'.classes', $schema.'.classes.classesID','=',$schema.'.student.classesID')->whereIn('student_id', $array)->get();
          }
        }

    public function count_overall_parents(){
    return DB::table('admin.all_parent')->select('sex', DB::raw('count(*) as total'))->where("status", '<>', 0)->whereIn('schema_name', $this->available_schemas())->groupBy('sex')->get();
     }
 
     public function count_overall_teachers(){
        return DB::table('admin.all_teacher')->select('sex', DB::raw('count(*) as total'))->where("status", '<>', 0)->whereIn('schema_name', $this->available_schemas())->groupBy('sex')->get();
     }
 
 
     public function count_overall_students(){
        return DB::table('admin.all_student')->select('sex', DB::raw('count(*) as total'))->where("status", '<>', 0)->whereIn('schema_name', $this->available_schemas())->groupBy('sex')->get();
     }

     public function count_overall_staffs(){
        return DB::table('admin.all_users')->whereIn('schema_name', $this->available_schemas())
        ->whereNotIn('usertype', ['Teacher','Class Teacher','Head Teacher','Academic Master','Descipline Master','Student','Parent'])
        ->where("status", '<>', 0)->whereNotIn('schema_name', ['public'])->count();
    }
    
    public function teachers_per_school(){
        return DB::table('admin.all_teacher')->select('schema_name', DB::raw('count(*) as total'))->where("status", '<>', 0)->whereIn('schema_name', $this->available_schemas())->groupBy('schema_name')->get();
    }

    public function parents_per_school(){
        return DB::table('admin.all_parent')->select('schema_name', DB::raw('count(*) as total'))->where("status", '<>', 0)->whereIn('schema_name', $this->available_schemas())->groupBy('schema_name')->get();
    }

    
    public function student_per_school(){
        return DB::table('admin.all_student')->select('schema_name', DB::raw('count(*) as total'))->where("status", '<>', 0)->whereIn('schema_name', $this->available_schemas())->groupBy('schema_name')->get();
    }

    public function staff_per_school(){
        return DB::table('admin.all_users')->select('schema_name', DB::raw('count(*) as total'))
        ->whereIn('schema_name', available_schemas())
        ->whereNotIn('usertype', ['Teacher','Class Teacher','Head Teacher','Academic Master','Descipline Master',
        'Student','Parent','Assistant head teac','Assistant of academic master','Discipline Master'])
        ->where("status", '<>', 0)->whereNotIn('schema_name', ['public'])->groupBy('schema_name')->get();
    }

    public function staff_per_job(){
        return DB::table('admin.all_users')->select('usertype','schema_name', DB::raw('count(*) as total'))
        ->whereIn('schema_name', available_schemas())
        ->whereNotIn('usertype', ['Teacher','Class Teacher','Head Teacher','Academic Master',
        'Descipline Master','Student','Parent','Assistant head teac','Assistant of academic master','Discipline Master'])
        ->where("status", '<>', 0)->whereNotIn('schema_name', ['public'])->groupBy('usertype','schema_name')->get();
    }

    public function load_user($type){
        if($type == 'Teachers'){
         return DB::table('admin.all_teacher')->whereIn('schema_name', available_schemas())->where("status", '<>', 0)->get();
        }else if($type == 'Parents'){
         return DB::table('admin.all_parent')->whereIn('schema_name', available_schemas())->where("status", '<>', 0)->get();
        }else if($type == 'Students'){
          return DB::table('admin.all_student')->whereIn('schema_name', available_schemas())->where("status", '<>', 0)->get();
        }else if($type == 'Staffs'){ 
        return DB::table('admin.all_users')->whereIn('schema_name', available_schemas())->
        whereNotIn('usertype', ['Teacher','Head Teacher','Class Teacher','Academic Master',
        'Descipline Master','Student','Parent','Assistant head teac','Assistant of academic master','Discipline Master'])
        ->where("status", '<>', 0)->whereNotIn('schema_name', ['public'])->get();
        } else{
        return DB::table('admin.all_users')->whereIn('schema_name', available_schemas())->where("status", '<>', 0)->get();
        }
    }
   
    public function count_teachers($schema,$type = NULL){
        if($type == 'Male'){
            return DB::table($schema.'.teacher')->where("status", '<>', 0)->where('sex','LIKE','%M%')->count();
        }else if($type == 'Female'){
            return DB::table($schema.'.teacher')->where("status", '<>', 0)->where('sex','LIKE','%F%')->count();  
        } else{
            return DB::table($schema.'.teacher')->where("status", '<>', 0)->count();
        }
    }


    public function count_staff($schema,$type = NULL){
        if($type == 'Male'){
            return DB::table($schema.'.user')->where('sex','LIKE','%M%')->where("status", '<>', 0)->count();
        } else if($type == 'Female'){
            return DB::table($schema.'.user')->where('sex','LIKE','%F%')->where("status", '<>', 0)->count();
        } else{
            return DB::table($schema.'.user')->where("status", '<>', 0)->count();
        }
    }

    public function count_students($schema,$class_id,$year_id,$type = NULL){
        if($type == 'Male'){
            if($class_id ==null){
                return DB::table($schema.'.student_archive')
            ->join($schema.'.section',$schema.'.section.sectionID', '=', $schema.'.student_archive.section_id')
            ->join($schema.'.classes',$schema.'.classes.classesID', '=', $schema.'.section.classesID')
            ->join($schema.'.academic_year',$schema.'.academic_year.id', '=', $schema.'.student_archive.academic_year_id')
            ->join($schema.'.student',$schema.'.student.student_id', '=', $schema.'.student_archive.student_id')
            ->where([$schema.'.student_archive.academic_year_id' => $year_id, $schema.'.student_archive.status' => '1'])
            ->where($schema.'.student.sex','LIKE','%M%')->count();
            }
            return DB::table($schema.'.student_archive')
            ->join($schema.'.section',$schema.'.section.sectionID', '=', $schema.'.student_archive.section_id')
            ->join($schema.'.classes',$schema.'.classes.classesID', '=', $schema.'.section.classesID')
            ->join($schema.'.academic_year',$schema.'.academic_year.id', '=', $schema.'.student_archive.academic_year_id')
            ->join($schema.'.student',$schema.'.student.student_id', '=', $schema.'.student_archive.student_id')
            ->where([$schema.'.student_archive.academic_year_id' => $year_id,$schema.'.classes.classesID' =>$class_id,$schema.'.student_archive.status' => '1'])
            ->where($schema.'.student.sex','LIKE','%M%')->count();

        }else if($type == 'Female'){
            if($class_id == null){
            return DB::table($schema.'.student_archive')
            ->join($schema.'.section',$schema.'.section.sectionID', '=', $schema.'.student_archive.section_id')
            ->join($schema.'.academic_year',$schema.'.academic_year.id', '=', $schema.'.student_archive.academic_year_id')
            ->join($schema.'.student',$schema.'.student.student_id', '=', $schema.'.student_archive.student_id')
            ->where([$schema.'.student_archive.academic_year_id' => $year_id, $schema.'.student_archive.status' => '1'])
            ->where($schema.'.student.sex','LIKE','%F%')->count();
                }
            return DB::table($schema.'.student_archive')
            ->join($schema.'.section',$schema.'.section.sectionID', '=', $schema.'.student_archive.section_id')
            ->join($schema.'.classes',$schema.'.classes.classesID', '=', $schema.'.section.classesID')
            ->join($schema.'.academic_year',$schema.'.academic_year.id', '=', $schema.'.student_archive.academic_year_id')
            ->join($schema.'.student',$schema.'.student.student_id', '=', $schema.'.student_archive.student_id')
            ->where([$schema.'.student_archive.academic_year_id' => $year_id,$schema.'.classes.classesID' =>$class_id,$schema.'.student_archive.status' => '1'])
            ->where($schema.'.student.sex','LIKE','%F%')->count();  
        }else{
            if($class_id == null){
            return DB::table($schema.'.student_archive')
            ->join($schema.'.section',$schema.'.section.sectionID', '=', $schema.'.student_archive.section_id')
            ->join($schema.'.classes',$schema.'.classes.classesID', '=', $schema.'.section.classesID')
            ->join($schema.'.academic_year',$schema.'.academic_year.id', '=', $schema.'.student_archive.academic_year_id')
            ->join($schema.'.student',$schema.'.student.student_id', '=', $schema.'.student_archive.student_id')
            ->where([$schema.'.student_archive.academic_year_id' => $year_id, $schema.'.student_archive.status' => '1'])->count();
              }
            return DB::table($schema.'.student_archive')
            ->join($schema.'.section',$schema.'.section.sectionID', '=', $schema.'.student_archive.section_id')
            ->join($schema.'.classes',$schema.'.classes.classesID', '=', $schema.'.section.classesID')
            ->join($schema.'.academic_year',$schema.'.academic_year.id', '=', $schema.'.student_archive.academic_year_id')
            ->join($schema.'.student',$schema.'.student.student_id', '=', $schema.'.student_archive.student_id')
            ->where([$schema.'.student_archive.academic_year_id' => $year_id,$schema.'.classes.classesID' =>$class_id,$schema.'.student_archive.status' => '1'])->count();
        }
    }


    public function count_parents($schema,$class_id,$type = NULL){
        if($type == 'Male'){
            if($class_id == null){
             return DB::table($schema.'.parent')->select('parent.*')
              ->where([$schema.'.parent.status' => '1'])->where($schema.'.parent.sex', 'LIKE','%M%')->count();
            }
            return DB::table($schema.'.parent')->select('parent.*')
            ->join($schema.'.student_parents', $schema.'.student_parents.parent_id','=',$schema.'.parent.parentID')
            ->join($schema.'.student', $schema.'.student.student_id','=',$schema.'.student_parents.student_id')
            ->where([$schema.'.student.classesID' => $class_id, $schema.'.parent.status' => '1'])->where($schema.'.parent.sex', 'LIKE','%M%')->count();
        } else if($type == 'Female'){
          if($class_id == null){
            return DB::table($schema.'.parent')->select('parent.*')->where([$schema.'.parent.status' => '1'])->where($schema.'.parent.sex', 'LIKE','%F%')->count();
           }
           return DB::table($schema.'.parent')->select('parent.*')
           ->join($schema.'.student_parents', $schema.'.student_parents.parent_id','=',$schema.'.parent.parentID')
           ->join($schema.'.student', $schema.'.student.student_id','=',$schema.'.student_parents.student_id')
           ->where([$schema.'.student.classesID' => $class_id, $schema.'.parent.status' => '1'])->where($schema.'.parent.sex', 'LIKE','%F%')->count();  
        } else{
          if($class_id == null){
           return DB::table($schema.'.parent')->select('parent.*')->where([$schema.'.parent.status' => '1'])->count();
            }
          return DB::table($schema.'.parent')->select('parent.*')
          ->join($schema.'.student_parents', $schema.'.student_parents.parent_id','=',$schema.'.parent.parentID')
          ->join($schema.'.student', $schema.'.student.student_id','=',$schema.'.student_parents.student_id')
          ->where([$schema.'.student.classesID' => $class_id, $schema.'.parent.status' => '1'])->count();
         }
      }


    public function load_all_users($schema,$year_id,$class_id,$type){
        if($type == 'Students'){
            if($class_id == null){
                return DB::table($schema.'.student_archive')
            ->select('student_archive.*','classes.classes as classes_name','section.section as section_name','academic_year.name as year')
            ->join($schema.'.section',$schema.'.section.sectionID', '=', $schema.'.student_archive.section_id')
            ->join($schema.'.classes',$schema.'.classes.classesID', '=', $schema.'.section.classesID')
            ->join($schema.'.academic_year',$schema.'.academic_year.id', '=', $schema.'.student_archive.academic_year_id')
            ->where([$schema.'.student_archive.academic_year_id' => $year_id, $schema.'.student_archive.status' => '1'])->get();
            }
             return DB::table($schema.'.student_archive')
            ->select('student_archive.*','student.*','classes.classes as classes_name','section.section as section_name','academic_year.name as year')
            ->join($schema.'.section',$schema.'.section.sectionID', '=', $schema.'.student_archive.section_id')
            ->join($schema.'.classes',$schema.'.classes.classesID', '=', $schema.'.section.classesID')
            ->join($schema.'.academic_year',$schema.'.academic_year.id', '=', $schema.'.student_archive.academic_year_id')
            ->join($schema.'.student',$schema.'.student.student_id', '=', $schema.'.student_archive.student_id')
            ->where([$schema.'.student_archive.academic_year_id' => $year_id,$schema.'.classes.classesID' =>$class_id,$schema.'.student_archive.status' => '1'])->get();
        } else if($type == 'Teachers'){
             return DB::table($schema.'.teacher')->where("status", '<>', 0)->get();
        } else if($type == 'Parents') {
            if($class_id == null){
             return  DB::table($schema.'.parent')->where("status", '<>', 0)->get();
            }
             return DB::table($schema.'.parent')->select('parent.*')
            ->join($schema.'.student_parents', $schema.'.student_parents.parent_id','=',$schema.'.parent.parentID')
            ->join($schema.'.student', $schema.'.student.student_id','=',$schema.'.student_parents.student_id')
            ->where([$schema.'.student.classesID' => $class_id, $schema.'.parent.status' => '1'])->get();
        } else {
            return DB::table($schema.'.user')->where("status", '<>', 0)->get();
        }
    }


    public function overall(){
       $this->data['overall'] = $overall = request()->segment(3);
       $this->data['schemas'] = $schemas = $this->load_schema(Auth::User()->id);
       $this->data['all'] = array('All schools','All teachers','All parents','All students','All staff');
        
        if(!is_null($overall)){
            switch ($this->data['overall']) {
                case "Allschools":
                    $this->data['name'] = "Schools";
                    $allschools = [];
                     foreach($schemas as $schema){
                        $schools =  $this->load_schools($schema->username);
                        $obj = ["name" => $schools->name, "email" => $schools->email,"phone"=> $schools->phone,"address" => $schools->address,"students_no"=> $schools->estimated_students,"username"=>$schools->username];
                        array_push($allschools, $obj);
                     } 
                      $this->data['schools'] = $allschools;
                    break;

                case "Allteachers":
                        $this->data['name'] = "Teachers";
                        $this->data['teachers'] = $this->load_user('Teachers');
                        $this->data['total_teachers'] = $this->count_overall_teachers();
                        $this->data['school_teachers'] = $this->teachers_per_school();
                    break;

                case "Allparents":
                        $this->data['name'] = "Parents";
                        $this->data['parents'] = $this->load_user('Parents');
                        $this->data['total_parents'] = $this->count_overall_parents();
                        $this->data['school_parents'] = $this->parents_per_school();
                    break;

                case "Allstudents":
                        $this->data['name'] = "Students"; 
                        $this->data['students'] = $this->load_user('Students');
                        $this->data['overall_students'] = $this->count_overall_students(); 
                        $this->data['students_per_school'] = $this->student_per_school();
                        $this->data['all_classes'] = $this->load_all_levelstudent();
                    break;

                case "Allstaff":
                        $this->data['name'] = "Staff"; 
                        $this->data['staffs'] = $this->load_user('Staffs');
                        $this->data['overall_staffs'] = $this->count_overall_staffs();
                        $this->data['school_staffs'] = $this->staff_per_school();
                        $this->data['count_school_staffs'] = $this->staff_per_job();
                      
                 break;
            }
        } 
        return view('users.overall', $this->data);
    }




     public function permissions()
     {  
          $id = request()->segment(3);
          $this->data['set']  = (int) $id;
          $this->data['roles']  = \App\Models\Role::latest()->get();
          $this->data['permission']  = \App\Models\Permission::get();
          $this->data['Permissionsgroup'] = \App\Models\PermissionGroup::get();
          $this->data['user_roles'] = DB::table('admin.portal_roles')->get();
          return view('operations.permission',$this->data);
     }


    public function storepermission() {
        $role = new \App\Models\PermissionRole();
        $role->permission_id = request('perm_id');
        $role->role_id = request('role_id');
        $role->created_by = \Auth::user()->id;
        $role->save();
        echo 'Permission created successfully';
    }

   public function removepermission() {
        $permission_id = request('perm_id');
        $role_id = request('role_id');
        \App\Models\PermissionRole::where(['permission_id' => $permission_id, 'role_id' => $role_id])->delete();
        echo 'success';
  }


  public function  storeroles(){
    if($_POST){
         $validateData = request()->validate([
           'name' => 'required|max:255',
           'description' => 'required|max:255',
       ]);

        $role = new \App\Models\Role();
        $role->name = request('name');
        $role->description = request('description');
        $role->created_by = Auth::user()->id;
        $role->save();
        return redirect()->back()->with('success', 'Role created successfully');
     }
  }


}