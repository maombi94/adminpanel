<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use DB;
use Auth;

class Exam extends Admin_Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $this->data['home'] = 'Home';
        return view('dashboard.index', $this->data);
    }



    public function schoolclass() {
        $this->data['this_schema'] = $schema = request()->segment(3);
        $academic_year_id = request()->segment(4);
        $class_id = request()->segment(5);
     
        $this->data['school_name']  =  school_name(Auth::User()->id);
        if($class_id > 0){
            $this->data['year'] = DB::table($schema.'.academic_year')->where('id', $academic_year_id)->first();
            $this->data['classes'] = DB::table($schema.'.classes')->where('classlevel_id', $class_id)->get();
          //  dd($this->data['classes']);
            $this->data['students'] = $this->load_levelstudent($schema, $academic_year_id, $class_id);
           // dd($this->data['students']);
            $this->data['all'] =      $this->count_all_students($schema, $class_id, $academic_year_id);
            $this->data['male'] =     $this->count_male_students($schema,$class_id,$academic_year_id);
            $this->data['female'] =   $this->count_female_students($schema,$class_id,$academic_year_id);
         }
          if($class_id == 'all'){
             $this->data['year'] = $this->get_year($schema,$academic_year_id);
             $this->data['school_name']  =  school_name(Auth::User()->id);
             $this->data['all_class'] = $all_class = request()->segment(5);
             $this->data['students']  = $this->get_all_students($schema,$academic_year_id);
             $this->data['all'] =      $this->count_all_students($schema,null,$academic_year_id);
             $this->data['male'] =     $this->count_male_students($schema,null,$academic_year_id);
             $this->data['female'] =   $this->count_female_students($schema,null,$academic_year_id);
           }
        $this->data['schools'] =  $this->load_school(Auth::User()->id);
        return view('exam.schoolclass', $this->data);
    }


    public function singlereport() {
            $this->data['exams'] = array();
           // $this->data['classes'] = DB::table('shulesoft.classes')->get();
            $classesID = request("classesID");
            $this->data['post_class_id'] = $classesID;
            $this->data['subjectID'] = 0;
            $this->data['students'] = array();
            $year = date("Y");
    
            if ($_POST) {
    
                $examID = request('examID');
                $this->data['this_schema'] = $schema = request('schema_name');
                $subjectID = (int) request('subjectID');
                $sectionID = request('sectionID');
                $academic_year_id = request('academic_year_id');
         
                $this->data['show_gender'] = request('show_gender') == 'on' ? 1 : 0;
                $classID = $classesID;
                $this->data['userclass'] = DB::table($schema.'.classes')->where('classesID', $classesID)->first();
                $class_info = DB::table($schema.'.classes')->where('classesID', $classesID)->first();
    
                $this->data["exams"] = DB::table($schema.'.exam')->where('examID', $examID)->first();
                $this->data['grades'] = $this->getGrades($class_info, $this->data["exams"], $sectionID, $schema);
                
                $subject_avg = DB::select('SELECT round(avg(a.mark),1) as average,a."subjectID",a."examID",a.subject_name FROM ' . $schema . '.mark_info a WHERE a.academic_year_id= '.$academic_year_id.' and "examID"='.$examID.' AND a."classesID"=' . $classID . ' and "subjectID" IN (SELECT "subjectID" FROM ' . $schema . '.subject where "classesID"=' . $classID . ')  and a.student_status=1 group by a."subjectID",a."examID",a.subject_name');
            
                $this->data['classlevel'] = DB::table($schema.'.classlevel')->where('classlevel_id', $class_info->classlevel_id)->first();
                if ($academic_year_id == '') {
                    $this->session->set_flashdata('error', $this->lang->line('set_year'));
                    return redirect(url('exam/report'));
                } else {
                    $this->data['academic_year'] = DB::table($schema.'.academic_year')->where('id', $academic_year_id);
                }
                if ($examID == '') {
                    $this->session->set_flashdata('error', 'specify well required field inputs');
                    return redirect(url('exam/report'));
                }
    
                $year = date("Y");
    
                if ($sectionID == 0) {
                    //get normal query
                    $view = 'report_optimized';
                    $this->data['subject_evaluations'] = DB::select('select round(AVG(mark),1), subject_name,"subjectID", sum(mark), rank() OVER (ORDER BY (avg(mark)) DESC) AS ranking from ' . $schema . '.mark_info where "examID"=' . $examID . '  and "classesID"=' . $classID . '  group by "subjectID",subject_name');
    
                    $this->data['total_students'] = DB::select('select a.*,b.name,b.photo,b.roll, b.sex,c.section, c."classesID" from ' . $schema . '.student_archive a JOIN ' . '  ' . $schema . '.student b ON '
                                    . '      b."student_id"= a.student_id JOIN ' . $schema . '.section c
                        ON  c."sectionID"=b."sectionID" WHERE ' . '      a.academic_year_id=' . $academic_year_id . '  AND ' . '      a.section_id IN (SELECT "sectionID" FROM ' . $schema . '.section WHERE "classesID"=' . $classID . ') and a.status=1 ');
                } else {
    
                    //get by section
                    $view = 'report_optimized';
                    $this->data['subject_evaluations'] = DB::select('select round(AVG(mark),1), subject_name,"subjectID", sum(mark), rank() OVER (ORDER BY (avg(mark)) DESC) AS ranking from ' . $schema . '.mark_info where "examID"=' . $examID . '  and "classesID"=' . $classID . ' and "sectionID"=' . $sectionID . ' group by "subjectID",subject_name');
    
                    $this->data['total_students'] = DB::select('select a.*,b.name,b.photo,b.sex, b.roll,c.section, c."classesID" from ' . $schema . '.student_archive a JOIN ' . '  ' . $schema . '.student b ON '
                                    . '      b."student_id"= a.student_id JOIN ' . $schema . '.section c
                                        ON  c."sectionID"=b."sectionID" WHERE '
                                    . '      a.academic_year_id=' . $academic_year_id . '  AND ' . '      a.section_id =' . $sectionID . ' and a.status=1');
                }
    
                $this->data['sectionID'] = $sectionID;
                $this->data['exam_info'] = DB::table($schema.'.exam')->where('examID', $examID)->first();
    
                if ($subjectID == 0) {
                    //GET all subjects with total average and ranks if 
                    //get students in that class first
                    $sec = $sectionID == 0 ? null : $sectionID;
                    $this->data['students'] = $this->create_single_report($classID, $examID, $sec, null, null, $this->data['show_gender'], $schema);
    
                    //get subjects taken by that class
    
                    $this->data['subjects'] = $sectionID == 0 ? DB::select('select a.*, b.code as subject_code,(select x.name from ' . $schema . '.teacher x join ' . $schema . '.section_subject_teacher s on (x."teacherID"=s."teacherID" AND s.refer_subject_id=b.subject_id and s.subject_id=a."subjectID") where s.academic_year_id=' . $academic_year_id . ' limit 1 ) as teacher from ' . $schema . '.subject a join ' . $schema . '.refer_subject b on a.subject_id=b.subject_id where a."classesID"=' . $classID . ' AND a."subjectID" in (select "subjectID" from ' . $schema . '.mark where "examID"=' . $examID . ') order by b.arrangement asc') :
                            DB::select('select a.*, b.code as subject_code,(select x.name from ' . $schema . '.teacher x join ' . $schema . '.section_subject_teacher s on (x."teacherID"=s."teacherID" AND s.refer_subject_id=b.subject_id and s.subject_id=a."subjectID") where s.academic_year_id=' . $academic_year_id . ' and s."sectionID"=' . $sectionID . ' limit 1 ) as teacher from ' . $schema . '.subject a join ' . $schema . '.refer_subject b on a.subject_id=b.subject_id where a."classesID"=' . $classID . ' AND a."subjectID" in (SELECT subject_id FROM ' . $schema . '.subject_section WHERE section_id=' . $sectionID . ')  order by b.arrangement asc');
                } else {
                    //get single subject with average
                    $this->data['students'] = $this->get_subject_with_rank($examID, $classID, $subjectID, $sectionID, $schema);
                    $this->data['subjects'] = array();
                }
    
                $this->data['eattendances'] = array();
    
                if (!empty($this->data['students'])) {
                    $section_res = $sectionID == 0 ? array() : array('sectionID' => $sectionID);
                    $sections = DB::table($schema.'.section')->where(array_merge(array("classesID" => $classID), $section_res))->get();
    
                    $this->data['sections'] = $sections;
    
                    if ($subjectID == 0) {
                        foreach ($sections as $key => $section) {
                            $this->data['allsection'][$section->section] = $this->create_single_report($classID, $examID, $section->sectionID, null, null, $this->data['show_gender'], $schema);
                            $this->data['subject_section_evaluations'][$section->section] = DB::select('select round(AVG(mark),1), subject_name,"subjectID", sum(mark), rank() OVER (ORDER BY (avg(mark)) DESC) AS ranking from ' . $schema . '.mark_info where "examID"=' . $examID . '  and "classesID"=' . $classID . '  and "sectionID"=' . $section->sectionID . ' group by "subjectID",subject_name');
                            $this->data['section'][$section->section]['sectionID'] = $section->sectionID;
                            $this->data['subject_section'][$section->section] = ('SELECT a."subjectID", a."classesID", a."teacherID", c.code as subject_code, a.teacher_name, a.is_counted, a.is_penalty, a.pass_mark, a.grade_mark, a.subject_id, a.subject_type, c.subject_name as subject, a.teacher_name as teacher, b.signature FROM ' . $schema . '.subject a JOIN ' . $schema . '.teacher b on b."teacherID"=a."teacherID" join ' . $schema . '.refer_subject c ON c.subject_id=a.subject_id WHERE a."subjectID" in (SELECT subject_id FROM ' . $schema . '.subject_section WHERE section_id=' . $section->sectionID . ') order by subject_type, subject desc');
                        }
                    } else { 
                        foreach ($sections as $key => $section) {
                            $this->data['allsection'][$section->section] = $this->get_subject_with_rank($examID, $classID, $subjectID, $section->sectionID, $schema);
                        }
                    }
                    $this->data['examID'] = $examID;
                    $this->data['classesID'] = $classID;
                    $this->data['this_school'] = $schema;
                    $this->data['subjectID'] = $subjectID;
                    $this->data['action'] = request('action');
                    $this->data['show_grade'] = request('show_grade') == 'on' ? 1 : 0;
                    $this->data['show_rank'] = request('show_rank') == 'on' ? 1 : 0;
                    $this->data['show_footer'] = request('show_footer') == 'on' ? 1 : 0;
    
                    $this->data['show_rank'] == 1 ? $this->getSubjectRank($examID, $classID, $schema) : '';
                    $this->data['pos_in_class'] = request('pos_in_class');
                    $this->data['pos_in_section'] = request('pos_in_section');
                    $this->data['show_subject_avg'] = request('show_subject_avg');
                    $this->data['academic_year_id'] = $academic_year_id;
                }
                $this->data['editable'] = 'contenteditable="true"';
    
                if (session("usertype") == "Teacher") {
                    $this->data['editable'] = '';
                }
                if (request('action') == 'export') {
                    $this->data["subview"] = "exam/ca_export";
                    echo view('exam.ca_export', $this->data);
                    $file_name = $schema . $examID . '_' . $academic_year_id . '_' . $sectionID . '_' . $classID . '_' . $this->data['show_grade'] . '_' . $this->data['show_footer'] . '.xls';
                    return response()->download('storage/app/' . $file_name, $file_name, array('Content-Type: application/excel'));
                }

                return view('exam.singlereport', $this->data);
            } else {
                return view('exam.singlereport', $this->data);
            }
    }

    
    public function studentreport() {
        $schema = request()->segment(3);
        $id = request()->segment(4);
        $checkstudent = student_profile($schema, $id);
        if (request('id') != NULL) {
            $this->updateReportSetting(request('id'));
        }
        if (!empty($checkstudent) && request('exam')) {

            $exam_id = request('exam');
            $class_id = request('class_id');
            $academic_year_id = request('year_no');
            $this->data['siteinfos'] = DB::table($schema.'.setting')->first();

            $this->data["student_archive"] = $student = DB::table($schema.'.student_archive')->where('student_id', $id)->where('academic_year_id', $academic_year_id)->first();
            $student_id = $id;
            $this->data['section_id'] = request('section_id');
            $this->data['empty_mark'] = '-';
            $this->data["student"] = $checkstudent;
            $this->data["schema"] = $schema;

            $class = $class_id;

            $classesID = $class;
            $this->data['set'] = $id;
            $this->data['class_id'] = $class_id;
            $this->data['academic_year'] = DB::table($schema.'.academic_year')->where('id', $academic_year_id)->first();
            $this->data['sectionID'] = $student->section_id;
            $this->data["classes"] = DB::table($schema.'.classes')->where('classesID',$classesID)->first();
            $this->data["siteinfos"] = DB::table($schema.'.setting')->first();


            $this->data["signature"] = $this->data['siteinfos']->signature;
            $this->data["class_teacher_signature"] = get_teacher_signature($this->data['sectionID'], $schema);

            $this->data["exams"] = DB::table($schema.'.exam')->where('examID', $exam_id)->first();
            $this->data['grades'] = $this->getGrades($this->data["classes"], $this->data["exams"], $student->section_id, $schema);
            if ($this->data["student"] && $this->data["classes"]) {

                if (empty($this->data['grades'])) {
                    return redirect('grade/index');
                }
                $this->data['report_setting'] = DB::table($schema.'.exam_report_settings')->where('exam_type', 'single')->where('classlevel_id', $this->data["classes"]->classlevel_id)->first();

                $order = 'c.subject_name';
                $sql = 'SELECT a."markID",b.is_penalty, b.pass_mark, b.is_counted_indivision, a."examID",a.exam,a."student_id",a."classesID",a."subjectID",a.mark,a.academic_year_id, c.subject_name AS subject FROM ' . $schema . '.mark a JOIN ' . $schema . '.subject b ON b."subjectID"=a."subjectID" join ' . $schema . '.refer_subject c ON c.subject_id=b.subject_id WHERE a."student_id"=' . $student_id . ' and a."classesID"=' . $classesID . ' AND a."examID"=' . $exam_id . ' order by  ' . $order;
                $this->data["marks"] = DB::select($sql);

                if (empty($this->data["marks"])) {
                    return redirect()->back()->with('info', 'No Marks recorded for this student , please advice teachers to record marks for ' . $student->student->name . ' ');
                }

                $this->data["section"] =  DB::table($schema.'.section')->where('classesID', $class_id)->get();
                $this->data['total_students'] = \collect(\DB::select('select count(distinct "student_id") as count from ' . $schema . '.mark_info where "classesID"=' . $classesID . ' AND "examID"=' . $exam_id . ' AND academic_year_id=' . $academic_year_id . ' AND mark is not null and mark <>0'))->first();

                $section_sql = 'select student_id from ' . $schema . '.student_archive where section_id=' . $student->section_id . ' and academic_year_id=' . $academic_year_id . ' AND student_id IN (SELECT distinct "student_id" FROM ' . $schema . '.mark_info WHERE "examID"=' . $exam_id . ' AND "classesID"=' . $classesID . ' and academic_year_id=' . $academic_year_id . ')';
                $this->data['total_students_section'] = DB::select($section_sql);

                $this->data['report'] = $this->get_subject_rank($exam_id, $classesID, $student_id, null, $schema);

                $this->data['classlevel'] = DB::table($schema.'.classlevel')->where('classlevel_id',$this->data['classes']->classlevel_id)->first();

                $this->data['classesID'] = $classesID;
                $this->data['allsections'] = DB::table($schema.'.section')->where('classesID', $classesID)->get();
                $this->data['exam_report'] = \collect(\DB::SELECT('SELECT * from ' . $schema . '.exam_report WHERE classes_id=' . $classesID . ' AND exam_id=' . $exam_id))->first();
                $this->data['totalstudent_insection'] = $this->get_student_per_class($classesID, $this->data['sectionID'], null, $schema);

                $this->data['character'] = DB::table($schema.'.general_character_assessment')->where('student_id', $student_id)->where('exam_id', $exam_id)->first();

                //Payment Reports
                $this->data["invoice"] = DB::table($schema.'.invoices')->where('student_id', $id)->where('academic_year_id', $academic_year_id)->first();
            if(!empty($this->data["invoice"])){
            /*   
                $start_date = $this->data['academic_year']->start_date;
                $sql = \collect(DB::SELECT('SELECT "settingID",invoice_guide FROM ' . $schema . '. setting'))->first();
                $this->data['invoice_guide'] = $sql;

                $this->data['previous_balance'] = collect(\DB::SELECT('SELECT SUM(balance) as last_balance from ' . $schema . '. invoice_balances where start_date < \'' . $start_date . '\' and student_id= ' . $this->data["invoice"]->student_id . ''))->first();
                
                $this->data['previous_advance'] = collect(\DB::SELECT('SELECT SUM(reminder) as advance_balance from ' . $schema . '. advance_payment_balance where student_id= ' . $this->data["invoice"]->student_id . ''))->first();
                $this->data["total_paid"] = \App\Model\PaymentsInvoicesFeesInstallment::whereIn('invoices_fees_installment_id', \App\Model\InvoicesFeesInstallment::whereIn('invoice_id', \App\Model\Invoice::where('student_id', $id)->where('academic_year_id', $academic_year_id)->get(['id']))->get(['id']))->sum('amount');
                $this->data["total_amount"] = \App\Model\FeesInstallmentsClass::where('class_id', $classesID)->whereIn('fees_installment_id', \App\Model\InvoicesFeesInstallment::whereIn('invoice_id', \App\Model\Invoice::where('student_id', $id)->where('academic_year_id', $academic_year_id)->get(['id']))->get(['fees_installment_id']))->sum('amount');
                */
            }
            return view('exam/studentreport', $this->data);
            } else {
                return redirect()->back()->with('info', 'No Marks recorded for this student , please advice teachers to record marks for ' . $student->student->name . ' ');
            }
        } else {
            return redirect()->back()->with('info', 'No Marks recorded for this student , please advice teachers to record marks for ' . $student->student->name . ' ');
        }
    }



    public function classreport() {
        $this->data['exams'] = array();
        $this->data['classes'] = DB::table('shulesoft.classes')->get();
        $classesID = request("classesID");
        $this->data['post_class_id'] = $classesID;
        $this->data['subjectID'] = 0;
        $this->data['students'] = array();
        $year = date("Y");

        if ($_POST) {

            $examID = request('examID');
            $this->data['this_schema'] = $schema = request('schema_name');
            $subjectID = (int) request('subjectID');
            $sectionID = request('sectionID');
            $academic_year_id = request('academic_year_id');
     
            $this->data['show_gender'] = request('show_gender') == 'on' ? 1 : 0;
            $classID = $classesID;
            $this->data['userclass'] = DB::table($schema.'.classes')->where('classesID', $classesID)->first();
            $class_info = DB::table($schema.'.classes')->where('classesID', $classesID)->first();

            $this->data["exams"] = DB::table($schema.'.exam')->where('examID', $examID)->first();
            $this->data['grades'] = $this->getGrades($class_info, $this->data["exams"], $sectionID, $schema);
            
            $subject_avg = DB::select('SELECT round(avg(a.mark),1) as average,a."subjectID",a."examID",a.subject_name FROM ' . $schema . '.mark_info a WHERE a.academic_year_id= '.$academic_year_id.' and "examID"='.$examID.' AND a."classesID"=' . $classID . ' and "subjectID" IN (SELECT "subjectID" FROM ' . $schema . '.subject where "classesID"=' . $classID . ')  and a.student_status=1 group by a."subjectID",a."examID",a.subject_name');
        
            $this->data['classlevel'] = DB::table($schema.'.classlevel')->where('classlevel_id', $class_info->classlevel_id)->first();
            if ($academic_year_id == '') {
                $this->session->set_flashdata('error', $this->lang->line('set_year'));
                return redirect(url('exam/report'));
            } else {
                $this->data['academic_year'] = DB::table($schema.'.academic_year')->where('id', $academic_year_id);
            }
            if ($examID == '') {
                $this->session->set_flashdata('error', 'specify well required field inputs');
                return redirect(url('exam/report'));
            }

            $year = date("Y");

            if ($sectionID == 0) {
                //get normal query
                $view = 'report_optimized';
                $this->data['subject_evaluations'] = DB::select('select round(AVG(mark),1), subject_name,"subjectID", sum(mark), rank() OVER (ORDER BY (avg(mark)) DESC) AS ranking from ' . $schema . '.mark_info where "examID"=' . $examID . '  and "classesID"=' . $classID . '  group by "subjectID",subject_name');

                $this->data['total_students'] = DB::select('select a.*,b.name,b.photo,b.roll, b.sex,c.section, c."classesID" from ' . $schema . '.student_archive a JOIN ' . '  ' . $schema . '.student b ON '
                                . '      b."student_id"= a.student_id JOIN ' . $schema . '.section c
                    ON  c."sectionID"=b."sectionID" WHERE ' . '      a.academic_year_id=' . $academic_year_id . '  AND ' . '      a.section_id IN (SELECT "sectionID" FROM ' . $schema . '.section WHERE "classesID"=' . $classID . ') and a.status=1 ');
            } else {

                //get by section
                $view = 'report_optimized';
                $this->data['subject_evaluations'] = DB::select('select round(AVG(mark),1), subject_name,"subjectID", sum(mark), rank() OVER (ORDER BY (avg(mark)) DESC) AS ranking from ' . $schema . '.mark_info where "examID"=' . $examID . '  and "classesID"=' . $classID . ' and "sectionID"=' . $sectionID . ' group by "subjectID",subject_name');

                $this->data['total_students'] = DB::select('select a.*,b.name,b.photo,b.sex, b.roll,c.section, c."classesID" from ' . $schema . '.student_archive a JOIN ' . '  ' . $schema . '.student b ON '
                                . '      b."student_id"= a.student_id JOIN ' . $schema . '.section c
                                    ON  c."sectionID"=b."sectionID" WHERE '
                                . '      a.academic_year_id=' . $academic_year_id . '  AND ' . '      a.section_id =' . $sectionID . ' and a.status=1');
            }

            $this->data['sectionID'] = $sectionID;
            $this->data['exam_info'] = DB::table($schema.'.exam')->where('examID', $examID)->first();

            if ($subjectID == 0) {
                //GET all subjects with total average and ranks if 
                //get students in that class first
                $sec = $sectionID == 0 ? null : $sectionID;
                $this->data['students'] = $this->create_single_report($classID, $examID, $sec, null, null, $this->data['show_gender'], $schema);

                //get subjects taken by that class

                $this->data['subjects'] = $sectionID == 0 ? DB::select('select a.*, b.code as subject_code,(select x.name from ' . $schema . '.teacher x join ' . $schema . '.section_subject_teacher s on (x."teacherID"=s."teacherID" AND s.refer_subject_id=b.subject_id and s.subject_id=a."subjectID") where s.academic_year_id=' . $academic_year_id . ' limit 1 ) as teacher from ' . $schema . '.subject a join ' . $schema . '.refer_subject b on a.subject_id=b.subject_id where a."classesID"=' . $classID . ' AND a."subjectID" in (select "subjectID" from ' . $schema . '.mark where "examID"=' . $examID . ') order by b.arrangement asc') :
                        DB::select('select a.*, b.code as subject_code,(select x.name from ' . $schema . '.teacher x join ' . $schema . '.section_subject_teacher s on (x."teacherID"=s."teacherID" AND s.refer_subject_id=b.subject_id and s.subject_id=a."subjectID") where s.academic_year_id=' . $academic_year_id . ' and s."sectionID"=' . $sectionID . ' limit 1 ) as teacher from ' . $schema . '.subject a join ' . $schema . '.refer_subject b on a.subject_id=b.subject_id where a."classesID"=' . $classID . ' AND a."subjectID" in (SELECT subject_id FROM ' . $schema . '.subject_section WHERE section_id=' . $sectionID . ')  order by b.arrangement asc');
            } else {
                //get single subject with average
                $this->data['students'] = $this->get_subject_with_rank($examID, $classID, $subjectID, $sectionID, $schema);
                $this->data['subjects'] = array();
            }

            $this->data['eattendances'] = array();

            if (!empty($this->data['students'])) {
                $section_res = $sectionID == 0 ? array() : array('sectionID' => $sectionID);
                $sections = DB::table($schema.'.section')->where(array_merge(array("classesID" => $classID), $section_res))->get();

                $this->data['sections'] = $sections;

                if ($subjectID == 0) {
                    foreach ($sections as $key => $section) {
                        $this->data['allsection'][$section->section] = $this->create_single_report($classID, $examID, $section->sectionID, null, null, $this->data['show_gender'], $schema);
                        $this->data['subject_section_evaluations'][$section->section] = DB::select('select round(AVG(mark),1), subject_name,"subjectID", sum(mark), rank() OVER (ORDER BY (avg(mark)) DESC) AS ranking from ' . $schema . '.mark_info where "examID"=' . $examID . '  and "classesID"=' . $classID . '  and "sectionID"=' . $section->sectionID . ' group by "subjectID",subject_name');
                        $this->data['section'][$section->section]['sectionID'] = $section->sectionID;
                        $this->data['subject_section'][$section->section] = ('SELECT a."subjectID", a."classesID", a."teacherID", c.code as subject_code, a.teacher_name, a.is_counted, a.is_penalty, a.pass_mark, a.grade_mark, a.subject_id, a.subject_type, c.subject_name as subject, a.teacher_name as teacher, b.signature FROM ' . $schema . '.subject a JOIN ' . $schema . '.teacher b on b."teacherID"=a."teacherID" join ' . $schema . '.refer_subject c ON c.subject_id=a.subject_id WHERE a."subjectID" in (SELECT subject_id FROM ' . $schema . '.subject_section WHERE section_id=' . $section->sectionID . ') order by subject_type, subject desc');
                    }
                } else { 
                    foreach ($sections as $key => $section) {
                        $this->data['allsection'][$section->section] = $this->get_subject_with_rank($examID, $classID, $subjectID, $section->sectionID, $schema);
                    }
                }
                $this->data['examID'] = $examID;
                $this->data['classesID'] = $classID;
                $this->data['this_school'] = $schema;
                $this->data['subjectID'] = $subjectID;
                $this->data['action'] = request('action');
                $this->data['show_grade'] = request('show_grade') == 'on' ? 1 : 0;
                $this->data['show_rank'] = request('show_rank') == 'on' ? 1 : 0;
                $this->data['show_footer'] = request('show_footer') == 'on' ? 1 : 0;

                $this->data['show_rank'] == 1 ? $this->getSubjectRank($examID, $classID, $schema) : '';
                $this->data['pos_in_class'] = request('pos_in_class');
                $this->data['pos_in_section'] = request('pos_in_section');
                $this->data['show_subject_avg'] = request('show_subject_avg');
                $this->data['academic_year_id'] = $academic_year_id;
            }
            $this->data['editable'] = 'contenteditable="true"';

            if (session("usertype") == "Teacher") {
                $this->data['editable'] = '';
            }
            if (request('action') == 'export') {
                $this->data["subview"] = "exam/ca_export";
                echo view('exam.ca_export', $this->data);
                $file_name = $schema . $examID . '_' . $academic_year_id . '_' . $sectionID . '_' . $classID . '_' . $this->data['show_grade'] . '_' . $this->data['show_footer'] . '.xls';
                return response()->download('storage/app/' . $file_name, $file_name, array('Content-Type: application/excel'));
            }

            return view('exam.classreport', $this->data);
        } else {
            return view('exam.classreport', $this->data);
        }
}


public function printall_combined() {
    $language = $this->session->userdata('lang');
    $this->lang->load('exam_report', $language);
    $id = clean_htmlentities(($this->uri->segment(3)));
    $classesID = request('class_id');
    $sectionID = request('section_id');
    $academic_year_id = request('academic_id');
    $percent = request('percent');
    $schema = request('schema');
    $this->data['siteinfos'] = DB::table($schema.'.setting')->first();

    $this->data['academic_year_name'] = DB::table($schema.'.academic_year')->where('id', $academic_year_id)->first();;
    $data = explode('_', request('exam'));
    $out = '';
    foreach ($data as $value) {
        $out .= ' "examID"=' . $value . ' or ';
    }

    $where = rtrim($out, ' or ');

    $this->data['exams'] = DB::select('SELECT * FROM ' .  $schema . '.exam WHERE ' . $where . '  ORDER BY date ASC');

    $this->data['marks'] = $this->getMarksForStudents($where, $classesID, $academic_year_id, $schema);

    $examarray = array();
    $i = 0;
    foreach ($this->data['exams'] as $value) {
        $examarray[$i] = $value->examID;
        $i++;
    }


    $this->data['semesters'] = DB::select('SELECT * FROM ' .  $schema . '.semester WHERE id IN (SELECT DISTINCT semester_id FROM ' .  $schema . '.exam WHERE ' . $where . ') AND academic_year_id=' . $academic_year_id . ' ORDER BY start_date ASC');

    foreach ($this->data['semesters'] as $semester) {

        $this->data['sem_exams'][$semester->id] = DB::table($schema.'.exam')->where('semester_id', $semester->id)->whereIn('examID', $data)->orderBy('semester_id', 'ASC')->orderBy('date', 'asc')->get();
    }

    $this->data['students'] = $this->combinedOptimized($classesID, $examarray, $sectionID, $academic_year_id, null, null, null, $schema);

    if (empty($this->data['students'])) {
        return redirect()->back()->with('warning', 'No students in this stream');
    }
    $students_inclass = $this->combinedOptimized($classesID, $examarray, NULL, $academic_year_id, null, null, null, $schema);

    foreach ($students_inclass as $class_rank_student) {

        $this->data['class_rank'][$class_rank_student->student_id] = $class_rank_student->rank;
        $sql = 'SELECT a.* from ' .  $schema . '.general_character_assessment a join ' .  $schema . '.exam b on a.exam_id=b."examID"  WHERE  a.student_id=' . $class_rank_student->student_id . ' and a.exam_id IN (' . implode(',', $data) . ') order by b.date desc limit 1';
        $this->data['character'][$class_rank_student->student_id] = \collect(DB::select($sql))->first();

        if ($this_schema == 'enaboishu.') {

            $this->data['character'] = DB::select('SELECT * FROM ' .  $schema . '.general_character_assessment WHERE student_id=' . $class_rank_student->student_id . ' and ' . str_replace('examID', 'exam_id', $where) . ' limit 1');
        }
    }
    $this->data["classes"] =  DB::table($schema.'.classes')->where('classesID', $classesID)->first();
    $this->data["grades"] = $this->getGrades($this->data["classes"], null, $sectionID);

    $student_archive = DB::table($schema.'.student_archive')->where('section_id', $sectionID)->where('academic_year_id', $academic_year_id)->where('status', 1)->first();
    $student = $student_archive->student;

    $students_per_section = $this->getStudentByYearClass($classesID, $academic_year_id,$schema, $sectionID);
    $students_per_class = $this->getStudentByYearClass($classesID, $academic_year_id,$schema, null);

    $this->data['students_per_section'] = sizeof($students_per_section);
    $this->data['students_per_class'] = sizeof($students_per_class);

    $this->data["class_teacher_signature"] = $this->get_teacher_signature($schema, $sectionID);

    $this->data['level'] = $this->get_result_format($schema, $student->student_id);
    $this->data['classesID'] = $classesID;
    $this->data['academic_year_id'] = $academic_year_id;
    $this->data['sectionID'] = $sectionID;
    $this->data['section'] = $student->section;
    $this->data['empty_mark'] = $this->data['siteinfos']->empty_mark;

    $this->data['exam_report'] = $this->getExamReportTitle($classesID, $academic_year_id, $schema);

    $this->data['report_setting'] = DB::table($schema. ".exam_report_settings")->where('exam_type', 'combined')->first();

    $this->data['subjects'] = $this->getStudentSubjects($academic_year_id, $classesID, $this->data['report_setting']->subject_order_by_arrangement, $schema);
    return view('examreport.printall_combined_semester', $this->data);
}



public function printall() {
    //should pass either class ID or SectionID with examID
    $id = clean_htmlentities(($this->uri->segment(3))); //examID
    $classesID = request('class_id');
    $sectionID = request('section_id');
    $academic_year_id = request('year_no');
    $schema = request('schema');
    $this->data['siteinfos'] = DB::table($schema.'.setting')->first();

    if (empty($classesID) && empty($sectionID)) {
        die('Wrong url supplied');
    } else {
        $exam_id = $id;
        $this->data['exam_id'] = $id;
        $this->data["signature"] = $this->data['siteinfos']->signature;
        $this->data["academic_year_id"] = $academic_year_id;
        $year = DB::table($schema.'.adademic_year')->where('id',$academic_year_id)->first();
        $this->data["classes"] =  DB::table($schema.'.classes')->where('classesID', $classesID)->first();
        $this->data["exams"] =  DB::table($schema.'.exam')->where('examID', $exam_id)->first();

        $this->data['year_name'] = $year->name;

        $this->data["grades"] = $this->getGrades($this->data['classes'], $this->data["exams"], $sectionID, $schema);
        $this->data["class_teacher_signature"] = $this->get_teacher_signature($schema, $sectionID);

        $this->data['students'] = DB::select('select b."student_id", x.section, a.section_id, a.academic_year_id,b.name,b.photo,b.roll,b.sex from ' .  $schema . '.student_archive a join ' .  $schema . '.student b on b."student_id"=a.student_id join ' .  $schema . '.section x on x."sectionID"=a."section_id" WHERE a.section_id=' . $sectionID . ' AND b."student_id" IN (select distinct "student_id" from ' .  $schema . '.mark where "classesID"=' . $classesID . ' AND "examID"=' . $exam_id . ' AND academic_year_id=' . $academic_year_id . ' AND mark is not null and mark <>0) and a.academic_year_id=' . $academic_year_id . '');

        $this->data['exam_report'] = DB::table($schema.'.exam_report')->where('classes_id', $classesID)->where('exam_id', $exam_id)->first();


        $this->data['total_students'] = \collect(DB::select('select count(distinct "student_id") as count from ' .  $schema . '.mark_info where "classesID"=' . $classesID . ' AND "examID"=' . $exam_id . ' AND academic_year_id=' . $academic_year_id . ' AND mark is not null and mark <>0 '))->first();

        $this->data['empty_mark'] = $this->data['siteinfos']->empty_mark;

        $this->data['level'] = $classlevel = DB::table($schema.'.classlevel')->where('classlevel_id', $this->data['classes']->classlevel_id)->first();

        $this->data['classrank'] = $this->getPosition($exam_id, $classesID, null, $schema);
        $this->data['sectionrank'] = $this->getPosition($exam_id, $classesID, $sectionID, $schema);

        $this->data['marks'] = $this->getMarksForStudents(' "examID"=' . $exam_id . ' ', $classesID, $academic_year_id, $schema);
        $this->data['report_setting'] = DB::table('exam_report_settings')->where('exam_type', 'single')->first();
        $this->data['subjects'] = $this->getStudentSubjects($academic_year_id, $classesID, $this->data['report_setting']->subject_order_by_arrangement, $schema);

        $this->data['report'] = $this->getStudentRank($exam_id, $classesID, null, $schema);
        $this->data['sectionreport'] = $this->getStudentRank($exam_id, $classesID, $sectionID, $schema);

        $this->data['character'] = $this->getCharactersAssessed($exam_id, $classesID, $schema);
        
    }

    return view('examreport/print', $this->data);
}

    public function teacher() {
        $this->data['exams'] = array();
        $this->data['classes'] = DB::table('shulesoft.classes')->get();
        $classesID = request("classesID");
        $this->data['post_class_id'] = $classesID;
        $this->data['subjectID'] = 0;
        $this->data['students'] = array();
        $year = date("Y");

        if ($_POST) {

            $examID = request('examID');
            $this->data['this_schema'] = $schema = request('schema_name');
            $subjectID = (int) request('subjectID');
            $sectionID = request('sectionID');
            $academic_year_id = request('academic_year_id');
     
            $this->data['show_gender'] = request('show_gender') == 'on' ? 1 : 0;
            $classID = $classesID;
            $this->data['userclass'] = DB::table($schema.'.classes')->where('classesID', $classesID)->first();
            $class_info = DB::table($schema.'.classes')->where('classesID', $classesID)->first();

            $this->data["exams"] = DB::table($schema.'.exam')->where('examID', $examID)->first();
            $this->data['grades'] = $this->getGrades($class_info, $this->data["exams"], $sectionID, $schema);
            
            $subject_avg = DB::select('SELECT round(avg(a.mark),1) as average,a."subjectID",a."examID",a.subject_name FROM ' . $schema . '.mark_info a WHERE a.academic_year_id= '.$academic_year_id.' and "examID"='.$examID.' AND a."classesID"=' . $classID . ' and "subjectID" IN (SELECT "subjectID" FROM ' . $schema . '.subject where "classesID"=' . $classID . ')  and a.student_status=1 group by a."subjectID",a."examID",a.subject_name');
        
            $this->data['classlevel'] = DB::table($schema.'.classlevel')->where('classlevel_id', $class_info->classlevel_id)->first();
            if ($academic_year_id == '') {
                $this->session->set_flashdata('error', $this->lang->line('set_year'));
                return redirect(url('exam/report'));
            } else {
                $this->data['academic_year'] = DB::table($schema.'.academic_year')->where('id', $academic_year_id);
            }
            if ($examID == '') {
                $this->session->set_flashdata('error', 'specify well required field inputs');
                return redirect(url('exam/report'));
            }

            $year = date("Y");

            if ($sectionID == 0) {
                //get normal query
                $view = 'report_optimized';
                $this->data['subject_evaluations'] = DB::select('select round(AVG(mark),1), subject_name,"subjectID", sum(mark), rank() OVER (ORDER BY (avg(mark)) DESC) AS ranking from ' . $schema . '.mark_info where "examID"=' . $examID . '  and "classesID"=' . $classID . '  group by "subjectID",subject_name');

                $this->data['total_students'] = DB::select('select a.*,b.name,b.photo,b.roll, b.sex,c.section, c."classesID" from ' . $schema . '.student_archive a JOIN ' . '  ' . $schema . '.student b ON '
                                . '      b."student_id"= a.student_id JOIN ' . $schema . '.section c
                    ON  c."sectionID"=b."sectionID" WHERE ' . '      a.academic_year_id=' . $academic_year_id . '  AND ' . '      a.section_id IN (SELECT "sectionID" FROM ' . $schema . '.section WHERE "classesID"=' . $classID . ') and a.status=1 ');
            } else {

                //get by section
                $view = 'report_optimized';
                $this->data['subject_evaluations'] = DB::select('select round(AVG(mark),1), subject_name,"subjectID", sum(mark), rank() OVER (ORDER BY (avg(mark)) DESC) AS ranking from ' . $schema . '.mark_info where "examID"=' . $examID . '  and "classesID"=' . $classID . ' and "sectionID"=' . $sectionID . ' group by "subjectID",subject_name');

                $this->data['total_students'] = DB::select('select a.*,b.name,b.photo,b.sex, b.roll,c.section, c."classesID" from ' . $schema . '.student_archive a JOIN ' . '  ' . $schema . '.student b ON '
                                . '      b."student_id"= a.student_id JOIN ' . $schema . '.section c
                                    ON  c."sectionID"=b."sectionID" WHERE '
                                . '      a.academic_year_id=' . $academic_year_id . '  AND ' . '      a.section_id =' . $sectionID . ' and a.status=1');
            }

            $this->data['sectionID'] = $sectionID;
            $this->data['exam_info'] = DB::table($schema.'.exam')->where('examID', $examID)->first();

            if ($subjectID == 0) {
                //GET all subjects with total average and ranks if 
                //get students in that class first
                $sec = $sectionID == 0 ? null : $sectionID;
                $this->data['students'] = $this->create_single_report($classID, $examID, $sec, null, null, $this->data['show_gender'], $schema);

                //get subjects taken by that class

                $this->data['subjects'] = $sectionID == 0 ? DB::select('select a.*, b.code as subject_code,(select x.name from ' . $schema . '.teacher x join ' . $schema . '.section_subject_teacher s on (x."teacherID"=s."teacherID" AND s.refer_subject_id=b.subject_id and s.subject_id=a."subjectID") where s.academic_year_id=' . $academic_year_id . ' limit 1 ) as teacher from ' . $schema . '.subject a join ' . $schema . '.refer_subject b on a.subject_id=b.subject_id where a."classesID"=' . $classID . ' AND a."subjectID" in (select "subjectID" from ' . $schema . '.mark where "examID"=' . $examID . ') order by b.arrangement asc') :
                        DB::select('select a.*, b.code as subject_code,(select x.name from ' . $schema . '.teacher x join ' . $schema . '.section_subject_teacher s on (x."teacherID"=s."teacherID" AND s.refer_subject_id=b.subject_id and s.subject_id=a."subjectID") where s.academic_year_id=' . $academic_year_id . ' and s."sectionID"=' . $sectionID . ' limit 1 ) as teacher from ' . $schema . '.subject a join ' . $schema . '.refer_subject b on a.subject_id=b.subject_id where a."classesID"=' . $classID . ' AND a."subjectID" in (SELECT subject_id FROM ' . $schema . '.subject_section WHERE section_id=' . $sectionID . ')  order by b.arrangement asc');
            } else {
                //get single subject with average
                $this->data['students'] = $this->get_subject_with_rank($examID, $classID, $subjectID, $sectionID, $schema);
                $this->data['subjects'] = array();
            }

            $this->data['eattendances'] = array();

            if (!empty($this->data['students'])) {
                $section_res = $sectionID == 0 ? array() : array('sectionID' => $sectionID);
                $sections = DB::table($schema.'.section')->where(array_merge(array("classesID" => $classID), $section_res))->get();

                $this->data['sections'] = $sections;

                if ($subjectID == 0) {
                    foreach ($sections as $key => $section) {
                        $this->data['allsection'][$section->section] = $this->create_single_report($classID, $examID, $section->sectionID, null, null, $this->data['show_gender'], $schema);
                        $this->data['subject_section_evaluations'][$section->section] = DB::select('select round(AVG(mark),1), subject_name,"subjectID", sum(mark), rank() OVER (ORDER BY (avg(mark)) DESC) AS ranking from ' . $schema . '.mark_info where "examID"=' . $examID . '  and "classesID"=' . $classID . '  and "sectionID"=' . $section->sectionID . ' group by "subjectID",subject_name');
                        $this->data['section'][$section->section]['sectionID'] = $section->sectionID;
                        $this->data['subject_section'][$section->section] = ('SELECT a."subjectID", a."classesID", a."teacherID", c.code as subject_code, a.teacher_name, a.is_counted, a.is_penalty, a.pass_mark, a.grade_mark, a.subject_id, a.subject_type, c.subject_name as subject, a.teacher_name as teacher, b.signature FROM ' . $schema . '.subject a JOIN ' . $schema . '.teacher b on b."teacherID"=a."teacherID" join ' . $schema . '.refer_subject c ON c.subject_id=a.subject_id WHERE a."subjectID" in (SELECT subject_id FROM ' . $schema . '.subject_section WHERE section_id=' . $section->sectionID . ') order by subject_type, subject desc');
                    }
                } else { 
                    foreach ($sections as $key => $section) {
                        $this->data['allsection'][$section->section] = $this->get_subject_with_rank($examID, $classID, $subjectID, $section->sectionID, $schema);
                    }
                }
                $this->data['examID'] = $examID;
                $this->data['classesID'] = $classID;
                $this->data['this_school'] = $schema;
                $this->data['subjectID'] = $subjectID;
                $this->data['action'] = request('action');
                $this->data['show_grade'] = request('show_grade') == 'on' ? 1 : 0;
                $this->data['show_rank'] = request('show_rank') == 'on' ? 1 : 0;
                $this->data['show_footer'] = request('show_footer') == 'on' ? 1 : 0;

                $this->data['show_rank'] == 1 ? $this->getSubjectRank($examID, $classID, $schema) : '';
                $this->data['pos_in_class'] = request('pos_in_class');
                $this->data['pos_in_section'] = request('pos_in_section');
                $this->data['show_subject_avg'] = request('show_subject_avg');
                $this->data['academic_year_id'] = $academic_year_id;
            }
            $this->data['editable'] = 'contenteditable="true"';

            if (session("usertype") == "Teacher") {
                $this->data['editable'] = '';
            }
            if (request('action') == 'export') {
                $this->data["subview"] = "exam/ca_export";
                echo view('exam.ca_export', $this->data);
                $file_name = $schema . $examID . '_' . $academic_year_id . '_' . $sectionID . '_' . $classID . '_' . $this->data['show_grade'] . '_' . $this->data['show_footer'] . '.xls';
                return response()->download('storage/app/' . $file_name, $file_name, array('Content-Type: application/excel'));
            }

            return view('exam.classreport', $this->data);
        } else {
            return view('exam.classreport', $this->data);
        }
    }

    public function subject() {
        $this->data['home'] = 'Home';
        return view('dashboard.index', $this->data);
    }

    public function school() {
       
        $this->data['this_schema'] = $schema = request()->segment(3);
        $academic_year_id = request()->segment(4);
        $class_id = request()->segment(5);
     
        $this->data['school_name']  =  school_name(Auth::User()->id);
        if($class_id > 0){
            $this->data['year'] = DB::table($schema.'.academic_year')->where('id', $academic_year_id)->first();
            $this->data['exams'] = $exam = DB::SELECT('SELECT a."examID",a.exam,a.abbreviation, c.name as semester, b.class_id, c.academic_year_id, d.classes from '.$schema.'.exam a JOIN  '.$schema.'.class_exam b ON a."examID"=b.exam_id JOIN  '.$schema.'.semester c on c.id=a.semester_id  JOIN  '.$schema.'.classes d on b.class_id=d."classesID" where b.academic_year_id ='.$academic_year_id.' ORDER BY class_id');
           $this->data['classes'] = DB::table($schema.'.classes')->where('classlevel_id', $class_id)->get();
        }
        $this->data['schools'] =  $this->load_school(Auth::User()->id);
        return view('exam.schoolexam', $this->data);
    }

    public function schoolreport() {

        $this->data['this_schema'] = $schema = request()->segment(3);
        $this->data['exam_id'] = $exam_id = request()->segment(4);
        $this->data['class_id'] = $class_id = request()->segment(5);
        $this->data['academic_year_id'] = $academic_year_id = request()->segment(6);
     
        $this->data['school_name']  =  school_name(Auth::User()->id);
        if($class_id > 0){
            $this->data['year'] = DB::table($schema.'.academic_year')->where('id', $academic_year_id)->first();
            $this->data['exams'] = $exam = DB::SELECT('SELECT a."examID",a.exam,a.abbreviation, c.name as semester, b.class_id, c.academic_year_id, d.classes from '.$schema.'.exam a JOIN  '.$schema.'.class_exam b ON a."examID"=b.exam_id JOIN  '.$schema.'.semester c on c.id=a.semester_id  JOIN  '.$schema.'.classes d on b.class_id=d."classesID" where b.academic_year_id ='.$academic_year_id.' ORDER BY class_id');
            $this->data['classes'] = $class = DB::table($schema.'.classes')->where('classesID', $class_id)->first();
            $this->data['classlevel'] = $class = DB::table($schema.'.classlevel')->where('classlevel_id', $class->classlevel_id)->first();
            $this->data['subjects']  = DB::table($schema.'.subject')->where('classesID', $class_id)->get();
            $this->data['grades'] = DB::table($schema.'.grade')->where('classlevel_id', $class->classlevel_id)->get();
            $this->data['students'] = DB::select('SELECT * from '.$schema.'.student_archive where academic_year_id='. $academic_year_id .' AND section_id in (select "sectionID" from '.$schema.'.section where "classesID"='. $class_id.') AND status=1');
        }
        return view('exam.school_exam_report', $this->data);
    }

    public function class($id)
    {
        return view('user.profile', [
            'user' => User::findOrFail($id)
        ]);
    }


    public function get_semester() {
        $academic_year_id = request('academic_year_id');
        $schema = request('schema_name');

        $semesters = DB::table($schema.'.semester')->where('academic_year_id', $academic_year_id)->get();
        if (empty($semesters)) {
            echo '0';
        } else {
            echo "<option value='0'>Select Semester</option>";
            foreach ($semesters as $semester) {
                echo '<option value=' . $semester->id . '>' . $semester->name . '</option>';
            }
        }
    }

    /**
     * @accessed -combined exam view, get list of subjects
     */
    public function getSubjectByClass($class = null) {
        $id_class = request('id');
        $schema = request('schema_name');
        (int) $id_class > 0 ? $id = request('id') : $id = $class;
        $skip_all = request('skip_all');
        if ((int) $id) {
            $usertype = session("usertype");
          
                $allsubject = DB::table($schema.'.subject')->where("classesID", $id)->get();
                echo "<option value='0'>", 'select_subject', "</option>";
                echo (int) $skip_all == 1 ? '' : "<option value='all'>All Subjects</option>";
                foreach ($allsubject as $value) {
                    echo "<option value=\"$value->subjectID\">", $value->subject, "</option>";
                }
            }
        }

    public function getSubjectByExam() {
        $id = request('id');
        $class_id = request('class_id');
        $schema = request('schema_name');
        if ((int) $id) {
            $allsubject = $this->db->query('SELECT  "subjectID", subject FROM ' . $schema . 'subject WHERE  "classesID"=' . $class_id)->result();
            echo "<option value='0'>", $this->lang->line("mark_select_subject"), "</option>";
            foreach ($allsubject as $value) {
                echo "<option value=\"$value->subjectID\">", $value->subject, "</option>";
            }
        }
    }


    public function getExamByClass() {
        $schema = request('schema_name');
        $classId = request('id');
        $academic_year_id = request('academic_year_id');
            $where = ' AND a.academic_year_id=' . $academic_year_id;
            $class_request_id = $classId;

        if ((int) $class_request_id) {
            $where = ' AND a.academic_year_id=' . $academic_year_id;
            $exams = DB::SELECT('SELECT b.exam, b."examID" from ' . $schema . '.class_exam a left join ' . $schema . '.exam b ON a.exam_id=b."examID" where  a.class_id=' . $class_request_id . $where);
            echo "<option value='0'>Select Exam</option>";
            foreach ($exams as $value) {
                echo "<option value=\"$value->examID\">", $value->exam, "</option>";
            }
        }
    }

    public function get_academic_years() {
        $class_id = request('id');
        $schema = request('schema_name');
        if (!empty($class_id)) {
            $class = DB::table($schema.'.classes')->where("classesID",$class_id)->first();
            if (!empty($class)) {
                $academics = DB::table($schema.'.academic_year')->where('class_level_id', $class->classlevel_id)->get();
                if (empty($academics)) {
                    echo '0';
                } else {
                    echo "<option value='0'>select year</option>";
                    foreach ($academics as $academic) {
                        echo '<option value=' . $academic->id . '>' . $academic->name . '</option>';
                    }
                }
            }
        }
    }


    public function getClasses() {
        $schema = request('schema_name');
        if (!empty($schema)) {
            $classes = DB::table($schema.'.classes')->get();
            if (count($classes)<1) {
                    echo '0';
                } else {
                    echo "<option value='0'>select year</option>";
                    foreach ($classes as $class) {
                        echo '<option value=' . $class->classesID . '>' . $class->classes . '</option>';
                    }
                }
            }else{
                echo '0';
            }
    }

    public function get_academic_years_bylevel() {
        $schema = request('schema_name');
        $class_level_id = request('id');
        $academics = DB::table($schema.'.academic_year')->where('class_level_id', $class_level_id)->get();
        if (empty($academics)) {
            echo '0';
        } else {
            echo "<option value='0'>select year</option>";
            foreach ($academics as $academic) {
                echo '<option value=' . $academic->id . '>' . $academic->name . '</option>';
            }
        }
    }

    public function getGrades($class_object, $exam_object = null, $section_id = null, $schema = null) {
        if ($exam_object != null && (int) $exam_object->special_grade_name_id > 0) {
            $grades = DB::table($schema.'.special_grades')->where('special_grade_name_id', $exam_object->special_grade_name_id)->orderBy('point', 'asc')->get();
            if (count($grades) < 3 || empty($grades)) {
                return $this->fallBackGrade($class_object, $schema);
            }
        } else if ((int) $class_object->special_grade_name_id > 0) {
            if ((int) $section_id == 0) {
                $grades = DB::table($schema.'.special_grades')->where('special_grade_name_id', $class_object->special_grade_name_id)->orderBy('grade')->get();
            } else {
                $section = DB::table($schema.'.section')->where("sectionID", $section_id)->first();

                if ((int) $section->special_grade_name_id > 0) {
                    $grades = DB::table($schema.'.special_grades')->where('special_grade_name_id', $section->special_grade_name_id)->orderBy('grade')->get();
                } else {
                    $grades = DB::table($schema.'.special_grades')->where('special_grade_name_id', $class_object->special_grade_name_id)->orderBy('grade')->get();
                }
            }
            if (count($grades) < 3 || empty($grades)) {
                return $this->fallBackGrade($class_object, $schema);
            }
        } else {
            $grades = $this->fallBackGrade($class_object, $schema);
        }
        return $grades;
    }

    private function fallBackGrade($class_object, $schema=null) {
        return DB::table($schema.'.grade')->where('classlevel_id', $class_object->classlevel_id)->orderBy('point', 'asc')->get();
    }


    public function create_single_report($class_id, $exam_id, $section_id = null, $avg_format = NULL, $student_id = NULL, $show_gender_rank = null,$schema=null) {
        $schema_name = $schema;
       $this->exam_avg_format = get_avg_format($schema);
       $section = $section_id == NULL ? '' : ' AND "sectionID"=' . $section_id;
       //get exams to be used in this loop
       $subject_sql = 'SELECT distinct subject_name FROM ' . $schema_name . '.mark_info where "classesID"=' . $class_id . ' AND "examID"=' . $exam_id . ' ' . $section . ' order by 1';
       $subject_status = 'SELECT distinct subject_name, is_counted FROM ' . $schema_name . '.mark_info where "classesID"=' . $class_id . ' AND "examID"=' . $exam_id . ' ' . $section . ' order by 1';
       $result = DB::select($subject_status);
       if (!empty($result)) {
           $list = '';
           $sum_string = '';
           $count = 0;
           foreach ($result as $subject) {
               $list .= ' "' . strtolower($subject->subject_name) . '" NUMERIC, ';
               if ($subject->is_counted == 1) {
                   $sum_string .= 'Coalesce("' . $subject->subject_name . '",0) + ';
                   $count++;
               }
           }
           $subject_list = rtrim($list, ', ') == '' ? 'null' : rtrim($list, ', ');
           $subject_list_add = rtrim($sum_string, ' + ');
       } else {
           $subject_list = ' "subject" NUMERIC';
       }
       if ($this->exam_avg_format == NULL || $this->exam_avg_format == 0) {
           /**
            * This is default, based on subjects done by that student
            */
           // $sql_avg = ' JOIN (SELECT COUNT(subject_name) as subject_count, "student_id" from ' . $schema_name . '.mark_info  WHERE "classesID"=' . $class_id . ' AND "examID"=' . $exam_id . '  AND is_counted=1 AND mark is not null GROUP BY "student_id") c ON c."student_id"=b."student_id"';
           $avg_table = '.sum_exam_average_done';
       } else {
           /* /*
            * based on subjects counts (core and subscribed option subjects)
            */
           $avg_table = '.sum_exam_average';
       }
       $gender_rank = '';

       if ($show_gender_rank == 1) {
           $gender_rank = ' ,rank() over (partition by sex order by average desc) as gender_rank ';
       }
       $sql_avg = ' JOIN ' . $schema_name . $avg_table . ' c ON c."student_id"=b."student_id" where c."classesID"=' . $class_id . ' AND b.status in (1,2) and c."examID"=' . $exam_id;

       $sql = 'with tempa as (
       select  b.name, b.sex,b.roll,a.*, c.subject_count, c.sum as total, c.average FROM 
           (SELECT * FROM public.crosstab( \'select "student_id"::integer, subject_name, mark from ' . $schema_name . '.mark_info where "classesID"=' . $class_id . ' AND "examID"=' . $exam_id . ' ' . $section . ' order by 1,2 \',\' ' . $subject_sql . '\')

             AS final_result("student_id" integer, ' . $subject_list . ')
   ) as a JOIN ' . $schema_name . '.student b ON a."student_id"=b."student_id" ' . $sql_avg . ' 
 ),
       tempb as (select * from tempa )
        select *, rank() over (order by average desc) as rank ' . $gender_rank . ' from tempb';
       $sql_string = $student_id == NULL ? $sql : ' select * from (' . $sql . ') f WHERE f."student_id"=' . $student_id; 

       return \collect(DB::select($sql_string))->toArray();
   }

   public function get_subject_with_rank($examID, $classID, $subjectID, $sectionID = 0, $schema=null) {
    $where_section = $sectionID == 0 ? '' : ' and c.section_id=' . $sectionID;
    $sql = 'select subject, name,roll, "student_id", section_id as "sectionID","examID", mark, rank FROM ( select b.name, b.roll, a.subject, a."examID", a."classesID", b."student_id",c.section_id, a.mark, rank() over (partition by a."subjectID" order by a.mark desc) from ' . $schema . '.mark a  JOIN ' . $schema . '.student b ON a."student_id"=b."student_id" JOIN ' . $schema . '.student_archive c ON (c.student_id=a."student_id" and a.academic_year_id=c.academic_year_id) where a."examID" = ' . $examID . ' ' . $where_section . ' and a."classesID" = ' . $classID . ' and a."subjectID" =' . $subjectID . ' and a.mark is not null) c';
    return DB::select($sql);
}

public function getSubjectRank($exam_id, $class_id, $schema=null) {
    $subjects = DB::select('select student_id,"subjectID",mark, rank()  over (PARTITION BY "subjectID" order by mark desc) as rank from ' . $schema . '.mark_info a where a."classesID"=' . $class_id . ' AND a.student_status=1 and a."examID"=' . $exam_id);
    $student_rank = [];
    foreach ($subjects as $subject) {
        $student_rank[$subject->student_id][$subject->subjectID] = [];
    }
    foreach ($subjects as $subject) {
        $student_rank[$subject->student_id][$subject->subjectID] = $subject->rank;
    }
    $this->data['subject_rank'] = $student_rank;
}

public function get_subject_rank($examID, $classesID, $student_id, $sectionID = null, $schema=null) {
    $avg_table_view = get_avg_format($schema) ? $schema.'.sum_exam_average_done' : $schema.'.sum_exam_average';

    if ($sectionID == NULL) {
        $sql = 'SELECT sum,rank,average FROM (
            select *, rank() over (order by average desc) from ' . $avg_table_view . ' where "examID" = ' . $examID . ' and "classesID" = ' . $classesID . ') a WHERE a."student_id"=' . $student_id . '';
        } else {
        $sql = 'SELECT sum,rank,average FROM (
            select *, rank() over (order by average desc) from ' . $avg_table_view . ' where "examID" = ' . $examID . ' and "classesID" = ' . $classesID . ' AND "student_id" in (select student_id from ' . $schema . '.student_archive where section_id = ' . $sectionID . ') ) a WHERE a."student_id"=' . $student_id . '';
        }
    return \collect(DB::select($sql))->first();
}


public function get_student_per_class($classesID, $sectionID = NULL, $academic_year_id = null, $schema=null) {
    $acc = $academic_year_id == NULL ? '' : 'AND academic_year_id=' . $academic_year_id;
    if ($sectionID == NULL) {
        $sql = 'select COUNT(*) as total_student from  ' . $schema . '.student_archive  where status=1 AND section_id IN (SELECT "sectionID" FROM ' . $schema . '.section WHERE "classesID"=' . $classesID . ') ' . $acc;
    } else {
        $sql = 'select COUNT(*) as total_student from  ' . $schema . '.student_archive  where status=1 AND section_id=' . $sectionID . ' ' . $acc;
    }
    return $total = DB::SELECT($sql);
}


public function teacherprofile(){
    $this->data['schema'] = $schema = request()->segment(3);
    if(request()->segment(4) > 0){
        $this->data['teacher'] = $this->teacher_info(request()->segment(3),request()->segment(4)); 
        $this->data['subjects_allocated'] = $this->subjects_allocated($schema,request()->segment(4));
        $this->data['avg_perfomance'] = $this->avg_perfomance($schema,request()->segment(4));
    }
    return view('users.teacher_profile', $this->data);
}



public function combined() {
     
    $id = request()->segment(3);
    $classesID = request("classesID");

    $this->data['subjectID'] = 0;
    $this->data['students'] = array();
    $year = date("Y");

    if ($_POST && $classesID != 0) {
        $classID = request('classesID');
        $academic_year_id = request('academic_year_id');
        $this->data['academic_year_id'] = $academic_year_id;
        $this->data['this_schema'] = $schema = request('schema_name');
        $subjectID = request('subjectID');
        $section_id = $this->data['section_id'] = request('sectionID');
        $examID = request('examID');
        $year = date("Y");
        
        $combine_exams_array = request('exam_id');
        $this->data['exam_percents'] = request('exam_percent');
        $this->data['exam_name'] = request('exam_name');
        
        if (empty($combine_exams_array)) {
           
            return redirect(url("exam/combined"));
        }
        $this->data['siteinfos'] = DB::table($schema.'.setting')->first();
        $this->data['exams_taken'] = DB::table($schema.'.exam')->whereIn('examID', $combine_exams_array)->get();
       
        $classesID = $classID;
        /*
         * ----------------------------------------------------------
         * Check the total marks from a certain exam and combine them
         * with a specified percentage
         * ----------------------------------------------------------
         */

        $this->data['sections'] = $sections = DB::table($schema.'.section')->where("classesID", $classesID)->get();
        $class = $this->data['class'] = DB::table($schema.'.classes')->where('classesID', $classesID)->first();
        $this->data['grades'] = $this->getGrades($class, null, $section_id, $schema);
        $this->data['classlevel'] = DB::table($schema.'.classlevel')->where('classlevel_id', $class->classlevel_id)->first();

        if (strtolower($subjectID) == 'all') {
           
            return $this->combinedSubjects($combine_exams_array, $classesID, $academic_year_id, $section_id, null, $schema);
        }

        $this->data['students'] = $this->getStudentByYearClass($classesID, $academic_year_id,$schema, null);

        if ($section_id == 0) {
        
            $this->data['combine_exams'] = (int) $subjectID > 0 ?
                    $this->combinedOptimized($classesID, $combine_exams_array, NULL, $academic_year_id, $subjectID, null, null, $schema) :
                    $this->combinedOptimized($classesID, $combine_exams_array, NULL, $academic_year_id, $subjectID, null, null, $schema);

            foreach ($sections as $section) {

                $this->data['allsection'][$section->section] = $this->combinedOptimized($classesID, $combine_exams_array, $section->sectionID, $academic_year_id, $subjectID, null, null, $schema);
            }
        } else {
            $this->data['sections'] = $sections = DB::table($schema.'.section')->where("sectionID", $section_id)->get();
            $this->data['combine_exams'] = $this->combinedOptimized($classesID, $combine_exams_array, $section_id, $academic_year_id, $subjectID, null, null, $schema);
            foreach ($sections as $section) {
                $this->data['allsection'][$section->section] = array();
            }
        }
        $this_class = DB::table($schema.'.classes')->where('classesID', $classesID)->first();
        $this->data['examID'] = $examID;
        $this->data['classesID'] = $classesID;
        $this->data['grades'] = $this->getGrades($this_class, null, null, $schema);
        $this->data['subjectID'] = $subjectID;
        $data = $combine_exams_array;
        $url_ids = array();
        $out = '';

        foreach ($data as $value) {
            if ((int) $value > 0) {
                $out .= $value != '' ? ' a."examID"=' . $value . ' or ' : '';
                $value != '' ? array_push($url_ids, $value) : '';
            }
        }
        
        $where = rtrim($out, ' or ');
        $title_sql = 'SELECT * FROM '. $schema . '.exam a WHERE (' . $where . ') order by date asc';
        $this->data['exams_title'] = DB::select($title_sql);
        $where_subject = (int) $subjectID > 0 ? ' AND a."subjectID"=' . $subjectID : '';
        $this->data['exams_average'] = DB::select('SELECT round(avg(a.mark),1) as average,b."exam",b."examID" FROM '. $schema . '.mark_info a join '. $schema . '.exam b on b."examID"=a."examID"  WHERE (' . $where . ') AND a.student_status=1 and a."classesID"=' . $classesID . $where_subject . ' group by b."exam",b."examID" order by b.date ASC ');
        $this->data['subject_average'] = $this->createSubjectAverage($where, $classesID, $subjectID, $schema);
        $report_sent = DB::select("SELECT DISTINCT combined_exams FROM " . $schema . '.exam_report WHERE classes_id=' . $classesID . ' ');
        $report_sent_status = 0;
    
        foreach ($report_sent as $sent_report) {
            $array1 = explode('_', $sent_report->combined_exams);

            $array2 = $url_ids;

            if ($array1 === array_intersect($array1, $array2) && $array2 === array_intersect($array2, $array1)) {
                $report_sent_status = 1;
            } else {
                 
            }
        }

        $this->data["academic"] = DB::table($schema.'.academic_year')->where('id', $academic_year_id)->first();
        $this->data['combined_exams_array'] = $combine_exams_array;
        $this->data['report_sent'] = $report_sent_status;
        $this->data['semesters'] = DB::table($schema.'.exam')->whereIn('examID', $combine_exams_array)->select(DB::raw('distinct semester_id '))->get(['semester_id']);
        return view('exam.combined', $this->data);
    } else {
        return view('exam.combined', $this->data);
    }
}

public function singlecombined($student_id = NULL) {

    $schema = request()->segment(3);
    $id = request()->segment(4);
    if ((int) $id) {
        if (request('id') != NULL) {
            $this->updateReportSetting($schema, request('id'));
        }
        $this->data['this_schema'] = $schema;
        $this->data['tag'] = request('tag');
        $data = explode('_', request('exam'));
        $academic_year_id = request('year_no');
        $out = '';
        $this->data['siteinfos'] = DB::table($schema.'.setting')->first();
        foreach ($data as $value) {
            $out .= ' "examID"=' . $value . ' or ';
        }

        $where = rtrim($out, ' or ');

        $this->data['exams'] = DB::table($schema.'.exam')->whereIn('examID', $data)->orderBy('date', 'desc')->orderBy('semester_id', 'DESC')->get();
        $this->data['semesters'] = DB::select('SELECT * FROM ' . $schema . '.semester WHERE id IN (SELECT DISTINCT semester_id FROM ' . $schema . '.exam WHERE ' . $where . ') AND academic_year_id=' . $academic_year_id . ' ORDER BY start_date ASC');

        foreach ($this->data['semesters'] as $semester) {
            $this->data['sem_exams'][$semester->id] = DB::table($schema.'.exam')->where('semester_id', $semester->id)->whereIn('examID', $data)->orderBy('semester_id', 'ASC')->orderBy('date', 'asc')->get();
        }

        $student_id_report = $id;

        $stud = $this->getClassByYear($schema, $student_id_report, $academic_year_id);

        $this->data["section"] = DB::table($schema.'.section')->where('sectionID', $stud->section_id)->first();
        $class_id = $this->data["section"]->classesID;
        $this->data['empty_mark'] = $this->data["siteinfos"]->empty_mark;
        $this->data["classes"] = DB::table($schema.'.classes')->where('classesID', $class_id)->first();
        $this->data['report_setting'] = DB::table($schema.".exam_report_settings")->where('exam_type', 'combined')->where('classlevel_id', $this->data["classes"]->classlevel_id)->first();
        if (empty($this->data['report_setting'])) {
            DB::statement("INSERT INTO ".$schema.".exam_report_settings ( exam_type, average_column_name, show_teacher, show_remarks, show_teacher_sign, show_grade,"
                    . "  show_pos_in_stream, show_csee_division, show_acsee_division, show_subject_point, show_division_by_exam, "
                    . "show_division_total_points, show_overall_division, show_pos_in_class, show_pos_in_section, show_subject_pos_in_class, "
                    . " show_subject_pos_in_section, semester_average_name, single_semester_avg_name, overall_semester_avg_name, "
                    . "show_classteacher_name,classlevel_id) VALUES ('combined', 'AVG', 1, 1, 1, 1, 0, 1, 1, NULL, 1, 1, 1, 1, 0, NULL, NULL, 'AVG',"
                    . " 'AVERAGE OF AVG', 'AVERAGE OF AVG', 0," . $this->data["classes"]->classlevel_id . ")");
            $this->data['report_setting'] = DB::table($schema.'.exam_report_settings')->where('exam_type', 'single')->where('classlevel_id', $this->data["classes"]->classlevel_id)->first();
        }
        $order = $this->data['report_setting']->subject_order_by_arrangement == 1 ? 'arrangement' : 'subject';

        $this->data['subjects'] = DB::table($schema.'.subject_count')->where('student_id', $student_id_report)->where('academic_year_id', $academic_year_id)->orderBy($order)->get();


        $this->data['subjectID'] = 0;
        if ($this->data['tag'] == 'all') {
            $this->data['all_students'] = $this->getStudentByYearClass($class_id, $academic_year_id,$schema,null);
        }

        $this->data['students_per_section'] = $this->getStudentByYearClass($class_id, $academic_year_id, $schema,$stud->section_id);

        $this->data['students_per_class'] = $this->getStudentByYearClass($class_id, $academic_year_id,$schema, null);

        $this->data['year'] = date("Y");
        $this->data["classes"] = DB::table($schema.'.classes')->where('classesID', $class_id)->first();

        $this->data['grades'] = $this->getGrades($this->data['classes'], null, $stud->section_id, $schema);

        $this->data["marks"] = DB::table($schema.'.mark')->where("student_id", $stud->student_id)->where("classesID", $class_id)->get();
        $this->data['level'] = $this->get_result_format($schema, $stud->student_id);
        $this->data['classesID'] = $class_id;
        $this->data['rank'] = request('rank');

        $exams = explode('_', request('exam'));
        foreach ($exams as $value) {
            $combine_exams_array['exam' . $value] = $value;
        }

        $data_array = $combine_exams_array;

        $rank_in_class = $this->combinedOptimized($class_id, $data_array, null, $academic_year_id, NULL, $this->data['siteinfos']->exam_avg_format, $stud->student_id, $schema);

        $this->data['rank_in_class'] = !empty($rank_in_class) ? $rank_in_class[0]->rank : null;

        $rank_in_section = $this->combinedOptimized($class_id, $data_array, $stud->section_id, $academic_year_id, NULL, $this->data['siteinfos']->exam_avg_format, $stud->student_id, $schema);

        $this->data['rank_in_section'] = !empty($rank_in_section) ? $rank_in_section[0]->rank : null;


        $this->data['academic_year_name'] = DB::table($schema.'.academic_year')->where('id', $academic_year_id)->first();
        $this->data['student'] = DB::table($schema.'.student')->where('student_id', $stud->student_id)->first();
        $this->data["class_teacher_signature"] = $this->get_teacher_signature($schema, $stud->section_id);
        $this->data['allsections'] = DB::table($schema.'.section')->where('classesID', $class_id)->get();

        $combined_exams = str_replace('_', '::TEXT,', request('exam'));

        $this->data['exam_report'] = DB::select('SELECT * from ' . $schema . '.exam_report WHERE  classes_id=' . $class_id . ' ');

        $sql = 'SELECT a.* from ' . $schema . '.general_character_assessment a join ' . $schema . '.exam b on a.exam_id=b."examID"  WHERE  a.student_id=' . $stud->student_id . ' and a.exam_id IN (' . implode(',', $data) . ') order by b.date desc limit 1';

        $this->data['character'] = \collect(DB::select($sql))->first();

        return view('exam.semester_report', $this->data);
    }
}


public function combinedSubjects($combine_exams_array, $classesID, $academic_id, $sectionID = NULL, $get = null, $schema=null) {
    $exam_or = ' AND ('; 
    $exams_combined = '';
    foreach ($combine_exams_array as $key) {
        $exam_or .= ' "examID"=' . $key . ' OR ';
        $exams_combined .= $key . '_';
    }
    
    $exam_list = rtrim($exam_or, 'OR ') . ' )';

    $sql = 'SELECT a."subjectID",a."classesID",a.subject,a.subject_type,a.is_penalty,a.is_counted,a.is_counted_indivision,a.pass_mark, (select t.name from '. $schema . '.teacher t where t."teacherID"=(select "teacherID" FROM '. $schema . '.section_subject_teacher WHERE "classesID"=' . $classesID . ' AND academic_year_id=' . $academic_id . ' and subject_id=a."subjectID" limit 1)) as teacher_name, c.code as subject_code FROM '. $schema . '.subject a  join '. $schema . '.refer_subject c on c.subject_id=a.subject_id where a."classesID"=' . $classesID;

    $this->data['subjects'] = DB::SELECT($sql);
    
    // Subject::where("classesID", $classesID)->get();
    $sections = $sectionID > 0 ? DB::table($schema.'.section')->where("sectionID", $sectionID)->get() : DB::table($schema.'.section')->where("classesID", $classesID)->get();

     
    //to get average of subjects by percentage specified
    $percent_values = array();
    $percent = request('exam_percent');
         
    if (is_array($percent) && !empty($percent)) {
        foreach ($percent as $key => $value) {
            !empty($value) ? array_push($percent_values, $value) : '';
        }
    }
       
    if (count($percent_values) == 0) {
        $this->data['students'] = $this->combineBySubject($classesID, $exam_list, null, null, $schema);

        $this->data['combine_exams'] = $this->combinedOptimized($classesID, $combine_exams_array, NULL, $academic_id, null, null, null, $schema);

        foreach ($sections as $section) {
            $this->data['allsection'][$section->section] = $this->combineBySubject($classesID, $exam_list, $section->sectionID, null, $schema);
            $this->data['combine_section'][$section->section] = $this->combinedOptimized($classesID, $combine_exams_array, $section->sectionID, $academic_id, null, null, null, $schema);
        }


        $this->data['total_semesters'] = 1; //this will always be 1 if you don't specify any percent
    } else {
        
        //select list of exams
        $out = '';

        foreach ($combine_exams_array as $exam_id) {

            $data_subj[$exam_id] = $this->create_single_report($classesID, $exam_id, $section_id = null, $avg_format = NULL, $student_id = NULL, $show_gender_rank = null,$schema);
           
            $out .= ' "examID"=' . $exam_id . ' or ';
        }
    
        $this->data['student_average']=[];

        $where = rtrim($out, ' or ');
        $status = DB::SELECT('select count(distinct semester_id) AS total_semesters FROM '. $schema . '.exam WHERE ' . $where);
        $this->data['total_semesters'] = $status->total_semesters;

        $this->data['students'] = $this->combinedOptimized($classesID, $combine_exams_array, NULL, $academic_id, null, null, null, $schema);
            
        foreach ($this->data['subjects'] as $sub) {
            $subject_name = strtolower($sub->subject);
            foreach ($this->data['students'] as $student) {

                $this->data['student_average']["$subject_name"][$student->student_id] = 0;
            }
        }
        foreach ($this->data['subjects'] as $subject) {
            $subject_name = strtolower($subject->subject);
            foreach ($data_subj as $key => $values) {
                //$key==examID  
                $percentage = !empty($percent[$key]) ? $percent[$key] / 100 : 1;
                
                foreach ($values as $val) {
                    # code...                 
                    $arr = $val->student_id;
                    if (isset($val->{$subject_name})) {
                        if (isset($this->data['student_average']["$subject_name"]["$arr"])) {
                            $this->data['student_average']["$subject_name"]["$arr"] += $val->{$subject_name} * $percentage;
                        }
                    }
                }
            }
        } 
        
        if(isset($this->data['student_average'])){
            $this->getCombinedSubjectRankFromPercent($this->data['student_average']);
        }
        
        foreach ($sections as $section) {
            $this->data['allsection'][$section->section] = $this->combinedOptimized($classesID, $combine_exams_array, $section->sectionID, $academic_id, null, null, null, $schema);
        }
    }

    foreach ($sections as $section) {
        $this->data['section_subjects'][$section->section] = DB::SELECT('SELECT distinct subject_name as subject FROM '. $schema . '.mark_info WHERE student_status=1 and "sectionID"=' . $section->sectionID);
    }

    $this->data['total_students'] = DB::SELECT('SELECT * from '.$schema.'.student_archive a where academic_year_id='. $academic_id.' AND section_id IN (select "sectionID" from '. $schema.'.section where "classesID"='. $classesID.') AND status=1');

    $this->data['combined_exams_array'] = $combine_exams_array;
    $this->data['exams_combined'] = rtrim($exams_combined, '_');
    $this->data['academic_year_id'] = $academic_id;
    $this->data['sections'] = $sections;
    $this->data['classesID'] = $classesID;
    $this->data['show_grade'] = request('show_grade') == 'on' ? 1 : 0;
    $this->data['show_rank'] = request('show_rank') == 'on' ? 1 : 0;
    $this->data['show_footer'] = request('show_footer') == 'on' ? 1 : 0;
    $this->data['show_rank'] == 1 ? $this->getCombinedSubjectRank($exam_list, $classesID, $schema) : '';
    $this->data["singleclass"] = DB::table($schema.'.classes')->where('classesID', $classesID)->first();
    if(request('show_type') == 'on'){
        return view('exam.combined_subject_necta', $this->data);
    }else if(request('show_type_avarage') == 'on'){
        return view('exam.combined_subject_average', $this->data);
    }else{
        return view('exam.combined_subject', $this->data);
    }
    if ($get == 1) {

        return $this->data['students'];
    } else {
        return view('exam.combined_subject', $this->data);
    }
}

public function getCombinedSubjectRank($exam_list, $class_id, $schema=null) {
    $subjects = DB::select('select "student_id"::integer,"subjectID", round(avg(mark),1),rank()  over (PARTITION BY "subjectID" order by round(avg(mark),1) desc) as rank  from '. $schema . '.mark_info where "classesID"=' . $class_id . ' ' . $exam_list . ' group by "student_id","subjectID" ');
    $student_rank = [];
    foreach ($subjects as $subject) {
        $student_rank[$subject->student_id][$subject->subjectID] = [];
    }
    foreach ($subjects as $subject) {
        $student_rank[$subject->student_id][$subject->subjectID] = $subject->rank;
    }
    $this->data['subject_rank'] = $student_rank;
}

public function getCombinedSubjectRankFromPercent($subjects) {

    $mark_value = [];
    $subject_prank = [];
    $subject_pxrank = [];
    foreach ($subjects as $subject_name => $subjs) {

        $subject_prank[$subject_name] = [];
        $subject_pxrank[$subject_name] = [];
        $mark_value[$subject_name] = [];
        foreach ($subjs as $student_id => $val) {
            $subject_pxrank[$subject_name][$student_id] = 0;
            $mark_value[$subject_name][$student_id] = [];
        }
         
        foreach ($subjs as $student_id => $val) {
            array_push($mark_value[$subject_name][$student_id], $val);
        }

        $ordered_values = $mark_value[$subject_name];
        foreach ($ordered_values as $key => $value) {
            $subject_prank[$subject_name][$key] = $value[0];
        }
        rsort($ordered_values);
        foreach ($ordered_values as $key => $value) {

            foreach ($ordered_values as $ordered_key => $ordered_value) {
                if (isset($value[0]) && $value[0] === $ordered_value[0]) {
                    $key = $ordered_key;
                    break;
                }
            }
            $rank = ((int) $key + 1);
            $keys = array_keys($subject_prank[$subject_name], $value[0]);
            foreach ($keys as $pkey) {
                $subject_pxrank[$subject_name][$pkey] = $rank;
            }
        }
    }
    
    $this->data['subject_prank'] = $subject_pxrank;
}




public function combinesql($classesID, $exams, $sectionID = NULL, $academicyear_id = NULL, $subjectID = NULL, $avg_format = NULL, $student_id = NULL, $schema=null) {
    $initial_letter = 97;
    $get_current_year = \collect(DB::select('select id FROM '.  $schema_name .'academic_year WHERE class_level_id='.$classesID.' ORDER BY  end_date DESC LIMIT 1'))->first();
    $schema_name = $schema.'.';
    $academic_year_id = $academicyear_id == NULL ? $get_current_year->id : $academicyear_id;
    $string = '';
    $join_query = '';
    $adding_sum = '';
    $this->exam_avg_format = get_avg_format($schema);
    $col = '';
    $total_exams = array();
    $i = 0;

    //remove _ in all exams
    $percent = $this->get_post_custom('exam_percent');

    //from array post keys
    $exam_name = $this->get_post_custom('exam_name');
    rsort($exams);

    foreach ($exams as $key) {
        $exam_name = \App\Model\Exam::find($key);
        $tag_names = !empty($exam_name) ? $exam_name->exam . '' . $key : 'randomchar';
        $value = preg_replace('/[^a-z0-9]/', '', strtolower($tag_names));

        if ((int) $key > 0) {

            //check if percentage value is set

            if ((is_array($percent) && isset($percent[$key]) && !empty($percent[$key])) || (isset($percent->{$key}) && !empty($percent->{$key}))) {

                $percentage = is_array($percent) ? $percent[$key] / 100 : $percent->{$key} / 100;
                //array_push($total_exams, $value.'_percent');
            } else {
                $percentage = 1;
            }
            array_push($total_exams, $value);
            $string = "$string" . chr($initial_letter) . '."' . $value . '", ';

            if ($sectionID == NULL) {
                $section = ' and a."student_id" in (select student_id from '. $schema . '.student_archive where status=1 and section_id IN (SELECT "sectionID" FROM '. $schema . '.section where "classesID"=' . $classesID . '))';
            } else {
                /**
                 * this is the logic
                 * If this student view results in the same academic year, then we should fetch section
                 * from the existing year.
                 * If we fetch results from the previous year, then we need to search section from previous
                 * year in student_archive so we should get the correct result
                 */
                $section = '  and a."sectionID"=' . $sectionID . ' ';
            }

            if ($subjectID == 0 || $subjectID == NULL) {
                $average = 'm.average';
                $subject = ' IN (SELECT subject_id FROM '. $schema . '.subject_count WHERE is_counted=1)';

                $avg_table = ($this->exam_avg_format == NULL || $this->exam_avg_format == 0) ? 'sum_exam_average_done' : 'sum_exam_average';

                $join_avg = 'join ' . $schema_name . $avg_table . ' m ON ( m."student_id"=a."student_id" AND m.academic_year_id=a.academic_year_id ) ';
                $avr_exam = ' and m."examID" = ' . $key . '';
            } else {
                $average = 'a.mark';
                $subject = '=' . $subjectID;
                $join_avg = '';
                $avr_exam = '';
            }


            $join_query .= '(select a."student_id", x.section, s.name, s.sex, s.roll, s.photo, ' . $average . '*' . $percentage . ' as "' . $value . '",  a."examID" from '. $schema . '.mark_info a join '. $schema . '.exam b on a."examID" = b."examID" ' . $join_avg . ' JOIN '. $schema . '.student  s ON s."student_id"=a."student_id" join '. $schema . '.student_archive q on (q.student_id=s.student_id and q.academic_year_id=' . $academic_year_id . ' ) join '. $schema . '.section x on x."sectionID"=q."section_id"  where s.status=1 and q.status=1 and a."examID" = ' . $key . '  ' . $avr_exam . ' AND a."subjectID"' . $subject . ' and a.mark is not NULL and a."classesID"=' . $classesID . ' and a."academic_year_id"=' . $academic_year_id . '  ' . $section . '  group by a."examID", x.section,s.sex, a."student_id",' . $average . ',s.name, s.roll,s.photo) as ' . chr($initial_letter);


            $join_query .= $initial_letter == 97 ? '' : ' on a."student_id" = ' . chr($initial_letter) . '."student_id"';
            $initial_letter++;
            $join_query .= ' left join ';

            $adding_sum .= 'Coalesce("' . $value . '",0) + ';
            $col .= "\"" . $value . "\"::int || ',' || ";
            $i++;
        }
    }
    //echo $col; 
    $query_string = rtrim($string, ', ');
    $adding_sum_sql = rtrim($adding_sum, ' + ');

    $sql = 'with tempa as (
            select a."student_id",a.roll,a.section, a.name, a.sex,  a.photo, ' . $query_string . ' from

            ' . rtrim($join_query, ' left join ') . '),
        tempb as (
            select *, ' . $adding_sum_sql . ' as total, '
            . ' round(((' . $adding_sum_sql . ')::numeric / (' . count($total_exams) . ')::numeric), 1)  as average from tempa
        )
            select *, rank() over (order by average desc) as rank from tempb';
    $sql_string = $student_id == NULL ? $sql : ' select * from (' . $sql . ') f WHERE f."student_id"=' . $student_id;

    return \collect(DB::select($sql_string))->toArray();
}

public function combinedOptimized($classesID, $exams, $sectionID = NULL, $academicyear_id = NULL, $subjectID = NULL, $avg_format = NULL, $student_id = NULL,$schema=null) {
    $schema_name= $schema.'.';
    $get_current_year = \collect(DB::select('select id FROM '.  $schema_name .'academic_year WHERE class_level_id='.$classesID.' ORDER BY  end_date DESC LIMIT 1'))->first();

    $initial_letter = 97;
    $academic_year_id = $academicyear_id == NULL ? $get_current_year->id : $academicyear_id;
    $string = '';
    $case = ' CASE ';
    $join_query = '';
    $adding_sum = '';
    $this->exam_avg_format = get_avg_format($schema);
    $col = '';
    $total_exams = array();
    $i = 0;

    //remove _ in all exams

    $percent = $this->get_post_custom('exam_percent');
    if (!is_object($percent) && is_string($percent)) {
        $percent = json_decode($percent);
    }
    //from array post keys
    $exam_name = $this->get_post_custom('exam_name');

    if ($subjectID == null || $subjectID == 0) {
        $avg_table = ($this->exam_avg_format == NULL || $this->exam_avg_format == 0) ? 'sum_exam_average_done' : 'sum_exam_average';
    } else {
        $avg_table = 'mark_info';
    }

    $exam_list = '';
    if ($sectionID == NULL || $sectionID == 0) {
        $section = ' ';
    } else {
        /**
         * this is the logic
         * If this student view results in the same academic year, then we should fetch section
         * from the existing year.
         * If we fetch results from the previous year, then we need to search section from previous
         * year in student_archive so we should get the correct result
         */
        $section = ' and "student_id" in (select student_id from ' . $schema . '.student_archive where section_id=' . $sectionID . ' and academic_year_id=' . $academic_year_id . ' )';
    }
    sort($exams);
    $and_exam = ' and "examID" IN (' . implode(',', $exams) . ')';

    foreach ($exams as $key) {

        $exam_name = DB::table($schema_name.'exam')->where('examID',$key)->first();
        $tag_names = !empty($exam_name) ? $exam_name->exam . '' . $key : 'randomchar';

        //check if no exam mark for this specified section
        if ((int) $sectionID > 0) {
            $sql_section = 'SELECT distinct "examID" FROM ' . $schema_name . $avg_table . ' where "examID"=' . $key . ' AND "classesID"=' . $classesID . $section . ' AND academic_year_id=' . $academic_year_id . ' order by 1';
            $check_exam = DB::select($sql_section);
            if (count($check_exam) == 0) {
                $section_name =  DB::table($schema_name.'section')->where('sectionID',$sectionID)->first();
                die('Exam name <b>' . $exam_name->exam . ' </b> have no marks  entered in  ' . $section_name->section . ' Stream');
            }
        }

        $value = preg_replace('/[^a-z0-9]/', '', strtolower($tag_names));

        if ((int) $key > 0) {

            //check if percentage value is set

            if ((is_array($percent) && isset($percent[$key]) && !empty($percent[$key])) || (isset($percent->{$key}) && !empty($percent->{$key}))) {

                $percentage = is_array($percent) ? $percent[$key] / 100 : $percent->{$key} / 100;

                $case .= '  WHEN ("examID"=' . $key . ') THEN round(average*' . $percentage . ',1)';
            } else {
                $percentage = 1;
                $case = '';
            }
            array_push($total_exams, $value);
            $string = "$string" . chr($initial_letter) . '."' . $value . '", ';

            $exam_list .= '"' . $value . '" NUMERIC,';
            $adding_sum .= 'Coalesce("' . $value . '",0) + ';
            $col .= "\"" . $value . "\"::int || ',' || ";

            $i++;
        }
    }
    $or_case = strlen($case) > 10 ? rtrim($case, ', ') : '';
    if ($subjectID == null || $subjectID == 0) {
        $final_case = strlen($case) > 10 ? $or_case . ' ELSE round(average,1) END AS average' : 'average';
    } else {
        $final_case = 'mark';
    }

    $subject_info = $subjectID == null || $subjectID == 0 ? '' : ' AND "subjectID"=' . $subjectID;
    $adding_sum_sql = rtrim($adding_sum, ' + ');
    $exam_list_sql = rtrim($exam_list, ', ');

    //coalase sum of examID
    //average by rounding
    //list of exams done

    $sql = 'with tempa as ( select  x.section, b.photo, b.sex,b.name,b.roll,a.*, ' . $adding_sum_sql . ' as total,round(((' . $adding_sum_sql . ')::numeric / (' . count($exams) . ')::numeric), 1) as average FROM ( SELECT * FROM public.crosstab( \'select "student_id"::integer, "examID", ' . $final_case . ' from ' . $schema_name . $avg_table . ' where "classesID"=' . $classesID . $section . $and_exam . ' AND academic_year_id=' . $academic_year_id . $subject_info . ' order by 1,2 \',\' SELECT distinct "examID" FROM ' . $schema_name . $avg_table . ' where "classesID"=' . $classesID . $section . $and_exam . ' AND academic_year_id=' . $academic_year_id . $subject_info . ' order by 1\') AS final_result("student_id" integer, ' . $exam_list_sql . ')) as a JOIN ' . $schema_name . 'student b ON a."student_id"=b."student_id" join ' . $schema . '.student_archive s on (s.student_id=b.student_id and s.academic_year_id=' . $academic_year_id . ') join ' . $schema_name . 'section x on x."sectionID"=s."section_id" where b.status=1), tempb as (select * from tempa ) select *, rank() over (order by average desc) as rank from tempb  order by total desc';

    $sql_string = $student_id == NULL ? $sql : ' SELECT * from (' . $sql . ') f WHERE f."student_id"=' . $student_id .' order by total desc';
    return \collect(DB::select($sql_string))->toArray();
}

public function createSubjectAverage($where, $class_id, $subjectID = null, $schema=null) {
    $where_subject = (int) $subjectID > 0 ? ' AND a."subjectID"=' . $subjectID : '';
    $subject_avg = DB::select('SELECT round(avg(a.mark),1) as average,a."subjectID",a."examID",a.subject_name FROM '. $schema . '.mark_info a WHERE (' . $where . ') AND a."classesID"=' . $class_id . $where_subject . ' and "subjectID" IN (SELECT "subjectID" FROM '. $schema . '.subject where "classesID"=' . $class_id . ')  and a.student_status=1 group by a."subjectID",a."examID",a.subject_name');

    $this->data['subjects_info'] = DB::select('SELECT distinct a."subjectID",a.subject_name FROM '. $schema . '.mark_info a WHERE (' . $where . ') AND a."classesID"=' . $class_id . $where_subject . ' and "subjectID" IN (SELECT "subjectID" FROM '. $schema . '.subject where "classesID"=' . $class_id . ') and a.student_status=1 group by a."subjectID",a."examID",a.subject_name');

    $pass_mark = (int) DB::table($schema.'.setting')->first()->pass_mark;
    $this->data['passed_students'] = \collect(DB::select('select count(*) from (
SELECT distinct a."student_id" FROM '. $schema . '.mark_info a WHERE (' . $where . ') AND a.student_status=1 and a."classesID"=' . $class_id . $where_subject . ' and a."subjectID" IN (SELECT "subjectID" FROM '. $schema . '.subject where a."classesID"=' . $class_id . ')  group by a."student_id" having round(avg(a.mark),1)>' . $pass_mark . '  ) b'))->first();
    $exam_subject = array();
    foreach ($subject_avg as $subject) {
        $exam_subject[$subject->examID][$subject->subjectID] = array();
    }
    foreach ($subject_avg as $subject) {
        array_push($exam_subject[$subject->examID][$subject->subjectID], $subject);
    }
    return $exam_subject;
}


function combineBySubject($class_id, $exams_with_or, $section_id = NULL, $student_id = NULL,$schema=null) {
    $schema_name = $schema.'.';
   $section = $section_id == NULL ? '' : ' AND "sectionID"=' . $section_id;
   //get exams to be used in this loop

   $subject_sql = 'SELECT distinct subject_name FROM ' . $schema_name. 'mark_info where "classesID"=' . $class_id . ' ' . $exams_with_or . ' ' . $section . ' order by 1';
   $subject_status = 'SELECT distinct subject_name, is_counted FROM ' . $schema_name . 'mark_info where "classesID"=' . $class_id . ' ' . $exams_with_or . ' ' . $section . ' order by 1';
   $result = DB::select($subject_status);
   if (!empty($result)) {
       $list = '';
       foreach ($result as $subject) {
           $list .= ' "' . strtolower($subject->subject_name) . '" NUMERIC, ';
       }
       $subject_list = rtrim($list, ', ') == '' ? 'null' : rtrim($list, ', ');
   } else {
       $subject_list = ' "subject" NUMERIC';
   }
   $student_section = $section_id == null ? '' : ' AND b."sectionID"=' . $section_id;
   $sql = 'with tempa as ( 
   select b.name,b.roll, b.sex, a.* ,
       (SELECT  round(avg(mark),1) from ' . $schema_name . 'mark_info WHERE "student_id"=b."student_id"  ' . $exams_with_or . ' ) AS average FROM 
           (SELECT * FROM public.crosstab(\'select "student_id"::integer, subject_name, round(avg(mark),1) from ' . $schema_name . 'mark_info where "classesID"=' . $class_id . ' ' . $exams_with_or . ' ' . $section . ' group by "student_id",subject_name order by 1,2 \',\' ' . $subject_sql . '\') AS final_result("student_id" integer,' . $subject_list . ') ) as a 
           JOIN ' . $schema_name . 'student b ON a."student_id"=b."student_id" WHERE b.status in (1,2)),
    tempb as (select * from tempa ) select *,rank() over (order by average desc) as rank from tempb';
   $sql_string = $student_id == NULL ? $sql : ' select * from (' . $sql . ') f WHERE f."student_id"=' . $student_id;

   return \collect(DB::select($sql_string))->toArray();
}


public function getClassExam() {

    $classId = request('id');
    $schema = request('schema_name');
    $academic_year_id = request('year_id');
    $done = request('done');
    if ((int) $classId) {

        $sql = $done == 1 ? 'select b.exam, b."examID", b.semester_id, c.name as semester_name from ' . $schema . '.class_exam a JOIN ' . $schema . '.exam b ON b."examID"=a.exam_id JOIN ' . $schema . '.semester c ON c.id=b.semester_id where a.academic_year_id=' . $academic_year_id . ' and a.class_id=' . $classId . ' AND b."examID" IN (SELECT DISTINCT "examID" FROM ' . $schema . '.mark WHERE "classesID"=' . $classId . ' and academic_year_id=' . $academic_year_id . ' and mark is not null) group by b.date,b.exam, b."examID",c.name, b.semester_id order by b.semester_id asc' :
                'select b.exam, b."examID" from ' . $schema . '.class_exam a JOIN ' . $schema . '.exam b ON b."examID"=a.exam_id where a.academic_year_id=' . $academic_year_id . ' and a.class_id=' . $classId;

        $exams = DB::select($sql);
        echo "<div class='form-group' >";
        echo "<label for=\"all\"  class=\"col-sm-2 control-label\">
             Select All Exams
              </label>";
        echo " <input type=\"checkbox\" id='select_all' name=\"all\">";
        echo "</div>";
        if (!empty($exams)) {
            foreach ($exams as $value) {

                echo "$value->semester_name";
                echo "<div class='form-group' >";
                echo "<label for=\"$value->exam\" class=\"col-sm-2 control-label\">
             $value->exam
              </label>";
                echo "<div class='col-sm-4'>";
                echo "<div class='input-group'>";
                echo "  <input type='number' max='100' min='0' name='exam_percent[$value->examID]' id='percentage' class=\"form-control percent_input\">";
                echo "  <input type='hidden' name='exam_name[$value->examID]' value='$value->exam'>";
                echo "<span class=\"input-group-btn\">";
                echo "<button type=\"button\" class=\"btn btn-primary\">%</button>";

                echo "</span>";
                echo " <input type=\"checkbox\" class='check form-control' name='exam_id[]' value=\"$value->examID\">";
                echo "</div>";
                echo "</div>";
                echo "</div>";
                echo "<script>
                    //select all checkboxes
                    $(\"#select_all\").change(function(){  //\"select all\" change
                    $(\".check\").prop('checked', $(this).prop(\"checked\")); //change all \".check\" checked status
                    });

                    //\".check\" change
                    $('.check').change(function(){
                    //uncheck \"select all\", if one of the listed checkbox item is unchecked
                    if(false == $(this).prop(\"checked\")){ //if this item is unchecked
                        $(\"#select_all\").prop('checked', false); //change \"select all\" checked status to false
                    }
                    //check \"select all\" if all checkbox items are checked
                    if ($('.check:checked').length == $('.check').length ){
                        $(\"#select_all\").prop('checked', true);
                    }
                    });

                        </script>";
            }
        } else {
            echo '<p class="alert alert-warning" align="center">Sorry, There is No Exams with marks Defined </p>';
        }
    }
}

    /**
     * @accessed -- add student view
     */
    function sectioncall() {
        $classesID = request('id');
        $schema = request('schema_name');
        if ((int) $classesID > 0) {
            $allsection = DB::table($schema.'.section')->where('classesID', $classesID)->get();
            if (!empty($allsection)) {
                echo "<option value='0'>", "student_select_section", "</option>";
                echo request('all_section') == 1 ? "<option value='all'>All</option>" : '';
                foreach ($allsection as $value) {
                    echo "<option value=\"$value->sectionID\">", $value->section, "</option>";
                }
            } else {
                echo "0";
            }
        }
    }

    
    function get_post_custom($value) {
        $val = request($value);
        if (is_array($val)) {
            return $val;
        } else if (is_string($val)) {
            return json_decode($val);
        }
    }

    public function updateReportSetting($type, $schema=null) {
        $settings = DB::table($schema.'.exam_report_setting')->where('id', $type)->first();
        $vars = get_object_vars(\DB::table($schema.'.exam_report_settings')->where('id', $type)->first());
        $obj = array();
        foreach ($vars as $key => $variable) {
            if (!in_array($key, array('exam_type', 'id'))) {
                $obj = array_merge($obj, array($key => request($key) == null ? 0 : request($key)));
            }
        }
        $settings->update($obj);
    }


    function getClassByYear($schema, $student_id, $academic_year_id) {
        return $student_archive = DB::table($schema.'.student_archive')->where('student_id', $student_id)->where('academic_year_id', $academic_year_id)->where('status', 1)->first();
    }

    function get_result_format($schema, $student_id) {
        return \collect(\DB::select('select a.result_format, a.stamp, b."classesID" from ' . $schema . '.student b JOIN ' . $schema . '.classes c ON c."classesID"=b."classesID" JOIN ' . $schema . '.classlevel a ON a.classlevel_id=c.classlevel_id WHERE b."student_id"=' . $student_id))->first();
    }
    

      public  function get_teacher_signature($schema, $sectionID){
            $sql='select a.signature, a.name,a.phone FROM ' . $schema . '.teacher a JOIN  ' . $schema . '.section b ON b."teacherID"=a."teacherID" where b."sectionID"='.$sectionID;
            return \collect(\DB::select($sql))->first();
        }

        public function get_rank_per_all_subjects($examID, $classesID, $student_id, $sectionID = null, $schema=null) {
            return $this->mark_m->get_subject_rank($examID, $classesID, $student_id, $sectionID, $schema);
        }

        public function getDivision($student_id, $examID, $classesID, $schema=null) {
        $level = $this->get_result_format($schema, $student_id);
        if ($level->result_format == 'ACSEE') {
            $divisionpoints = $this->getAcseeDivision($student_id, $examID, $classesID, $schema);
            $division = count($divisionpoints) == 3 ? 'INC' : acseeDivision($divisionpoints[0], $divisionpoints[1]);
            $points = $divisionpoints[0];
        } else if ($level->result_format == 'CSEE') {
            $divisionpoints = $this->getCseeDivision($student_id, $examID, $classesID, $schema);
            $division = cseeDivision($divisionpoints[0], $divisionpoints[1]);
            $points = $divisionpoints[0];
        } else {
            $division = 'NULL';
            $points = 'NULL';
        }
        return array($division, $points);
    }


    public function getMarksForStudents($exam_list, $class_id, $academic_year_id, $schema=null) {
        $mark_list = DB::select('select mark as total_mark,"examID","student_id","subjectID" from '.$schema.'.mark where  ' . $exam_list . ' AND "classesID"=' . $class_id . '  AND academic_year_id=' . $academic_year_id);
        $student_marks = array();

        foreach ($mark_list as $key => $value) {
            $student_marks[$value->student_id][$value->examID][$value->subjectID] = array();
        }
        foreach ($mark_list as $key => $value) {

            array_push($student_marks[$value->student_id][$value->examID][$value->subjectID], (array) $value);
        }
        return $student_marks;
    }

    public function getExamReportTitle($classesID, $academic_year_id, $schema=null) {
        $exams_combined = explode('_', request('exam'));
        $report_generated = 0;
        $exam_report = array();
        $reports = DB::select('SELECT * from ' . $schema . '.exam_report WHERE  classes_id=' . $classesID . ' and academic_year_id=' . $academic_year_id);

        foreach ($reports as $value) {
            $arr_exam = explode('_', $value->combined_exams);
            $status = identical_values($arr_exam, $exams_combined);
            if ($status == TRUE) {
                $report_generated = 1;
                $exam_report = $value;
            }
        }
        return $exam_report;
    }

    public function getStudentSubjects($academic_year_id, $class_id, $order_by_arrangement = null, $schema=null) {
        $order = $order_by_arrangement == null ? 'a.subject' : 'a.arrangement';
        $section_teacher_sql = 'select a.subject_id,a.student_id, a.is_counted_indivision,a.is_counted,a.is_penalty,a.pass_mark, case when a.teacher_id is null then (select name from ' .  $schema . '.teacher where "teacherID" IN (SELECT "teacherID" FROM ' .  $schema . '.section where "sectionID"=a.section_id LIMIT 1) limit 1) else (select name from ' .  $schema . '.teacher where "teacherID"=a.teacher_id) end as teacher,  (select signature from ' .  $schema . '.teacher where "teacherID"=a.teacher_id),a.subject from ' .  $schema . '.subject_count a  where a.class_id=' . $class_id . ' and a.academic_year_id=' . $academic_year_id . ' order by ' . $order;
        $subjects = DB::select($section_teacher_sql);
        $student_subjects = array();

        foreach ($subjects as $key => $value) {
            $student_subjects[$value->student_id] = array();
        }
        foreach ($subjects as $key => $value) {

            array_push($student_subjects[$value->student_id], (array) $value);
        }
        return $student_subjects;
    }

    public function getPosition($examID, $classesID, $sectionID = null, $schema=null) {

        $add_section = $sectionID == NULL ? '' : ' and "student_id" in (select student_id from ' .  $schema . '.student_archive where section_id = ' . $sectionID . ' )';
        $sql = 'select subject,"student_id","subjectID","examID", mark, rank FROM ( select subject, a."subjectID", a."examID", a."classesID", a."student_id", a.mark, rank() over (partition by a."subjectID" order by a.mark desc) from ' .  $schema . '.mark a where a."examID" = ' . $examID . ' and a."classesID" = ' . $classesID . ' ' . $add_section . ' and a.mark is not null and a."student_id" IN (SELECT "student_id" FROM ' .  $schema . '.student where status=1)) a  ';
        $mark_list = DB::select($sql);

        $student_marks = array();

        foreach ($mark_list as $key => $value) {
            $student_marks[$value->student_id][$value->examID][$value->subjectID] = array();
        }
        foreach ($mark_list as $key => $value) {

            array_push($student_marks[$value->student_id][$value->examID][$value->subjectID], (array) $value);
        }
        return $student_marks;
    }

    public function getCharactersAssessed($exam_id, $class_id, $schema)
    {
        $charactes = DB::select('select * from ' .  $schema . '.general_character_assessment where student_id in (select student_id from ' .  $schema . '.student_archive where section_id in (select "sectionID" from ' .  $schema . '.section where "classesID"=' . $class_id . ')) and  exam_id=' . $exam_id . '');
        $list = array();

        if (!empty($charactes)) {
            foreach ($charactes as $value) {
                $list[$value->student_id] = array();
            }
            foreach ($charactes as $value) {
                array_push($list[$value->student_id], $value);
            }
        }
        return $list;
    }

    function getStudentRank($examID, $classesID, $sectionID = null, $schema=null) {
        $this->data['siteinfos'] = DB::table($schema.'.setting')->first();
        $siteinfos = $this->data['siteinfos'];
        $this_schema = $schema.'.';
        $avg_table_view = $siteinfos->exam_avg_format == 0 ? 'sum_exam_average_done' : 'sum_exam_average';

        $section_where = $sectionID == NULL ? '' : ' AND "student_id" in (select student_id from ' . $this_schema . 'student_archive where section_id = ' . $sectionID . ')  ';

        $sql = 'SELECT sum,rank,average,"student_id" FROM (
    select *, rank() over (order by average desc) from ' . $this_schema . $avg_table_view . ' where "examID" = ' . $examID . ' and "classesID" = ' . $classesID . ' ' . $section_where . ') a ';
        $ranks = DB::select($sql);
        $student_ranks = array();

        foreach ($ranks as $key => $value) {
            $student_ranks[$value->student_id] = array();
        }
        foreach ($ranks as $key => $value) {
            array_push($student_ranks[$value->student_id], (array) $value);
        }
        return $student_ranks;
    }


}

