<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Model\UserDeduction;
use DB;

class Deduction extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
        parent::__construct();
    }

    public function index() {
            if($_POST){
                $this->data['schema_name'] = $schema_name = clean_htmlentities(request('schema'));
                $this->data['type'] = $id = clean_htmlentities((request('deduction_category_id')));
                if ((int) $id > 0) {
                    $this->data['deductions'] = \DB::table($schema_name.'.deductions as a')->where('a.category', $id)->get();
                    return view('account.payroll.deduction_custom',$this->data);
                } else {
                    $this->data['deductions'] = [];
                    return view('account.payroll.deduction_custom',$this->data);
                }
            }else{
                return view('account.payroll.deduction_index');
            }
    }

    public function monthly() {
        if (can_access('manage_payroll')) {
            $this->data['type'] = 0;
            $this->data['deductions'] = \App\Model\Deduction::where('category', '<>', 1)->get();
            $this->data['subview'] = 'deduction/index';
            $this->load->view('_layout_main', $this->data);
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function add() {
        if (can_access('add_deduction')) {
            $this->data['type'] = clean_htmlentities(($this->uri->segment(3)));
            if ($_POST) {
                $this->validate(request(), [
                    'name' => 'required|max:255',
                    "is_percentage" => "required",
                    "description" => "required"
                        ], $this->custom_validation_message);
                $deduction = \App\Model\Deduction::create(request()->except('_token'));
                if ((int) $deduction->employer_percent > 0 || (int) $deduction->employer_amount > 0) {
                    $code = strtoupper(substr(set_schema_name(), 0, 2));
                    \App\Model\ReferExpense::create(['name' => $deduction->name, 'financial_category_id' => 2, 'note' => 'Deductions', 'code' => 3232, 'code' => $code . '-OPEX-' . rand(1900, 582222),
                        'predefined' => 1]);
                }
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                return redirect(base_url("deduction/index/" . $deduction->category));
            } else {
                $this->data["subview"] = "deduction/add";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function edit() {
        if (can_access('manage_payroll')) {
            $id = clean_htmlentities(($this->uri->segment(3)));
            if ((int) $id) {
                $this->data['deduction'] = \App\Model\Deduction::find($id);
                if ($this->data['deduction']) {
                    if ($_POST) {
                        $this->validate(request(), [
                            'name' => 'required|max:255',
                            "is_percentage" => "required",
                            "description" => "required"
                                ], $this->custom_validation_message);
                        $this->data['deduction']->update(request()->except('_token'));
                        if ((int) $this->data['deduction']->employer_percent > 0 || (int) $this->data['deduction']->employer_amount > 0) {
                            $scheck = \App\Model\ReferExpense::where('name', $this->data['deduction']->name)->first();
                            if (empty($scheck)) {
                                $code = strtoupper(substr(set_schema_name(), 0, 2));
                                \App\Model\ReferExpense::create(['name' => $this->data['deduction']->name, 'financial_category_id' => 2, 'note' => 'Deductions', 'code' => 3232, 'code' => $code . '-OPEX-' . rand(1900, 582222),
                                    'predefined' => 1]);
                            }
                        }
                        $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                        return redirect(base_url("deduction/index/" . $this->data['deduction']->category));
                    } else {
                        $this->data["subview"] = "deduction/edit";
                        $this->load->view('_layout_main', $this->data);
                    }
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function delete() {
        $setting = \App\Model\Setting::first();
        if (can_access('manage_payroll')) {
            $id = clean_htmlentities(($this->uri->segment(3)));
            if ((int) $id) {
                $user_deductions = UserDeduction::where('deduction_id', $id)->first();
                $salary_deductions = \App\Model\SalaryDeduction::where('deduction_id', $id)->first();
                if (!empty($user_deductions) || !empty($salary_deductions)) {
                    //you cannot delete this
                    $this->session->set_flashdata('error', 'You cannot delete this deduction because some users are already allocated on this deduction');
                } else {
                    \App\Model\Deduction::destroy($id);
                    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                }
                return redirect()->back();
            } else {
                return redirect()->back();
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function subscribe() {
        $id = clean_htmlentities((request()->segment(3)));
        $this->data['schema_name'] = $schema_name = request()->segment(4); 
        if ((int) $id) {
            $this->data['set'] = $id;
            $this->data['type'] = 'deduction';
            $this->data['allowance'] = \DB::table($schema_name.'.deductions as a')->where('a.id',$id)->first();
            $subscriptions = \DB::table($schema_name.'.user_deductions as b')->where('b.deduction_id', $id)->get();
            $data = [];
            foreach ($subscriptions as $value) {
                $data = array_merge($data, array($value->user_id . $value->table));
            }
            $this->data['users'] = (new \App\Http\Controllers\Payroll())->getUsers($schema_name);
            $this->data['subscriptions'] = $data;
            return view('account.payroll.deduction_subscription',$this->data);
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function monthlysubscribe() {
        $id = clean_htmlentities((request()->segment(3)));
        if ((int) $id) {
            $this->data['set'] = $id;
            $this->data['type'] = 'deduction';
            $this->data['schema_name'] = $schema_name = request()->segment(4);
            $this->data['allowance'] = \DB::table($schema_name.'.deductions as a')->where('a.id',$id)->first();
            $subscriptions = \DB::table($schema_name.'.user_deductions as a')->where('a.deduction_id', $id)->get();
            $data = [];
            foreach ($subscriptions as $value) {
                $data = array_merge($data, array($value->user_id . $value->table));
            }
            $this->data['users'] = (new \App\Http\Controllers\Payroll())->getUsers($schema_name);
            $this->data['subscriptions'] = $data;
            return view('account.payroll.deduction_subscription',$this->data);
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    function monthlyAddSubscriber() {
        $deduction = UserDeduction::where('user_id', request('user_id'))->where('table', request('table'))->where('type', 0)->where('deduction_id', request('deduction_id'));
        $obj=array_merge(request()->except(['_token', 'deadline']), ['deadline'=> date('Y-m-d', strtotime(request('deadline')))]);
       // dd($obj);
        if (!empty($deduction->first())) {
           
            $deduction->update($obj);
        } else {
            UserDeduction::create($obj);
        }
        echo 'success';
    }

    function excel() {
        $this->data['users'] = \App\Model\Teacher::all();

        $this->data["subview"] = "deduction/excel";
        $this->load->view('_layout_main', $this->data);
    }

    public function uploadFileByExcel() {
        ini_set('max_execution_time', 300); //overwrite execution time, 5min
        $data = $this->uploadExcel();
        $status = $this->excelCheckKeysExists($data, array('phone', 'amount', 'deadline', 'deduction_name'));
        if ((int) $status == 1 && count($data) > 0) {
            $status = '';
            foreach ($data as $val) {
                $value = array_change_key_case($val, CASE_LOWER);

                $deduction_name = isset($value['deduction_name']) ? $value['deduction_name'] : 'null';
                $deduction = \App\Model\Deduction::where(DB::raw('lower(name)'), strtolower($deduction_name))->first();

                $phone = isset($value['phone']) ? $value['phone'] : 'null';
                $valid = validate_phone_number($phone);

                $valid_phone = is_array($valid) ? $valid[1] : 'null';
                $user = \App\Model\User::where('phone', $valid_phone)->first();
                if (empty($user)) {
                    $status .= '<p class="alert alert-danger">User with this number ' . $value['phone'] . ' does not exists</p>';
                } else if (empty($deduction)) {
                    $status .= '<p class="alert alert-danger">This deduction name ' . $value['dediction_name'] . ' does not exists. Please define it first or write it correctly</p>';
                } else {
                    $user_deduction = UserDeduction::where('user_id', $user->id)->where('table', $user->table)->where('type', 0)->where('deadline', '>', date('Y-m-d', strtotime($value['deadline'])));

                    $obj = ['user_id' => $user->id, 'table' => $user->table, 'deduction_id' => $deduction->id, 'deadline' => date('Y-m-d', strtotime($value['deadline'])), 'type' => 0, 'amount' => $value['amount']];

                    if (!empty($user_deduction->first())) {
                        $user_deduction->update($obj);
                    } else {
                        UserDeduction::create($obj);
                    }
                    $status .= '<p class="alert alert-success">Deduction for user "' . $user->name . '" '
                            . '  added successfully</p>';
                }
            }
        }
        $this->data['status'] = $status;
        $this->data["subview"] = "mark/upload_status";
        $this->load->view('_layout_main', $this->data);
    }

}
