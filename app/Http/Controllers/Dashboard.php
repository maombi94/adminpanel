<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use DB;

class Dashboard extends Admin_Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $this->data['users'] = $this->load_users(Auth::User()->id);
        $this->data['list_users'] = DB::table('portal_users')->get();
        return view('dashboard.index', $this->data);
    }

    public function accounts(){
        $this->data['set_schema'] = $set_schema = request()->segment(3);
        $year = (int)request()->segment(4) > 0 ? request()->segment(4) : date('Y');
        $this->data['year'] = $year;

        $where =  (int)$year > 0 ? " WHERE c.date>'".date($year.'-01-01')."' " : " WHERE c.date>'".date('Y-01-01')."' ";
        $this->data['total_transactions'] = \DB::select("SELECT date_trunc('month', created_at) AS month, sum(amount) as amount FROM  " . $set_schema . ".total_revenues a WHERE  extract(year from a.created_at)=".$year."  GROUP BY month order by month");

        $this->data['revenue'] =  DB::select("with tempa as (select a.date,a.revenue, b.expense from ( select  sum(amount) as revenue,date_trunc('month', date) as date from " . $set_schema . ".total_revenues group by date_trunc('month', date) order by date_trunc('month', date) desc
        ) as a left join ( select  sum(amount::numeric) as expense,date_trunc('month', create_date) as create_date from " . $set_schema . ".expense group by date_trunc('month', create_date) order by date_trunc('month', create_date) asc ) as b on date_trunc('month', b.create_date)= date_trunc('month', a.date) ), tempb as ( select * from tempa ) select * from tempb order by date desc limit 12");
    
        
        $sql = 'SELECT coalesce(coalesce(sum(a.total_amount),0)-sum(a.discount_amount),0) as amount, coalesce(coalesce(sum(a.total_payment_invoice_fee_amount),0)+ coalesce(sum(a.total_advance_invoice_fee_amount)),0) as paid_amount, sum(a.balance) from ' . $set_schema . '.invoice_balances a join ' . $set_schema . '.student b on a.student_id=b.student_id join ' . $set_schema . '.invoices c on c.id=a.invoice_id' . $where;
        $this->data['collections'] =  \collect(DB::select($sql))->first();

        $this->data['installments'] =  DB::select("select  coalesce(coalesce(sum(a.total_amount),0)-sum(a.discount_amount),0) as amount, coalesce(coalesce(sum(a.total_payment_invoice_fee_amount),0)+ coalesce(sum(a.total_advance_invoice_fee_amount)),0) as paid_amount, sum(a.balance), i.name,l.name as level
            from " . $set_schema . ".invoice_balances a join " . $set_schema . ".invoices c on c.id=a.invoice_id join " . $set_schema . ".installments i on i.id=a.installment_id join " . $set_schema . ".academic_year b on c.academic_year_id=b.id  JOIN  " . $set_schema . ".classlevel l on l.classlevel_id=b.class_level_id ".$where." group by i.name,l.name");

            $this->data['fees'] =  DB::select("select  coalesce(coalesce(sum(a.total_amount),0)-sum(a.discount_amount),0) as amount, coalesce(coalesce(sum(a.total_payment_invoice_fee_amount),0)+ coalesce(sum(a.total_advance_invoice_fee_amount)),0) as paid_amount, sum(a.balance), b.name
                from  " . $set_schema . ".invoice_balances a join  " . $set_schema . ".invoices c on c.id=a.invoice_id join  " . $set_schema . ".fees_installments i on i.id=a.fees_installment_id join  " . $set_schema . ".fees b on i.fee_id=b.id ".$where." group by b.name");

        $this->data['expenses'] =  DB::select("SELECT a.name, sum(c.amount) as amount from " . $set_schema . ".expense c join " . $set_schema . ".refer_expense b on c.refer_expense_id=b.id join  " . $set_schema . ".account_groups a on a.id = b.account_group_id ".$where." group by a.name order by a.name");
                
        return view('dashboard.accountant', $this->data);
    }


    public function show($id)
    {
        return view('user.profile', ['user' => User::findOrFail($id)]);
    }

    public function usage() {
        $this->data['home'] = 'Home';
        $id = Auth::User()->id;
        $this->data['user'] = $user =  \App\Models\GroupUser::where('user_id', $id)->first();
        $this->data['users'] =  \App\Models\GroupUser::where('group_id', $user->group_id)->get();
        $this->data['clients'] = \App\Models\Client::whereIn('id', \App\Models\ClientGroup::where('group_id', \App\Models\GroupUser::where('user_id', $id)->first()->group_id)->get(['client_id']))->get(['username']);
        return view('dashboard.usage', $this->data);
    }

    public function docs() {
        $this->data['parts'] = DB::table('portal_parts')->get();
        return view('dashboard.index', $this->data);
    }

    public function help() {
        $this->data['set'] = request()->segment(3);

        $this->data['parts'] = \App\Models\Task::where('user_id', Auth::User()->id)->get();
        if ($_POST) {
        // 'body', 'client_id', 'comment', 'user_id', 'body', 'priority', 'created_at', 'updated_at', 'subpart_id', 'to_user_id','end_date','status';

            $array = [
                'body' => request('body'),
                'user_id' => Auth::User()->id,
                'client_id' => \App\Models\Client::where('username', request('schema_name'))->first()->id,
                'subpart_id' => request('subpart_id'),
                'priority' => 1
            ];
          //  dd($array);
            \App\Models\Task::create($array);
        }
        return view('dashboard.support', $this->data);
    }

    public function all_parts() {
        $part_id = request('id');
        if (!empty($part_id)) {
            $parts = DB::table('portal_subparts')->where('part_id', $part_id)->get();
                echo "<option value='0'>select here</option>";
                foreach ($parts as $part) {
                    echo '<option value=' . $part->id . '>' . $part->name . '</option>';
                }
            }else{
                echo '0';
            }
        }
 

}
