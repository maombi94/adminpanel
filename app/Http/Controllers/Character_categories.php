<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
class Character_categories extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
        parent::__construct();
    }

    protected function rules() {
        $rules = array(
            array(
                'field' => 'character_category',
                'label' => $this->lang->line("character_category"),
                'rules' => 'required|max:255'
            )
        );
        return $rules;
    }

    public function index() {
        $id = request()->segment(3);
        $this->data['this_schema'] = $schema = !empty($id) ? $id : default_school(Auth::user()->id)->username;
        $this->data['all_character_categories'] = DB::table($schema.'.character_categories')->get();

        return view('character_categories/index', $this->data);
    }

    public function lists() {
        $id = request()->segment(3);
        $this->data['this_schema'] = $schema = !empty($id) ? $id : default_school(Auth::user()->id)->username;
           // $sql_1 = 'SELECT a.id as character_category_id,b.position, a.character_category, b.id, b.description, b.code FROM '. $schema. '.character_categories a JOIN '.$schema.'. characters b ON b.character_category_id = a.id WHERE b.id = '.$id;
            $sql = "SELECT * FROM ". $schema. ".character_categories a JOIN ".$schema.". characters b ON b.character_category_id = a.id";
            $this->data['all_characters'] = DB::select($sql);
         //   $this->data['character_categories'] = DB::select($sql);
            return view('character_categories/lists', $this->data);
    }

    public function add() {
            if ($_POST) {
                $rules = $this->rules();
                $this->form_validation->set_rules($rules);
                if (request("character_category") == '') {
                    $this->session->set_flashdata('success', "Define Characters");
                    $this->data["subview"] = "character_categories/add";
                    $this->load->view('_layout_main', $this->data);
                } else {
                    $array = array(
                        "character_category" => request("character_category"),
                        "based_on" => request("subject_based"),
                        'position'=> request('position')
                    );
                    $this->character_categories_m->insert_character_categories($array);
                    $this->session->set_flashdata('success', $this->lang->line('menu_success'));

                    return redirect(url("character_categories/index"));
                }
            } else {
                $this->data["subview"] = "character_categories/add";
                $this->load->view('_layout_main', $this->data);
            }
       
    }

    public function edit() {
        if (can_access('edit_character')) {
            $id = request()->segment(3);
            if ((int) $id) {
                $this->data['character_categories'] = $this->character_categories_m->get_character_categories($id);
                if ($this->data['character_categories']) {
                    if ($_POST) {
                        $rules = $this->rules();
                        $this->form_validation->set_rules($rules);
                        if (request("character_category") == '') {
                            $this->data["subview"] = "character_categories/edit";
                            $this->load->view('_layout_main', $this->data);
                        } else {
                            $array = array(
                                "character_category" => request("character_category"),
                                "based_on" => request("subject_based"),
                                 'position'=> request('position')
                            );
                            $this->character_categories_m->update_character_categories($array, $id);
                            $this->session->set_flashdata('success', $this->lang->line('menu_success'));

                            return redirect(base_url("character_categories/index"));
                        }
                    } else {
                        $this->data["subview"] = "character_categories/edit";
                        $this->load->view('_layout_main', $this->data);
                    }
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function delete() {
        if (can_access('delete_character')) {
            $id = request()->segment(3);
            if ((int) $id) {
                $character_category = $this->character_categories_m->get_character_categories($id);

                $this->character_categories_m->delete_character_categories($id);
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));

                return redirect(base_url("character_categories/index"));
            } else {
                return redirect(base_url("character_categories/index"));
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function assessed()
    {
        $id = request()->segment(3);
        $this->data['this_schema'] = $schema = !empty($id) ? $id : default_school(Auth::user()->id)->username;
        $class_level_id = request()->segment(4);
            $this->data['semester_id'] = $semester_id = request()->segment(5);
            $academic_year_id = request()->segment(6);
            $section_id = request()->segment(7);
            $classes_id = request()->segment(8);
            $this->data['exam_id'] = request()->segment(9);
            if ($academic_year_id != '' && $semester_id != '') {
                $this->data['exam'] = \App\Model\Exam::find($this->data['exam_id']);
                $this->data['section'] = \App\Model\Section::find($section_id);
                $this->data['class_id'] = $this->data['section']->classesID;
                $this->data['character_gradings'] = DB::table('refer_character_grading_systems')->orderBy('points', 'asc')->get();
                $students = \App\Model\StudentArchive::where('section_id', $section_id)->where('academic_year_id', $academic_year_id)->where('status', 1);
                $this->data['active_section_students'] = $students->get(['student_id']);
                $this->data['assessed'] = \App\Model\StudentCharacter::where('exam_id', $this->data['exam_id'])->whereIn('student_id', $this->data['active_section_students'])->count(DB::raw('DISTINCT student_id'));
                $this->data['students'] = $students->get();
                $this->data['character_categories'] = \App\Model\CharacterCategory::orderBy('position', 'asc')->get();
                $this->data['total_characters'] = $this->characters_m->totalCharacters()->total;
                $this->data['classes_to_assess'] = $this->student_characters_m->get_classes_to_assess();
                $this->data['classlevels'] = $this->classlevel_m->get_classlevel();
            } else {
                $this->data['$student_complete_assessed'] = null;
                $this->data['classes_to_assess'] = DB::table($schema.'.classes')->get();
                $this->data['classlevels'] = DB::table($schema.'.classlevel')->get();

            }
            return view('character_categories/assessed', $this->data);
        }


}
