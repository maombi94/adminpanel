<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use DB;

class Characters extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
        parent::__construct();
        $this->lang->load('characters');
    }

    protected function rules($id = null) {
        $ignore = Rule::unique('characters')->ignore($id, 'id');
        $except_decription = $id == null ? 'required|unique:characters,description' : ['required', $ignore];
        return $this->validate(request(), [
                    'character_category_id' => 'required',
                    'description' => $except_decription,
                    'classes' => 'required'
                        ], $this->custom_validation_message);
    }

    public function index() {
        if (can_access('assess_character')) {
            $this->data['all_characters'] = $this->characters_m->get_all_join_characters();
            $this->data['character_categories'] = $this->character_categories_m->get_all_character_categories();
            $this->data["subview"] = "characters/index";
            $this->load->view('_layout_main', $this->data);
        } else {
            $this->data["subview"] = "permission";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function category() {
        $this->data['all_characters'] = $this->characters_m->get_all_join_characters();
        $this->data['character_categories'] = $this->character_categories_m->get_all_character_categories();
        $this->data["subview"] = "characters/index";
        $this->load->view('_layout_main', $this->data);
    }

    public function createCode($last_code = 12345) {
        $number_part = substr($last_code, -3);
        return 'CH:' . ((int) $number_part + 1);
    }

    public function add() {
        $id = $this->session->userdata("id");
        if (can_access('add_character')) {
            $this->data['character_categories'] = $this->character_categories_m->get_all_character_categories();
            if ($_POST) {
                $this->rules();
                if (null != request('generate_code')) {
                    $last_character_code = \App\Model\Character::latest()->first();

                    $last_code = !empty($last_character_code) ? $last_character_code->code : null;

                    $code = $this->createCode($last_code);

                    $array = array(
                        "character_category_id" => request("character_category_id"),
                        "code" => $code,
                        "description" => request("description"),
                        "created_by" => session('table'),
                        "created_by_id" => $id
                    );
                } else {
                    $array = array(
                        "character_category_id" => request("character_category_id"),
                        "code" => request("code"),
                        "description" => request("description"),
                        "created_by" => session('table'),
                        "created_by_id" => $id
                    );
                }
                $id = DB::table('characters')->insertGetId($array);
                $classes_id = request('classes');
                foreach ($classes_id as $value) {

                    DB::table('character_classes')->insert([
                        'class_id' => $value,
                        'character_id' => $id,
                        'created_by' => session('id'),
                        'created_by_table' => session('table')
                    ]);
                }
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));

                return redirect(base_url("characters/index"));
            } else {
                $this->data["subview"] = "characters/add";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function edit() {
        if (can_access('edit_character')) {
            $id = clean_htmlentities(($this->uri->segment(3)));
            if ((int) $id) {

                $this->data['characters'] = $this->characters_m->get_characters($id);
                $this->data['character_categories'] = $this->character_categories_m->get_all_character_categories();

                if ($this->data['characters']) {
                    if ($_POST) {
                        $this->rules($id);

                        $array = array(
                            "code" => request('code'),
                            "description" => request("description"),
                            "updated_at" => date("Y-m-d h:i:sa"),
                            'position' => request('position'),
                            'character_category_id' => request('character_category_id')
                        );
                        $classes_id = request('classes');
                        \App\Model\CharacterClass::where('character_id', $id)->delete();
                        foreach ($classes_id as $value) {

                            DB::table('character_classes')->insert([
                                'class_id' => $value,
                                'character_id' => $id,
                                'created_by' => session('id'),
                                'created_by_table' => session('table')
                            ]);
                        }

                        $this->characters_m->update_characters($array, $id);
                        $this->session->set_flashdata('success', $this->lang->line('menu_success'));

                        return redirect(base_url("characters/index"));
                    } else {
                        $this->data["subview"] = "characters/edit";
                        $this->load->view('_layout_main', $this->data);
                    }
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function delete() {
        if (can_access('delete_character')) {
            $id = clean_htmlentities(($this->uri->segment(3)));
            if ((int) $id) {
                //$characters = $this->characters_m->get_characters($id);

                $this->characters_m->delete_characters($id);
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));

                return redirect(base_url("characters/index"));
            } else {
                return redirect(base_url("characters/index"));
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    function charactercall() {
        $character_category_id = request('character_category_id');
        $characters = \App\Model\Character::where('character_category_id', $character_category_id)->get();
        echo '<option value="">Select character</option>';
        foreach ($characters as $character) {
            echo "<option value=\"$character->id\">" . $character->description . "</option>";
        }
    }

}
