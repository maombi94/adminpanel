<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Model\UserAllowance;
use App\Model\SalaryAllowance;

class Allowance extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
        parent::__construct();
    }

    public function index() {
            if($_POST){
                $this->data['schema_name'] = $schema_name = clean_htmlentities(request('schema'));
                $this->data['category'] = $id = clean_htmlentities((request('allowance_category_id')));
                if ((int) $id > 0) {
                
                    $this->data['allowances'] = \DB::table($schema_name.'.allowances as a')->where('a.category', $id)->get();
                    return view('account.payroll.allowance_custom',$this->data);
                } else {
                    $this->data['allowances'] = [];
                    return view('account.payroll.allowance_index',$this->data);
                }
            }else{
                return view('account.payroll.allowance_index');
            }
    }

    public function add() {
        if (can_access('add_allowance')) {
            if ($_POST) {
                $this->validate(request(), [
                    'name' => 'required|max:255',
                    "is_percentage" => "required",
                    "description" => "required"
                        ], $this->custom_validation_message);
                $allowance=\App\Model\Allowance::create(request()->except('_token'));
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                return redirect(base_url("allowancpe/index/".$allowance->category));
            } else {
                $this->data["subview"] = "allowance/add";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function edit() {
        if (can_access('manage_payroll')) {
            $id = clean_htmlentities(($this->uri->segment(3)));

            if ((int) $id) {
                $this->data['allowance'] = \App\Model\Allowance::find($id);
                if ($this->data['allowance']) {
                    if ($_POST) {
                        $this->validate(request(), [
                            'name' => 'required|max:255',
                            "is_percentage" => "required",
                            "description" => "required"
                                ], $this->custom_validation_message);
                        $this->data['allowance']->update(request()->except('_token'));
                        $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                        return redirect(base_url("allowance/index/".$this->data['allowance']->category));
                    } else {
                        $this->data["subview"] = "allowance/edit";
                        $this->load->view('_layout_main', $this->data);
                    }
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function delete() {
        if (can_access('delete_grade')) {
            $id = clean_htmlentities(($this->uri->segment(3)));
            if ((int) $id) {
                $user_allowances = UserAllowance::where('allowance_id', $id)->first();
                $salary_allowance = SalaryAllowance::where('allowance_id', $id)->first();
                if (!empty($user_allowances) || !empty($salary_allowance)) {
                    //you cannot delete this
                    $this->session->set_flashdata('error', 'You cannot delete this allowance because some users are already allocated on this allowance');
                } else {
                    \App\Model\Allowance::destroy($id);
                    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                }
                return redirect()->back();
            } else {
                return redirect()->back();
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function subscribe() {
        $id = clean_htmlentities((request()->segment(3)));
        $this->data['schema_name'] = $schema_name = clean_htmlentities(request()->segment(4));
        if ((int) $id) {
            $this->data['set'] = $id;
            $this->data['type'] = 'allowance';
            $this->data['allowance'] = \DB::table($schema_name.'.allowances as a')->where('a.id',$id)->first();
            $subscriptions = \DB::table($schema_name.'.user_allowances as b')->where('b.allowance_id', $id)->get();
            $data = [];
            foreach ($subscriptions as $value) {
                $data = array_merge($data, array($value->user_id . $value->table));
            }
            $this->data['users'] = (new \App\Http\Controllers\Payroll())->getUsers($schema_name);
            $this->data['subscriptions'] = $data;
            $this->data["subview"] = "payroll/subscribe";
            return view('account.payroll.subscribe_allowance',$this->data);
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

     public function monthlysubscribe() {
        $id = clean_htmlentities((request()->segment(3)));
        $this->data['schema_name'] = $schema_name = clean_htmlentities(request()->segment(4));
        if ((int) $id) {
            $this->data['set'] = $id;
            $this->data['type'] = 'allowance';
            $this->data['allowance'] = \DB::table($schema_name.'.allowances as a')->where('a.id',$id)->first();
            $subscriptions = \DB::table($schema_name.'.user_allowances as b')->where('b.allowance_id', $id)->get();
            $data = [];
            foreach ($subscriptions as $value) {
                $data = array_merge($data, array($value->user_id . $value->table));
            }
            $this->data['users'] = (new \App\Http\Controllers\Payroll())->getUsers($schema_name);
            $this->data['subscriptions'] = $data;
            return view('account.payroll.allowance_subscriptions',$this->data);
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }
    
        function monthlyAddSubscriber() {
        $allowance = UserAllowance::where('user_id', request('user_id'))->where('table', request('table'))->where('allowance_id', request('allowance_id'))->where('deadline', '>', date("Y-m-d", strtotime(request('deadline'))));
        //dd(request()->except('deadline'));
  //      dd($allowance);
        if (!empty($allowance->first())) {
            $allowance->update(array_merge(request()->except('deadline'), ['deadline' => date("Y-m-d", strtotime(request('deadline')))]));
            echo 'success';
            exit;
        } else {
            UserAllowance::create(array_merge(request()->except('deadline'), ['deadline' => date("Y-m-d", strtotime(request('deadline')))]));
            echo 'updated';
            exit;
        }

    }
}
