<?php
namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Models\User;

class Accounts extends Admin_Controller
{
    public function __construct() {
        // $this->middleware('auth');
    }
    
    public function index() {
        $id = clean_htmlentities((request()->segment(3)));
        if ((int) $id) {
            $this->data['id'] = $id;
            if ($_POST) {
                $this->data['schema_name'] = $schema_name = request('schema');
                $to_date = request("to_date");
                $from_date = request("from_date");
            } else {
                // $academic_year_to = \DB::table($schema_name.'.academic_year')->whereYear('start_date', date('Y'))->first();
                // $academic_year_from = \DB::table($schema_name.'.academic_year')->orderBy('start_date', 'asc')->first();

                // $from_date = $academic_year_from->start_date;
                // !empty($academic_year_to) ? $to_date = $academic_year_to->end_date : $to_date = date("Y-12-30");
                return view('account.fixed_assets.index');
            
            }
            $this->data['from_date'] = $from_date;
            $this->data['to_date'] = $to_date;
            $this->data['expenses'] = $this->getCategories_by_date($schema_name,$id, $from_date, $to_date);
            return view('account.index', $this->data);
        }
    }

   

    public function expenses() {
        $this->data['fixed_value'] = $fixed_value = 1;
        if($_POST){
           $this->data['schema'] = $schema = request('schema');
           $this->data['siteinfos'] = \DB::table($schema.'.setting')->first();
            return $this->custom_summary($schema,$fixed_value);
        }
       return view('account.expenses.index',$this->data);
    }
    
    public function revenues() {
        $this->data['fixed_value'] = $fixed_value = 3;
        if($_POST){
            $this->data['schema'] = $schema = request('schema');
            return $this->custom_summary($schema,$fixed_value);
        }
        return view('account.revenues.index', $this->data);
    }

    public function fees() {
        $this->data['schools'] =  $this->load_school(Auth::User()->id);
        if($_POST){     
            return $this->fee_custom_summary();
        }
        return view('account.fee_collections.index', $this->data);
    }


    public function liability($id)
    {
        return view('user.profile', [
            'user' => User::findOrFail($id)
        ]);
    }

    public function fee_custom_summary(){
        //Single Fee Report 
      
        $this->data['schema_name'] = $schema_name = request('schema');
    
        $this->data['fee_id'] = $fee_id = request('fee_id');
        $fee = \DB::table($schema_name.'.fees')->where('id',$fee_id)->first();
        $this->data['type'] = isset($fee) ? $fee->name : '';
        $start = $this->data['from'] = date("Y-m-d", strtotime(request('from_date')));
        $end = $this->data['to'] = date("Y-m-d", strtotime(request('to_date')));
        //$fee_id = explode(',',$fee_id);
        // $transactions = \App\Model\PaymentsInvoicesFeesInstallment::whereIn('invoices_fees_installment_id', \App\Model\InvoicesFeesInstallment::whereIn('fees_installment_id', \App\Model\FeesInstallment::where('fee_id', $fee_id)->get(['id']))->get(['id']))
        // ->whereIn('payment_id', \App\Model\Payment::whereBetween('date', [$start, $end])->orderBy('id', 'desc')->get(['id']))->get();
     
        $this->data['transactions'] = $transactions = \DB::table($schema_name.'.payments_invoices_fees_installments')
                                    ->join($schema_name.'.invoices_fees_installments', $schema_name.'.payments_invoices_fees_installments.invoices_fees_installment_id','=',$schema_name.'.invoices_fees_installments.id')
                                    ->join($schema_name.'.fees_installments',$schema_name.'.invoices_fees_installments.fees_installment_id','=',$schema_name.'.fees_installments.id')
                                    ->where($schema_name.'.fees_installments.fee_id','=',$fee_id)
                                    ->join($schema_name.'.payments', $schema_name.'.payments_invoices_fees_installments.payment_id','=',$schema_name.'.payments.id')
                                    ->whereBetween($schema_name.'.payments.date',[$start,$end])
                                    ->join('constant.payment_types', $schema_name.'.payments.payment_type_id','=','constant.payment_types.id')->get();
                            //dd($transactions);
        return view('account.fee_collections.fee_custom_summary',$this->data);
    }
    
    // Loading custom summaries based on selected schema 
    public function custom_summary($schema, $fixed_value){
        $this->data['report_type'] = $report_type = $fixed_value;
        $this->data['schema_name'] = $schema_name = $schema;
        $start = $this->data['from'] = date("Y-m-d", strtotime(request('from_date')));
        $end = $this->data['to'] = date("Y-m-d", strtotime(request('to_date')));
        $check_fee = explode(',', $report_type);

        if ((int) $report_type == 1 && count($check_fee) < 2) {
            //expenses 
            $this->data['type'] = 'Expense';
            $transactions = \DB::table($schema_name.'.expense')->whereBetween('date', [$start, $end])->get();
        } else if ((int) $report_type == 2 &&  count($check_fee) < 2) {
            //payments 
            $this->data['type'] = 'Payments';
            $transactions = \DB::table($schema_name.'.payments')->whereBetween('date', [$start, $end])->orderBy('id', 'desc')->get();
        } else if ((int) $report_type == 3 &&  count($check_fee) < 2) {
            //revenues 
            $this->data['type'] = 'Revenues';
            $transactions = \DB::table($schema_name.'.revenues')->whereBetween('date', [$start, $end])->get();
        } else {
            //Single Fee Report 
            $fee_id = $check_fee[0];
            $this->data['type'] = $check_fee[1];
          
            $transactions = \App\Model\PaymentsInvoicesFeesInstallment::whereIn('invoices_fees_installment_id', \App\Model\InvoicesFeesInstallment::whereIn('fees_installment_id', \App\Model\FeesInstallment::where('fee_id', $fee_id)->get(['id']))->get(['id']))
             ->whereIn('payment_id', \App\Model\Payment::whereBetween('date', [$start, $end])->orderBy('id', 'desc')->get(['id']))->get();
             //  dd($transactions);
            $this->data['transactions'] = $transactions;
            $this->data["subview"] = "fee/fee_payment_summary";
        
        }
        $this->data['transactions'] = $transactions;
       // $this->data['schools'] =  $this->load_school(Auth::User()->id);
        return view('account.custom_summary',$this->data);
    
    }
    
    //load all summary for the allSchema
    public function all_schema_custom_summary(){
        
    }

    public function voucher() {
         $this->data['schema_name'] = $schema_name = clean_htmlentities((request()->segment(3)));
        $id = clean_htmlentities((request()->segment(4)));
        $cat_id = clean_htmlentities((request()->segment(5)));
        
        if ($cat_id == 5) {
            $this->data['voucher'] = \collect(DB::SELECT('SELECT * from ' . $schema_name. ' current_assets WHERE id=' . $id . ''))->first();
        } else {
            $this->data['voucher'] = \DB::table($schema_name.'.expense')->where('expenseID',$id)->first();
            $this->data['productexpenses'] = \DB::table($schema_name.'.product_purchases')->where('expense_id',$id)
                                                ->join($schema_name.'.product_alert_quantity', $schema_name.'.product_alert_quantity.id','=', $schema_name.'.product_purchases.product_alert_id')
                                                ->join('constant.metrics','constant.metrics.id','=', $schema_name.'.product_alert_quantity.metric_id')
                                                ->join($schema_name.'.refer_expense', $schema_name.'.refer_expense.id','=', $schema_name.'.product_purchases.expense_id')
                                                // ->select($schema_name.'.product_purchases.*',$schema_name.'.product_alert_quantity.*','constant.metrics.*',$schema_name.'.refer_expense*')
                                                ->get();
        }
        return view('account.voucher.voucher', $this->data);
    }

    public function getCategories_by_date($schema_name,$id, $from_date, $to_date) {
        switch ($id) {
            case 1:
                $result = \DB::table($schema_name.'.refer_expense')->where('financial_category_id', 4)->get();
                break;
            case 2:
                $result = \DB::table($schema_name.'.refer_expense')->where('financial_category_id', 6)->get();
                break;
            case 3:
                $result = \DB::table($schema_name.'.refer_expense')->where('financial_category_id', 7)->get();
                break;
            case 4:
                $result = \DB::table($schema_name.'.refer_expense')->whereIn('financial_category_id', [2, 3])->get();
                break;
            case 5:
                $result = \DB::table($schema_name.'.refer_expense')->where('financial_category_id', 5)->whereBetween('date',[$from_date,$to_date])->get();
                break;
            default:
                $result = array();
                break;
        }
        return $result;
    }


    public function view_expense() {
        $id = clean_htmlentities((request()->segment(3)));
        $refer_id = clean_htmlentities((request()->segment(4)));
        $bank_id = clean_htmlentities((request()->segment(5)));
        $this->data['schema_name'] = $schema_name = clean_htmlentities(request()->segment(6));
        $academic_year_to = \DB::table($schema_name.'.academic_year')->whereYear('end_date', date('Y'))->first();
        $academic_year_from = \DB::table($schema_name.'.academic_year')->orderBy('start_date', 'asc')->first();
        $from_date = $academic_year_from->start_date;
        $to_date = $academic_year_to->end_date;
        if ((int) $id) {
            $refer_expense = \DB::table($schema_name.'.refer_expense')->where('id',$id)->first();
                if ($_POST) {
                    if (request("to_date")) {
                        $d1 = request("to_date");
                        $to_date = $this->createDate($d1);
                    } else {
                        $to_date = request("to_date");
                    }
                    if (request("from_date")) {
                        $d2 = request("from_date");
                        $from_date = $this->createDate($d2);
                    } else {
                        $from_date = request("from_date");
                    }
                }
                if ($refer_id == 5) {
                    if (strtoupper($refer_expense->name) == 'CASH') {

                        $this->data['expenses'] = DB::SELECT('SELECT * from ' . set_schema_name() . ' bank_transactions WHERE  payment_type_id =1 and "date" >= ' . "'$from_date'" . ' AND "date" <= ' . "'$to_date'" . '');

                        $this->data['current_assets'] = DB::SELECT('SELECT * from ' . set_schema_name() . ' current_asset_transactions WHERE refer_expense_id=' . $id . ' and "date" >= ' . "'$from_date'" . ' AND "date" <= ' . "'$to_date'" . ' ');
                    } else if (strtoupper($refer_expense->name) == 'ACCOUNT RECEIVABLE') {

                        $this->data['expenses'] = array();
                        $this->data['current_assets'] = DB::SELECT('SELECT * from ' . set_schema_name() . ' current_asset_transactions WHERE refer_expense_id=' . $id . ' and "date" >= ' . "'$from_date'" . ' AND "date" <= ' . "'$to_date'" . ' ');

                        $this->data['fees'] = DB::select('select sum(a.balance + coalesce((c.amount-c.due_paid_amount),0)) as total_amount,b.name from ' . set_schema_name() . ' invoice_balances a join ' . set_schema_name() . ' student b on b.student_id=a.student_id LEFT JOIN ' . set_schema_name() . ' dues_balance c on c.student_id=b.student_id WHERE  a.balance <> 0.00 AND a."created_at" >= \'' . $from_date . '\' AND a."created_at" <= \'' . $to_date . '\' group by b.name');

                        $this->data['bank_opening_balance'] = \collect(DB::select('select sum(coalesce(opening_balance,0)) as opening_balance from ' . set_schema_name() . ' bank_accounts'))->first();
                    } else if ((int) $bank_id) {
                        $this->data['expenses'] = DB::SELECT('SELECT transaction_id,date,amount,' . "'Bank'" . ' as payment_method , payment_type_id,note from ' . set_schema_name() . ' bank_transactions WHERE bank_account_id=' . $bank_id . ' and payment_type_id <> 1 and "date" >= ' . "'$from_date'" . ' AND "date" <= ' . "'$to_date'" . 'order by date desc');

                        $this->data['current_assets'] = DB::SELECT('SELECT * from ' . set_schema_name() . ' current_asset_transactions WHERE refer_expense_id=' . $id . ' and "date" >= ' . "'$from_date'" . ' AND "date" <= ' . "'$to_date'" . 'order by date desc');
                        $this->data['bank_id'] = $bank_id;
                    } else {
                        $this->data['expenses'] = array();
                        $this->data['current_assets'] = DB::SELECT('SELECT * from ' . set_schema_name() . ' current_asset_transactions WHERE refer_expense_id=' . $id . ' and "date" >= ' . "'$from_date'" . ' AND "date" <= ' . "'$to_date'" . ' ');
                    }
                } else if (in_array(strtolower($refer_expense->name), array('inventory', 'dispensary')) && (int) $refer_id == 4) {
                    
                    $this->data['expenses'] = \App\Model\Expense::where('refer_expense_id', $id)->where('date', '>=', $from_date)->where('date', '<=', $to_date)->get();
                
                }  else if ((int) $refer_id == 4) {

                    if (preg_match('/EC-1001/', $refer_expense->code) && (int) $refer_expense->predefined > 0) {
                        $sql = 'select sum(b.employer_amount) as amount ,payment_date as date,\'' . $refer_expense->name . '\' as note,\' ' . $refer_expense->name . '\' as name, \'Payroll\' as payment_method,null as "expenseID",extract(month from payment_date)||\'\'||extract(year from payment_date) as ref_no, 1 AS predefined, null as id  from ' . set_schema_name() . 'salaries a join ' . set_schema_name() . 'salary_pensions b on a.id=b.salary_id where b.pension_id=' . $refer_expense->predefined . '  group by a.payment_date UNION ALL (SELECT a.amount, a.date, a.note, b.name, a.payment_method, a."expenseID", a.ref_no, null as predefined, b.id FROM ' . set_schema_name() . 'expense a JOIN ' . set_schema_name() . 'refer_expense b ON a.refer_expense_id=b.id WHERE b.id=' . $refer_expense->id . ' ORDER BY a.date DESC)';
                        $this->data['expenses'] = DB::SELECT($sql);
                    } else {
                        $this->data['expenses'] = \App\Model\ReferExpense::where('refer_expense.id', $id)->LeftJoin('expense', 'expense.refer_expense_id', 'refer_expense.id')->where('expense.date', '>=', $from_date)->where('expense.date', '<=', $to_date)->get();
                    }
                } else if (strtoupper($refer_expense->name) == 'DEPRECIATION') {

                    $this->data['expenses'] = DB::select('select coalesce(sum(b.open_balance::numeric * b.depreciation*(\'' . $to_date . '\'::date-a.date::date)/365),0) as open_balance,sum(amount-amount* a.depreciation *(\'' . $to_date . '\'::date-a.date::date)/365) as total,sum(amount* a.depreciation*(\'' . $to_date . '\'::date-a.date::date)/365) as amount, refer_expense_id,a.date,a.note,a.recipient,b.name,a."expenseID",b.predefined from ' .$schema_name . 'expense a join ' . set_schema_name() . 'refer_expense b  on b.id=a.refer_expense_id where b.financial_category_id=4 AND  a.date  <= \'' . $to_date . '\' group by a.refer_expense_id,b.open_balance,a.date,a.note,b.name,a."expenseID",b.predefined  ORDER BY a.date desc');
                    $this->data['depreciation'] = 1;
                } else {

                    $this->data['expenses'] = \DB::table($schema_name.'.refer_expense')->where('refer_expense.id', $id)->JOIN($schema_name.'.expense', 'expense.refer_expense_id', 'refer_expense.id')->whereBetween($schema_name.'.expense.date',[$from_date,$to_date])->get();
                }
                $this->data['period'] = 1;
                $this->data['predefined'] = $refer_expense->predefined;
                $this->data['id'] = $refer_id;
                $this->data['refer_id'] = $refer_id;
                $this->data['refer_expense_name'] = $refer_expense->name;
            if (in_array(strtolower($refer_expense->name), array('inventory', 'dispensary')) && (int) $refer_id == 4) {
                $this->data["subview"] = "expense/inventory_category";
                $this->load->view('_layout_main', $this->data);
            }else{
              return view('account.expenses.expense_category',$this->data);
            }
            
        } else {
            redirect()->back()->with('warning', 'Something Went Wrong Please Try Again......!');
        }
    }

    public function getFees() {
        $schema_name = request('schema_name');
        $schema_fees = \DB::table($schema_name.'.fee')->orderBy('priority', 'ASC')->get();
        if (!empty($schema_fees)) {
            echo '<option value="" disabled selected>select fee</option>';
            foreach ($schema_fees as $fee) {
                echo '<option value="' . $fee->id . '">' . $fee->name . '</option>';
            }
        } else {
            echo "0";
        }
    }
    
}
