<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubPart extends Model {

    /**
     * Generated
     */
    protected $table = 'portal_subparts';
    protected $fillable = ['id', 'name', 'part_id', 'created_at', 'updated_at'];
    
        public function part() {
            return $this->belongsTo(\App\Models\PortalPart::class, 'part_id', 'id');
        }

}
