<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Task
 *
 * @author hp
 */
class Task extends Model {

    //put your code here
    protected $table = 'portal_tasks';
    protected $fillable = ['id','body', 'client_id', 'comment', 'user_id', 'body', 'priority', 'created_at', 'updated_at', 'subpart_id', 'to_user_id','end_date','status'];
    
    public function user() {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id')->withDefault(['name' => 'Not allocated']);
    }

    public function client() {
        return $this->belongsTo(\App\Models\Client::class, 'client_id', 'id')->withDefault(['name' => 'Not Defined']);
    }

    public function toUser() {
        return $this->belongsTo(\App\Models\SuperUser::class, 'to_user_id', 'id');
    }

    public function part() {
        return $this->belongsTo(\App\Models\SubPart::class, 'subpart_id', 'id');
    }

}
