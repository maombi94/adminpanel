<?php $root = url('/public/'); ?>

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Schools Admin Portal</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords"
        content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?=$root?>/images/shulesoft_logo.png" type="image/png">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?=$root?>/bower_components/bootstrap/css/bootstrap.min.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/icon/feather/css/feather.css">
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/icon/icofont/css/icofont.css">

    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/icon/themify-icons/themify-icons.css">

    <!-- Sweet alert -->
    <link rel="stylesheet" type="text/css" href="<?=$root?>/bower_components/sweetalert/css/sweetalert.css">

    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="<?=$root?>/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/pages/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$root?>/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">

    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/datatables/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/datatables/css/responsive.bootstrap4.min.css">
    <!-- radial chart -->
    <link rel="stylesheet" href="<?=$root?>/assets/pages/chart/radial/css/radial.css" type="text/css" media="all">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/css/jquery.mCustomScrollbar.css">

    <!-- accounting styles -->
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/css/account_custom.css">

    <!-- select 2 -->
    <link rel="stylesheet" type="text/css" href="<?=$root?>/bower_components/select2/css/select2.min.css">

    <!-- end accounting styles -->
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  --}}
      
    <script type="text/javascript" src="<?=$root?>/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/select2/js/select2.full.min.js"></script>
</head>
<body>




<!-- Pre-loader end -->
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">

        <div class="pcoded-main-container">
            <div class="pcoded-wrappere">
     
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                <!-- Page-header start -->
                                
                                
                                <div class="page-header">
                                    <div class="row align-items-end">
                                        <div class="col-lg-10">
                                            <div class="page-header-title">
                                                <div class="d-inline">
                                                    <h4>Job Search - Panel List</h4>
                                                
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            
                                        </div>
                                    </div>
                                </div> 

                                <!-- Page-header end -->
                                    <!-- Page body start -->
                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-lg-12 col-xl-12">
                                                <!-- Open position card start -->
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5>Open Position</h5>

                                                    </div>
                                                    <div class="card-block">

                                                      <?php if(isset($job_listings)) {  ?>
                                                        <?php foreach($job_listings as $job_listing) {  ?>
                                                        <div class="job-cards">
                                                            <div class="media">
                                                               
                                                                <div class="media-body">
                                                                    <div class="company-name m-b-10">
                                                                        <p><?= $job_listing->name ?></p>
                                                                        <i class="text-muted f-14">Deadline : <?= date('F,d Y', strtotime($job_listing->deadline_date)) ?></i></div>
                                                                      <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                                                         when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                                                      <a href="<?= url('recruiments/descriptions/'.$job_listing->id) ?>" class="btn btn-primary btn-sm">view </a>
                                                                </div>
                                                                <div class="media-right">
                                                                    <div class="label-main">
                                                                        <label class="label bg-primary">New</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                      <?php } ?>
                                                     

                                                    </div>
                                                </div>
                                            
                                            </div>



                                        </div>
                                    </div>
                                    <!-- Page body end -->
                                </div>
                            </div>
                          
                        </div>
                    </div>
                
            </div>
        </div>
    </div>

    
    <script type="text/javascript" src="<?=$root?>/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/assets/pages/widget/excanvas.js"></script>

    <!-- select 2 -->
    <script type="text/javascript" src="<?=$root?>/bower_components/select2/js/select2.full.min.js"></script>
<!-- Multiselect js -->
     <script type="text/javascript" src="<?=$root?>/bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/multiselect/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="<?=$root?>/assets/js/jquery.quicksearch.js"></script>
<!-- Custom js -->
    <script type="text/javascript" src="<?=$root?>/assets/pages/advance-elements/select2-custom.js"></script>

    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?=$root?>/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/sweetalert/js/sweetalert.min.js"></script>

    <!-- data-table js -->

    <!-- modernizr js -->
    <script type="text/javascript" src="<?=$root?>/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?=$root?>/assets/js/SmoothScroll.js"></script>
    <script src="<?=$root?>/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?=$root?>/assets/js/jquery.mousewheel.min.js"></script>
    <!-- Chart js -->
    <script type="text/javascript" src="<?=$root?>/bower_components/chart.js/js/Chart.js"></script>
    <!-- gauge js -->
    <script src="<?=$root?>/assets/pages/widget/amchart/amcharts.js"></script>
    <script src="<?=$root?>/assets/pages/widget/amchart/serial.js"></script>
    <script src="<?=$root?>/assets/pages/widget/amchart/light.js"></script>
    <!-- Custom js -->
    <!-- <script type="text/javascript" src="<?=$root?>/assets/pages/widget/custom-widget1.js"></script> -->
    <script src="<?=$root?>/assets/pages/data-table/js/data-table-custom.js"></script>

    <script type="text/javascript" src="<?=$root?>/assets/js/SmoothScroll.js"></script>
    <script src="<?=$root?>/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?=$root?>/assets/js/jquery.mousewheel.min.js"></script>
    <script src="<?=$root?>/assets/js/pcoded.min.js"></script>
    <script src="<?=$root?>/assets/js/vartical-layout.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/assets/js/script.js"></script>
</body>

</html>
