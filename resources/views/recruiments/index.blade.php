@extends('layouts.app')
@section('content')

<div class="page-body">
<div class="row">
 <div class="col-sm-12">
  <div class="card">
    <div class="card-block">
          <a type="button" href="<?= url('recruiments/createjob') ?>" class="btn btn-primary m-b-20">+ Add new</a>
        <div class="dt-responsive table-responsive">
            <table id="simpletable" class="table table-striped table-bordered nowrap">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name of the job</th>
                    <th>Applicants</th>
                    <th>Office</th>
                    <th>Deadline date</th>
                    <th>View</th>
                </tr>
                </thead>
                <tbody>
                   <?php if(isset($job_listings)) { ?>
                   <?php $i = 1; foreach ($job_listings as $job_listing) { ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td><?= $job_listing->name ?></td>
                        <td class="text-center">
                        <?= DB::table('admin.portal_applicant')->where('job_id',$job_listing->id)->count();  ?>
                        </td>
                        <td><?= $job_listing->name ?></td>
                        <td><?= date('d /m /Y', strtotime($job_listing->deadline_date)) ?></td>
                        <td>
                        <a href="<?= url('recruiments/applicants/'.$job_listing->id) ?>" class="btn btn-info btn-sm">view </a>
                        </td>
                    </tr>
                  <?php }  ?>
                <?php }  ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</div>
</div>








@endsection