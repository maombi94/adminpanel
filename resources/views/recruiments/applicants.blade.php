@extends('layouts.app')
@section('content')

<div class="page-body">
<div class="row">
 <div class="col-sm-12">
  <div class="card">
    <div class="card-block">
          <h4> Applicants for <?= $job_data->name ?></h4>
        <div class="dt-responsive table-responsive">
            <table id="simpletable" class="table table-striped table-bordered nowrap">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Full name</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Gender</th>
                    <th>View</th>
                </tr>
                </thead>
                <tbody>
                    <?php if(isset($applicants) && !empty($applicants)) { ?>
                     <?php $i = 1; foreach($applicants as $applicant)  { ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td><?= $applicant->name ?> </td>
                        <td><?= $applicant->name ?></td>
                        <td><?= $applicant->email ?></td>
                        <td><?= $applicant->phone_number ?></td>
                        <td><?= $applicant->gender ?></td>
                        <td>
                            <a href="<?= url('recruiments/profile/'.$applicant->id) ?>" class="btn btn-info btn-sm">view </a>
                        </td>
                    </tr>
                    <?php $i++; } ?>
                 <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</div>
</div>

@endsection