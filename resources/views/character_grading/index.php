
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-list"></i> panel_title')?></h3>

        <ol class="breadcrumb">
            <li><a href="<?=url("dashboard/index")?>"><i class="fa fa-laptop"></i> menu_dashboard')?></a></li>
            <li class="active">menu_character_grading')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <?php 
                    $usertype = session("usertype");
                    if(can_access('add_character')) {
                        ?> 
                    <h5>
                        <a class="btn btn-success" href="<?php echo url('character_grading/add') ?>">
                            <i class="fa fa-plus"></i> 
                            add_title')?>
                        </a>
                    </h5>
                <?php } ?>

                <div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th class="col-lg-2">slno')?></th>
                                <th class="col-lg-2">grade')?></th>
                                <th class="col-lg-2">points')?></th>
                                <th class="col-lg-2">grade_remark')?></th>
                                <th class="col-lg-2">action')?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($character_gradings)) {$i = 1; foreach($character_gradings as $character_grading) { ?>
                                <tr>
                                    <td data-title="slno')?>">
                                        <?php echo $i; ?>
                                    </td>
                                    <td data-title="grade')?>">
                                        <?php echo $character_grading->grade; ?>
                                    </td>
                                    <td data-title="points')?>">
                                        <?php echo $character_grading->points; ?>
                                    </td>
                                    <td data-title="grade_remark')?>">
                                        <?php echo $character_grading->grade_remark; ?>
                                    </td>
                                    <td data-title="action')?>">
                                        <?php echo btn_edit('character_grading/edit/'.$character_grading->id, 'Edit') ?>
                                        <?php echo btn_delete('character_grading/delete/'.$character_grading->id, 'delete') ?>
                                    </td>
                                </tr>
                            <?php $i++; }} ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
