
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-list"></i> panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=url("dashboard/index")?>"><i class="fa fa-laptop"></i> menu_dashboard')?></a></li>
            <li><a href="<?=url("character_grading/index")?>"></i> menu_character_grading')?></a></li>
            <li class="active">menu_edit')?> menu_character_grading')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8">
                <form class="form-horizontal" role="form" method="post">


                    <?php 
                        if(form_error($errors,'grade')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="grade" class="col-sm-2 control-label">
                            <?=$data->lang->line("grade")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="grade" name="grade" value="<?=set_value('grade',$grade->grade)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error($errors,'grade'); ?>
                        </span>
                        </div>


                        <?php 
                        if(form_error($errors,'grade_remark')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="grade_remark" class="col-sm-2 control-label">
                            <?=$data->lang->line("grade_remark")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="grade_remark" name="grade_remark"  value="<?=set_value('grade_remark',$grade->grade_remark)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error($errors,'grade_remark'); ?>
                        </span>
                        </div>
                         
        <?php 
                        if(form_error($errors,'points')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="grade_remark" class="col-sm-2 control-label">
                            <?=$data->lang->line("points")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="number" min="1" max="200" class="form-control" id="points" name="points"  value="<?= set_value('point',$grade->points)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error($errors,'points'); ?>
                        </span>
                        </div>
    
                        <?php 
                        if(form_error($errors,'description')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="description" class="col-sm-2 control-label">
                            <?=$data->lang->line("description")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="description" name="description"  value="<?=set_value('description',$grade->description)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error($errors,'description'); ?>
                        </span>
                        </div>


                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-6">
                            <input type="submit" class="btn btn-success btn-block" value="<?=$data->lang->line("save")?>" >
                        </div>
                    </div>

                <?= csrf_field() ?>
</form>


            </div>
        </div>
    </div>
</div>
