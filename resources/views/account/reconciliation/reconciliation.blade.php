@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">

            <form style="" class="form-horizontal" role="form" method="post">
              <div class="row">
                  <div class="col-sm-12 col-xl-3 m-b-30">
                        <h4 class="sub-title">School</h4>
                        <select id="schema" name="schema" class="form-control form-control-primary">
                              <option value="" disabled selected>Select school</option>
                                @foreach (load_schemas() as $school)
                                    <option value="{{$school->username}}">{{$school->username}}</option>
                                @endforeach
                        </select>
                    </div>
                 
                    <div class="col-sm-12 col-xl-3 m-b-30">
                      <h4 class="sub-title">Bank Name</h4>
                      <select id="schema_bank_id" name="bank_id" class="form-control form-control-primary">
                              
                      </select>
                   </div>
  
                    <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form1">
                      <h4 class="sub-title">From Date</h4>
                      <div class=" col-xs-12">
                        <input type="date" required="true" class="form-control calendar" id="from" name="from" value="" autocomplete="off">
                      </div>
                  </div>
                    
                    <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                        <h4 class="sub-title">To Date</h4>
                        <div class=" col-xs-12">
                          <input type="date" required="true" class="form-control calendar" id="to" name="to" value="" autocomplete="off">
                        </div>
                    </div>
                    
                    <div class="col-sm-12 col-xl-3 m-b-30">
                      <h4 class="sub-title">Transaction Type</h4>
                      <select name="method" class="form-control">
                        <option selected disabled>Select Transaction</option>
                        <option value="All">All</option>
                        <option value="revenue">Revenue</option>
                        <option value="payment">Payments (From Invoice) </option>
                        <option value="current_assets">Current Assets</option>
                        <option value="liability">Liability</option>
                        <option value="capital">Capital</option>
                        <option value="fixed_assets">Fixed Assets</option>
                      </select> 
                   </div>
                    <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                      <div class=" col-xs-12">
                          <input type="submit" class="btn btn-success submit_btn_custom" value="Submit">
                      </div>
                      </div>
                       
                    <?= csrf_field() ?>
                </form>
            </div>
        <div class="col-sm-6  offset-sm-2 list-group">
            <div class="list-group-item">
                <table class="table  nowrap table-md">
                    <thead>
                        <tr>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Transaction Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?= $from ?></td>
                            <td><?= $to ?></td>
                            <td><?= $table ?></td>
                        </tr>
                    </tbody>
                </table>
                
            </div>
        </div> <br>
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Expenses</a>
                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Summary</a>
                
                </div>
            </nav>
            <br>
          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="table-responsive dt-responsive">
                    <table id="dom-jqry" class="table table-striped table-bordered dataTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Bank Name</th>
                                <th>Bank Account</th>
                                <th>Transaction ID</th>
                                <th>Transaction Date</th>
                                <th>Amount</th>
                                <th>Notes</th>
                                <th>Transaction Type</th>
                                <th>Status</th>
                                <th>Action</th>      
                            </tr>
                        </thead>
                                <!-- body start -->
                                <tbody>
                                    <?php
                                    $total_amount = 0;
                                    $total_payments = 0;
                                    $amount_reconciled = 0;
                                    $bank_amount = [];
                                            $bank_amount_reconciled = [];
                                            $bank_amount_not_reconciled = [];
                                            foreach ($banks as $bank) {
                                                $bank_amount[$bank->id] = 0;
                                                $bank_amount_reconciled[$bank->id] = 0;
                                                $bank_amount_not_reconciled[$bank->id] = 0;
                                            }
                                    $i = 1;?>
                                    @if (isset($payments) && !empty($payments))
                                       
                                        @foreach ($payments as $payment)
                                        
                                            <tr>
                                                <td data-title="Number">
                                                    <?php echo $i; ?>
                                                </td>
                                                <td data-title="bank_name"> 
                                                    {{$payment->bank_name}}                                     
                                                </td>
                                                <td data-title="Bank_account">
                                                    {{$payment->account_number}}
                                                </td>
                                                <td data-title="transaction_id">
                                                    {{$payment->transaction_id}}
                                                </td>
                                                <td data-title="payment_date">
                                                    {{date('d M Y', strtotime($payment->date))}} 
                                                </td>
                                                <td data-title="payment amount">
                                                    {{money($payment->amount)}} 
                                                </td>
                                                <td>
                                                    {{$payment->payer_name}}
                                                </td>
                                                <td data-title="Type">
                                                    @if($payment->is_payment ==5)
                                                        <b class="label label-info">Current Assets</b>
                                                    @elseif($payment->is_payment ==6)
                                                        <b class="label label-info">Liabilities</b>                                               
                                                    @elseif($payment->is_payment ==7)
                                                        <b class="label label-info">Fixed Assets</b>
                                                    @elseif($payment->is_payment == 1)
                                                        <b class="label label-success">Invoiced</b>
                                                    @else 
                                                        <b class="label label-info">Direct Revenue</b>  
                                                    @endif
                                                </td>
                                                <td data-title="Status">
                                                    <?php
                                                    if ($payment->reconciled == 1) {
                                                        echo '<b class="label label-success">Reconciled</b>';
                                                        $amount_reconciled += $payment->amount;
                                                        isset($bank_amount_reconciled[$payment->bank_account_id]) ? $bank_amount_reconciled[$payment->bank_account_id]+= $payment->amount :'';
                                                    }
                                                    ?>
                                                </td>
                                                <td data-title="Action">
                                                    <?php if ($payment->reconciled <> 1) { ?>
                                                        <input type="checkbox" class="reconcile btn btn-success" id="<?php echo $payment->id; ?>" data-table="<?= isset($payment->is_expense) && $payment->is_expense ==1 ? 'total_expenses' :$table ?>" data-type='<?= $payment->is_payment ?>' data-placement="top" data-toggle="tooltip" data-original-title="Reconcile">
                                                    <?php } else { ?>
                                                    <a href="#" onclick="return false" onmousedown="unreconcile('<?= $payment->id ?>', '<?= $payment->is_payment ?>')" class="btn btn-xs btn-danger">un reconcile</a>                                   
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            <?php
                                            $i++; ?>
                                        @endforeach
                                    @endif
                                    
                                </tbody>
                                <!-- body end -->
                        <tfoot>
                            <tr>
                                <td colspan="5">Total</td>
                                <td><?= money($total_payments) ?></td>
                                <td colspan="5"></td>
                            </tr>
                        </tfoot>
                    </table>
                </div> <!-- End datatable -->
             </div>
             <!-- summary-->
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <h5> Summary </h5> 
                    <table id="example1" class="table table-striped table-bordered table-hover  no-footer">
                    <thead>
                        <tr>
                            <th class="col-sm-2">#</th>
                            <th class="col-sm-2">Total Amounts</th>
                            <th class="col-sm-2">Reconcilled Amount</th>
                            <th class="col-sm-2">Un Reconcilled Amount</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td><?= money($total_payments) ?></td>
                            <td><?= money($amount_reconciled) ?></td>
                            <td><?= money($total_payments - $amount_reconciled) ?></td>
                        </tr>
                    </tbody>
                </table>
                </div> <!-- End summary -->

                <div class="title_left">
                    <br/><br/>
                    <h3>Bank Summary</h3>
                    <br/>
                </div>
                <div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th class="col-sm-2">#</th>
                                <th class="col-sm-2">Bank Name</th>
                                <th class="col-sm-2">Account Name</th>
                                <th class="col-sm-2">Account Number</th>
                                <th class="col-sm-2">Total Amount</th>
                                <th class="col-sm-2">Amount Reconciled</th>
                                <th class="col-sm-2">Amount Un-Reconciled</th>

                            </tr>
                        </thead>
                        <tbody> <?php
                             $b = 1;
                               foreach ($banks as $bank) {
                                     ?>
                                <tr>
                                    <td><?= $b ?></td>
                                    <td><?= $bank->name ?></td>
                                    <td><?= $bank->name ?></td>
                                    <td><?= $bank->number ?></td>
                                    <td><?php
                                       $b_amount = isset($bank_amount[$bank->id]) ? $bank_amount[$bank->id] : 0;
                                     echo money($b_amount);
                                   ?></td>
                                    <td><?php
                                $b_rec = isset($bank_amount_reconciled[$bank->id]) ? $bank_amount_reconciled[$bank->id] : 0;
                                echo money($b_rec)
                                     ?></td>
                                    <td><?= money($b_amount - $b_rec) ?></td> 
                                </tr>
                                <?php
                                $b++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
        </div>
   </div>

   <script type="text/javascript">
    $('#schema').change(function (event) {
    var schema_name = $(this).val();
    if (schema_name === 'allSchema') {
   
    } else {
        $.ajax({ 
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?= url('paymentController/getSchemaBanks') ?>',
            data: {schema_name: schema_name},
            dataType: "html", 
            cache: false,
            success: function (data) { 
            $('#schema_bank_id').html(data);
            }
        });

    }
});
</script>

<script type="text/javascript">
    function unreconcile(a, b) {
        $.ajax({
            type: 'POST',
            url: "<?= url('PaymentController/unreconcile') ?>",
            data: {"id": a, type: b},
            dataType: "html",
            success: function (data) {
                toast(data);
            }
        });
    }
    $('.reconcile').click(function () {
        var id = $(this).attr("id");
        var type = $(this).attr('data-type');
        var table = $(this).attr('data-table');
        if (parseInt(id)) {
            $.ajax({
                type: 'POST',
                url: "<?= url('PaymentController/reconcile') ?>",
                data: {"id": id, type: type, table: table},
                dataType: "html",
                success: function (data) {
                    toast(data);
                }
            });
        }
    });
</script>
@endsection