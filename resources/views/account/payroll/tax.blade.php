@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">
        </div>
         <br>
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Taxes</a>
                {{-- <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Summary</a> --}}
                </div>
            </nav>
            <br>
          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="table-responsive dt-responsive">
                    <table id="dom-jqry" class="table table-striped table-bordered dataTable">
                        <thead>
                            <tr>
                                <th class="col-sm-1">#</th>
                                <th class="col-sm-2">Name</th>
                                <th class="col-sm-2">From Amount</th>
                                <th class="col-sm-2">To Amount</th>
                                <th class="col-sm-1">Tax Rate</th>
                                <th class="col-sm-1">Tax Amount Excess</th>
                                <th class="col-sm-2">Description</th>
                            </tr>
                        </thead>
                                <!-- body start -->
                                <tbody>
                                    <?php
                                        $i = 1;
                                     foreach($taxes as $tax) { ?>
                                        <tr>
                                        <td data-title="#">
                                            <?php echo $i; ?>
                                        </td>
                                        <td data-title="name">
                                            <?php echo $tax->name; ?>
                                        </td>
                                        <td data-title="payroll from">
                                            <?php echo $tax->from; ?>
                                        </td>
                                        <td data-title="payroll to">
                                            <?php echo $tax->to; ?>
                                        </td>
                                        <td data-title="rate">
                                            <?php echo $tax->tax_rate; ?>
                                        </td>
                                        <td data-title="excess amount">
                                            <?php echo $tax->tax_plus_amount; ?>
                                        </td>
                                    
                                        <td data-title="description">
                                            <?php echo $tax->description; ?>
                                        </td>
                                        
                                    </tr>
                            <?php $i++; 
                                        }   
                                    ?>
                                </tbody>
                                <!-- body end -->
                    </table>
                </div> <!-- End datatable -->
             </div>
             <!-- summary-->
                {{-- <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <h5> Summary </h5> 
         
                </div>  --}}
                <!-- End summary -->
        </div>
   </div>

@endsection