@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">

        <form style="" class="form-horizontal" role="form" method="post">
            <div class="row">
                <div class="col-sm-12 col-xl-3 m-b-30">
                      <h4 class="sub-title">School</h4>
                      <select id="schema" name="schema" class="form-control form-control-primary">
                            <option value="" disabled selected>Select school</option>
                              @foreach (load_schemas() as $school)
                                  <option value="{{$school->username}}">{{$school->username}}</option>
                              @endforeach
                      </select>
                  </div>
               
                  <div class="col-sm-12 col-xl-3 m-b-30">
                    <h4 class="sub-title">Fee Name</h4>
                    <select id="schema_fee_id" name="fee_id" class="form-control form-control-primary">
                            
                    </select>
                 </div>

                  <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form1">
                    <h4 class="sub-title">Start Date</h4>
                    <div class=" col-xs-12">
                      <input type="date" required="true" class="form-control calendar" id="from_date" name="from_date" value="" autocomplete="off">
                    </div>
                </div>
                  
                  <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                      <h4 class="sub-title">End Date</h4>
                      <div class=" col-xs-12">
                        <input type="date" required="true" class="form-control calendar" id="to_date" name="to_date" value="" autocomplete="off">
                      </div>
                  </div>

                  <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                    <div class=" col-xs-12">
                        <input type="submit" class="btn btn-success submit_btn_custom" value="Submit">
                    </div>
                    </div>
                     
                  <?= csrf_field() ?>
              </form>
            </div>

            <script type="text/javascript">
                    $('#schema').change(function (event) {
                    var schema_name = $(this).val();
                    if (schema_name === 'allSchema') {
                   
                    } else {
                        $.ajax({ 
                            type: 'POST',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: '<?= url('Accounts/getFees') ?>',
                            data: {schema_name: schema_name},
                            dataType: "html", 
                            cache: false,
                            success: function (data) { 
                            $('#schema_fee_id').html(data);
                            }
                        });
            
                    }
                });
            </script>

@endsection