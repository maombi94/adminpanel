@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">
            <form style="" class="form-horizontal" role="form" method="post">
                <div class="row">
                    <div class="col-sm-12 col-xl-3 m-b-30">
                          <h4 class="sub-title">School</h4>
                          <select id="schema" name="schema" class="form-control form-control-primary">
                              <option value="" disabled selected>Select school</option>
                              <option value="allSchema">All schools</option>
                                  @foreach (load_schemas() as $school)
                                      <option value="{{$school->username}}">{{$school->username}}</option>
                                  @endforeach
                          </select>
                      </div>
                   
                      <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form1">
                          <h4 class="sub-title">Start Date</h4>
                          <div class=" col-xs-12">
                            <input type="date" required="true" class="form-control calendar" id="from_date" name="from_date" value="" autocomplete="off">
                          </div>
                      </div>
                      
                      <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                          <h4 class="sub-title">End Date</h4>
                          <div class=" col-xs-12">
                            <input type="date" required="true" class="form-control calendar" id="to_date" name="to_date" value="" autocomplete="off">
                          </div>
                      </div>
    
                      <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                        <h4 class="sub-title"><br> </h4>
                        <div class=" col-xs-12">
                            <input type="submit" class="btn btn-success submit_btn_custom" value="Submit">
                        </div>
                        </div>
                         
                      <?= csrf_field() ?>
                  </form>
        </div>   
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa icon-expense"></i>
                <?php
                $global_id = $id;
                $name = '';
                if ($id == 4) {
                    $add = " Expenses";
                    } elseif ($id == 1) {
                        $add = " Fixed Assets";
                    } else if ($id == 2) {
                        $add = "Liabilities";
                    } else if ($id == 3) {
                        $add = "Capital Management";
                    } else if ($id == 5) {
                        $add = 'Current Assets';
                    } else if ($id == 6) {
                        $add = 'Inventory';
                    }
                    echo 'School '. $add;
                ?>
            </h3>
        </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                    {{-- <h5 class="page-header">
                        <a class="btn btn-success" href="<?php echo url('expense/add/' . $id . '') ?>">
                            <i class="fa fa-plus"></i> 
                            Add expense
                        </a>
                    </h5> --}}
                              
                    <div id="hide-table">
                        <table  id="dom-jqry" class="table table-striped table-bordered dataTable">
                            <thead>
                                <tr>
                                    <th class="col-sm-1">#</th>
                                    <th class="col-sm-1">Code</th>
                                    <th class="col-sm-2">Name</th>
                                    <th class="col-sm-2">Category</th>
                                    <th class="col-sm-2">Group name</th>
                                    <th class="col-sm-2">Sum</th>
                                    <th class="col-sm-2">Open balance</th>
                                    <th class="col-sm-2">Descriptions</th>
                                    <th class="col-sm-2">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $total_expense = 0;
                                $i = 1;
                                if (count($expenses) > 0) {
                                    foreach ($expenses as $expense) {
                                        ?>
                                        <tr>
                                            <td data-title="#">
                                                <?php echo $i; ?>
                                            </td>
                                            <td data-title="Code">
                                                <?php echo $expense->code; ?>
                                            </td>
                                            <td data-title="Name">
                                                <?php echo $expense->name; ?>
                                            </td>
                                            <td data-title="Category">
                                                <?php $category = \DB::table('constant.financial_category')->where('id',$expense->financial_category_id)->first(); ?>
                                                {{ $category->name }}
                                            </td>   
                                            <td data-title="Group name">
                                                <?php $group = \DB::table($schema_name.'.account_groups')->where('id',$expense->account_group_id)->first(); 
                                                 echo isset($group) ? $group->name : '';
                                                ?>
                                            </td>

                                            <td data-title="Sum">
                                                <?php
                                               // $open_bal=0;
                                                $open_bal = isset($expense->open_balance) > 0 ? $expense->open_balance : 0;
                                                if ($id == 2 && $expense->name == 'Unearned Revenue') {
                                                    $unearned = \DB::table($schema_name.'.advance_payments')->sum('amount') - \DB::table($schema_name.'.advance_payments_invoices_fees_installments')->sum('amount');
                                                    echo money($unearned);
                                                } else if ($id == 4) {
                                                    if (preg_match('/EC-1001/', $expense->code) && isset($expense->predefined) && (int) $expense->predefined > 0) {
                                                        //this is employer contribution, so lets check the code
                                                            $pension = \DB::table($schema_name.'.salary_pensions')->where('pension_id', $expense->predefined)->sum('employer_amount');
                                                            echo money($pension + $expense->expenses()->where('date', '>=', date("Y-m-d", strtotime($from_date)))->where('date', '<=', date("Y-m-d", strtotime($to_date)))->sum('amount'));
                                                        } else {
                                                        echo money($expense->expenses()->where('date', '>=', date("Y-m-d", strtotime($from_date)))->where('date', '<=', date("Y-m-d", strtotime($to_date)))->sum('amount'));
                                                    }
                                                } else if ($id == 5) {
                                                    $new_account = new \App\Http\Controllers\expense();
                                                      $total_current_assets = \collect(DB::SELECT('SELECT sum(coalesce(amount,0)) as total_current from ' . set_schema_name() . ' current_asset_transactions WHERE refer_expense_id=' . $expense->id . '  and "date" >= ' . "'$from_date'" . ' AND "date" <= ' . "'$to_date'" . ''))->first();

                                                    if (strtoupper($expense->name) == 'CASH') {
                                                        //$amount_=$expense->expenses()->sum('amount');

                                                        $total_current_assets_cash = $new_account->getCashtransactions($from_date, $to_date, 1);

                                                        $total_amount = $total_current_assets_cash->amount + $total_current_assets->total_current;
                                                       
                                                    } elseif (strtoupper($expense->name) == 'ACCOUNT RECEIVABLE') {

                                                        //$bank_opening_balance = \collect(DB::select('select sum(coalesce(opening_balance,0)) as opening_balance from ' . set_schema_name() . ' bank_accounts'))->first()->opening_balance;

                                                        $total_receivable= \collect(DB::select('select sum(a.balance + coalesce((c.amount-c.due_paid_amount),0)) as total_amount from ' . set_schema_name() . ' invoice_balances a join ' . set_schema_name() . ' student b on b.student_id=a.student_id LEFT JOIN ' . set_schema_name() . ' dues_balance c on c.student_id=b.student_id WHERE  a.balance <> 0.00 AND a."created_at" >= \'' . $from_date . '\' AND a."created_at" <= \'' . $to_date . '\''))->first();                  


                                                        $total_amount = $total_receivable->total_amount + $total_current_assets->total_current;
                                                       
                                                    } else {
                                                       // $total_bank = \collect(DB::SELECT('SELECT sum(coalesce(amount,0)) as total_bank from ' . set_schema_name() . ' bank_transactions WHERE bank_account_id=' . $expense->predefined . ' and payment_type_id <> 1 and "date" >= ' . "'$from_date'" . ' AND "date" <= ' . "'$to_date'" . ''))->first();

                                                       // $total_current_assets = \collect(DB::SELECT('SELECT sum(coalesce(amount,0)) as total_current from ' . set_schema_name() . ' current_asset_transactions WHERE refer_expense_id=' . $expense->id . ' '))->first();


                                                        //$total_amount = $total_bank->total_bank + $total_current_assets->total_current + $bank_open_bal; 
                                                    $bank=\App\Model\BankAccount::find($expense->predefined);
                                                    $transfered_sum =\collect(DB::SELECT('SELECT sum(coalesce(amount,0)) as total_transfer from ' . set_schema_name() . ' current_asset_transactions WHERE refer_expense_id=' . $expense->id . ' and "date" >= ' . "'$from_date'" . ' AND "date" <= ' . "'$to_date'" . ''))->first();
                                  
                                                    $bank_trans_sum = \collect(DB::SELECT('SELECT sum(coalesce(amount,0)) as total from ' . set_schema_name() . ' bank_transactions WHERE bank_account_id=' . $expense->predefined . ' and payment_type_id <> 1 and "date" >= ' . "'$from_date'" . ' AND "date" <= ' . "'$to_date'" . ''))->first();
                                                
                                                    $open_bal = isset($bank) && !empty($bank)? $bank->opening_balance : 0; 
                                                
                                                    $total_amount=$bank_trans_sum->total+ $total_current_assets->total_current + $open_bal;
                      
                                                    }
                                                
                                                    echo money($total_amount);
                                                } elseif (strtoupper($expense->name) == 'DEPRECIATION') {

                                                    $total_depreciation = \collect(DB::select('select sum(coalesce(amount,0)* coalesce(a.depreciation,0)* coalesce((\'' . $to_date . '\'::date-a.date::date),0)/365) as deprec,sum(coalesce(b.open_balance::numeric,0)* coalesce(a.depreciation,0) * coalesce((\'' . $to_date . '\'::date-b.create_date::date),0)/365) as deprec_open_balance from ' . set_schema_name() . 'expense a join ' . set_schema_name() . 'refer_expense b  on b.id=a.refer_expense_id where b.financial_category_id=4 AND a.date  <= \'' . $to_date . '\''))->first();

                                                    $total_amount = $total_depreciation->deprec + $total_depreciation->deprec_open_balance;
                                                    //$open_balance=$total_depreciation->open_balance;

                                                    echo money($total_amount);
                                                } else {
                                                     $expenses = \DB::table($schema_name.'.expense')->where('refer_expense_id',$expense->id);
                                                    echo !empty($expenses->get()) ? money($expenses->where('date', '>=', date("Y-m-d", strtotime($from_date)))->where('date', '<=', date("Y-m-d", strtotime($to_date)))->sum('amount') + $open_bal) : $open_bal;
                                                }
                                                ?>
                                            </td>

                                            <td data-title="Opening Balance">
                                                <?php
                                                echo money($open_bal);
                                                ?>
                                            </td>
                                            <td data-title="Descriptions">
                                                <?php echo $expense->note; ?>
                                            </td>

                                                <td data-title="Action">
                                                    <?php
                                                    if ($id == 2 && $expense->name == 'Unearned Revenue') {
                                                        echo btn_view('invoices/wallet', 'View');
                                                    } else {
                                                        // echo btn_view('expense/view_expense/' . $expense->id . '/'.$global_id, $data->lang->line('view'));

                                                        echo btn_view('accounts/view_expense/' . $expense->id . '/' . $id . '/' . $expense->predefined .'/'.$schema_name, 'View');
                                                    }
                                                    ?>
                                                 </td>
                                             </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>
</div>

@endsection