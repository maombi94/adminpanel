<style>
.card-counter{
    box-shadow: 2px 2px 10px #dadada;
    margin: 5px;
    padding: 20px 10px;
    background-color: #fff;
    height: 100px;
    border-radius: 5px;
    transition: .3s linear all;
  }

  .card-counter:hover{
    box-shadow: 0px 10px 40px rgba(25, 185, 154, 0.5);
    transition: .3s linear all;
  }

  .card-counter i{
    font-size: 3em;
    opacity: 0.2;
    color: #19b99a;
  }

  .card-counter .count-numbers{
    position: absolute;
    right: 35px;
    top: 20px;
    font-size: 22px;
    display: block;
  }

  .card-counter .count-name{
    position: absolute;
    right: 35px;
    top: 65px;
    font-style: italic;
    text-transform: capitalize;
    opacity: 0.5;
    display: block;
    font-size: 18px;
  }
  .profile_title{background:#F5F7FA;border:0;padding:7px 0;display:-webkit-box;display:-ms-flexbox;display:flex}ul.stats-overview{border-bottom:1px solid #e8e8e8;padding-bottom:10px;margin-bottom:10px}ul.stats-overview li{display:inline-block;text-align:center;padding:0 15px;width:30%;font-size:14px;border-right:1px solid #e8e8e8}ul.stats-overview li:last-child{border-right:0}ul.stats-overview li .name{font-size:12px}ul.stats-overview li .value{font-size:14px;font-weight:bold;display:block}ul.stats-overview li:first-child{padding-left:0}ul.project_files li{margin-bottom:5px}ul.project_files li a i{width:20px}.project_detail p{margin-bottom:10px}.project_detail p.title{font-weight:bold;margin-bottom:0}
</style>
<div class="card">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between bg-dark">
        <h3 class="card-title text-white"><i class="fa fa-sitemap"></i> expense_revenue') ?></h3>

        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= url("dashboard/index") ?>"><i class="fa fa-laptop"></i> menu_dashboard') ?></a></li>
            <li class="breadcrumb-item active">menu_account') ?></li>
        </ol>
    </div>
     <div class="card-body">
    <div class="row">
        <div class="col-sm-12">
            <form style="" class="form-horizontal list-group-item list-group-item-warning" role="form" method="post"> 
                <div class="col-md-3">
                    <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 ">start_date') ?></label>
                        <div class=" col-sm-12">
                            <input type="text" required="true" class="form-control calendar" id="from_date" name="from_date" value="" autocomplete="off">
                        </div>
                    </div>
                </div>


                <div class="col-md-3">
                    <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 col-xs-12">end_date') ?></label>
                        <div class="col-sm-12">
                            <input type="text" required="true" class="form-control calendar" id="to_date" name="to_date" value="">
                        </div>
                    </div>
                </div>                     


                <div class="col-md-4">
                    <div class='form-group row' >
                        <label for="class_level_id" class="col-form-label col-md-5 col-sm-5">
                            type') ?>
                        </label>
                        <div class="col-sm-12">
                            <?php
                            $array = array("0" => $data->lang->line("type"));
                            $array['1'] = 'Expenses';
                            $array['2'] = 'Payments';
                            $array['3'] = 'Revenue';
                            $fees = \App\Model\Fee::orderBy('priority', 'ASC')->get();
                            foreach ($fees as $fee) {
                                    $array[$fee->id.','.$fee->name] = $fee->name;
                            }
                            echo form_dropdown("report_type", $array, old("report_type"), "id='class_level_id' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-4 col-xs-12 col-form-label">
                            <?php echo form_error($errors, 'classlevel'); ?>
                        </span>
                    </div> 
                </div>

                <div class="col-md-2">
                    <div class="form-group row">
                        <label for="class_level_id" class="col-form-label col-md-2 col-sm-2">
                            <br/>
                        </label>
                        <div class="col-sm-12">
                            <br/> <?= csrf_field() ?>
                            <input type="submit" class="btn btn-success" value="Submit">
                        </div>
                    </div>
                </div> 


            </form>

        </div>
    </div>
        <br/>
        

        <h2>summary') ?></h2>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 ">
                <div class="card-counter">
                <i class="fa fa-file-o"></i>
        <span class="count-numbers"><?= $no_invoice ?></span>
        <span class="count-name">no_invoice') ?></span>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 ">
                <div class="card-counter">
                <i class="fa fa-credit-card"></i>
                    <span class="count-numbers"><?= $no_payments ?></span>
                    <span class="count-name">no_payments') ?></span>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 ">
                <div class="card-counter">
                <i class="fa fa-envelope"></i>
                    <span class="count-numbers"><?= $payments_received ?></span>
                    <span class="count-name">payments_received') ?></span>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 ">
                <div class="card-counter">
                <i class="fa fa-archive"></i>
                    <span class="count-numbers"><?= $revenue_received ?></span>
                    <span class="count-name">revenue_received') ?></span>
                </div>
            </div>
        </div>
<br/>
        <div class="row">
            <div class="col-md-12">
                <div class="card">


                    <div class="card-body">

                        <div class="col-md-9 col-sm-9">

                            <ul class="stats-overview">
                                <li>
                                    <span class="name">today_collect') ?></span>
                                    <span class="value text-success"> <?= $siteinfos->currency_symbol . ' ' . money($today_amount->sum) ?> </span>
                                </li>
                                <li>
                                    <span class="name">week_collect') ?></span>
                                    <span class="value text-success"><?= $siteinfos->currency_symbol . ' ' . money($weekly_amount->sum) ?> </span>
                                </li>
                                <li class="hidden-phone">
                                    <span class="name"> month_collect') ?> </span>
                                    <span class="value text-success"> <?= $siteinfos->currency_symbol . ' ' . money($monthly_amount->sum) ?></span>
                                </li>


                            </ul>
                            <br>

                            <div id="mainb">

                                <script type="text/javascript">
                                    $(function () {
                                        $('#container').highcharts({
                                            data: {
                                                table: 'datatable'
                                            },
                                            chart: {
                                                type: 'column'
                                            },
                                            title: {
                                                text: 'Expense and Revenue Per Month '
                                            },
                                            yAxis: {
                                                allowDecimals: false,
                                                title: {
                                                    text: 'Amounts (<?= $siteinfos->currency_symbol ?>)'
                                                }
                                            },
                                            legend: {
                                                enabled: false
                                            },
                                            plotOptions: {
                                                series: {
                                                    borderWidth: 0,
                                                    dataLabels: {
                                                        enabled: true,
                                                        format: '{point.y:.1f}'
                                                    }
                                                }
                                            },
                                            tooltip: {
                                                formatter: function () {
                                                    return '<b>' + this.series.name + '</b><br/>' +
                                                            this.point.y + ' ' + this.point.name.toLowerCase();
                                                }
                                            }
                                        });
                                    });
                                </script>
                                <script src="<?= url('public/assets/js/highchart.js') ?>"></script>
                                <script src="<?= url('public/assets/js/exporting.js') ?>"></script>
                                <script src="<?= url('public/assets/js/data.js') ?>"></script>

                                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

                                <table id="datatable" style="display: none">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>expense') ?></th>
                                            <th>revenue') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($revenue as $value) { ?>
                                            <tr>
                                                <th><?= date('M', strtotime($value->date)) ?></th>
                                                <td><?= $value->expense ?></td>
                                                <td><?= $value->revenue ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                            </div>
                            <div class="row">
                                <script type="text/javascript">
                                    $(function () {
                                        $('#container2').highcharts({
                                            chart: {
                                                plotBackgroundColor: null,
                                                plotBorderWidth: null,
                                                plotShadow: false,
                                                type: 'pie'
                                            },
                                            title: {
                                                text: 'Browser market shares January, 2015 to May, 2015'
                                            },
                                            tooltip: {
                                                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                            },
                                            plotOptions: {
                                                pie: {
                                                    allowPointSelect: true,
                                                    cursor: 'pointer',
                                                    dataLabels: {
                                                        enabled: true,
                                                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                        style: {
                                                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                        }
                                                    }
                                                }
                                            },
                                            series: [{
                                                    name: 'Brands',
                                                    colorByPoint: true,
                                                    data: [{
                                                            name: 'Microsoft Internet Explorer',
                                                            y: 56.33
                                                        }, {
                                                            name: 'Chrome',
                                                            y: 24.03,
                                                            sliced: true,
                                                            selected: true
                                                        }, {
                                                            name: 'Firefox',
                                                            y: 10.38
                                                        }, {
                                                            name: 'Safari',
                                                            y: 4.77
                                                        }, {
                                                            name: 'Opera',
                                                            y: 0.91
                                                        }, {
                                                            name: 'Proprietary or Undetectable',
                                                            y: 0.2
                                                        }]
                                                }]
                                        });
                                    });
                                </script>
                                <!--<div id="container2" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>-->

                            </div>

                        </div>

                        <!-- start project-detail sidebar -->
                        <div class="col-md-3 col-sm-3 col-xs-12">

                            <section class="card">
                                <div class="alert alert-default">
                                    <div class="card-title">
                                        <h2>project_collect') ?></h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="card-body">
                                        <h3 class="green"><i class="fa fa-paint-brush"></i>revenue') ?></h3>
                                        <div class="project_detail">
                                            <p class="title">expected_amount') ?></p>
                                            <p><?= $siteinfos->currency_symbol . ' ' . money($expected_amount->sum) ?>/=</p>
                                            <p class="title">Collected Revenue</p>
                                            <p><?= $siteinfos->currency_symbol . ' ' . money($collected_amount->sum) ?>/=</p>
                                        </div>

                                    </div>
                                </div>
                                <div class="alert alert-link">
                                    <div class="card-title">
                                        <h2>expense_project') ?></h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="card-body">
                                        <h3 class="green"><i class="fa fa-paint-brush"></i>expense') ?></h3>
                                        <div class="project_detail">
                                            <p class="title">expected_amount') ?></p>
                                            <p><?= $siteinfos->currency_symbol . ' ' . money($expected_expense->sum) ?>/=</p>
                                            <p class="title">Amount Spent </p>
                                            <p><?= $siteinfos->currency_symbol . ' ' . money($expense->sum) ?>/=</p>
                                        </div>

                                    </div>
                                </div>
                            </section>

                        </div>
                        <!-- end project-detail sidebar -->

                    </div>
                </div>
            </div>
        </div>
    </div>
     </div>
</div>
