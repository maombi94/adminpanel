@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">

        <form style="" class="form-horizontal" role="form" method="post">
            <div class="row">
                <div class="col-sm-12 col-xl-3 m-b-30">
                      <h4 class="sub-title">School</h4>
                      <select id="schema" name="schema" class="form-control form-control-primary">
                          <option value="" disabled selected>Select school</option>
                          <option value="allSchema">All schools</option>
                              @foreach (load_schemas() as $school)
                                  <option value="{{$school->username}}">{{$school->username}}</option>
                              @endforeach
                      </select>
                  </div>
               
                  <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form1">
                      <h4 class="sub-title">Start Date</h4>
                      <div class=" col-xs-12">
                        <input type="date" required="true" class="form-control calendar" id="from_date" name="from_date" value="" autocomplete="off">
                      </div>
                  </div>
                  
                  <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                      <h4 class="sub-title">End Date</h4>
                      <div class=" col-xs-12">
                        <input type="date" required="true" class="form-control calendar" id="to_date" name="to_date" value="" autocomplete="off">
                      </div>
                  </div>

                  <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                    <h4 class="sub-title"><br> </h4>
                    <div class=" col-xs-12">
                        <input type="submit" class="btn btn-success submit_btn_custom" value="Submit">
                    </div>
                    </div>
                     
                  <?= csrf_field() ?>
              </form>
            </div>
      
<div class="box">
    {{-- <div class="box-header">
        <h3 class="box-title"><i class="fa icon-expense"></i> <?php
            if ($id == 4) {
                echo $data->lang->line('panel_title');
            } elseif ($id == 1) {
                echo "Fixed Assets";
            } else if ($id == 2) {
                echo "Liabilities";
            } else if ($id == 3) {
                echo "Capital Management";
            } else if ($id == 5) {
                echo 'Current Assets';
            }
            ?></h3>


        <ol class="breadcrumb">
            <li><a href="<?= url("dashboard/index") ?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
            <li class="active"> <?php
                if ($id == 4) {
                    echo "Office Equipment And Fittings";
                } elseif ($id == 1) {
                    echo "Fixed Assets";
                } else if ($id == 2) {
                    echo "Liabilities";
                } else if ($id == 3) {
                    echo "Capital Management";
                } else if ($id == 5) {
                    echo 'Current Assets';
                }
                ?></li>
        </ol>
    </div><!-- /.box-header --> --}}
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <?php $usertype = session("usertype");?>
                    <h5 class="page-header">
                        <?php
                        if (isset($refer_expense_name) && !empty($refer_expense_name)) {
                            echo ucwords($refer_expense_name);
                        }                       
                        ?>
                    </h5>
                    {{-- <h5 class="page-header">
                            <a class="btn btn-success" href="<?php echo url('expense/add/' . $id . '/' . $refer_id . '') ?>">
                                <i class="fa fa-plus"></i> 
                                Add
                            </a>
                    </h5> --}}
                    
                 <?php if ($period==1) { ?>
                    <div id="hide-table">
                
                 <table id="dom-jqry" class="table table-striped table-bordered dataTable">
                            <thead>
                                <tr>
                                    <th class="col-sm-1">#</th>
                                      <th class="col-sm-2">Name</th>
                                      <th class="col-sm-2">Date</th>
                                      <th class="col-sm-1">Amount</th>                               
                                      <th class="col-sm-2">Descriptions</th>
                                    
                                        @if (isset($depreciation)) 
                                                <th class="col-sm-2">Asset Name</th>         
                                        @else       
                                                <th class="col-sm-2">Payment Method</th>       
                                        @endif
                                        
                                    <th class="col-sm-2">Reference Number</th>
                                    {{-- <?php if (can_access("delete_expense") || can_access('edit_expense')) { ?> --}}
                                        <th class="col-sm-2">Action</th>
                                    {{-- <?php } ?>         --}}
                                </tr>
                            </thead>
                            <tbody>
                                <?php $total_expense = 0;
                                $i=1;
                                if (isset($expenses) && !empty($expenses)) {
                                    $i = 1;
                                    foreach ($expenses as $expense) {
                                        
                                        ?>
                                        <tr>
                                            <td data-title="#">
                                                <?php echo $i; ?>
                                            </td>
                                             <td data-title="Recipient">
                                          <?php echo isset($expense->recipient) ? $expense->recipient:''; ?>
                                            </td>

                                            <td data-title="Date">
                                                <?php echo date("d M Y", strtotime($expense->date)); ?>
                                            </td>

                                            <td data-title="Amount">
                                                <?php echo money($expense->amount); ?>
                                            </td>


                                            <td data-title="Description">
                                                <?php echo $expense->note; ?>
                                            </td>

                                        <?php if (isset($depreciation)){ ?>
                                           <td data-title="Asset Name">
                                            <?php echo $expense->name; ?>
                                            </td>      
                                                
                                            <?php } else { ?>
                                                
                                         <td data-title="Payment Method">
                                          <?php 
                                             $exp=isset($expense->payment_type_id) ? \DB::table($schema_name.'.payment_types')->where('id',$expense->payment_type_id)->first() :[];
                                             echo isset($exp) ? $exp->name : '';
                                         ?>
                                            </td>        
                                           <?php }?>
                                            
                                            <td data-title="Reference Number">
                                                <?php
                                                if (isset($expense->transaction_id)) {
                                                    echo $expense->transaction_id;
                                                } else {
                                                    //echo $expense->ref_no;
                                                }
                                                ?>
                                            </td>
                    
                                                <td data-title="Action">
                                                    <?php
                                                    if($id !=5 && !isset($depreciation)) {          
                                                            if ((int) $expense->expenseID >0) { ?>
                                                                <?php  echo btn_edit('expense/edit/' . $expense->expenseID . '/' . $id . '/' . $schema_name, 'Edit')?>
                                                                <?php  echo btn_delete('expense/delete/' . $expense->expenseID . '/' . $id . '/' . $schema_name, 'Delete') ?>
                                                                <?php echo btn_add_pdf('expense/voucher/' . $expense->expenseID.'/'.$id . '/' . $schema_name, 'Payment Voucher'); ?>
                                                    <?php } } ?>
                                                </td>       
                                        </tr>
                                <?php $i++;
                                $total_expense += $expense->amount;
                                }
                             } 
                                if ( isset($current_assets) && !empty($current_assets) && $id==5) {
                                
                                    foreach ($current_assets as $current_asset) { ?>
                                        <tr>
                                            <td data-title="slno') ?>">
                                                 <?php echo $i; ?>
                                            </td>
                                             <td data-title="Name">
                                          <?php echo isset($current_asset->recipient) ? $current_asset->recipient:''; ?>
                                            </td>

                                            <td data-title="expense_date') ?>">
                                                <?php echo date("d M Y", strtotime($current_asset->date)); ?>
                                            </td>

                                                <td data-title="expense_amount') ?>">
                                                <?php echo money($current_asset->amount); ?>
                                                </td>

                                          
                                                <td data-title="expense_note') ?>">
                                                <?php echo $current_asset->note; ?>
                                                </td>

                                            <td data-title="payment_method') ?>">
                                                <?php echo isset($current_asset->payment_method) ? $current_asset->payment_method:''; ?>
                                            </td> 
                                            <td data-title="ref_no') ?>">
                                                <?php
                                                if (isset($current_asset->transaction_id)) {
                                                    echo $current_asset->transaction_id;
                                                } else {
                                                    //echo $expense->ref_no;
                                                }
                                                ?>
                                            </td>
                                            <?php if (can_access("delete_expense") || can_access('edit_expense')) { ?>
                                                <td data-title="action') ?>">                       
                                                <?php echo can_access("delete_expense") ? btn_delete('expense/delete/' . $current_asset->id . '/' . $id, 'delete') : '' ?>
                                                <?php echo btn_add_pdf('expense/voucher/' . $current_asset->id.'/'.$id, 'Payment Voucher'); ?>  
                                                </td>
                                            <?php } ?>
                                        </tr>
                            <?php $i++;
                            $total_expense += $current_asset->amount;
                            }
      
                    } ?> 
                                        
                <?php 
                    if ( isset($fees) && !empty($fees) && $id==5) {              
                       foreach ($fees as $fee) {        
                                    ?>
                                        <tr>
                                            <td data-title="slno') ?>">
                                                <?php echo $i; ?>
                                            </td>
                                             <td data-title="Name">
                                                <?php echo isset($fee->name) ? $fee->name:''; ?>
                                            </td>

                                            <td data-title="expense_date') ?>">
                                                 <?php echo date("d M Y"); ?>
                                            </td>

                                                <td data-title="expense_amount') ?>">
                                                <?php echo money($fee->total_amount); ?>
                                                </td>

                                                <td data-title="expense_note') ?>">
                                                <?php echo 'To be Collected from fees'?>
                                                </td>
                                                
                                                 <td data-title="payment_method') ?>">
                                                <?php echo isset($fee->payment_method) ? $fee->payment_method:''; ?>
                                            </td>
                                            </td> 
                                            <td data-title="ref_no') ?>">
                                                <?php
                                                if (isset($expense->transaction_id)) {
                                                    echo $expense->transaction_id;
                                                } else {
                                                    //echo $expense->ref_no;
                                                }
                                                ?>
                                            </td>
                                            <?php if (can_access("delete_expense") || can_access('edit_expense')) { ?>
                                                <td data-title="Action">Action</td>
                                            <?php } ?>

                                            </tr>
                                <?php $i++;
                                        $total_expense += $fee->total_amount;
                            } } ?> 
                                                                 
                    </tbody>
                            <tfoot>
                                <tr>
                                    <td>Total
                                         {{-- {{ $siteinfos->currency_symbol }} --}}
                                    </td>
                                    <td></td>                
                                    <td></td>
                                    <td><?php 
                                        if(isset($bank_id) && $bank_id>0) {
                                            $check_balance = \App\Model\BankAccount::find($bank_id);
                                                $bank_open_bal = isset($check_balance) && !empty($check_balance)  ? $check_balance->opening_balance : 0;
                                                $total_expense=$total_expense+$bank_open_bal;
                                 
                                             }                
                                    echo money($total_expense) . '/=' ?></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>            
                                </tr>
                            </tfoot>
                        </table>                         
                    </div>
                
                    <div class="col-sm-4 col-sm-offset-8 total-marg">
                        <div class="well well-sm">
                            <table style="width:100%; margin:0px;">
                                <tr>
                                    <td width="50%">
                                        <?php echo  "Total Expense : ";?>
                                    </td>
                                    <td style="width:50%;padding-left:10px">
                                        <?php
                                        echo money($total_expense) . " TK";
                                        ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <?php } ?> 
            </div>
        </div>
    </div>
</div>

@endsection