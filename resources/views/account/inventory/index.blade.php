@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">

        <form style="" class="form-horizontal" role="form" method="post">
            <div class="row">
                <div class="col-sm-12 col-xl-3 m-b-30">
                      <h4 class="sub-title">School</h4>
                      <select id="schema" name="schema" class="form-control form-control-primary">
                          <option value="" disabled selected>Select school</option>
                          <option value="allSchema">All schools</option>
                              @foreach (load_schemas() as $school)
                                  <option value="{{$school->username}}">{{$school->username}}</option>
                              @endforeach
                      </select>
                  </div>
    
                  <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                        <h4 class="sub-title"><br> </h4>
                        <div class=" col-xs-12">
                            <input type="submit" class="btn btn-success submit_btn_custom" value="Submit">
                        </div>
                  </div>
                     
                  <?= csrf_field() ?>
              </form>
            </div>
        </div>
@endsection