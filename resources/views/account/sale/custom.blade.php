
@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<style>

    #blinking {
        animation:blinkingText 1.2s infinite;
    }
    @keyframes blinkingText {
        0%{     color: #ff0000;    }
        49%{    color: #ff0000; }
        60%{    color: transparent; }
        99%{    color:transparent;  }
        100%{   color: #ff0000;    }
    }
    </style>
    <div class="page-body">
        <div class="card">
            <div class="card-block">
                <form style="" class="form-horizontal" role="form" method="post">
                    <div class="row">
                        <div class="col-sm-12 col-xl-3 m-b-30">
                              <h4 class="sub-title">School</h4>
                              <select id="schema" name="schema" class="form-control form-control-primary">
                                  <option value="" disabled selected>Select school</option>
                                      @foreach (load_schemas() as $school)
                                          <option value="{{$school->username}}">{{$school->username}}</option>
                                      @endforeach
                              </select>
                          </div>
                       
                          <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form1">
                              <h4 class="sub-title">Start Date</h4>
                              <div class=" col-xs-12">
                                <input type="date" required="true" class="form-control calendar" id="from_date" name="from_date" value="" autocomplete="off">
                              </div>
                          </div>
                          
                          <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                              <h4 class="sub-title">End Date</h4>
                              <div class=" col-xs-12">
                                <input type="date" required="true" class="form-control calendar" id="to_date" name="to_date" value="" autocomplete="off">
                              </div>
                          </div>
        
                          <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                            <h4 class="sub-title"><br> </h4>
                            <div class=" col-xs-12">
                                <input type="submit" class="btn btn-success submit_btn_custom" value="Submit">
                            </div>
                            </div>
                             
                          <?= csrf_field() ?>
                      </form>
                       
    
                        <div class="nav-tabs-custom">
                            <div class="tab-content">
                                <div id="all" class="tab-pane active">
                                    <div id="hide-table">
                                        <table id="dom-jqry" class="table table-striped table-bordered dataTable">
                                            <thead>
                                                <tr>
                                                    <th class="col-sm-1">#</th>
                                                    <th class="col-sm-2">Item Name</th>
                                                    <th class="col-sm-2">Total Quantity</th>
                                                    <th class="col-sm-2">Used Quantity</th>
                                                    <th class="col-sm-2">Remained Quantity</th>
                                                    <th class="col-sm-2">Status</th>
                                                    {{-- <th class="col-sm-2">Action</th> --}}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (!empty($items)) {
    
                                                    $i = 1;
                                                    foreach ($items as $item) {
                                                        ?>
                                                        <tr>
                                                            <td data-title="#">
                                                                <?php echo $i; ?>
                                                            </td>
    
                                                            <td data-title="Name">
                                                                <?php
                                                                echo $item->name;
                                                                ?>
                                                            </td>
    
                                                            <td data-title="Total quantity">
                                                                <?php echo $item->total_quantity; ?>
                                                            </td> 
                                                             <td data-title="Used quantity">
                                                                <?php echo $item->total_used; ?>
                                                            </td>  
                                                            <td data-title="Reamined quantity">
                                                                <?php echo $item->remained_quantity; 
                                                               ?>
                                                            </td>
                                                            
                                                            <td data-title="Status">
                                                                <?php 
                                                                if(($item->total_quantity-$item->total_used) > $item->alert_quantity){?>
                                                                    
                                                                <div class="alert alert-success" role="alert">
                                                                    Normal
                                                                </div>                                   
                                                                    <?php } else {?>
                                                                
                                                                <div class="alert alert-danger" role="alert" id="blinking">
                                                                    Below Quantity
                                                                </div>
                                                                    
                                                                <?php } ?>
                                                            </td>
                                                            
                                                            {{-- <td data-title="Action">
                                                                <?php
                                                                 echo btn_view('inventory/show/' . $item->product_alert_id . '/', 'View'); 
                                                                ?>
                                                            </td> --}}
                                                         </tr>
                                                                <?php
                                                                $i++;
                                                            }
                                                        }
                                                        ?>
                                            </tbody>
                                        </table>
                                    </div>
    
                                </div>
                            </div>
                        </div> 
                    </div>
                </div> 
            </div>
    <script type="text/javascript">
        $('#category').change(function () {
            var category = $(this).val();
    
            if (category === '') {
                alert('Select Category First');
            } else {
                $.ajax({
                    type: 'POST',
                    url: "<?= url('inventory/purchase_list') ?>",
                    data: "id=" + category,
                    dataType: "html",
                    success: function (data) {
                        window.location.href = data;
                    }
                });
            }
        });
    </script>
@endsection