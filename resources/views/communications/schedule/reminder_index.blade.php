
@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
<div class="page-body">
    <div class="card">
       <div class="row">
        <div class="col-sm-12">
            <!-- Material tab card start -->
            <div class="card">
        
                <div class="card-block">
                    <!-- Row start -->
                    <div class="row m-b-30">
                        <div class="col-lg-12 col-xl-12">
                            <div class="sub-title">

                                   <div class="col-sm-12 col-xl-4 m-b-30">
                                    <h5 class="sub-title">
                                        <?php if(isset($schema_name))  { ?>
                                                <?= school($schema_name)->name ?> 
                                                <?php } else { ?>
                                                School
                                            <?php } ?>
                                        </h5>
                                <select id="schema" class="form-control form-control-primary">
                                        <option value="opt1">Select One School</option>
                                            @if (count(load_schemas()) > 0)
                                            @foreach (load_schemas() as $schema)
                                                <option value="{{ $schema->username }}"> {{ school($schema->username)->name }}</option>
                                            @endforeach
                                            @endif
                                    </select>
                            </div>
                            </div>
                            <!-- Nav tabs -->
                             <?php if(isset($schema_name)) { ?>
                            <ul class="nav nav-tabs md-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#home3" role="tab">SMS &  Email remainder schedule</a>
                                    <div class="slide"></div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#profile3" role="tab">Letters and Notifications</a>
                                    <div class="slide"></div>
                                </li>
                              
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content card-block">
                                <div class="tab-pane active" id="home3" role="tabpanel">
                                   <div class="m-2">
                                      <a href="<?= url('sms/add_schedule/'.$schema_name) ?>" class="btn btn-sm btn-info">Schedule reminder </a>
                                   </div>
                                        <div class="table-responsive dt-responsive">
                                           <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                            <thead>
                                                <tr>
                                                    <th class="col-sm-1">#</th>
                                                    <th class="col-sm-2">Title</th>
                                                    <th class="col-sm-2">Message</th>
                                                    <th class="col-sm-2">day/date</th>
                                                    <th class="col-sm-3">Time</th>
                                                    <th class="col-sm-2">To Date</th>
                                                    {{-- <th class="col-sm-2">Sent To</th> --}}
                                                    <th class="col-sm-2">Users</th>
                                                    <th class="col-sm-2">Is repeated</th>
                                                    <th colspan="2">Action</th>
                                                </tr>
                                            </thead>
                                             
                                                 <tbody>
                                                    <?php
                                                    if (isset($reminders) && !empty($reminders)) {

                                                        $i = 1;
                                                        foreach ($reminders as $reminder) {
                                                            ?>
                                                            <tr>
                                                                <td data-title="<?= ('#') ?>">
                                                                    <?php echo $i; ?>
                                                                </td>
                                                                <td data-title="<?= ('title') ?>">
                                                                    <?php echo ucfirst($reminder->title) ?>
                                                                </td>
                                                                <td data-title="<?= ('message') ?>">
                                                                    <?php echo ucfirst($reminder->message); ?>
                                                                </td>

                                                                <td data-title="<?= ('date') ?>">
                                                                    <?php
                                                                   
                                                                    echo (int)date('Y', strtotime($reminder->date)) == 1970 ? $reminder->days : date('Y-m-d', strtotime($reminder->date));
                                                                    ?>
                                                                </td>
                                                                <td data-title="<?= ('date') ?>">
                                                                    <?php
                                                                    echo date('h:i', strtotime($reminder->time));
                                                                    ?>
                                                                </td>
                                                               
                                                                <td data-title="<?= ('date') ?>">
                                                                    <?php
                                                                    echo date('h:i', strtotime($reminder->last_schedule_date));
                                                                    ?>
                                                                </td>
                                                    
                                                                <td data-title="<?= ('users') ?>">
                                                                    <?php
                                                                    //Replace User Characters form Reminders table
                                                                     $user_ID = $reminder->user_id;
                                                                     $str = ['[',']','"'];
                                                                     $rplc =['','',''];
                                 
                                                                     $user_ids = str_replace($str,$rplc,$user_ID); 

                                                                   $usercount = count(explode(',', $user_ids));
                                                                   if((int)$usercount>1){
                                                                    $users = DB::table($schema_name.'.users')->whereIn('id', explode(',', $user_ids))->where('role_id',$reminder->role_id)->get();
                                                                   }else{
                                                                    $users = DB::table($schema_name.'.users')->where('id', $user_ids)->where('role_id',$reminder->role_id)->get();
                                                                   } 
                                                                   if($users){
                                                                        foreach ($users as $user) {
                                                                             ?>
                                                                        <span class="label label-success pull-right"><?= strtoupper($user->name) ?></span>
                                                                    <?php }
                                                                            }
                                                                        ?>
                                                                </td>
                                                                <td data-title="<?= ('is_repeated') ?>">
                                                                    <?php
                                                                    echo (int) $reminder->is_repeated == 1 ? 'Repeated' : 'One time';
                                                                    ?>
                                                                </td>
                                                                <td data-title="<?= ('action') ?>">

                                                                  
                                                                    <?php echo btn_delete('sms/deleteReminder/' .$schema_name .'/' . $reminder->id, ('delete')) ?>

                                                                </td>
                                                            </tr>
                                                            <?php
                                                            $i++;
                                                        }
                                                    }
                                                    ?>
                                                </tbody>
                                          
                                        </table>
                                     </div>
                                </div>


                                <div class="tab-pane" id="profile3" role="tabpanel">
                                      
                                      
                                    </div>
                                </div>
                              <?php } ?>
                            </div>
                        </div>


                    </div>
            
                </div>
            </div>
        
        </div>
    </div>
   </div>
</div>


<script type="text/javascript">
        $('#schema').change(function (event) {
        var schema_name = $(this).val();
        if (schema_name === '0') {
        } else {
            window.location.href = "<?= url('SMS/schedule') ?>/" + schema_name.replace(/\s+/g, '');
        }
    });
</script>
@endsection