<?php
$email = session('email');
$usertype = session('usertype');

$whatsapp = 0;
foreach ($sms_keys as $key) {
    if (isset($key->name) && $key->name == 'whatsapp') {
        $whatsapp = 1;
    }
}

?>
{{-- <style>
    /*Reply*/

    .reply {
        /*height: 60px;*/
        width: 100%;
        background-color: #f5f1ee;
        padding: 10px 5px 10px 5px !important;
        margin: 0 !important;
        z-index: 1000;
    }

    .reply-emojis {
        padding: 5px !important;
    }

    .reply-emojis i {
        text-align: center;
        padding: 5px 5px 5px 5px !important;
        color: #93918f;
        cursor: pointer;
    }

    .reply-recording {
        padding: 5px !important;
    }

    .reply-recording i {
        text-align: center;
        padding: 5px !important;
        color: #93918f;
        cursor: pointer;
    }

    .reply-send {
        padding: 5px !important;
    }

    .reply-send i {
        text-align: center;
        padding: 5px !important;
        color: #93918f;
        cursor: pointer;
    }

    .reply-main {
        padding: 2px 5px !important;
    }

    .reply-main textarea {
        width: 100%;
        resize: none;
        overflow: hidden;
        padding: 5px !important;
        outline: none;
        border: none;
        text-indent: 5px;
        box-shadow: none;
        height: 100%;
        font-size: 16px;
    }

    .reply-main textarea:focus {
        outline: none;
        border: none;
        text-indent: 5px;
        box-shadow: none;
    }


</style> --}}



<div class="row">
    <div class="col-sm-1"></div>
    <div class="col-sm-11">
        <?php
      //  $schema = str_replace('.', null, set_schema_name());

        $message_left = 0;
        ?>

        <div class="row">
            <div class="col-lg-8">
                <div>
                    <span id="channel_quick-sms" ></span>
                    <span id="channel_whatsapp" ></span>
                    <!--<span id="channel_telegram" ></span>-->
                    <span id="channel_phone-sms" ></span>
                    <!--<span id="channel_email" ></span>-->
                </div>
                <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
                    
                        <div class="form-group">

                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="form-field-select-0">
                                     Select Schools  <a><i class="fa fa-question-circle"  title="You can change this is SMS settings in main system settings by linking other phone or use internet message"></i></a>
                                </label>
                            </div>
                            <div class="col-sm-10">
                                 <select id="schema_name" class="js-example-basic-multiple form-control"  name ="schema_name[]" multiple="multiple">
                                    <option value="all">All Schools</option>
                                    @if (count(load_schemas()) > 0)
                                        @foreach (load_schemas() as $schema)
                                            <option value="{{ $schema->username }}">{{ $schema->username }} </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="has-error">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="form-field-select-0">
                                    {{trans('Channels')}}  <a><i class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="right" data-trigger="hover"  title="You can change this is SMS settings in main system settings by linking other phone or use internet message"></i></a>
                                </label>
                            </div>
                            <div class="col-sm-10">
                                 <select id="sms_keys_id" class="js-example-basic-multiple form-control"  name ="sms_keys_id[]" multiple="multiple">
                                 <option value="phone-sms">Phone SMS</option>
                                    <option value="quick-sms">Bulk SMS</option>
                                    <option value="whatsapp">WhatsApp</option>
                                    <option value="telegram">Telegram</option>
                                    <option value="email">Email</option>
                                </select>
                            </div>
                            <div class="has-error">
                            </div>
                        </div>

                        
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="form-field-select-0">Send SMS to</label>
                            </div>
                            <div class="col-sm-10">
                                 <select id="form-field-select-0" class="form-control"  name ="usertype" onchange="setFirstCriteria(this.value);">
                                    <option value=""></option>
                                    <option value="00">Parents</option>
                                    <option value="01">Teachers</option>
                                    <option value="03">Other user (Group)</option>
                                    <option value="02">Input Number</option>
                                </select>
                            </div>
                            <div class="has-error">
                                 <?php if (form_error($errors, 'criteria')): ?>
                                    <?php echo form_error($errors, 'criteria'); ?>
                                <?php endif ?>
                            </div>
                        </div>


                        <div class="form-group" id="custom_number_selection" style="display: none">
                            <div class="col-sm-4">
                                <label for="form-field-select-3">
                                    <?= ('write_Phone_numbers') ?>
                                </label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" id="tags_1" data-role="tagsinput" class="tags form-control"  name ="custom_numbers" placeholder="separate by comma or space"  />
                            </div>
                            <div class="has-error">
                                <?php if (form_error($errors, 'custom_numbers')): ?>
                                    <?php echo form_error($errors, 'custom_numbers'); ?>
                                <?php endif ?>
                            </div>
                        </div>
                        <div class="form-group" id="parents_selection"  style="display: none">
                            <div class="col-sm-4">
                                <label for="form-field-select-3">
                                    <?= ('select_criteria') ?>
                                </label>
                            </div>
                            <div class="col-sm-10">
                                <select id="form-field-select-3" class="form-control"  name ="criteria" onchange="setCriteria(this.value);">
                                    <option value="0"><?= ('all_parents') ?></option>
                                    <option value="1"><?= ('based_on_class') ?></option>
                                    <option value="2"><?= ('based_on_feetype') ?></option>
                                    <option value="3"><?= ('based_on_accounts') ?></option>
                                    <option value="5"><?= ('based_on_transport_routes') ?></option>
                                    <option value="6"><?= ('based_on_hostel') ?></option>
                                    <option value="4"><?= ('custom_selection') ?></option>
                                </select>
                            </DIV>
                            <div class="has-error">
                                <?php if (form_error($errors, 'criteria')): ?>
                                    <?php echo form_error($errors, 'criteria'); ?>
                                <?php endif ?>
                            </div>
                        </div>


                        <div class="form-group" id="teachers_selection"  style="display: none">
                            <div class="col-sm-4">
                                <label for="form-field-select-4">
                                    <?= ('teacher_type') ?>
                                </label>
                            </div>
                            <div class="col-sm-10">
                                <select id="form-field-select-4" class="form-control"  name ="teachersCriteria" onchange="setTeachersCriteria(this.value);">
                                    <option value="{{old('criteria')}}">Select</option>
                                    <option value="001"><?= trans('mailandsms_lang.all_teachers') ?></option>
                                    <option value="002"><?= trans('mailandsms_lang.based_on_class') ?></option>
                                    <option value="003"><?= trans('mailandsms_lang.custom_selection') ?></option>
                                </select>
                            </div>
                            <div class="has-error">
                                <?php if (form_error($errors, 'criteria')): ?>
                                    <?php echo form_error($errors, 'criteria'); ?>
                                <?php endif ?>
                            </div>
                        </div>


                        <div id="load_payment_amount" class="form-group" style="display: none">

                            <div class="form-group" class="col-sm-12">
                                <label class="col-sm-3">
                                    Select Amount Criteria
                                </label>
                                <label class="col-sm-3">
                                    Less Than   <input type="radio" id="optionsRadios1" name="less_than" value="1" >
                                </label>
                                <label class="col-sm-3">
                                    Greater than  
                                    <input type="radio" id="optionsRadios2" name="less_than"  value="0" > 
                                </label>
                                <label class="col-sm-3">
                                    Equals to 
                                    <input type="radio" id="optionsRadios3" name="less_than"  value="2" > 
                                </label>
                            </div>

                            <div class="col-sm-4">
                                <label for="amount" class="control-label">
                                    Specify Amount :
                                </label>
                            </div>
                            <div  class="col-sm-10">
                                <input type="number" name="amount" placeholder="Enter Amount Here" id= "amount" class="form-control"/>
                            </div>
                        </div>
                    
                    </div>
                    <div id="category6" class="form-group" style="display: none">

                        <div  class="col-sm-4">
                            <label for="load_transport" class="control-label">
                                Select Based On
                            </label>
                        </div>
                        <div  class="col-sm-10">
                            <select id="form-field-select-2" class="form-control" onchange="setCriteria(this.value);">
                                <option value="0">Select an Option</option>
                                <option value="9"><?= ('based_on_names') ?></option>
                                <option value="10"><?= ('based_on_phone_number') ?></option>
                            </select>
                        </div>
                    </div>
                  
                    <div class='form-group' >
                        <div class="col-sm-4">
                            <label>
                                 Template
                            </label>
                        </div>
                         <div class="col-sm-10">

                            <?php
                            $array = array(
                                'select' => ('mailandsms_select_template'),
                            );
                            $templates = DB::table('public.mailandsmstemplate')->where('status', '1')->get();
                            if (count($templates)) {
                                foreach ($templates as $etemplate) {
                                    strtolower($etemplate->type) == 'sms' ? $array[$etemplate->id] = $etemplate->name : '';
                                }
                            }
                            echo form_dropdown("template", $array, old("email_template"), "id='sms_template' data-type='content_area' class='js-example-basic-single  form-control'");
                            ?>

                        </div> 
                        <span class="col-sm-1 control-label">
                            <?php echo form_error($errors, 'sms_template'); ?>
                        </span>
                    </div>

                    <div class="form-group">

                        <div class="col-sm-10">
                            <textarea class="form-control" id="text_area_message" name="message" maxlength="1010" rows="6" placeholder="<?= ('message') ?>" ><?= old('sms_message') ?></textarea>
                            <br>
                            <button type="submit" class="btn btn-primary btn-sm" id="send_btn"> <?= ('send') ?> </button>
                       
                        <div class="col-sm-12">
                            <span><b id="word_counts">0</b>/<b id="sms_count">1</b></span>
                            <div>
                                <span class="badge bg-green" data-toggle="modal" data-target=".bs-keys-modal-lg">KEYS (harsh names) Guide</span>

                            </div>


                            <div id="account_tags" style="display: none">
                                <code style="color: green;">#name</code> =Parent Name, <code style="color: green;">#student_name </code>= Student Name, <code style="color: green;">#username </code>=parent username, <code style="color: green;">#balance </code>= requested amount a student needs to pay, <code style="color: green;">#invoice </code>=invoice number
                            </div>
                        </div>

                        <div class="has-error">
                            <?php if (form_error($errors, 'message')): ?>
                                <?php echo form_error($errors, 'message'); ?>
                            <?php endif ?>
                        </div>
                    </div>

                    </div> 
                    <?= csrf_field() ?>
                </form>
            </div>

            <div class="col-lg-4">
                <div class="col-md-12 col-sm-12  bg-white">
                    <div class="x_title">
                        <h2>Available Channels</h2>
                        <div class="clearfix"></div>
                    </div>
                    <table class="data table table-striped no-margin">
                        <thead>
                            <tr>

                                <th>Channel</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td> <img src="<?= asset('public/assets/images/whatsapp.jpg') ?>"  style="height:18px;width:24px; position: relative;top: 1px; left: 2px;"/>
                                    WhatsApp</td>
                                <td>
                                    <?php
                                    $action = 'Upgrade';
                                    if (isset($whatsapp) && $whatsapp == 1) {

                                        $client = DB::table('admin.clients')->where('username', $schema)->first();
                                        //check addons payments
                                  
                                        $addons = DB::table('admin.addons_payments')->where('client_id', $client->id)->where('end_date', '>=', date('Y-m-d H:i', time()))->first();
                                       
                                        if (!empty($addons)) {
                                            $status = '<b class="badge badge-success">Connected</b>'
                                                    . 'Expired on <b class="label label-warning">' . date('d M Y H:i', strtotime($addons->end_date)) . '</b>';
                                        } else {
                                            $status = '<b class="label label-danger">Expired</b>';
                                            $action = 'Add Payment';
                                        }
                                        echo $status;
                                        ?>

                                    <?php } else { ?>
                                        <b class="badge badge-danger">Not Connected</b>
                                    <?php } ?>

                                </td>
                                <td class="hidden-phone">
                                    <?php
                                    if (isset($whatsapp)) {
                                        ?>
                                        <span class="badge bg-green" data-toggle="modal" data-target=".bs-whatsapp-modal-lg">
                                            <?= $action ?></span>
                                    <?php } ?>
                                </td>

                            </tr>
                            <tr>
                                <td><img src="<?= asset('public/assets/images/telegram.png') ?>"  style="height:18px;width:24px; position: relative;top: 1px; left: 2px;"/> Telegram</td>
                                <td><b class="badge badge-success">Connected</b></td>
                                <td class="hidden-phone">
                                    <!--<a href="#" class="badge bg-info">View Info</a>-->
                                </td>

                            </tr>
                            <tr>
                                <td>Quick SMS</td>
                                <td><span id="message_left_count"><?= $message_left ?></span> Remains</td>
                                <td class="hidden-phone">
                                    <span class="badge bg-green" data-toggle="modal" data-target=".bs-example-modal-lg">
                                        Add More</span>
                                </td>

                            </tr>
                            <tr>
                                <td>Phone SMS</td>
                                <td><b id="phone_last_online"></b></td>
                                <td class="hidden-phone">

                                </td>

                            </tr>
                           
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <!--</div><!-- /.box-body -->
        <div class="col-sm-4"></div>
    </div>
    <!--</div>-->
</div>
    </div>
</div>
<script>


    $('#classesID').change(function (event) {
        var classesID = $(this).val();
        if (classesID === '0') {
            $('#load_section').val(0);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('student/sectioncall') ?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function (data) {
                    $('#sectionID').html(data);
                }
            });
        }
    });



    $('#student_type').change(function (event) {
        var types = $(this).val();
        if (types === '0') {
            $('#load_types').val(0);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('mailandsms/loadtypes') ?>",
                data: "type=" + types,
                dataType: "html",
                success: function (data) {
                    $('#load_types').html(data);
                }
            });
        }
    });
    
    function setFirstCriteria(value) {
        
        switch (value) {
            
            case '00':
                $('#parents_selection').show();
                $('#custom_number_selection,#show_user_group,#load_classes,#load_section,#load_student_types,#load_payment_status,#load_types,#teachers_selection,#teachersCat,#teachersPhones').hide();
                break;
            case '01':
                $('#custom_number_selection,#show_user_group,#load_classes,#load_section,#load_types,#load_student_types,#load_payment_status,#category6,#category8,#account_tags,#parents_selection').hide();
                $('#teachers_selection').show();
                break;
            case '02':
                $('#show_user_group,#load_classes,#load_section,#load_types,#load_student_types,#load_payment_status,#category6,#category8,#account_tags,#parents_selection,#teachers_selection').hide();
                $('#custom_number_selection').show();
                break;
            case '03':
                $('#custom_number_selection,#load_classes,#load_section,#load_types,#load_student_types,#load_payment_status,#category6,#category8,#account_tags,#parents_selection,#teachers_selection').hide();
                $('#show_user_group').show();
                break;
            default:
                
                //document.getElementById('category').style.display = "block";
                break;
        }
    }
    
    function setTeachersCriteria(value) {
        
        switch (value) {
            case '001':
                
                break;
            case '002':
                $('#load_classes,#load_section').show();
                break;
                
            case '003':
                $('#teachersCat').show();
                $('#load_classes,#load_section').hide();
                break;
                
            case '09':
                $('#teachersPhones').hide();
                $('#teachersNames').show();
                break;
            case '010':
                $('#teachersPhones').show();
                $('#teachersNames').hide();
                break;
                
            default:
                
                //document.getElementById('category').style.display = "block";
                break;
        }
    }
    
</script>
