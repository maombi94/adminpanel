
@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
<div class="page-body">
    <div class="card">
       <div class="row">
        <div class="col-sm-12">
            <!-- Material tab card start -->
            <div class="card">
        
                <div class="card-block">
                    <!-- Row start -->
                    <div class="row m-b-30">
                        <div class="col-lg-12 col-xl-12">
                            <div class="sub-title">

                                   <div class="col-sm-12 col-xl-4 m-b-30">
                                    <h5 class="sub-title">
                                        <?php if(isset($schema_name))  { ?>
                                                <?= school($schema_name)->name ?> 
                                                <?php } else { ?>
                                                School
                                            <?php } ?>
                                        </h5>
                                <select id="schema" class="form-control form-control-primary">
                                        <option value="opt1">Select One School</option>
                                            @if (count(load_schemas()) > 0)
                                            @foreach (load_schemas() as $schema)
                                                <option value="{{ $schema->username }}"> {{ school($schema->username)->name }}</option>
                                            @endforeach
                                            @endif
                                    </select>
                                </div>
                            </div>
                            <!-- Nav tabs -->
                             <?php if(isset($schema_name)) { ?>
                        
                            <!-- Tab panes -->
                            <div class="tab-content card-block">
                                <div class="tab-pane active" id="home3" role="tabpanel">
                                        <div class="table-responsive dt-responsive">
                                         <div class="status"></div>
                    
                                       <a class="btn btn-danger btn-sm pull-right delete_selected_invoices link" id="delete_selected_invoices" tags='' style="display: none" name="" 
                                        onclick="return confirm('you are about to delete selected SMS. This cannot be undone. are you sure? ')">Delete Selected <?= $type == 1 ? 'SMS' : 'Emails' ?></a>

                                           <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                            <thead>
                                                   <tr>
                                                        <th >#</th>
                                                        <th  class="col-sm-2">Name</th>
                                                        <th><?= $type == 1 ? 'Phone' : 'Email' ?></th>
                                                        <?= $type == 1 ? '' : '<th>Subject</th>' ?>
                                                        <th  class="col-sm-4"> Message</th>

                                                        <th>Status</th>
                                                        <?php if ($type == 1) { ?>
                                                            <th>Return Status</th>
                                                        <?php } ?>
                                                        <th>
                                                            Sent Date
                                                        </th>
                                                        <th>
                                                            <input type="checkbox"  name="all" id="toggle_all"> 
                                                        </th>
                                                    </tr>
                                            </thead>
                                             
                                                 <tbody>
                                                    <?php
                                                    if (isset($smss) && !empty($smss)) {

                                                        $i = 1;
                                                        foreach ($smss as $sms) {
                                                             $user = DB::table($schema_name.'.users')->where('id', $sms->user_id)->where('table', $sms->table)->first();
                                                                 $_id = $type == 1 ? $sms->sms_id : $sms->email_id;
                                                                 ?>
                                                            
                                                            <tr id="<?= $_id . '_' . $type ?>">

                                                            <td><?= $i ?></td>
                                                            <td>
                                                                <a><?= !empty($user) ? $user->name : '' ?></a>
                                                            </td>
                                                            <td>
                                                                <?= $type == 1 ? $sms->phone_number : $sms->email ?>

                                                            </td>
                                                            <?= $type == 1 ? '' : '<td>' . $sms->subject . '</td>' ?>
                                                            <td class="project_progress" style="max-width:10em">

                                                                <?= $sms->body ?>

                                                            </td>
                                                            <td class="project_progress">
                                                                <?= (int) $sms->status == 0 ? 'Pending' : 'Sent' ?>
                                                            </td>
                                                            <?php if ($type == 1) { ?>
                                                                <td class="project_progress">
                                                                    <?= $sms->return_code ?>
                                                                </td>
                                                            <?php } ?>

                                                            <td class="project_progress">
                                                                <?= timeAgo($sms->created_at) ?>
                                                            </td>
                                                            <td class="project_progress">
                                                                <?php
                                                                echo (int) $sms->status == 1 ? '<a  href="' . url("/sms/resend/" . $_id . "/" . $type) . '  " class="btn btn-info btn-sm"><i class="fa fa-pencil"></i> Resend </a>' :
                                                                        ' <input type="checkbox"  class="check" name="select[]" value="' . $_id . '">';
                                                                //'<a href="#" class="btn btn-danger btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="delete" onclick="return confirm(&quot;Are you sure you want to delete this message&quot;)" onmousedown="actionDelete(' . $_id . ',' . $type . ')"><i class="fa fa-trash-o"></i> Delete</a>'
                                                                ?>

                                                            </td>
                                                        </tr>
                                                <?php
                                                $i++;
                                            }
                                          }
                                        ?>
                                      </tbody>
                                          
                                     </table>
                                     </div>
                                </div>


                                <div class="tab-pane" id="profile3" role="tabpanel">
                                      
                                      
                                    </div>
                                </div>
                              <?php } ?>
                            </div>
                        </div>


                    </div>
            
                </div>
            </div>
        
        </div>
    </div>
   </div>
</div>


<script type="text/javascript">
        $('#schema').change(function (event) {
        var schema_name = $(this).val();
        if (schema_name === '0') {
        } else {
            window.location.href = "<?= url('SMS/sentItems') ?>/" + schema_name.replace(/\s+/g, '');
        }
    });


    search_checked = function () {

        $('#toggle_all').click(function () {
            $('.check').prop('checked', this.checked);
            if (this.checked == false) {
                $('#delete_all_invoices').hide();
            } else {
                $('#delete_all_invoices').show();
            }

        });

        $('.check').click(function () {
            var value = $(this).val();
            var status = $(this).is(':checked');
            //uncheck "select all", if one of the listed checkbox item is unchecked
            if (false == $(this).prop("checked")) { //if this item is unchecked
                $("#toggle_all").prop('checked', false); //change "select all" checked status to false
                $('#delete_all_invoices').hide();
            }
            //check "select all" if all checkbox items are checked

            if ($('.check:checked').length == $('.check').length) {
                $("#toggle_all").prop('checked', true);
                $('#delete_selected_invoices').hide();
                $('#delete_all_invoices').show();

            } else if ($('.check:checked').length != null) {
                $('#delete_selected_invoices').show();
            }

            if (status === true) {
                var text = $('#row' + value).html();
                $('#search_checked_table').show();
                $('#search_checked').after('<tr id="s_table' + value + '">' + text + '</tr>');
                var ex = $('.link').attr('tags');
                var url = '<?= url('sms/delete/'.$schema_name.'/bulk/' . $type) ?>/';
                var param = ex.split(",");
                param.push(value);
                $('#delete_selected_invoices').attr('tags', param.join(","));
                $('#delete_selected_invoices').attr('href', url + param.join(","));
                console.log(param);

            } else {

                var ex = $('.link').attr('tags');
                var url = '<?= url('sms/delete/'.$schema_name.'/bulk/' . $type) ?>';
                var param = ex.split(",");
                param = jQuery.grep(param, function (val) {
                    return val != value;
                });
                var arr = param;

                var result = arr.filter(function (elem) {
                    return elem != value;
                });
                console.log(result);

         
                $('#delete_selected_invoices').attr('tags', result.join(","));
                $('#delete_selected_invoices').attr('href', url + result.join(","));

                $('#s_table' + value).remove();
            }
        });
    }
    $(document).ready(search_checked);
</script>
@endsection