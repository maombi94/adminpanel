@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
<div class="page-body">
    <div class="card">
        <!-- Email-card start -->
        <div class="card-block email-card">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="mail-body">
                            <div class="mail-body-content">
                                      @include('communications.sms_mail.parent_sms_mail')
                            </div>
                       </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $('#sms_template').change(function () {
            var templateID = $(this).val();
            $.ajax({
                type: 'POST',
                url: "<?= url('SMS/getTemplateContent') ?>",
                data: "_token=" + "{{ csrf_token() }}" + "&templateID=" + templateID,
                dataType: "html",
                success: function (data) {
                    $('#text_area_message').val(data);
                }
            });

        })

        $("#sms_user").change(function () {
            var user = $(this).val();
            /*
             * This function will be upgraded to allow advanced option for each user
             * as follows
             */
            if (user == 'parent') {

            } else if (user == 'teacher') {

            }
            $.ajax({
                type: 'POST',
                url: "<?= url('mailandsms/alltemplate') ?>",
                data: "user=" + user + "&type=" + "sms",
                dataType: "html",
                success: function (data) {
                    $('#sms_template').html(data);
                }
            });
        });

     
    });
</script>
@endsection