@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
                               <!-- Page-body start -->
<div class="page-body">
    <div class="card">
        <!-- Email-card start -->
        <div class="card-block email-card">
            <div class="row">
                <div class="col-xl-3">
                    <div class="user-head row">
                        <div class="user-face">
                           <h5 class="">   <?=  isset($schema_name) ? school($schema_name)->name : 'Email/SMS' ?>  </h5>
                        </div>
                    </div>
                </div>
                <div class="col-xl-9">
                    <div class="card-body row">
                        <div class="col-md-12 row d-flex justify-content-between">
                              <form style="" class="form-horizontal" role="form" method="post">
                                    <div class="row">
                                        <div class="col-sm-12 col-xl-12 m-b-30">
                                            <select id="schema" class="form-control form-control-primary">
                                                <option value="opt1">Select One School</option>
                                                        @if (count(load_schemas()) > 0)
                                                        @foreach (load_schemas() as $schema)
                                                            <option value="{{ $schema->username }}"> {{ school($schema->username)->name }}</option>
                                                        @endforeach
                                                        @endif
                                                </select>
                                            </div>
                                      </div>
                                        <?= csrf_field() ?>
                                    </form>


                                 <form class="form-horizontal" role="form" method="post">
                                    <div class="row">
                                           <div class="col-sm-12 col-xl-12 m-b-30">
                                             <?php  $array[1] = 'Inbox'; $array[2] = 'Received SMS Only'; ?>
                                              <select  id="option" class="form-control form-control-primary">
                                                   <option>Choose option </option>
                                                    @foreach($array as $value)
                                                        <option value="{{ $value }}"> {{ $value }} </option>
                                                    @endforeach
                                              </select>
                                          </div>
                                      </div>
                                        <?= csrf_field() ?>
                                    </form>
                              </div>
                         </div>
                      </div>
                 </div>
            <div class="row">
                <!-- Left-side section start -->
                <div class="col-lg-12 col-xl-3">
                    <div class="user-body">
                        <div class="p-20 text-center">
                            <a href="<?= url('SMS/composeEmail')?>" class="btn btn-danger">Compose</a>
                        </div>

                        <?php if(isset($users)) {  ?>
                          <div class="card">
                            <div class="card-header">
                                <input type="text" class="form-control" id="search_id" name="name" value="<?= old('name') ?>" placeholder="Search name" >
                            </div>
                            <div class="card-block" style="max-height: 390px; overflow: scroll; padding: 7px">
                              <div id="search_result"></div>

                                <?php
                                  $i = 1;
                                   foreach ($users as $user) {?>
                                   <div class="job-cards">
                                    <div class="media">
                                        <a class="media-left media-middle" href="#">
                                            <img class="media-object m-r-4 m-l-4" style="width: 30px; height:30px" src="<?=$root?>/assets/images/browser/chrome.png" alt="Generic placeholder image">
                                        </a>
                                        <div class="media-body">
                                            <div class="company-name m-b-10">
                                              <p style="font-size: 14px;">  Name: <?= $user->name ?> </p>
                                              <h6> Role: <?= $user->usertype ?> </h6>
                                                <i class="text-muted f-14">Phone: <?= $user->phone ?></i></div>
                                            {{-- <p class="text-muted">it to make a type specimen book.</p> --}}
                                        </div>
                                    </div>

                                      <?php
                                        if ($i == 1) {
                                            $user_phone = $user->phone;
                                            $username = $user->name;
                                            $user_id = $user->id;
                                            $usertable = $user->table == 'parent' ? 'parents' : $user->table;
                                        } $i++;
                                        ?>
                                   </div>
                                  <?php   }  ?>
                    


                            </div>
                          </div>

                      <?php } ?>
                    </div>
                </div>
            
                <div class="col-lg-12 col-xl-9">
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="e-inbox" role="tabpanel">

                            <div class="">
                                <div class="mail-body-contentio">
                                    <div class="">
                                        <?php if ($id == 1) { ?> 
                                        <div class="table-responsive dt-responsive">
                                           <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                            <thead>
                                                <tr>
                                                    <th class="col-sm-1">#</th>
                                                    <th class="col-sm-2">Photo</th>
                                                    <th class="col-sm-2">Name</th>
                                                    <th class="col-sm-2">Phone</th>
                                                    <th class="col-sm-3">Message</th>
                                                    <th class="col-sm-2">Date</th>
                                                    <th colspan="2">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                @foreach ($reply_sms as $key => $sms)
                                                    <tr>
                                                    <td class="col-sm-1"><?= $key++?></td>
                                                    <td class="col-sm-2">
                                                        <?php
                                                        $array = array(
                                                            "src" => url('storage/uploads/images/' . $sms->photo),
                                                            'width' => '35px',
                                                            'height' => '35px',
                                                            'class' => 'img-rounded rotate'
                                                        );
                                                        echo img($array);
                                                        ?>
                                                    </td>
                                                    <td class="col-sm-2"><?= $sms->name ?></td>
                                                    <td class="col-sm-2"><?= $sms->from ?></td>
                                                    <td class="col-sm-3"><?= $sms->message ?></td>
                                                    <td class="col-sm-2"><?= $sms->date ?></td>
                                                    <td class="text-center">
                                                        <a href="#" class="btn btn-danger btn-sm btn-out-dotted btn-sm" onmousedown="delete_message('<?=$sms->id ?>')"> Delete </a>
                                                        <a href="#" class="btn btn-info btn-sm btn-out-dotted btn-sm" onmousedown="reply_message('<?= $sms->from ?>')"> reply </a>
                                                    </td>
                                                    </tr>  
                                                @endforeach
                                    
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th class="col-sm-1">#</th>
                                                    <th class="col-sm-2">Photo</th>
                                                    <th class="col-sm-2">Name</th>
                                                    <th class="col-sm-2">Phone</th>
                                                    <th class="col-sm-3">Message</th>
                                                    <th class="col-sm-2">Date</th>
                                                    <th class="col-sm-1">Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        </div>
                                        <?php } else { ?>

                                         {{-- <table class="table">
                                            <tr class="unread">
                                                <td>
                                                    <div class="check-star">
                                                        <div class="checkbox-fade fade-in-primary checkbox">
                                                            <label>
                                                                <input type="checkbox" value="">
                                                                <span class="cr"><i class="cr-icon icofont icofont-verification-check txt-primary"></i></span>
                                                            </label>
                                                        </div>
                                                        <i class="icofont icofont-star text-warning"></i>
                                                    </div>
                                                </td>
                                                <td><a href="email-read.htm" class="email-name">Google Inc</a></td>
                                                <td><a href="email-read.htm" class="email-name">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a></td>
                                                <td class="email-attch"><a href="#"><i class="icofont icofont-clip"></i></a></td>
                                                <td class="email-time">08:01 AM</td>
                                            </tr>
                                        </table> --}}

                                           

                                <div class="col-sm-12 table">
                                   <div class="inbox-body">
                                    <div>
                                        <h5>Sent SMS and Received SMS</h5>
                                        <?php if (isset($user_phone)) { ?>
                                            <div id="profile_name"><ul class="list-unstyled msg_list">
                                                    <li>
                                                        <span>
                                                            <span>&nbsp; <?= $username ?> &nbsp;</span> | <span><a href="<?= url($usertable == 'parent' ? 'parents' : $usertable . '/view/' . $user_id) ?>">View Profile</a></span>
                                                        </span>
                                                    </li>
                                                </ul>
                                            </div>
                                        <?php } ?>
                                        <!-- end of user messages -->
                                        <ul class="messages">
                                            <?php
                                            if (isset($user_phone)) {
                                                $messages = \DB::select('select sms_id,body, user_id, created_at, phone_number,1 as is_sent  from ' . $schema_name . '.sms where phone_number like \'%' . $user_phone . '%\' UNION ALL (select id as sms_id,message as body, device_id::integer as user_id, created_at,  "from" as phone_number,2 as is_sent from ' . $schema_name . '.reply_sms where "from" like \'%' . $user_phone . '%\')');
                                                foreach ($messages as $message) {
                                                    if ($message->is_sent == 1) {
                                                        ?>
                                                        <li>
                                                            <div class="message_date">
                                                                <h6 class="date text-info"><?= timeAgo($message->created_at) ?></h6>
                                                            </div>
                                                            <div class="message_wrapper">
                                                                <h4 class="heading"></h4>
                                                                <blockquote class="email-name"><?= $message->body ?></blockquote>
                                                                <br>

                                                            </div>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li>
                                                            <h3 class="date text-info text-right"><br/>
                                                                <?= timeAgo($message->created_at) ?></h3>
                                                            <br/>
                                        
                                                            <div class="message_wrapper text-right">
                                                                <h4 class="heading">
                                                                    <a href="<?= url($user->table == 'parent' ? 'parents' : $user->table . '/view/' . $user->id) ?>"><?= $user->name ?></a></h4>
                                                                <blockquote class="message text-right"><?= $message->body ?></blockquote>
                                                                <br>

                                                            </div>
                                                        </li>  

                                                        <?php
                                                    }
                                                }
                                                ?>
                                                <div id="inbox_message"></div>
                                                <li>
                                                    <p class="url" style="margin-left: 10%;">
                                                        <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                                        <textarea id="content" data-id="<?php echo $user_phone ?>" required="required" class="form-control" name="message" 
                                                            data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" placeholder="Write your SMS">
                                                        </textarea>
                                                        <a href="#" id="reply_sms_btn" class="btn btn-sm btn-success mt-1"><i class="fa fa-reply"></i> Reply</a>
                                                    </p>
                                                </li>
                                            <?php }
                                            ?>

                                                </ul>
                                                <!-- end of user messages -->
                                                </div>
                                            </div>
                                        </div>


                                         <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                   
                    </div>
                </div>
                <!-- Right-side section end -->
            </div>
        </div>
        <!-- Email-card end -->
    </div>
</div>
            
<script type="text/javascript">

    $('#schema').change(function (event) {
        var schema_name = $(this).val();
        if (schema_name === '0') {
        } else {
            window.location.href = "<?= url('SMS/inbox') ?>/" + schema_name.replace(/\s+/g, '');
        }
    });

    // $('#option'change(function (event) {
    //         var option = $(this).val();
    //         alert(option);
    //         $schema_name = '<?= $schema_name ?>';
    //         if (option === '0') {
    //         } else {
    //             window.location.href = "<?= url('SMS/inbox') ?>/" + schema_name+ '/' + option;
    //         }
    //     });
    
    //    search = function () {
    //     $('#search_id').keyup(function () {
    //         var wd = $(this).val();
    //         if (wd !== '') {
    //             $.ajax({
    //                 url: '<?= url('SMS/search/null') ?>',
    //                 dataType: 'html',
    //                 method: 'post',
    //                 data: {wd: wd},
    //                 success: function (data) {
    //                     $('#search_result').html(data);
    //                 }
    //             });

    //         }
    //     });
    // }


     delete_message = function (id) {
       var schema = '<?php echo $schema_name ?>';
        // console.log(schema);
        $.get('<?= url("SMS/destroy/null/") ?>', {id: id,schema:schema}, function (data) {
            if (data === '1') {
                $('#sms' + id).hide();
            }
        });
    }


      reply_sms = function () {
       var schema_name = '<?php echo $schema_name ?>';

        $('#reply_sms_btn').mousedown(function () {
            var content = $('#content').val();
            var data_id = $('#content').attr('data-id');
            console.log(data_id);
            $.ajax({
                url: '<?= url('SMS/create/null') ?>',
                dataType: 'html',
                method: 'post',
                data: {content: content, data_id: data_id, schema_name:schema_name},
                success: function (data) {
                    $('#inbox_message').append(data);
                    $('#content').val('');
                }
            })
        });
    }


     reply_message = function (phone) {
       var schema_name = '<?php echo $schema_name ?>';
     //  console.log($schema_name);
        swal({
            title: "Reply SMS to sender",
            text: "SMS will be sent to sender phone:",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            inputPlaceholder: "Write something"
        }, function (inputValue) {
            if (inputValue === false)
                return false;
            if (inputValue === "") {
                swal.showInputError("You need to write something!");
                return false
            }
            $.getJSON('<?= url('SMS/reply/null') ?>', {phone: phone, content: inputValue,schema:schema_name}, function (data) {
                if (data.success == '0') {
                    swal.showInputError("SMS failed to be sent. Please try again later!");
                    return false
                } else {
                    swal("Success!", "SMS sent to : " + inputValue, "success");
                }
            });

        });
    }

  $(document).ready(search);
  $(document).ready(delete_message);
  $(document).ready(reply_sms);

    </script>
                        
@endsection

