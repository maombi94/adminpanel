@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>

<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">

            
        <div class="row">
            <div class="col-sm-8">
                <h4 class="sub-title">Create</h4>
                 <div class="text-muted well well-sm no-shadow">
                  If you send certain information most of times,  SMS and Email template is a quick way to define a message so that you can reuse content 
                </div>
                <form role="form" method="post">
                     <div class="form-group row">
                        <div class="col-sm-8">
                            <label> Type </label>
                                 <?php  $array['SMS'] = 'SMS'; $array['Email'] = 'Email'; ?>
                               <select type="text" class="form-control" name="type" required>
                                  @foreach ($array as $value)
                                       <option value="<?= $value?>"> <?= $value ?></option>
                                  @endforeach
                              </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <label> Name </label>
                            <input type="text"  name="name" class="form-control" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <label>Tags </label>
                             <input type="text" class="form-control" value="#name ,#username, #default_password,#email,#phone,#role" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label>Template </label>
                            <input type="text" class="form-control form-control-lg" placeholder="" name="template" id="template" required>
                        </div>
                    </div>

                      <div class="form-group row">
                        <div class="col-sm-12">
                            <input type="hidden" class="form-control form-control-lg" value="<?= $schema_name?>" name="schema_name">
                            <input type="submit" class="btn btn-success" value="Submit" >
                        </div>
                    </div>
                     <?= csrf_field() ?>

                </form>
            </div>
            <div class="col-sm-4 mobile-inputs">
             <h4 class="sub-title">Template Example</h4>
            <p>Hello #name, your username is #username and your phone number is #phone</p>

                <div class="sub-title"></div>
                <h5><b>SMS to be delivered to John</b></h5>
                <div class="sub-title"></div>
                <p>Hello John Doe, your username is 123123 and your phone number is 0655406004</p>
            </div>
        </div>

        </div>
    </div>
   </div>

@endsection
