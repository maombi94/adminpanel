@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>

<div class="page-body">
    <div class="card">
        <div class="card-header">
            <a href="{{ url('users/create') }}" class="btn btn-success">Add User</a>
        </div>
        <div class="card-block">

            <?php 
                    $usertype = session("usertype");
                    if(can_access('add_character')) {
                ?>
            <h5 class="page-header">
                <a class="btn btn-success" href="<?php echo url('character_categories/add') ?>">
                    <i class="fa fa-plus"></i>
                    Add
                </a>
            </h5>
            <?php } ?>

            <center>
                <div class="row">
                    <div class="col-sm-8">
                        <form class="form-horizontal" role="form" method="post">

                            <?php
		    if (form_error($errors,'character_category_id'))
			echo "<div class='form-group has-error' >";
		    else
			echo "<div class='form-group' >";
		    ?>
                            <label for="character_category_id" class="col-sm-2 control-label">
                                character_category
                            </label>
                            <div class="col-sm-6">

                                <select name="character_category_id" id="character_category_id" class="form-control">
                                    <option value="">select_category</option>
                                    <?php foreach ($character_categories as $character_category) {
                        echo '<option value="'.$character_category->id.'">'.$character_category->character_category.'</option>';
                     } ?>
                                </select>
                            </div>
                            <span class="col-sm-4 control-label">
                                <?php echo form_error($errors,'character_category_id'); ?>
                            </span>
                    </div>

                    <?php 
            if(form_error($errors,'code')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                    <label for="classes" class="col-sm-2 control-label">
                        character_code
                    </label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="code" name="code" placeholder="character_code"
                            value="<?=old('code')?>">
                    </div>
                    <input type="checkbox" name="generate_code" value="Auto Generate"> auto_generate
                    <span class="col-sm-4 control-label">
                        <?php echo form_error($errors,'code'); ?>
                    </span>
                </div>
                <?php if(form_error($errors,'description')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                <label for="classes" class="col-sm-2 control-label">
                    character
                </label>
                <div class="col-sm-6">
                    <textarea type="text" class="form-control" id="description" name="description"
                        placeholder="Description" value="<?=old('description')?>"></textarea>
                </div>
                <span class="col-sm-4 control-label">
                    <?php echo form_error($errors,'description'); ?>
                </span>
        </div>
        <?php 
                        if(form_error($errors,'position')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                        ?>
        <label for="classes" class="col-sm-2 control-label">
            <?=$data->lang->line("position")?>
        </label>
        <div class="col-sm-6">
            <input type="text" class="form-control" id="position" name="position"
                placeholder="<?=$data->lang->line("position")?>" value="<?=old('position')?>" required>
        </div>
        <span class="col-sm-4 control-label">
            <?php echo form_error($errors,'position'); ?>
        </span>

    </div>
    <?php
    if (form_error($errors, 'class_id'))
        echo "<div class='form-group has-error' >";
    else
        echo "<div class='form-group' >";
    ?>
    <label for="classes" class="col-sm-2 control-label">
        menu_classes") ?>
    </label>
    <div class="col-sm-6">
        <?php
        $array = array("0" => $data->lang->line("all"));
        
        echo form_dropdown("classes[]", $array, old("classes"), "id='classes'  class='form-control select2_multiple' multiple");
        ?>
    </div>
    <span class="col-sm-4 control-label">
        <?php echo form_error($errors, 'description'); ?>
    </span>
</div>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-6">
        <input type="submit" class="btn btn-success btn-block" value="<?=$data->lang->line("save")?>">
    </div>
</div>

<?= csrf_field() ?>
</form>


</div>
</div>
</div>
</div>

@endsection
