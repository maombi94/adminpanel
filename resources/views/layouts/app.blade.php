<?php $root = url('/public/'); ?>

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>ShuleSoft Connect School Portal</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords"
        content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?=$root?>/images/shulesoft_logo.png" type="image/png">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?=$root?>/bower_components/bootstrap/css/bootstrap.min.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/icon/feather/css/feather.css">
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/icon/icofont/css/icofont.css">

    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/icon/themify-icons/themify-icons.css">

    <!-- Sweet alert -->
    <link rel="stylesheet" type="text/css" href="<?=$root?>/bower_components/sweetalert/css/sweetalert.css">

    
    <link rel="stylesheet" href="<?=$root?>/bower_components/select2/css/select2.min.css">

    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="<?=$root?>/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/pages/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$root?>/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">

    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/datatables/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/datatables/css/responsive.bootstrap4.min.css">
    <!-- radial chart -->
    <link rel="stylesheet" href="<?=$root?>/assets/pages/chart/radial/css/radial.css" type="text/css" media="all">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/css/jquery.mCustomScrollbar.css"> 

    <!-- jpro forms css -->
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/pages/j-pro/css/demo.css">
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/pages/j-pro/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/pages/j-pro/css/j-pro-modern.css">

    <!-- accounting styles -->
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/css/account_custom.css">

    <!-- select 2 -->
    <link rel="stylesheet" type="text/css" href="<?=$root?>/bower_components/select2/css/select2.min.css">

      <!-- summernote -->
    <link rel="stylesheet" type="text/css" href="<?=$root?>/bower_components/summernote/css/summernote-bs4.min.css">

    <!-- end accounting styles -->
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  --}}
      
    <script type="text/javascript" src="<?=$root?>/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/select2/js/select2.full.min.js"></script>
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">

            <nav class="navbar header-navbar pcoded-header">
                <div class="navbar-wrapper">

                    <div class="navbar-logo">
                        <a class="mobile-menu" id="mobile-collapse" href="#!">
                            <i class="feather icon-menu"></i>
                        </a>
                        <a href="{{ url('/') }}">
                            <img class="img-fluid" src="<?=$root?>/images/shulesoft_logo.png" alt="Theme-Logo" style="height: 35px">
                        </a>
                        <a class="mobile-options">
                            <i class="feather icon-more-horizontal"></i>
                        </a>
                    </div>

                    <div class="navbar-container container-fluid">
                        <ul class="nav-left">
                            <li class="header-search">
                                <div class="main-search morphsearch-search">
                                    <div class="input-group">
                                        <span class="input-group-addon search-close"><i
                                                class="feather icon-x"></i></span>
                                        <input type="text" class="form-control">
                                        <span class="input-group-addon search-btn"><i
                                                class="feather icon-search"></i></span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a href="#!" onclick="javascript:toggleFullScreen()">
                                    <i class="feather icon-maximize full-screen"></i>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav-right">
                            <li class="header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="feather icon-bell"></i>
                                        <span class="badge bg-c-pink">5</span>
                                    </div>
                                    <ul class="show-notification notification-view dropdown-menu"
                                        data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                        <li>
                                            <h6>Notifications</h6>
                                            <label class="label label-danger">New</label>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="d-flex align-self-center img-radius"
                                                    src="<?=$root?>/assets/images/avatar-4.jpg"
                                                    alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="notification-user">John Doe</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer
                                                        elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="d-flex align-self-center img-radius"
                                                    src="<?=$root?>/assets/images/avatar-3.jpg"
                                                    alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="notification-user">Joseph William</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer
                                                        elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="d-flex align-self-center img-radius"
                                                    src="<?=$root?>/assets/images/avatar-4.jpg"
                                                    alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="notification-user">Sara Soudein</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer
                                                        elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <!-- <li class="header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="displayChatbox dropdown-toggle" data-toggle="dropdown">
                                        <i class="feather icon-message-square"></i>
                                        <span class="badge bg-c-green">3</span>
                                    </div>
                                </div>
                            </li> -->
                            <li class="user-profile header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                        <img src="<?=$root?>/assets/images/avatar-4.jpg" class="img-radius"
                                            alt="User-Profile-Image">
                                        <span> {{ Auth::User()->name }}</span>
                                        <i class="feather icon-chevron-down"></i>
                                    </div>
                                    <ul class="show-notification profile-notification dropdown-menu"
                                        data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                        <li>
                                            <a href="{{ url('dashboard/usage') }}">
                                                <i class="feather icon-settings"></i> Settings
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ url('users/userinfo') }}">
                                                <i class="feather icon-user"></i> Profile
                                            </a>
                                        </li>
                                        <li>
                                        <a href="{{ url('users/userinfo') }}">
                                                <i class="feather icon-mail"></i> My Messages
                                            </a>
                                        </li>
                                        <!-- <li>
                                            <a href="auth-lock-screen.htm">
                                                <i class="feather icon-lock"></i> Lock Screen
                                            </a>
                                        </li> -->
                                        <li>
                                        <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                           document.getElementById('logout-form').submit();"><i class="feather icon-lock"></i> Logout</a></li>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                 
                                    </ul>

                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <!-- Sidebar inner chat end-->
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <nav class="pcoded-navbar">
                        <div class="pcoded-inner-navbar main-menu">
                            <!-- <div class="pcoded-navigatio-lavel">Navigation</div> -->
                            <ul class="pcoded-item pcoded-left-item">
                            <li class="pcoded-hasmenu pcoded-trigger">
                                    <a href="{{ url('dashboard/index') }}">
                                        <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                                        <span class="pcoded-mtext">Dashboard</span>
                                    </a>
                                    <!-- <ul class="pcoded-submenu active">
                                        <li class="">
                                            <a href="{{ url('dashboard/index') }}">
                                                <span class="pcoded-mtext">Default</span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="{{ url('dashboard/index') }}">
                                                <span class="pcoded-mtext">CRM</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="{{ url('dashboard/index') }}">
                                                <span class="pcoded-mtext">Analytics</span>
                                                <span class="pcoded-badge label label-info ">NEW</span>
                                            </a>
                                        </li>
                                    </ul> -->
                                </li>

                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-users"></i></span>
                                        <span class="pcoded-mtext">All Users</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                       <?php if (can_access('view_students')) { ?>
                                        <li class="">
                                            <a href="{{ url('Users/student') }}">
                                                <span class="pcoded-mtext">Students</span>
                                            </a>
                                        </li>
                                        <?php } ?>

                                        <?php if(can_access('view_parents')) { ?>
                                        <li class=" ">
                                        <a href="{{ url('Users/parent') }}">
                                                <span class="pcoded-mtext">Parents</span>
                                            </a>
                                        </li>
                                        <?php } ?>

                                        <?php if(can_access('view_teachers')) { ?>
                                        <li class="">
                                        <a href="{{ url('Users/teacher') }}">
                                                <span class="pcoded-mtext">Teachers</span>
                                            </a>
                                        </li>
                                        <?php } ?>

                                        <?php if(can_access('view_staff')) { ?>
                                        <li class="">
                                        <a href="{{ url('Users/user') }}">
                                                <span class="pcoded-mtext">Staffs</span>
                                            </a>
                                        </li>
                                        <?php } ?>

                                        <?php if(can_access('view_overall')) { ?>
                                          <li class="">
                                            <a href="{{ url('Users/overall') }}">
                                                    <span class="pcoded-mtext">Over all</span>
                                                </a>
                                           </li>
                                        <?php } ?>
                                    </ul>
                                </li>


                              <?php if (can_access('view_operations')) { ?>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-layers"></i></span>
                                        <span class="pcoded-mtext">Operations</span>
                                    </a>

                                     <ul class="pcoded-submenu">
                                    <?php if (can_access('permissions')) { ?>
                                    <li class=" ">
                                        <a href="{{ url('users/permissions') }}">
                                                <span class="pcoded-mtext">Permissions</span>
                                        </a>
                                    </li>
                                     <?php } ?>
                                    <li class=" ">
                                        <a href="{{ url('recruiments/index') }}">
                                                <span class="pcoded-mtext">Job listing</span>
                                            </a>
                                       </li>

                                    </ul>
                                </li>
                              <?php } ?>

                              <?php if (can_access('view_academics')) { ?>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-sliders"></i></span>
                                        <span class="pcoded-mtext">Academics</span>
                                    </a>

                                    <ul class="pcoded-submenu">
                                    <li class=" ">
                                        <a href="{{ url('Exam/schoolclass') }}">
                                                <span class="pcoded-mtext">List of Classes</span>
                                            </a>
                                        </li>
                                        
                                        <li class=" ">
                                            <a href="{{ url('Exam/school') }}">
                                                    <span class="pcoded-mtext">School Exams</span>
                                                </a>
                                            </li>
                                        <li class=" ">
                                        <a href="{{ url('Exam/singlereport') }}">
                                                <span class="pcoded-mtext">Single Exam Reports</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                        <a href="{{ url('Exam/combined') }}">
                                                <span class="pcoded-mtext">Combined Exam Reports</span>
                                            </a>
                                        </li>
                                        
                                        <li class=" ">
                                        <a href="{{ url('Exam/classreport') }}">
                                                <span class="pcoded-mtext">Subjects Perfomance</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                        <a href="{{ url('Exam/teacher') }}">
                                                <span class="pcoded-mtext">Teachers Perfomance</span>
                                            </a>
                                        </li>
                                       
                                    </ul>
                                </li>
                            <?php } ?>

                               <?php if(can_access('view_communications')) { ?>
                                 <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                    <span class="pcoded-micon"><i class="feather icon-message-square"></i></span>
                                        <span class="pcoded-mtext">Communications</span>
                                    </a>

                                    <ul class="pcoded-submenu">
                                    <li class=" ">
                                        <a href="{{ url('SMS/inbox') }}">
                                                <span class="pcoded-mtext">Inbox</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                        <a href="{{ url('SMS/composeEmail') }}">
                                                <span class="pcoded-mtext">Compose</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                        <a href="{{ url('SMS/template') }}">
                                                <span class="pcoded-mtext">Template</span>
                                            </a>
                                        </li>
                                         <li class=" ">
                                        <a href="{{ url('SMS/schedule') }}">
                                                <span class="pcoded-mtext">Schedule</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                        <a href="{{ url('SMS/sentItems') }}">
                                                <span class="pcoded-mtext">Sent Summary</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                               <?php }  ?>

                             
                               <?php if(can_access('view_accounts')) {  ?>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-package"></i></span>
                                        <span class="pcoded-mtext">Accounts</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="{{ url('accounts/expenses') }}">
                                                <span class="pcoded-mtext">Expenses</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="{{ url('accounts/revenues') }}">
                                                <span class="pcoded-mtext">Revenues</span>
                                            </a>
                                        </li>
                                        {{-- <li class=" ">
                                            <a href="{{ url('accounts/payments') }}">
                                                <span class="pcoded-mtext">Payments</span>
                                            </a>
                                        </li> --}}
                                        <li class=" ">
                                            <a href="{{ url('accounts/fees') }}">
                                                <span class="pcoded-mtext">Fee Collections</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="{{ url('accounts/index/1') }}">
                                                <span class="pcoded-mtext">Fixed assets</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="{{ url('accounts/index/2') }}">
                                                <span class="pcoded-mtext">Liabilities</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="{{ url('accounts/index/5') }}">
                                                <span class="pcoded-mtext">Current assets</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="{{ url('accounts/index/3') }}">
                                                <span class="pcoded-mtext">Capital</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="{{ url('paymentController/reconciliation') }}">
                                                <span class="pcoded-mtext">Reconciliation</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                               <?php }  ?>

                               <?php if(can_access('view_inventory')) {  ?>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                                        <span class="pcoded-mtext">Invetory</span>
                                    </a>

                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="{{ url('Inventory/index') }}">
                                                <span class="pcoded-mtext">Items</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="{{ url('Inventory/purchase') }}">
                                                <span class="pcoded-mtext">Purchases</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="{{ url('Inventory/sale') }}">
                                                <span class="pcoded-mtext">Usage</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="{{ url('Vendor/index') }}">
                                                <span class="pcoded-mtext">Vendors</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <?php } ?>
                            </ul>

                           
                            <ul class="pcoded-item pcoded-left-item">
                                <?php if(can_access('view_payroll')) {  ?>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-clipboard"></i></span>
                                        <span class="pcoded-mtext">Payroll</span>
                                    </a>

                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="{{ url('Payroll/index') }}">
                                                <span class="pcoded-mtext">Salaries</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="{{ url('Payroll/taxes') }}">
                                                <span class="pcoded-mtext">Tax Status</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="{{ url('Payroll/pension') }}">
                                                <span class="pcoded-mtext">Pension Funds</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="{{ url('Deduction/index') }}">
                                                <span class="pcoded-mtext">Deduction</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="{{ url('Allowance/index') }}">
                                                <span class="pcoded-mtext">Allowance</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="{{ url('Payroll/allowance') }}">
                                                <span class="pcoded-mtext">Report</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <?php } ?>

                             <?php if(can_access('view_our_schools')) {  ?>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-command"></i></span>
                                        <span class="pcoded-mtext">Our Schools</span>
                                    </a>
                                <ul class="pcoded-submenu">
                                    @if (count(load_schemas()) > 0)
                                        @foreach (load_schemas() as $schema)
                                        <li class=" ">
                                            <a href="{{ url('users/profile/'.$schema->username) }}">
                                                <span class="pcoded-mtext">{{ $schema->username }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                    @endif
                                </ul>
                              </li>
                             <?php } ?>

                              <?php if(can_access('view_usage')) {  ?>
                                <li class=" ">
                                    <a href="{{ url('Dashboard/usage') }}">
                                        <span class="pcoded-micon"><i class="feather icon-battery-charging"></i></span>
                                        <span class="pcoded-mtext">System Usage</span>
                                    </a>
                                </li>
                             <?php } ?>
                            </ul>

                           <?php if(can_access('view_support')) {  ?>
                            <div class="pcoded-navigatio-lavel">Support</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="">
                                    <a href="{{ url('users/index') }}" target="_blank">
                                        <span class="pcoded-micon"><i class="feather icon-monitor"></i></span>
                                        <span class="pcoded-mtext">Staff Users</span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="{{ url('Dashboard/help') }}" target="_blank">
                                        <span class="pcoded-micon"><i class="feather icon-help-circle"></i></span>
                                        <span class="pcoded-mtext">Submit Issue</span>
                                    </a>
                                </li>
                            </ul>
                        <?php } ?>
                        </div>
                    </nav>
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-header start 
                                    <div class="page-header">
                                        <div class="row align-items-end">
                                            <div class="col-lg-8">
                                                <div class="page-header-title">
                                                    <div class="d-inline">
                                                        <h4>Widget</h4>
                                                        <span>More than 100+ widget</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="page-header-breadcrumb">
                                                    <ul class="breadcrumb-title">
                                                        <li class="breadcrumb-item">
                                                            <a href="index-1.htm"> <i class="feather icon-home"></i> </a>
                                                        </li>
                                                        <li class="breadcrumb-item"><a href="#!">Widget</a> </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Page-header end -->
                                    <!-- Page-body start -->
                                    <main class="py-4">
                                        @yield('content')
                                    </main>


                                </div>
                                <!-- <div id="styleSelector"> </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Required Jquery -->
    
    <script type="text/javascript" src="<?=$root?>/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/assets/pages/widget/excanvas.js"></script>

    <!-- summernote -->
    <script type="text/javascript" src="<?=$root?>/bower_components/summernote/js/summernote-bs4.min.js"></script>

    <script type="text/javascript" src="<?=$root?>/bower_components/select2/js/select2.full.min.js"></script>
<!-- Multiselect js -->
     <script type="text/javascript" src="<?=$root?>/bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/multiselect/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="<?=$root?>/assets/js/jquery.quicksearch.js"></script>
<!-- Custom js -->
    <script type="text/javascript" src="<?=$root?>/assets/pages/advance-elements/select2-custom.js"></script>

    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?=$root?>/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/sweetalert/js/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/select2/js/select2.full.min.js"></script>

    
    <!-- data-table js -->
    <script src="<?=$root?>/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?=$root?>/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?=$root?>/assets/pages/data-table/js/jszip.min.js"></script>
    <script src="<?=$root?>/assets/pages/data-table/js/pdfmake.min.js"></script>
    <script src="<?=$root?>/assets/pages/data-table/js/vfs_fonts.js"></script>
    <script src="<?=$root?>/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?=$root?>/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?=$root?>/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?=$root?>/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?=$root?>/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <!-- data-table js -->

    <!-- modernizr js -->
    <script type="text/javascript" src="<?=$root?>/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?=$root?>/assets/js/SmoothScroll.js"></script>
    <script src="<?=$root?>/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?=$root?>/assets/js/jquery.mousewheel.min.js"></script>
    <!-- Chart js -->
    <script type="text/javascript" src="<?=$root?>/bower_components/chart.js/js/Chart.js"></script>
    <!-- gauge js -->
    <script src="<?=$root?>/assets/pages/widget/amchart/amcharts.js"></script>
    <script src="<?=$root?>/assets/pages/widget/amchart/serial.js"></script>
    <script src="<?=$root?>/assets/pages/widget/amchart/light.js"></script>
    <!-- Custom js -->
    <!-- <script type="text/javascript" src="<?=$root?>/assets/pages/widget/custom-widget1.js"></script> -->
    <script src="<?=$root?>/assets/pages/data-table/js/data-table-custom.js"></script>

    <script src="<?=$root?>/assets/pages/wysiwyg-editor/wysiwyg-editor.js"></script>


    <script type="text/javascript" src="<?=$root?>/assets/js/SmoothScroll.js"></script>
    <script src="<?=$root?>/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?=$root?>/assets/js/jquery.mousewheel.min.js"></script>
    <script src="<?=$root?>/assets/js/pcoded.min.js"></script>
    <script src="<?=$root?>/assets/js/vartical-layout.min.js"></script>

    <script type="text/javascript" src="<?=$root?>/assets/js/script.js"></script>

    <script type="text/javascript" src="<?=$root?>/bower_components/chart.js/js/Chart.js"></script>
  
    <!-- Custom js -->
    <!-- <script type="text/javascript" src="<?=$root?>/assets/pages/dashboard/analytic-dashboard.min.js"></script> -->

       <!-- j-pro js -->
    <script type="text/javascript" src="<?=$root?>/assets/pages/j-pro/js/jquery.ui.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/assets/pages/j-pro/js/jquery.j-pro.js"></script>
</body>

</html>
