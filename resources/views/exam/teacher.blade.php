@extends('layouts.app')

@section('content')
<?php $root = url('/public/'); ?>
<!-- Page-body start -->
<div class="page-body">
    <div class="row">
        <!-- Server Side Processing table start -->
        <div class="col-sm-12">
            <!-- Basic Inputs Validation start -->
            <div class="card">
                <div class="card-header">
                    <h5>Basic Inputs Validation</h5>
                    <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                </div>
                <div class="card-block">
                    <form id="main" method="post" action="">
                        @csrf
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Select School</label>
                            <div class="col-sm-10">
                                <select name="schema_name" id="schema_name" class="form-control form-control-danger">
                                    <option value="opt1">Select One School</option>
                                    @if (count(load_schemas()) > 0)
                                    @foreach (load_schemas() as $schema)
                                    <option value="{{ $schema->username }}"> {{ $schema->username }}</option>
                                    @endforeach
                                    @endif

                                </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Select Class</label>
                            <div class="col-sm-10">
                                <select name="classesID" id="classesID" class="form-control form-control-info">
                                    <option value="0">Select One Value Only</option>

                                </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Select Academic Year</label>
                            <div class="col-sm-10">
                                <select name="academic_year_id" id="academic_year_id"
                                    class="form-control form-control-primary">

                                </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Select Semester</label>
                            <div class="col-sm-10">
                                <select name="semester_id" id="semester_id" class="form-control form-control-info">

                                </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Select Exam</label>
                            <div class="col-sm-10">
                                <select name="examID" id="examID" class="form-control form-control-info">

                                </select>
                                <span class="messages"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2"></label>
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- contact data table card start -->
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-header-text">Table of Examination Performance</h5>
                                </div>
                                <div class="card-block contact-details">
                                    <div class="data_table_main table-responsive dt-responsive">
                                        <table id="simpletable" class="table  table-striped table-bordered nowrap">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Exam name</th>
                                                    <th>Subject name</th>
                                                    <th>Class name</th>
                                                    <th>Year</th>
                                                    <th>Max score</th>
                                                    <th>Min score</th>
                                                    <th>Average</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i=1;if(isset($avg_perfomance)) { ?>
                                                <?php foreach ($avg_perfomance as $avg) { ?>
                                                <tr>
                                                    <th><?=$i?></th>
                                                    <th><?=$avg->exam?></th>
                                                    <th><?=$avg->subject_name?></th>
                                                    <th><?=$avg->classes?></th>
                                                    <th><?=$avg->academic_year?></th>
                                                    <th><?=to2decimal($avg->max)?></th>
                                                    <th><?=to2decimal($avg->min)?></th>
                                                    <th><?=to2decimal($avg->avg)?></th>

                                                </tr>
                                                <?php $i++;} ?>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Required Jquery -->
<script type="text/javascript" src="<?=$root?>/bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?=$root?>/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script src="<?= url('public/assets/js/highchart.js') ?>"></script>
<script src="<?= url('public/assets/js/exporting.js') ?>"></script>

<script type="text/javascript">
jQuery('#academic_year_id').change(function(event) {
    var academic_year_id = $(this).val();
    var schema_name = $('#schema_name').val();
    if (academic_year_id === '0') {
        $('#semester_id').val(0);
    } else {
        $.ajax({
            type: 'POST',
            url: "<?= url('exam/get_semester') ?>",
            data: "_token=" + "{{ csrf_token() }}" + "&academic_year_id=" + academic_year_id +
                "&schema_name=" + schema_name,
            dataType: "html",
            success: function(data) {
                if (data === '0') {
                    $('#semester_id').html('');
                    swal('warning',
                        'No semester(s) have been added in this level. Please define semester(s) first in this academic year',
                        'warning');
                    $('#sem_id').html();
                } else {
                    $('#sem_id').html('');
                    $('#semester_id').html(data);
                }
            }
        });
        var class_id = $('#classesID').val();
        $.ajax({
            type: 'POST',
            url: "<?= url('exam/getSubjectByClass') ?>",
            data: "_token=" + "{{ csrf_token() }}" + "&year_id=" + academic_year_id + "&id=" +
                class_id + "&schema_name=" + schema_name,
            dataType: "html",
            success: function(data) {
                $('#subjectID').html(data);
            }
        });
    }
});

$('#semester_id').change(function(event) {
    var schema_name = $('#schema_name').val();
    var academic_year_id = $('#academic_year_id').val();
    var semester_id = $(this).val();
    var classesID = $('#classesID').val();
    if (academic_year_id === '0') {
        $('#examID').val(0);
    } else {
        $.ajax({
            type: 'POST',
            url: "<?= url('exam/getExamByClass') ?>",
            data: "_token=" + "{{ csrf_token() }}" + "&id=" + classesID + "&academic_year_id=" +
                academic_year_id + '&semester_id=' + semester_id + '&done=1' + "&schema_name=" +
                schema_name,
            dataType: "html",
            success: function(data) {
                $('#examID').html(data);
            }
        });
    }
});

$('#classesID').change(function(event) {
    var schema_name = $('#schema_name').val();
    var classesID = $(this).val();
    if (classesID === '0') {
        $('#academic_year_id').val(0);
        $('#report_filter_div').hide();
    } else {
        $('#report_filter_div').show();
        $.ajax({
            type: 'POST',
            url: "<?= url('exam/get_academic_years') ?>",
            data: "_token=" + "{{ csrf_token() }}" + "&id=" + classesID + "&schema_name=" + schema_name,
            dataType: "html",
            success: function(data) {
                $('#academic_year_id').html(data);
            }
        });
    }
});


$('#schema_name').change(function(event) {
    var schema_name = $('#schema_name').val();
    var schema_name = $(this).val();
    if (schema_name === '0') {
        $('#academic_year_id').val(0);
        $('#report_filter_div').hide();
    } else {
        $('#report_filter_div').show();
        $.ajax({
            type: 'POST',
            url: "<?= url('exam/getClasses') ?>",
            data: "_token=" + "{{ csrf_token() }}" + "&schema_name=" + schema_name,
            dataType: "html",
            success: function(data) {
                $('#classesID').html(data);
            }
        });
    }
});

/*    $('#academic_year_id').change(function(event) {
        var academic_year_id = $(this).val();
        var classesID = $('#classesID').val();
        if (academic_year_id === '0') {
            $('#sectionID').val(0);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('student/sectioncall') ?>",
                data: "id=" + classesID + '&academic_year_id=' + academic_year_id + '&all_section=1',
                dataType: "html",
                success: function(data) {
                    $('#sectionID').html(data);
                }
            });
        }
    }); */
</script>
</script>
@endsection