@extends('layouts.app')

@section('content')
<?php $root = url('/public/'); ?>
<!-- Page-body start -->
<div class="page-body">
    <div class="row">

        <!-- Server Side Processing table start -->
        <div class="col-sm-12">
            <!-- Basic Inputs Validation start -->
            <div class="card">
                <div class="card-header">
                    <h5>Fill all fields for Validation</h5>
                    <!-- <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->

                </div>
                <div class="card-block">
                    <form id="main" method="post" action="">
                    @csrf
                    <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Select School</label>
                            <div class="col-sm-10">
                            <select name="schema_name" id="schema_name" class="form-control form-control-danger">
                                <option value="opt1">Select One School</option>
                                @if (count(load_schemas()) > 0)
                                @foreach (load_schemas() as $schema)
                                    <option value="{{ $schema->username }}"> {{ $schema->username }}</option>
                                @endforeach
                                @endif
                               
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Select Class</label>
                            <div class="col-sm-10">
                            <select name="classesID" id="classesID" class="form-control form-control-info">
                                <option value="0">Select One Value Only</option>
                               
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Select Academic Year</label>
                            <div class="col-sm-10">
                            <select name="academic_year_id" id="academic_year_id" class="form-control form-control-primary">
                               
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Select Semester</label>
                            <div class="col-sm-10">
                            <select name="semester_id" id="semester_id" class="form-control form-control-info">
                             
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Select Exam</label>
                            <div class="col-sm-10">
                            <select name="examID" id="examID" class="form-control form-control-info">
                             
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                     
                        <div class="form-group row">
                            <label class="col-sm-2"></label>
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <?php if (isset($exam_info) && !empty($exam_info) && !empty($students)) {
    ?>
            <!-- Basic Inputs Validation end -->
            <div class="card">
                <div class="card-header">
                    <h5>Server Side Processing</h5>
                    <span>The example below shows DataTables loading data for a table from arrays as the data source,
                        where the structure of the row's data source in this example is:</span>
                </div>
                <div class="card-block">
                <div class="dt-responsive table-responsive">
                        <table id="simpletable" class="table table-striped table-bordered nowrap">
                            <thead>
                            <?php
                            $sub_rank = isset($show_rank) && $show_rank == 1 && $show_grade == 1 ? 3 : 2;

                            $col_span = isset($show_grade) && $show_grade == 1 || $show_rank == 1 ? 'colspan="' . $sub_rank . '"' : '';
                            $row_span = isset($show_grade) && $show_grade == 1 || $show_rank == 1 ? 'rowspan="2"' : '';
                            ?>
                                    <tr>
                                        <th class="col-sm-1" <?= $row_span ?>>S/n</th>

                                        <th class="col-sm-2" <?= $row_span ?>>Name</th>
                                        <th class="col-sm-1" <?= $row_span ?>>Roll</th>

                                    <?php

                                        if ($subjectID == 0) {
                                            $average = 'AVG'; 
                                            //Loop in all subjects to show list of them here
                                            if (isset($subjects)) {
                                                foreach ($subjects as $subject) {
                                                    $subj = strtolower($subject->subject);
                                                    $sum_subj[$subj] = 0;
                                                    $pass["$subj"] = 0;
                                                    $fail["$subj"] = 0;
                                                    $subject_sum["$subj"] = 0;
                                                    $words = explode(' ', str_replace('/', '', $subject->subject));
                                                   
                                                    $this_subject = substr(strtoupper($subject->subject), 0, 4);
                                                        
                                                    echo !empty($subject) ?
                                                        '<th class="col-sm-1 verticalTableHeader" ' . $col_span . '>'
                                                        . '<p>' . $this_subject . ''
                                                        . '</p></th>' : '<th></th>';
                                                        }
                                                    }
                                                } else {
                                                    $average = 'Subject Average';
                                                    echo '<th class="col-sm-2">Subject Name</th>';
                                                }
                                                ?>

                                        <th class="col-sm-1" <?= $row_span ?>>Total</th>
                                        <?php if ($subjectID == 0) { ?>
                                            <!--<th class="col-sm-2">Subject Counted</th>-->
                                        <?php } ?>
                                        <th class="col-sm-2" <?= $row_span ?>><?= $average ?></th>
                                        <th class="col-sm-2" <?= $row_span ?>>GD</th>

                                        <?php if ($exam_info->show_division == 1 && $subjectID == 0) { ?>
                                            <th <?= $row_span ?>>Div</th>
                                            <th <?= $row_span ?>>Point</th>
                                        <?php } ?>
                                        <th class="col-sm-1"> Rank</th>
                                        <?php
                                        if (isset($show_gender) && $show_gender == 1) {
                                        ?>
                                            <th class="col-sm-2" <?= $row_span ?>>Rank per Gender</th>
                                        <?php } ?>
                                        <th class="col-sm-3" id="action_option" <?= $row_span ?>>Action</th>
                                    </tr>
                                    <?php if (isset($subjects) && isset($show_grade) && $show_grade == 1 || $show_rank == 1) { ?>
                                        <tr>

                                            <?php
                                            $subject_th = $show_rank == 1 ? '<th class="col-sm-2">Rank</th>' : '';
                                            $grade_th = $show_grade == 1 ? '<th class="col-sm-1">GD</th>' : '';
                                            foreach ($subjects as $subject) {
                                                if (!empty($subject)) {
                                            ?>
                                                    <th class="col-sm-1"><?= substr(strtoupper($subject->subject), 0, 3) ?></th>
                                                    <?= $grade_th ?>
                                                    <?= $subject_th ?>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </tr>
                                    <?php } ?>
                                </thead>
                                <tbody>
                                    <?php
                                    if (count($students)) {
                                        $i = 1;
                                        $student_pass = 0;
                                        $total_average = 0;
                                        $total_div_I = 0;
                                        $total_div_II = 0;
                                        $total_div_III = 0;
                                        $total_div_IV = 0;
                                        $total_div_0 = 0;

                                        foreach ($subjects as $subject) {
                                            $subject_name = strtolower($subject->subject);
                                            foreach ($grades as $grade) {
                                                ${$subject_name . '_' . $grade->grade} = 0;
                                            }
                                        }
                                        foreach ($students as $student) {
                                            $student = is_object($student) ? (array) $student : $student;
                                    ?>
                                            <tr>
                                                <td>
                                                    <?php echo $i ?>
                                                </td>
                                                <td>
                                                    <?= $student['name']; ?>
                                                </td>
                                              
                                                <td>
                                                    <?= $student['roll']; ?>
                                                </td>
                                                <?php
                                                
                                                /*
                                                 * 
                                                 * -----------------------------------------
                                                 * This part show list of all subjects or ONE
                                                 *  subject depends on user selection
                                                 * -----------------------------------------
                                                 * 
                                                 */

                                                if ($subjectID == 0) {

                                                    $point[$student['student_id']] = [];
                                                    foreach ($subjects as $subject) {
                                                        $subject_name = strtolower($subject->subject);


                                                        if (isset($student["$subject_name"])) {
                                                            if ($student["$subject_name"] < $classlevel->pass_mark && $student["$subject_name"] != NULL) {

                                                                $color = 'style="background: pink;"';
                                                                $fail["$subject_name"]++;
                                                            } else {
                                                                $color = "";
                                                                $student["$subject_name"] != NULL ? $pass["$subject_name"]++ : '';
                                                            }
                                                            $subject_sum["$subject_name"] = $subject_sum["$subject_name"] + (float) $student["$subject_name"];

                                                            echo '<td '.$color.'>';
                                                            if(in_array(strtolower($classlevel->name) , ['primary','nursery']) !== false ){
                                                                echo (int)$student["$subject_name"];
                                                            }else{
                                                                echo $student["$subject_name"];
                                                            }
                                                            echo '</td>';
                                                            if (isset($show_grade) && $show_grade == 1) {
                                                                foreach ($grades as $grade) {
                                                                    if ($grade->gradefrom <= round($student["$subject_name"], 0) && $grade->gradeupto >= round($student["$subject_name"], 0)) {
                                                                        echo ' <td>' . $grade->grade . '</td>';
                                                                    }
                                                                }
                                                            }
                                                            echo $show_rank == 1 ? '<th class="col-sm-2">' . $subject_rank[$student['student_id']][$subject->subjectID] . '</th>' : '';

                                                            $sum_subj[$subject_name] = $sum_subj[$subject_name] + (float) $student["$subject_name"];
                                                            //
                                                            foreach ($grades as $grade) {
                                                                if ($grade->gradefrom <= round($student["$subject_name"], 0) && $grade->gradeupto >= round($student["$subject_name"], 0)) {
                                                                    ${strtolower($subject_name) . '_' . $grade->grade}++;
                                                                    $sub = [
                                                                        'subject_mark' => round($student["$subject_name"], 0),
                                                                        'point' => $grade->point,
                                                                        'penalty' => $subject->is_penalty,
                                                                        'pass_mark' => $subject->pass_mark,
                                                                        'is_counted_indivision' => $subject->is_counted_indivision
                                                                    ];
                                                                    array_push($point[$student['student_id']], $sub);
                                                                }
                                                            }
                                                        } else {
                                                            //here you will check if this student subscribe to this subject or not . if yes, place a yellow box, if not disable the input if result format is based on the subject counted otherwise, place yellow and content editable in all cases

                                                            echo '<td ' . $editable . '  data-title="' . $subject_name . '" class="mark" subject_id="' . $subject->subjectID . '" student_id="' . $student['student_id'] . '"></td>';
                                                            echo isset($show_grade) && $show_grade == 1 ? '<td></td>' : '';
                                                            echo $show_rank == 1 ? '<th class="col-sm-2"></th>' : '';
                                                        }
                                                    }
                                                } else {

                                                    echo '<td data-title="subject">';
                                                    echo $student['subject'];
                                                    echo '</td>';
                                                }

                                                if ($subjectID == '0') {
                                                    echo '<td data-title="Total">' . $student['total'] . ' </td>';
                                                } else {
                                                    if ((isset($student['mark']) && $student['mark'] < $classlevel->pass_mark)) {

                                                        $color = "pink";
                                                    } else {
                                                        $color = "";
                                                        $student_pass++;
                                                    }

                                                    //

                                                    echo '<td style="background-color:' . $color . '">' . $student['mark'] . '</td>';
                                                }
                                                ?>
                                                <?php if ($subjectID == 0) { ?>
                                                    <!--   <td><a href="<?= url('subject/subject_count/' . $student['student_id'] . '/' . $academic_year_id) ?>"><?= isset($student['subject_count']) ? $student['subject_count'] : '' ?></a></td>-->
                                                <?php } ?>
                                                <?php
                                                if ($subjectID == '0') {

                                                    /**
                                                     * For the whole class, find total marks of that student
                                                     * and devide by total subjects taken by such student
                                                     */
                                                    if ((isset($student) && $student['average'] < $classlevel->pass_mark)) {

                                                        $color = "pink";
                                                    } else {
                                                        $color = "";
                                                        $student_pass++;
                                                    }
                                                    $total_average = $total_average + (float) $student['average'];
                                                    echo '<td data-title="" style="background: ' . $color . ';">';
                                                    echo $student['average'];
                                                    echo '</td>';

                                                    $grade_column = '';
                                                    foreach ($grades as $grade) {

                                                        if ($grade->gradefrom <= round($student['average'], 0) && $grade->gradeupto >= round($student['average'], 0)) {
                                                            $grade_column = '<td>' . $grade->grade . '</td>';
                                                        }
                                                    }
                                                    if ($grade_column == '') {
                                                        echo '<td></td>';
                                                    } else {
                                                        echo $grade_column;
                                                    }
                                                } else {
                                                    /*
                                                     * For one subject, just find total marks 
                                                     * for that subject and devide by number of students
                                                     */
                                                    $total_marks = 0;
                                                    foreach ($students as $student_sum) {

                                                        $total_marks = $total_marks + (float) $student_sum->mark;
                                                    }
                                                    $class_average_marks = $total_marks / count($students);
                                                    $color = $class_average_marks < $classlevel->pass_mark ? "pink" : "";
                                                    echo '<td data-title="AVG" style="background: ' . $color . ';">';
                                                    echo round($class_average_marks, 2);
                                                    echo '</td>';
                                                    $grade_column = '';
                                                    foreach ($grades as $grade) {

                                                        if ($grade->gradefrom <= round($class_average_marks, 0) && $grade->gradeupto >= round($class_average_marks, 0)) {
                                                            $grade_column = '<td>' . $grade->grade . '</td>';
                                                        }
                                                    }
                                                    if ($grade_column == '') {
                                                        echo '<td></td>';
                                                    } else {
                                                        echo $grade_column;
                                                    }
                                                }
                                                ?>
                                                <?php if ($exam_info->show_division && $subjectID == '0') { ?>
                                                    <td data-title='Div'>
                                                    <?php
                                                        $division = getDivisionBySort($point[$student['student_id']], $classlevel->result_format);
                                                        echo $division[0];
                                                        $total_div_0 = $division[0] == '0' ? $total_div_0 + 1 : $total_div_0;
                                                        $total_div_I = $division[0] == 'I' ? $total_div_I + 1 : $total_div_I;
                                                        $total_div_II = $division[0] == 'II' ? $total_div_II + 1 : $total_div_II;
                                                        $total_div_III = $division[0] == 'III' ? $total_div_III + 1 : $total_div_III;
                                                        $total_div_IV = $division[0] == 'IV' ? $total_div_IV + 1 : $total_div_IV;
                                                        ?>
                                                    </td>
                                                    <td data-title='points'>
                                                        <span class="point" id="points<?= $student['student_id'] . $examID . $classesID ?>"></span>
                                                        <?php
                                                        echo $division[1];
                                                        ?></td>
                                                <?php } ?>
                                                <td>
                                                    <?php echo $student['rank']; ?>
                                                </td>
                                                <?php if (isset($show_gender) && $show_gender == 1) { ?>
                                                    <td>
                                                        <?php echo isset($student['gender_rank']) ? $student['gender_rank'] : ''; ?>
                                                    </td>
                                                <?php } ?>
                                                <td class="action_btn">
                                                    <a href="<?= url('exam/studentreport/'.$this_school.'/' . $student['student_id'] . '?exam=' . $examID . '&class_id=' . $classesID . '&year_no=' . $academic_year_id) ?>" class="btn btn-success btn-sm btn-out-dotted" data-placement="top" data-toggle="tooltip" data-original-title="Download Report"><i class="fa fa-file"></i>Preview</a>

                                                    <a href="<?= url('exam/singleReportAnalysis/'.$this_school.'/' . $student['student_id'] . '/' . $examID . '/' . $academic_year_id) ?>" class="btn btn-info btn-sm btn-out-dotted" data-placement="top" data-toggle="tooltip" data-original-title="View graph"><i class="fa fa-file-o"></i> Graph</a>

                                                    <!-- <a href="<?= url('subject/subject_count/'.$this_school.'/' . $student['student_id'] . '/' . $academic_year_id) ?>"> <i class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="left" data-trigger="hover" data-content="<?= isset($student['subject_count']) ? $student['subject_count'] : '' ?> subjects done by   <?= $student['name']; ?>" title="Subject Counted">Info</i></a> -->
                                                    <?php if (strtolower(trim($classlevel->result_format)) == 'cambridge') { ?>
                                                        <a href="<?= url('cambridge/single/'.$this_school.'/' . $student['student_id'] . '/' . $examID . '/' . $academic_year_id) ?>" class="btn btn-dark btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="View single report"><i class="fa fa-paperclip"></i></a>
                                                    <?php } ?>

                                                </td>
                                            </tr>
                                            <?php
                                            $i++;
                                            }
                                        }
                                        ?>
                                </tbody>
                            <!-- <tfoot>
                                <tr>
                                    <th>First name</th>
                                    <th>Last name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                    <th>First name</th>
                                    <th>Last name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                    <th>First name</th>
                                    <th>Last name</th>
                                    
                                </tr>
                            </tfoot> -->
                        </table>
                    </div>
                </div>
            </div>
            <!-- Server Side Processing table end -->
<?php } ?>
        </div>
    </div>
</div>
  <!-- Required Jquery -->
    <script type="text/javascript" src="<?=$root?>/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script src="<?= url('public/assets/js/highchart.js') ?>"></script>
    <script src="<?= url('public/assets/js/exporting.js') ?>"></script>

<script type="text/javascript">

    jQuery('#academic_year_id').change(function(event) {
        var academic_year_id = $(this).val();
        var schema_name = $('#schema_name').val();
        if (academic_year_id === '0') {
            $('#semester_id').val(0);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/get_semester') ?>",
                data: "_token=" + "{{ csrf_token() }}" + "&academic_year_id=" + academic_year_id + "&schema_name=" + schema_name,
                dataType: "html",
                success: function(data) {
                    if (data === '0') {
                        $('#semester_id').html('');
                        swal('warning', 'No semester(s) have been added in this level. Please define semester(s) first in this academic year', 'warning');
                        $('#sem_id').html();
                    } else {
                        $('#sem_id').html('');
                        $('#semester_id').html(data);
                    }
                }
            });
            var class_id = $('#classesID').val();
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/getSubjectByClass') ?>",
                data: "_token=" + "{{ csrf_token() }}" + "&year_id=" + academic_year_id + "&id=" + class_id + "&schema_name=" + schema_name,
                dataType: "html",
                success: function(data) {
                    $('#subjectID').html(data);
                }
            });
        }
    });

    $('#semester_id').change(function(event) {
        var schema_name = $('#schema_name').val();
        var academic_year_id = $('#academic_year_id').val();
        var semester_id = $(this).val();
        var classesID = $('#classesID').val();
        if (academic_year_id === '0') {
            $('#examID').val(0);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/getExamByClass') ?>",
                data: "_token=" + "{{ csrf_token() }}" + "&id=" + classesID + "&academic_year_id=" + academic_year_id + '&semester_id=' + semester_id + '&done=1' + "&schema_name=" + schema_name,
                dataType: "html",
                success: function(data) {
                    $('#examID').html(data);
                }
            });
        }
    });

    $('#classesID').change(function(event) {
        var schema_name = $('#schema_name').val();
        var classesID = $(this).val();
        if (classesID === '0') {
            $('#academic_year_id').val(0);
            $('#report_filter_div').hide();
        } else {
            $('#report_filter_div').show();
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/get_academic_years') ?>",
                data: "_token=" + "{{ csrf_token() }}" + "&id=" + classesID + "&schema_name=" + schema_name,
                dataType: "html",
                success: function(data) {
                    $('#academic_year_id').html(data);
                }
            });
        }
    });

    
    $('#schema_name').change(function(event) {
        var schema_name = $('#schema_name').val();
        var schema_name = $(this).val();
        if (schema_name === '0') {
            $('#academic_year_id').val(0);
            $('#report_filter_div').hide();
        } else {
            $('#report_filter_div').show();
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/getClasses') ?>",
                data: "_token=" + "{{ csrf_token() }}" + "&schema_name=" + schema_name,
                dataType: "html",
                success: function(data) {
                    $('#classesID').html(data);
                }
            });
        }
    });
</script>
@endsection