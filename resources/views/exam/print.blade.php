<?php
ini_set('max_execution_time', 360);
/**
 * Description of print
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */

$exams_report_setting = \DB::table('exam_report_settings')->where('exam_type', 'single')->where('classlevel_id', $classes->classlevel_id)->first();

if ((int) $exams_report_setting->show_total_marks == 1) {
    $max_grade = [];
    foreach ($grades as $grade) {
        array_push($max_grade, $grade->gradeupto);
    }
    asort($max_grade);

    $student_total = [];
    $part= isset($max_grade[0]) ? $max_grade[0] : NULL ;
   $sql='select (subject_count * ' .$part. ') as total_max,student_id from student_total_subject where class_id = ' . $classes->classesID . ' and academic_year_id=' . $academic_year_id . ' group by student_id,subject_count';
    $total_max_students = DB::select($sql);
    foreach ($total_max_students as $value) {
        $student_total[$value->student_id] = $value->total_max;
    }
}
$data = $data_language;

$usertype = session("usertype");
$section_id = request('section_id'); //otherwise when we specify to provide report by section and by class
?>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
        <title>Exam Report</title>

        <link href="<?php echo url('public/assets/css/print_all/certificate.css'); ?>" rel="stylesheet"
              type="text/css">

        <link href="<?php echo url('public/assets/print.css'); ?>?v=98" rel="stylesheet" type="text/css">

        <link rel="shortcut" href="<?= url(); ?>public/assets/images/favicon.ico">

        <!-- Begin embedded font definitions -->
        <style id="fonts1" type="text/css">

            @font-face {
                font-family: DejaVuSans_b;
                src: url("../../public/assets/fonts/DejaVuSans_b.woff") format("woff");
            }

            table.table.table-striped td, table.table.table-striped th, table.table {
                border: 1px solid #000000;
                margin: 1px auto !important;
            }

            #report_footer {
                margin-left: 2em;
            }

            .headings {
                margin-left: 2em;
            }

            .school_logo {
                float: left;
                height: 130px;
                width: 20%
            }

            .head_letter_photo {
                float: right;
                height: 130px;
                width: 79%
            }

            .school_info_heading {
                float: left;
                text-align: center;
                width: 77%;
                height: 139px;
            }

            .student_photo {
                float: right;
                width: 22%;
                height: 139px;
            }

            @media print {
                /*.panel{*/
                /*padding-left: 10%;*/
                /*}*/
                /*table.table.table-striped td,table.table.table-striped th, table.table {*/
                /*border: 1px solid #000000 !important;*/
                /*margin: 1px auto !important;*/
                /*}*/
                /*.table-stripe {*/
                /*background-color: #dedede !important;*/
                /*border: 1px solid #000000 !important;*/
                /*}*/
                /*body, .panel {*/
                /*margin: 0;*/
                /*box-shadow: 0;*/
                /*}*/
                /*.headings{*/
                /*margin-left:2em;*/
                /*}*/
                /*#t7_1{*/
                /*width: 23em !important;*/
                /*}*/
                #wrappers {
                    page-break-before: always;
                }

                h3, h4 {
                    page-break-after: avoid;
                    font-size: 15px;
                    /* line-height: 18px; */
                }

            }

            h3, h4 {
                page-break-after: avoid;
                font-size: 15px;
                /* line-height: 18px; */
            }
        </style>
        <?php if ($level->result_format == 'CSEE') { ?>
            <style type="text/css">
                .panel {
                    color: #000000 !important;

                }

                table.table.table-striped td, table.table.table-striped th, table.table {
                    font-size: 12px !important;
                    margin-left: 10%;
                }
            </style>
        <?php } ?>
    </head>
    <body onload="window.print()">
        <?php  foreach ($students as $student) {
            ?>
            <div class="row">
                <div class="col-md-12">

                    <div id="wrappers" style="overflow:hidden;">

                        <section class="panel">
                     

                            <div>
                                <?php
                                $array = array(
                                    "src" => url('storage/uploads/images/' . $siteinfos->photo),
                                    'width' => '90%',
                                    'height' => '90%',
                                    'class' => 'img-rounded',
                                );
                                
                                if(strpos($student->photo, 'https:') !== false ){
                                    $image = $student->photo;
        
                                } else{
                                    $image = is_file('storage/uploads/images/' . $student->photo) ? url('storage/uploads/images/' . $student->photo) : url('storage/uploads/images/defualt.png');
                                }
                                
                                $photo = array(
                                    "src" => "$image",
                                    'width' => '90%',
                                    'height' => '90%',
                                    'class' => 'img-rounded',
                                );
                                ?>
                                <div class="row">
                                    <div class="school_logo">
                                        <?php echo img($array); ?>
                                    </div>
                                    <div class="head_letter_photo">
                                        <div class="school_info_heading">
                                            <br/>
                                            <h2 style="font-weight: bolder; font-size: 24px;"
                                                class="text-uppercase"><?= $siteinfos->sname ?></h2>
                                            <h4><?= 'P.O BOX ' . $siteinfos->box . ', ' . $siteinfos->address ?>.</h4>
                                            <h3>cell') ?>
                                                : <?= str_replace(',', ' / ', $siteinfos->phone) ?></h3>
                                            <h3> email') ?>:<?= $siteinfos->email ?></h3>
                                            <?=$siteinfos->website !='' ? '<h3>'. $data->lang->line('website') .':'. $siteinfos->website .'</h3>' : '' ?>
                                        </div>
                                        <div class="student_photo">
                                            <?php echo img($photo);
                                            ?>
                                        </div>
                                        <!-- <div style="clear: both"></div> -->
                                    </div>
                                    <!-- <div style="clear: both"></div> -->
                                </div>
                                <!-- <hr/> -->

                            </div>

                            <div class="">
                                <h1 align='center' style="font-size:18px;  font-weight: bolder;">
                                <?php
                                    if(preg_match('/nursery/i', strtolower($level->name))){
                                    $report_name = $data->lang->line("child_report_title");
                                    }elseif(preg_match('/primary/i', strtolower($level->name))){
                                        $report_name = $data->lang->line("pupil_report_title");
                                    }else{
                                        $report_name = $data->lang->line("single_report_title");
                                    }
                                    ?>
                                    <?= (!empty($exam_report) && $exam_report->name != '') ? strtoupper($exam_report->name) : $report_name ?> (<?= strtoupper($classes->classes) . ' ' . $year_name ?>)</h1>
                            </div>

                            <div class="panel-body profile-view-dis" style="margin-top: .3em; padding: 0 5%;">
                                <div class="row">
                                    <div class="col-sm-9" style="float:left;">
                                        <h4 style="font-weight: bolder;">  @lang('exam_report_lang.name') <?= strtoupper($student->name) ?></h4>
                                        <!-- <?php // if(set_schema_name() == 'stjohnbosco.'){ }else{ ?> <h4 style="margin-top: 2%;"> @lang('exam_report_lang.admission_no') <student->roll ?></h4> <?php // } ?> -->

                                       <!-- // <h4 style="margin-top: 2%;">  @lang('exam_report_lang.admission_no') : = $student->roll ?></h4> -->
                                    </div>
                                    <div class="col-sm-3" style="float: right; font-weight: bolder;">
                                        <h6>@lang('exam_report_lang.eattendance_section'): <?=
                                            $student->section
                                            ?></h6>
                                    </div>
                                    <div style="clear:both;"></div>
                                </div>

                            </div>
                            <?php if (!empty($exams)) {
                                ?>

                                <div class="row" style="margin: 2%;">
                                    <?php
                                    $student_mark = isset($marks[$student->student_id]) ? $marks[$student->student_id] : array();
                                    $count_mark = count($student_mark);

                                    if ($count_mark == 0) {
                                        echo "<div style='padding:10%; text-align:center; border: 1px solid whitesmoke;'>No results uploaded for " . $student->name . '</div>';
                                    } else {

                                        //$examController = new \App\Http\Controllers\mark();
                                        ?>

                                        <div class="col-lg-12">

                                            <?php
                                            /*  $map1 = function($r) {
                                              return intval($r->examID);
                                              };
                                              $marks_examsID = array_map($map1, $marks[$student->student_id]);
                                              $max_semester = max($marks_examsID);

                                              $map2 = function($r) {
                                              return intval($r->examID);
                                              };
                                              //$examsID = array_map($map2, $exams);

                                              $map3 = function($r) {
                                              return array("mark" => intval($r->mark), "semester" => $r->examID);
                                              };
                                              $all_marks = array_map($map3, $marks[$student->student_id]);

                                              $map4 = function($r) {
                                              return array("gradefrom" => $r->gradefrom, "gradeupto" => $r->gradeupto);
                                              };
                                              $grades_check = array_map($map4, $grades); */


                                            echo "<table  class=\"table table-striped table-bordered\" style=\"margin-bottom: 6%;\">";
                                            // if ($exams->examID <= $max_semester) {
                                            //$check = array_search($exams->examID, $marks_examsID);
                                            //if ($check >= 0) {
                                            $f = 1;
                                            /* foreach ($grades_check as $key => $range) {
                                              foreach ($all_marks as $value) {
                                              if ($value['semester'] == $exams->examID) {
                                              if ($value['mark'] >= $range['gradefrom'] && $value['mark'] <= $range['gradeupto']) {
                                              $f = 1;
                                              }
                                              }
                                              }
                                              if ($f == 1) {
                                              break;
                                              }
                                              } */


                                            echo "<thead>";
                                            echo "<tr>";
                                            echo "<th style='font-weight: bolder;'>";
                                            echo strtoupper($data->lang->line("mark_subject"));
                                            echo "</th>";
                                            if ($exams_report_setting->show_marks == 1) {
                                                echo "<th style='font-weight: bolder;'>";
                                                echo strtoupper($exams->exam);
                                                echo "</th>";
                                            }

                                            if (count($grades) && $f == 1) {
                                                if ($exams_report_setting->show_subject_point == 1) {
                                                    echo "<th style='font-weight: bolder;'>";
                                                    echo strtoupper($data->lang->line("mark_point"));
                                                    echo "</th>";
                                                }
                                                echo "<th style='font-weight: bolder;'>";
                                                echo strtoupper($data->lang->line("mark_grade"));
                                                echo "</th>";
                                            }
                                            if ($exams_report_setting->show_grade == 1) {
                                                echo "<th style='font-weight: bolder;'>";
                                                echo strtoupper('Status');
                                                echo "</th>";
                                            }
                                            if ($exams_report_setting->show_subject_pos_in_class == 1) {
                                                echo "<th style='font-weight: bolder;'>";
                                                echo 'P/C';
                                                echo "</th>";
                                            }
                                            if ($exams_report_setting->show_subject_pos_in_section == 1) {
                                                echo "<th style='font-weight: bolder;'>";
                                                echo $data->lang->line("pos_in_section");
                                                echo "</th>";
                                            }
                                            echo "</tr>";

                                            echo "</thead>";
                                            //	}
                                            //  }

                                            echo "<tbody style='font-weight: bold;'>";

                                            $subjects_lists = isset($subjects[$student->student_id]) ? $subjects[$student->student_id] : array();
                                            $point = [];
                                            foreach ($subjects_lists as $mark_info) {

                                                $mark = (object) $mark_info;
                                                $mark_score = isset($student_mark[$exam_id][$mark->subject_id][0]['total_mark']) ? $student_mark[$exam_id][$mark->subject_id][0]['total_mark'] : NULL;
                                                if ($mark_score != NULL) {
                                                    //if ($exams->examID == $mark->examID) {
                                                    echo "<tr>";
                                                    echo "<td  data-title='" . $data->lang->line('mark_subject') . "' style='font-weight: bolder;'>";
                                                    echo strtoupper($mark->subject);
                                                    echo "</td>";
                                                    if ($exams_report_setting->show_marks == 1) {
                                                        echo "<td align='center' data-title='" . $data->lang->line('mark_mark') . "'>";
                                                        // echo $mark->mark;
                                                      
                                                        if(strpos(strtolower($classes->classlevel->name) , 'primary') !== false ){
                                                            echo (int)$mark_score;
                                                        }else{
                                                            echo $mark_score;
                                                        }

                                                        echo "</td>";
                                                    }
                                                    if (count($grades)) {
                                                        foreach ($grades as $grade) {
                                                            if ($grade->gradefrom <= round($mark_score, 0) && $grade->gradeupto >= round($mark_score, 0)) {
                                                                if ($exams_report_setting->show_subject_point == 1) {
                                                                    echo "<td align='center' data-title='" . $data->lang->line('mark_point') . "'>";
                                                                    echo $grade->point;
                                                                    echo "</td>";
                                                                }

                                                                if ($exams_report_setting->show_grade == 1) {
                                                                    echo "<td data-title='" . $data->lang->line('mark_grade') . "'>";
                                                                    echo $grade->grade;
                                                                    echo "</td>";
                                                                }
                                                                $sub = [
                                                                    'subject_mark' => round($mark_score, 0),
                                                                    'point' => $grade->point,
                                                                    'penalty' => $mark->is_penalty,
                                                                    'pass_mark' => $mark->pass_mark,
                                                                    'is_counted_indivision' => $mark->is_counted_indivision
                                                                ];
                                                                array_push($point, $sub);
                                                                break;
                                                            }
                                                        }
                                                    }

                                                    echo "<td data-title='" . $data->lang->line('mark_mark') . "'>";
                                                    echo $grade->note;
                                                    echo "</td>";
                                                    if ($exams_report_setting->show_subject_pos_in_class == 1) {
                                                        echo "<td align='center' data-title='" . $data->lang->line('mark_mark') . "'>";

                                                        $subject_info = isset($classrank[$student->student_id][$exam_id][$mark->subject_id]) ? $classrank[$student->student_id][$exam_id][$mark->subject_id] : array();

                                                        echo count($subject_info) ? $subject_info[0]['rank'] : '';
                                                        echo "</td>";
                                                    }

                                                    //$subject_info_insection = $examController->get_position($mark->student_id, $mark->examID, $mark->subjectID, $mark->classesID, $student->section_id);
                                                    $section_subject_info = isset($sectionrank[$student->student_id][$exam_id][$mark->subject_id]) ? $sectionrank[$student->student_id][$exam_id][$mark->subject_id] : array();

                                                    if ($exams_report_setting->show_subject_pos_in_section == 1) {
                                                        echo "<td align='center' data-title='" . $data->lang->line('mark_mark') . "'>";
                                                        //echo $subject_info_insection->rank;
                                                        echo count($section_subject_info) ? $section_subject_info[0]['rank'] : '';
                                                        echo "</td>";
                                                    }
                                                    echo "</tr>";
                                                }
                                            }
                                            echo "</tbody>";
                                            echo "</table>";

                                            // if (strtolower($usertype) == 'parent') {
                                            //$sectionreport = $examController->get_rank_per_all_subjects($exams->examID, $student->classesID, $student->student_id, $student->section_id);
                                            //$totalstudent_insection = $examController->get_student_per_class($student->classesID, $student->section_id,$academic_year_id);
                                            // }
                                            //$report[$student->student_id] = $examController->get_rank_per_all_subjects($exams->examID, $student->classesID, $student->student_id);
                                            ?>


                                            <table class="table table-striped table-bordered" style=" width: 100%; margin: 0%;">
                                                <thead>
                                                    <tr>
                                                        <th style="font-weight: bolder; width:29.2%;">total") ?>:
                                                            <strong><?= isset($report[$student->student_id][0]) ? $report[$student->student_id][0]['sum'] : '' ?></strong>
                                                            <?php
                                                          //  if ((int) $exams_report_setting->show_total_marks == 1) {
                                                                ?>
                                                                <!-- / <strong>student_total[$student->student_id] ?></strong> -->
                                                            <?php // } ?>
                                                        </th>
                                                        <th style="font-weight: bolder;width:20.6%;">AVERAGE:
                                                            <strong><?= isset($report[$student->student_id][0]) ? round($report[$student->student_id][0]['average'], 1) : '' ?></strong>
                                                        </th>
                                                        <?php if ($exams_report_setting->show_pos_in_class == 1) { ?>
                                                            <th style="font-weight: bolder">P/C:
                                                                <strong><?= isset($report[$student->student_id][0]) ? $report[$student->student_id][0]['rank'] : '' ?></strong>
                                                                out of <?= count($classrank); ?></th>
                                                        <?php } ?>
                                                        <?php if ($exams_report_setting->show_pos_in_stream == 1) { ?>
                                                            <th style="font-weight: bolder">P/S:<strong>
                                                                    <?= isset($sectionreport[$student->student_id][0]) ? $sectionreport[$student->student_id][0]['rank'] : '' ?></strong>
                                                                out of <strong>

                                                                    <?= count($sectionrank); ?></strong></th>
                                                        <?php } ?>
                                                        <?php
                                                    if(preg_match('/level/i', strtolower($level->name))){
                                                        if ($exams_report_setting->show_csee_division == 1 || $exams->show_division == 1) {

                                                            $division = getDivisionBySort($point, $level->result_format);
                                                            if (strtolower(trim($level->result_format)) == 'cambridge') {


                                                                echo '<th>' . cambridgeDivision($division[1]) . '</th>';
                                                            } else {
                                                                ?>
                                                                <th>@lang('exam_report_lang.division'): <?= $division[0] ?></th>
                                                                <th>mark_point") ?>: <?= $division[1] ?></th>

                                                            <?php }
                                                        }
                                                    }
                                                        ?>

                                                    </tr>
                                                </thead>

                                            </table>

                                            <div style="float: left; width: 100%">
                                                <table class="table table-striped table-bordered" style="  float: left; margin: 1% 0 0 0;">
                                                
                                                    <thead>
                                                        <tr>
                                                            <th colspan="<?=count($grades)?>" style="font-weight: bolder; line-height: 20px; text-align: center;">
                                                                <?= strtoupper($data->lang->line('menu_grade')) ?>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    
                                                    <tr>
                                                            <?php foreach ($grades as $grade) { ?>
                                                                <th style="font-weight: bolder; line-height: 20px; text-align: center;"><?= $grade->grade ?>
                                                                </th>
                                                                <?php } ?>
                                                        </tr>
                                                        <tr>
                                                            <?php foreach ($grades as $grade) { ?>
                                                                <td style="font-size: smaller; line-height: 20px; text-align: left;background-color: white;color: #000;"><?= $grade->gradefrom ?>
                                                                    - <?= $grade->gradeupto ?>
                                                                </td>
                                                                <?php } ?>
                                                        </tr>
                                                        <?php if(strtoupper($classes->classes) == 'FORM ONE' && set_schema_name() != 'stpeterclaver.') {    ?>
                                                        <tr>
                                                            <th colspan="5" style="font-weight: bolder; text-align: center; "><strong>KEY:</strong>
                                                                P/S:=pos_in_section") ?>,
                                                                P/C:=pos_in_class") ?></th>
                                                        </tr> 
                                                        <?php
                                                        }
                        if(set_schema_name() == 'stjohnbosco.'){ ?>
                                  <tr>
                                <th colspan="<?=count($grades)?>" style="font-weight: bolder; text-align: center; "><strong>Grade Status:</strong>
                                <?php $i = 1;
                                 foreach ($grades as $grade) {
                                    echo ' '. $grade->grade .' - '. $grade->note.' | ';
                                }
                                    ?>

                                </th>
                            </tr>
                            
                           
                        <?php }  ?>
                                                    </tbody>

                                                </table>
                                            </div>
                                            <?php
                                            /**
                                             *
                                             * ----------------------------------------------
                                             *
                                             * Report footer
                                             * ----------------------------------------------
                                             *
                                             *
                                             */
                                            if ($exams_report_setting->show_all_signature_on_the_footer == 0) {
                                                ?>
                                                <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:56%;">
                                                                <b style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px"><?= $exams_report_setting->class_teacher_remark ?> :</b><br> <?php
                                                                if (count($character) && isset($character[$student->student_id]) && $character[$student->student_id][0]->class_teacher_comment != '') {

                                                                    echo "<span class=''>";
                                                                    echo $character[$student->student_id][0]->class_teacher_comment;
                                                                    echo "</span>";
                                                                } else {

                                                                    $savg = isset($report[$student->student_id][0]) ? $report[$student->student_id][0]['average'] : 0;
                                                                    foreach ($grades as $grade) {
                                                                        if ($grade->gradefrom <= round($savg, 0) && $grade->gradeupto >= round($savg, 0)) {
                                                                            echo "<span class='' >";
                                                                            echo $grade->overall_academic_note;
                                                                            echo "</span>";
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                                ?></th>
                                                            <th>
                                                                <?php
                                                                $name = explode(' ', $class_teacher_signature->name);
                                                                $firstname = isset($name[0]) ? $name[0] : 'NIL';
                                                                $lastname = isset($name[count($name) - 1]) ? $name[count($name) - 1] : 'NILL';
                                                                ?>

                                                                <?php if (isset($report_setting) && is_object($report_setting) && $report_setting->show_classteacher_name == 1) { ?>

                                                                    class_teacher') ?> : <?= $class_teacher_signature->name ?>
                                                                    <br>Phone: <u><?= $class_teacher_signature->phone ?></u><br>
                                                                    <?php
                                                                    if ($class_teacher_signature->signature == NULL) {
                                                                        echo '<u>' . strtoupper($class_teacher_signature->name[0] . '.' . str_after($class_teacher_signature->name, ' ')[0]) . '</u>';
                                                                    }else {
                                                                        ?>
                                                                        <img src="<?= $class_teacher_signature->signature ?>" width="54"
                                                                             height="32">
                                                                             <?php
                                                                         }
                                                                     } else {
                                                                         ?>
                                                                    Class Teacher's Signature<br>
                                                                    <?php
                                                                     if ($class_teacher_signature->signature == NULL) {
                                                                        echo '<u>' . strtoupper($class_teacher_signature->name[0] . '.' . str_after($class_teacher_signature->name, ' ')[0]) . '</u>';
                                                                    } else {
                                                                        ?>
                                                                        <img src="<?= $class_teacher_signature->signature ?>" width="54"
                                                                             height="32">
                                                                             <?php
                                                                         }
                                                                     }
                                                                     ?>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                                <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 56%; "><b
                                                                    style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px"><?= $exams_report_setting->head_teacher_remark ?>
                                                                    :</b><br> <?php
                                                                if (count($character) && isset($character[$student->student_id]) && $character[$student->student_id][0]->head_teacher_comment != '') {
                                                                    echo "<span class=''>";
                                                                    echo $character[$student->student_id][0]->head_teacher_comment;
                                                                    echo "</span>";
                                                                } else {

                                                                    // $cavg = isset($report[$student->student_id][0][0]) ? $report[$student->student_id][0]['average'] : 0;
                                                                    $savg = isset($report[$student->student_id][0]) ? $report[$student->student_id][0]['average'] : 0;

                                                                    foreach ($grades as $grade) {
                                                                        if ($grade->gradefrom <= round($savg, 0) && $grade->gradeupto >= round($savg, 0)) {
                                                                            echo "<span class=''>";
                                                                            echo $grade->overall_note;
                                                                            echo "</span>";
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                                ?>  <?php
                                                                if (set_schema_name() == 'enaboishu.') {
                                                                    ?>
                                                                    <p>
                                                                    <hr/>
                                                                    <b style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px">Head
                                                                        Master comment: </b>Work Hard
                                                                    </p>
                <?php } ?></th>
                                                            <th style="width: 50%;"><?= $siteinfos->headname ?>'s<br/> Signature<br/></th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                                <?php
                                                /**
                                                 *
                                                 * ----------------------------------------------
                                                 *
                                                 * End of Report footer with buttom signature
                                                 * ----------------------------------------------
                                                 *
                                                 *
                                                 *
                                                 */
                                            } else {
                                                ?>

                                                <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                                    <thead>
                                                        <tr>
                                                            <th style=" width:56%;"><b
                                                                    style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px"><?= $exams_report_setting->class_teacher_remark ?>
                                                                    :</b><br> <?php
                                                                if (count($character) > 0 && isset($character[$student->student_id]) && $character[$student->student_id][0]->class_teacher_comment != '') {

                                                                    echo "<span class=''>";
                                                                    echo $character[$student->student_id][0]->class_teacher_comment;
                                                                    echo "</span>";
                                                                } else {

                                                                    $savg = isset($report[$student->student_id][0]) ? $report[$student->student_id][0]['average'] : 0;
                                                                    foreach ($grades as $grade) {
                                                                        if ($grade->gradefrom <= round($savg, 0) && $grade->gradeupto >= round($savg, 0)) {
                                                                            echo "<span class=''>";
                                                                            echo $grade->overall_academic_note;
                                                                            echo "</span>";
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                                ?></th>

                                                        </tr>
                                                        <tr>
                                                            <th style="width: 56%; "><b
                                                                    style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px"><?= $exams_report_setting->head_teacher_remark ?>
                                                                    :</b><br> <?php
                                                                if (count($character) && isset($character[$student->student_id]) && $character[$student->student_id][0]->head_teacher_comment != '') {
                                                                    echo "<span class=''>";
                                                                    echo $character[$student->student_id][0]->head_teacher_comment;
                                                                    echo "</span>";
                                                                } else {

                                                                    // $cavg = isset($report[$student->student_id][0][0]) ? $report[$student->student_id][0]['average'] : 0;
                                                                    $savg = isset($report[$student->student_id][0]) ? $report[$student->student_id][0]['average'] : 0;

                                                                    foreach ($grades as $grade) {
                                                                        if ($grade->gradefrom <= round($savg, 0) && $grade->gradeupto >= round($savg, 0)) {
                                                                            echo "<span class=''>";
                                                                            echo $grade->overall_note;
                                                                            echo "</span>";
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                                ?>  <?php
                                                                if (set_schema_name() == 'enaboishu.') {
                                                                    ?>
                                                                    <p>
                                                                    <hr/>
                                                                    <b style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px">Head
                                                                        Master comment: </b>Work Hard
                                                                    </p>
                <?php } ?></th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                                <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                <?php if (isset($report_setting) && is_object($report_setting) && $report_setting->show_classteacher_name == 1) { ?>
                                                                    <?= $class_teacher_signature->name ?>'s Signature<br>
                                                                    <?php
                                                                    if (isset($class_teacher_signature->signature) && $class_teacher_signature->signature == NULL) {
                                                                        echo strtoupper($firstname[0] . '.' . $lastname[0]);
                                                                    } else {
                                                                        ?>
                                                                        <img src="<?= $class_teacher_signature->signature ?>" width="54"
                                                                             height="32">
                                                                             <?php
                                                                         }
                                                                     } else {
                                                                         ?>
                                                                    Class Teacher's Signature<br>
                                                                    <?php
                                                                    if (isset($class_teacher_signature->signature) && $class_teacher_signature->signature == NULL) {
                                                                        echo strtoupper($firstname[0] . '.' . $lastname[0]);
                                                                    } else {
                                                                        ?>
                                                                        <img src="<?= $class_teacher_signature->signature ?>" width="54"
                                                                             height="32">
                                                                             <?php
                                                                         }
                                                                     }
                                                                     ?>
                                                            </th>

                                                            <th style="width: 50%;"> <?= $siteinfos->headname ?>'s<br/> Signature<br/></th>
                                                        </tr>
                                                    </thead>
                                                </table>

                                                <?php
                                                /**
                                                 *
                                                 * -----------------------------------------
                                                 *
                                                 * End of footer with both signature at the bottom
                                                 * ------------------------------------------
                                                 */
                                            }
                                            ?>


                                            <div style="padding-left:5%;">

                                                <div style="">

                                                    <?php
                                                    $path = "storage/uploads/images/" . $level->stamp . "?v=1";
                                                    ?>
                                                    <?php
                                                    $array = array(
                                                        "src" => $path,
                                                        'width' => '100',
                                                        'height' => '100',
                                                        'class' => '',
                                                        'id' => "",
                                                        'style' => 'position:relative; margin:-13% 10% 0 0; float:right; padding-bottom: 0px;'
                                                    );
                                                    echo img($array);
                                                    ?>

                                                </div>
                                            </div>
            <?php if (isset($report_setting->show_parent_comment) && $report_setting->show_parent_comment == 1) { ?>
                                                <div style="float: left; width: 100%">
                                                    <table class="table table-striped table-bordered" style="  float: left; margin: -13% 0 0 0;">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="4" style="font-weight: bolder; text-align: left;">
                                                                    Parent Comment:
                                                                    <br><br><br><br><br><br>

                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th colspan="2">
                                                                    <u> Parent Name</u>
                                                                    <br> <br>.................................................................
                                                                </th>
                                                                <th>
                                                                    <u> Signature</u>
                                                                    <br> <br>.....................................
                                                                </th>
                                                                <th>
                                                                    <u> Date</u>
                                                                    <br> <br> ...../...../...........
                                                                </th>

                                                            </tr>
                                                        </thead>
                                                    </table>
            <?php } ?>

            <?php 
              //Payment Reports
              if($report_setting->show_fee_payment_status == 1){
                $invoice = \App\Model\Invoice::where('student_id', $student->student_id)->where('academic_year_id', $academic_year_id)->first();
                if(!empty($invoice)){
                $total_paid = \App\Model\PaymentsInvoicesFeesInstallment::whereIn('invoices_fees_installment_id', \App\Model\InvoicesFeesInstallment::whereIn('invoice_id', \App\Model\Invoice::where('student_id', $student->student_id)->where('academic_year_id', $academic_year_id)->get(['id']))->get(['id']))->sum('amount');
                $total_amount = \App\Model\FeesInstallmentsClass::where('class_id', $classes->classesID)->whereIn('fees_installment_id', \App\Model\InvoicesFeesInstallment::whereIn('invoice_id', \App\Model\Invoice::where('student_id', $student->student_id)->where('academic_year_id', $academic_year_id)->get(['id']))->get(['fees_installment_id']))->sum('amount');
                $unpaid = $total_amount - $total_paid;
                if(set_schema_name() == 'stpeterclaver.'){
                    $bank_accounts = \App\Model\BankAccount::whereIn('id', [1,8,4])->get();
                }else{
                    $bank_accounts = \App\Model\BankAccount::limit(3)->get();
                }
                if($unpaid > 0){
            ?>
            <table class="table table-striped table-bordered" >
                <thead>
                    <tr>
                    <tr>
                        <th colspan="<?=count($bank_accounts)-1?>">Total {{ $invoice->academicYear->name }} Invoice Amount is <?= money($total_amount) ?></th>
                        <th colspan="1">Total Paid Amount is: <?= money($total_paid) ?></th>
                    </tr>
                    </tr>
                    <tr>
                        <th colspan="<?=count($bank_accounts)?>" style="text-align: center">
                            <b>Total Unpaid Amount is: <?= $unpaid > 0 ? money($unpaid) : '0' ?>/-</b>
                        </th>       
                    </tr>
                    <tr>
                    <th colspan="<?=count($bank_accounts)?>"><b>Please pay before opening date. For proper records use control number - <?php  echo $invoice->reference; if(substr($invoice->reference, 0, 4 ) === "SASA"){ echo ' - CRDB Only'; }else{ echo ' - NMB Only'; } ?> </b> </th>
                    </tr>
                   
                    <tr>
                        @foreach($bank_accounts as $account)
                        <th><?= ucfirst(strtolower($account->name))?> (<?=$account->number?>) - <?=$account->referBank->abbreviation?></th>
                        @endforeach
                    </tr>
                     
                </thead>
            </table>     
            <?php } } }  ?>
            </div>

                                            <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center;">
                                                            <?php if (!empty($exam_report) && date('dmY', strtotime($exam_report->reporting_date)) != '01011970') { ?>
                                                                <b><?php echo "Date of Reporting: " . date('jS M Y', strtotime($exam_report->reporting_date)) ?> </b>
            <?php } ?>
                                                        </th>
                                                    </tr>
                                                </thead>
                                            </table>

                                        </div>
        <?php } ?>


    <?php } ?>
                        </section>

                    </div>


                </div>
            </div>

            <?php
        }
        ?>
    </body>
</html>
<script>
    function nocontext(e) {
        var clickedTag = (e === null) ? event.srcElement.tagName : e.target.tagName;
        if (clickedTag === "IMG") {
            alert(alertMsg);
            return false;
        }
    }

    var alertMsg = "Please this document is copyrighted";
    document.oncontextmenu = nocontext;
</script>