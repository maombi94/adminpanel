
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-book"></i> panel_title') ?></h3>


        <ol class="breadcrumb">
            <li><a href="<?= url("dashboard/index") ?>"><i class="fa fa-laptop"></i> menu_dashboard') ?></a></li>
            <li class="active">menu_report') ?></li>

        </ol>

    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <?php
                $usertype = session("usertype");
                ?>

                <div class="col-sm-6 col-sm-offset-3 list-group">
                    <div class="list-group-item list-group-item-warning">

                        <p align="center"><span class="label label-info">class_report') ?></span></p>
                        <form class="form-horizontal" role="form" method="post">

                            <?php
                            if (form_error($errors, 'classesID'))
                                echo "<div class='form-group has-error' >";
                            else
                                echo "<div class='form-group' >";
                            ?>
                           
                            <div class="col-sm-6 col-xs-12 col-sm-offset-3">
                                <?php
                                $array = array("0" => $data->lang->line("eattendance_select_classes"));
                                foreach ($classes as $classa) {
                                    $array[$classa->classesID] = $classa->classes;
                                }
                                $class_id = isset($classesID) ? $classesID : '0';
                                $class_name = $array[$class_id];
                                echo form_dropdown("classesID", $array, NULL, "id='classesID' class='form-control'");
                                ?>
                            </div>
                    </div>
                    <div id="report_filter_div" style="display:none">
                        <?php
                        if (form_error($errors, 'academic_year_id'))
                            echo "<div class='form-group has-error' >";
                        else
                            echo "<div class='form-group' >";
                        ?>
                       
                        <div class="col-sm-6 col-xs-12 col-sm-offset-3">
                            <?php
                            $array = array("0" => $data->lang->line("exam_select_year"));
                            if (isset($academic_years)) {
                                foreach ($academic_years as $academic) {
                                    $array[$academic->id] = $academic->name;
                                }
                            }

                            echo form_dropdown("academic_year_id", $array, old("academic_year_id"), "id='academic_year_id' class='form-control'");
                            ?>
                        </div>
                    </div>

                    <?php
                    if (form_error($errors, 'sectionID'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                   
                    <div class="col-sm-6 col-xs-12 col-sm-offset-3">
                        <?php
                        $array = array("0" => $data->lang->line("exam_select_section"));
                        if (isset($sections)) {
                            foreach ($sections as $section) {
                                $array[$section->sectionID] = $section->section;
                            }
                        }

                        echo form_dropdown("sectionID", $array, old("sectionID"), "id='sectionID' class='form-control'");
                        ?>
                    </div>
                </div>

                <?php
                if (form_error($errors, 'semester_id'))
                    echo "<div class='form-group has-error' >";
                else
                    echo "<div class='form-group' >";
                ?>
               
                <div class="col-sm-6 col-xs-12 col-sm-offset-3">
                    <?php
                    $array = array("0" => $data->lang->line("exam_select_semester"));
                    if (isset($semesters)) {
                        foreach ($semesters as $semester) {
                            $array[$semester->id] = $semester->name;
                        }
                    }
                    echo form_dropdown("semester_id", $array, old("semester_id"), "id='semester_id' class='form-control'");
                    ?>
                </div> <span id="sem_id"></span>
            </div>
            <?php
            if (form_error($errors, 'examID'))
                echo "<div class='form-group has-error' >";
            else
                echo "<div class='form-group' >";
            ?>
            
            <div class="col-sm-6 col-xs-12 col-sm-offset-3">
                <?php
                $array = array("0" => $data->lang->line("eattendance_select_exam"));
                //  dd($exams);
                //                foreach ($exams as $exam) {
                //                    $array[$exam->examID] = $exam->exam;
                //                }
                echo form_dropdown("examID", $array, old("examID"), "id='examID' class='form-control'");
                ?>
            </div>
        </div>



        <?php
        if (form_error($errors, 'subjectID'))
            echo "<div class='form-group has-error' >";
        else
            echo "<div class='form-group' >";
        ?>
        
        <div class="col-sm-6 col-xs-12 col-sm-offset-3">
            <?php
            $array = array('0' => $data->lang->line("eattendance_select_subject"));
            if (isset($subjects) && count($subjects) > 0) {
                foreach ($subjects as $subject) {
                    $array[$subject->subjectID] = $subject->subject;
                }
            }

            $sID = 0;
            if ($subjectID == 0) {
                $sID = 0;
            } else {
                $sID = $subjectID;
            }

            echo form_dropdown("subjectID", $array, old("subjectID", $sID), " id='subjectID' class='form-control'");
            ?>
        </div>

    </div>


    <div class="form-group">
        <div class="col-sm-offset-4 col-sm-8">

            <!-- <p><input type="checkbox" name="show_rank" /> show_rank") ?></p> -->
            <p><input type="checkbox" name="show_grade" /> show_grade") ?></p>
            <p><input type="checkbox" name="show_footer" /> show_footer") ?></p>
            <p><input type="checkbox" name="show_gender" /> Show Report Rank by Gender</p>
            <input type="submit" class="btn btn-success" style="margin-bottom:0px" value="view_report") ?>">
        </div>
    </div>
</div>
<?= csrf_field() ?>
</form>
</div>

</div>
<?php if (isset($exam_info) && !empty($exam_info)) { ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-6 col-sm-offset-3 list-group">
                <div class="list-group-item">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>exam_name') ?></th>
                                <th>exam_dates') ?></th>
                                <th>class_name') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?= $exam_info->exam ?></td>
                                <td><?= date('d M Y', strtotime($exam_info->date)) ?></td>
                                <td><?= $class_name ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    <?php
}

if (!empty($students)) {
    ?>

        <div class="col-sm-12">

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#all" aria-expanded="true">eattendance_all_students") . '-' . $class_name ?></a></li>
                    <?php
                    foreach ($sections as $key => $section) {
                        echo '<li class=""><a data-toggle="tab" href="#' . $section->sectionID . '" aria-expanded="false">' . $data->lang->line("section") . " " . $section->section . " ( " . $section->category . " )" . '</a></li>';
                    }
                    ?>
                    <li class=""><a data-toggle="tab" href="#summary" aria-expanded="false">summary') ?></a></li>


                    <a data-toggle="modal" data-target="#export_report" style="float: right;" class="btn btn-success btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Export"><i class="fa fa-cloud-download"></i>CA Export</a>

                </ul>


                <div class="tab-content">

                    <div id="all" class="tab-pane active">
                        <div id="hide-table" class="table-responsive">
                            <?php
                            $sub_rank = isset($show_rank) && $show_rank == 1 && $show_grade == 1 ? 3 : 2;

                            $col_span = isset($show_grade) && $show_grade == 1 || $show_rank == 1 ? 'colspan="' . $sub_rank . '"' : '';
                            $row_span = isset($show_grade) && $show_grade == 1 || $show_rank == 1 ? 'rowspan="2"' : '';
                            ?>
                            <table class="table table-striped table-bordered table-hover dataTable no-footer">
                                <thead>
                                    <tr>
                                        <th class="col-sm-1" <?= $row_span ?>>slno') ?></th>

                                        <th class="col-sm-2" <?= $row_span ?>>eattendance_name') ?></th>
                                        {{-- <th class="col-sm-1" <?= $row_span ?>>gender') ?></th> --}}
                                        <th class="col-sm-1" <?= $row_span ?>>eattendance_roll') ?></th>

                                    <?php

                                        if ($subjectID == 0) {
                                            $average = 'AVG'; 
                                            //Loop in all subjects to show list of them here
                                            if (isset($subjects)) {
                                                foreach ($subjects as $subject) {
                                                    $subj = strtolower($subject->subject);
                                                    $sum_subj[$subj] = 0;
                                                    $pass["$subj"] = 0;
                                                    $fail["$subj"] = 0;
                                                    $subject_sum["$subj"] = 0;
                                                    $words = explode(' ', str_replace('/', '', $subject->subject));
                                                    $this_subject = "";
                                                    if(set_schema_name() == 'stpeterclaver.' || set_schema_name() == 'ibunjazarprimary'){
                                                        $check_ = str_replace('/', '', $subject->subject);
                                                    if(str_word_count($check_) > 1 && str_word_count($subject->subject) > 1){
                                                        $this_subject .= substr(strtoupper($words[1]), 0, 4);
                                                    }else{
                                                       $this_subject = substr(strtoupper($subject->subject), 0, 4);
                                                    }
                                                }else{
                                                    $this_subject = substr(strtoupper($subject->subject), 0, 4);
                                                }
                                                        
                                                    echo !empty($subject) ?
                                                        '<th class="col-sm-1 verticalTableHeader" ' . $col_span . '>'
                                                        . '<p>' . $this_subject . ''
                                                        . '</p></th>' : '<th></th>';
                                                }
                                            }
                                        } else {
                                            $average = 'Subject Average';
                                            echo '<th class="col-sm-2">Subject Name</th>';
                                        }
                                        ?>

                                        <th class="col-sm-1" <?= $row_span ?>>Total</th>
                                        <?php if ($subjectID == 0) { ?>
                                            <!--<th class="col-sm-2">Subject Counted</th>-->
                                        <?php } ?>
                                        <th class="col-sm-2" <?= $row_span ?>><?= $average ?></th>
                                        <th class="col-sm-2" <?= $row_span ?>>GD</th>

                                        <?php if ($exam_info->show_division == 1 && $subjectID == 0) { ?>
                                            <th <?= $row_span ?>>Div</th>
                                            <th <?= $row_span ?>>Point</th>
                                        <?php } ?>
                                        <th class="col-sm-2" <?= $row_span ?>>Overall Rank</th>
                                        <?php
                                        if (isset($show_gender) && $show_gender == 1) {
                                        ?>
                                            <th class="col-sm-2" <?= $row_span ?>>Rank per Gender</th>
                                        <?php } ?>
                                        <th class="col-sm-3" id="action_option" <?= $row_span ?>>action') ?></th>
                                    </tr>
                                    <?php if (isset($subjects) && isset($show_grade) && $show_grade == 1 || $show_rank == 1) { ?>
                                        <tr>

                                            <?php
                                            $subject_th = $show_rank == 1 ? '<th class="col-sm-2">Rank</th>' : '';
                                            $grade_th = $show_grade == 1 ? '<th class="col-sm-1">GD</th>' : '';
                                            foreach ($subjects as $subject) {
                                                if (!empty($subject)) {
                                            ?>
                                                    <th class="col-sm-1"><?= substr(strtoupper($subject->subject), 0, 3) ?></th>
                                                    <?= $grade_th ?>
                                                    <?= $subject_th ?>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </tr>
                                    <?php } ?>
                                </thead>
                                <tbody>
                                    <?php
                                    if (count($students)) {
                                        $i = 1;
                                        $student_pass = 0;
                                        $total_average = 0;
                                        $total_div_I = 0;
                                        $total_div_II = 0;
                                        $total_div_III = 0;
                                        $total_div_IV = 0;
                                        $total_div_0 = 0;

                                        foreach ($subjects as $subject) {
                                            $subject_name = strtolower($subject->subject);
                                            foreach ($grades as $grade) {
                                                ${$subject_name . '_' . $grade->grade} = 0;
                                            }
                                        }
                                        foreach ($students as $student) {
                                            $student = is_object($student) ? (array) $student : $student;
                                    ?>
                                            <tr>
                                                <td data-title="slno') ?>">
                                                    <?php echo $i ?>
                                                </td>
                                                <td data-title="eattendance_name') ?>">
                                                    <?= $student['name']; ?>
                                                </td>
                                              
                                                <td data-title="eattendance_roll') ?>">
                                                    <?= $student['roll']; ?>
                                                </td>
                                                <?php
                                                
                                                /*
                                                 * 
                                                 * -----------------------------------------
                                                 * This part show list of all subjects or ONE
                                                 *  subject depends on user selection
                                                 * -----------------------------------------
                                                 * 
                                                 */

                                                if ($subjectID == 0) {

                                                    $point[$student['student_id']] = [];
                                                    foreach ($subjects as $subject) {
                                                        $subject_name = strtolower($subject->subject);


                                                        if (isset($student["$subject_name"])) {
                                                            if ($student["$subject_name"] < $classlevel->pass_mark && $student["$subject_name"] != NULL) {

                                                                $color = "pink";
                                                                $fail["$subject_name"]++;
                                                            } else {
                                                                $color = "";
                                                                $student["$subject_name"] != NULL ? $pass["$subject_name"]++ : '';
                                                            }
                                                            $subject_sum["$subject_name"] = $subject_sum["$subject_name"] + (float) $student["$subject_name"];

                                                            echo '<td ' . $editable . ' class="mark" subject_id="' . $subject->subjectID . '" student_id="' . $student['student_id'] . '" data-title="' . $subject_name . '" style="background: ' . $color . ';">';
                                                            if(in_array(strtolower($userclass->classlevel->name) , ['primary','nursery']) !== false ){
                                                                echo (int)$student["$subject_name"];
                                                            }else{
                                                                echo $student["$subject_name"];
                                                            }
                                                            echo '</td>';
                                                            if (isset($show_grade) && $show_grade == 1) {
                                                                foreach ($grades as $grade) {
                                                                    if ($grade->gradefrom <= round($student["$subject_name"], 0) && $grade->gradeupto >= round($student["$subject_name"], 0)) {


                                                                        echo ' <td>' . $grade->grade . '</td>';
                                                                    }
                                                                }
                                                            }
                                                            echo $show_rank == 1 ? '<th class="col-sm-2">' . $subject_rank[$student['student_id']][$subject->subjectID] . '</th>' : '';

                                                            $sum_subj[$subject_name] = $sum_subj[$subject_name] + (float) $student["$subject_name"];
                                                            //
                                                            foreach ($grades as $grade) {
                                                                if ($grade->gradefrom <= round($student["$subject_name"], 0) && $grade->gradeupto >= round($student["$subject_name"], 0)) {
                                                                    ${strtolower($subject_name) . '_' . $grade->grade}++;
                                                                    $sub = [
                                                                        'subject_mark' => round($student["$subject_name"], 0),
                                                                        'point' => $grade->point,
                                                                        'penalty' => $subject->is_penalty,
                                                                        'pass_mark' => $subject->pass_mark,
                                                                        'is_counted_indivision' => $subject->is_counted_indivision
                                                                    ];
                                                                    array_push($point[$student['student_id']], $sub);
                                                                }
                                                            }
                                                        } else {
                                                            //here you will check if this student subscribe to this subject or not . if yes, place a yellow box, if not disable the input if result format is based on the subject counted otherwise, place yellow and content editable in all cases



                                                            echo '<td ' . $editable . '  data-title="' . $subject_name . '" class="mark" subject_id="' . $subject->subjectID . '" student_id="' . $student['student_id'] . '"></td>';
                                                            echo isset($show_grade) && $show_grade == 1 ? '<td></td>' : '';
                                                            echo $show_rank == 1 ? '<th class="col-sm-2"></th>' : '';
                                                        }
                                                    }
                                                } else {

                                                    echo '<td data-title="subject">';
                                                    echo $student['subject'];
                                                    echo '</td>';
                                                }

                                                if ($subjectID == '0') {
                                                    echo '<td data-title="Total">' . $student['total'] . ' </td>';
                                                } else {
                                                    if ((isset($student['mark']) && $student['mark'] < $classlevel->pass_mark)) {

                                                        $color = "pink";
                                                    } else {
                                                        $color = "";
                                                        $student_pass++;
                                                    }

                                                    //

                                                    echo '<td style="background-color:' . $color . '">' . $student['mark'] . '</td>';
                                                }
                                                ?>
                                                <?php if ($subjectID == 0) { ?>
                                                    <!--                                                    <td><a href="<?= url('subject/subject_count/' . $student['student_id'] . '/' . $academic_year_id) ?>"><?= isset($student['subject_count']) ? $student['subject_count'] : '' ?></a></td>-->
                                                <?php } ?>
                                                <?php
                                                if ($subjectID == '0') {

                                                    /**
                                                     * For the whole class, find total marks of that student
                                                     * and devide by total subjects taken by such student
                                                     */
                                                    if ((isset($student) && $student['average'] < $classlevel->pass_mark)) {

                                                        $color = "pink";
                                                    } else {
                                                        $color = "";
                                                        $student_pass++;
                                                    }
                                                    $total_average = $total_average + (float) $student['average'];
                                                    echo '<td data-title="" style="background: ' . $color . ';">';
                                                    echo $student['average'];
                                                    echo '</td>';

                                                    $grade_column = '';
                                                    foreach ($grades as $grade) {

                                                        if ($grade->gradefrom <= round($student['average'], 0) && $grade->gradeupto >= round($student['average'], 0)) {
                                                            $grade_column = '<td>' . $grade->grade . '</td>';
                                                        }
                                                    }
                                                    if ($grade_column == '') {
                                                        echo '<td></td>';
                                                    } else {
                                                        echo $grade_column;
                                                    }
                                                } else {
                                                    /*
                                                     * For one subject, just find total marks 
                                                     * for that subject and devide by number of students
                                                     */
                                                    $total_marks = 0;
                                                    foreach ($students as $student_sum) {

                                                        $total_marks = $total_marks + (float) $student_sum->mark;
                                                    }
                                                    $class_average_marks = $total_marks / count($students);
                                                    $color = $class_average_marks < $classlevel->pass_mark ? "pink" : "";
                                                    echo '<td data-title="AVG" style="background: ' . $color . ';">';
                                                    echo round($class_average_marks, 2);
                                                    echo '</td>';
                                                    $grade_column = '';
                                                    foreach ($grades as $grade) {

                                                        if ($grade->gradefrom <= round($class_average_marks, 0) && $grade->gradeupto >= round($class_average_marks, 0)) {
                                                            $grade_column = '<td>' . $grade->grade . '</td>';
                                                        }
                                                    }
                                                    if ($grade_column == '') {
                                                        echo '<td></td>';
                                                    } else {
                                                        echo $grade_column;
                                                    }
                                                }
                                                ?>
                                                <?php if ($exam_info->show_division && $subjectID == '0') { ?>
                                                    <td data-title='Div'><?php
                                                                            $division = getDivisionBySort($point[$student['student_id']], $classlevel->result_format);

                                                                            echo $division[0];




                                                                            $total_div_0 = $division[0] == '0' ? $total_div_0 + 1 : $total_div_0;
                                                                            $total_div_I = $division[0] == 'I' ? $total_div_I + 1 : $total_div_I;
                                                                            $total_div_II = $division[0] == 'II' ? $total_div_II + 1 : $total_div_II;
                                                                            $total_div_III = $division[0] == 'III' ? $total_div_III + 1 : $total_div_III;
                                                                            $total_div_IV = $division[0] == 'IV' ? $total_div_IV + 1 : $total_div_IV;
                                                                            ?></td>
                                                    <td data-title='points'>
                                                        <span class="point" id="points<?= $student['student_id'] . $examID . $classesID ?>"></span>
                                                        <?php
                                                        echo $division[1];
                                                        ?></td>
                                                <?php } ?>
                                                <td data-title="rank') ?>">
                                                    <?php echo $student['rank']; ?>
                                                </td>
                                                <?php
                                                if (isset($show_gender) && $show_gender == 1) {
                                                ?>
                                                    <td data-title="rank') ?>">
                                                        <?php echo isset($student['gender_rank']) ? $student['gender_rank'] : ''; ?>
                                                    </td>
                                                <?php } ?>
                                                <td data-title="action') ?>" class="action_btn">
                                                    <a href="<?= url('exam/singlereport/' . $student['student_id'] . '?exam=' . $examID . '&class_id=' . $classesID . '&year_no=' . $academic_year_id) ?>" class="btn btn-success btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Download Report"><i class="fa fa-file"></i>Preview</a>

                                                    <a href="<?= url('exam/singleReportAnalysis/' . $student['student_id'] . '/' . $examID . '/' . $academic_year_id) ?>" class="btn btn-info btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="View graph"><i class="fa fa-file-o"></i></a>

                                                    <a href="<?= url('subject/subject_count/' . $student['student_id'] . '/' . $academic_year_id) ?>"> <i class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="left" data-trigger="hover" data-content="<?= isset($student['subject_count']) ? $student['subject_count'] : '' ?> subjects done by   <?= $student['name']; ?>" title="Subject Counted">Info</i></a>
                                                    <?php if (can_access('view_exam') && strtolower(trim($classlevel->result_format)) == 'cambridge') { ?>
                                                        <a href="<?= url('cambridge/single/' . $student['student_id'] . '/' . $examID . '/' . $academic_year_id) ?>" class="btn btn-dark btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="View single report"><i class="fa fa-paperclip"></i></a>
                                                    <?php } ?>

                                                </td>
                                            </tr>
                                    <?php
                                            $i++;
                                        }
                                    }
                                    ?>
                                </tbody>

                                <tfoot style="<?= isset($show_footer) && $show_footer == 1 ? '' : 'display:none' ?>">
                                    <tr>
                                        <th class="col-sm-1"></th>
                                        <th class="col-sm-2">Total</th>
                                        <?php
                                        if (isset($show_gender) && $show_gender == 1) {
                                        ?>
                                            <th class="col-sm-1"></th>
                                        <?php } ?>
                                        <th class="col-sm-1"></th>

                                        <?php
                                        $total_of_total = 0;
                                        if ($subjectID == 0) {
                                            //Loop in all subjects to show list of them here
                                            if (isset($subjects)) {
                                                foreach ($subjects as $subject) {
                                                    if (isset($subject_evaluations)) {
                                                        foreach ($subject_evaluations as $subject_ev) {

                                                            if ($subject->subjectID == $subject_ev->subjectID) {
                                                                echo '<th class="col-sm-2"  ' . $col_span . '> ' . $subject_ev->sum . '</th>';
                                                                $total_of_total += $subject_ev->sum;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {

                                            echo '<th class="col-sm-2">Subject Name</th>';
                                        }
                                        ?>

                                        <th class="col-sm-2"><?= $total_of_total ?></th>
                                        <th class="col-sm-2"></th>
                                        <th></th>

                                        <?php if ($exam_info->show_division == 1) { ?>
                                            <th></th>
                                            <th></th>
                                        <?php } ?>
                                        <th class="col-sm-1"></th>
                                        <?php
                                        if (isset($show_gender) && $show_gender == 1) {
                                        ?>
                                            <th class="col-sm-1"></th>
                                        <?php } ?>
                                        <th class="col-sm-3" id="action_option"></th>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-1"></th>

                                        <th class="col-sm-2">AVG</th>
                                        <?php
                                        if (isset($show_gender) && $show_gender == 1) {
                                        ?>
                                            <th class="col-sm-1"></th>
                                        <?php } ?>
                                        <th class="col-sm-1"></th>

                                        <?php
                                        if ($subjectID == 0) {
                                            //Loop in all subjects to show list of them here
                                            if (isset($subjects)) {
                                                foreach ($subjects as $subject) {
                                                    if (isset($subject_evaluations)) {
                                                        foreach ($subject_evaluations as $subject_class_ev) {

                                                            if ($subject->subjectID == $subject_class_ev->subjectID) {
                                                                echo '<th class="col-sm-2"> ' . $subject_class_ev->round . '</th>';
                                                                if (isset($show_grade) && $show_grade == 1) {
                                                                    foreach ($grades as $grade) {
                                                                        if ($grade->gradefrom <= round($subject_class_ev->round, 0) && $grade->gradeupto >= round($subject_class_ev->round, 0)) {


                                                                            echo ' <td>' . $grade->grade . '</td>';
                                                                        }
                                                                    }
                                                                }
                                                                echo $show_rank == 1 ? '<th></th>' : '';
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {

                                            echo '<th class="col-sm-2">Subject Name</th>';
                                        }
                                        ?>

                                        <th class="col-sm-2">
                                            <?= round($total_of_total / count($students), 1) ?>
                                        </th>
                                        <?php if ($subjectID == 0) { ?>
                                            <!--<th class="col-sm-2">Subject Counted</th>-->
                                        <?php } ?>
                                        <th class="col-sm-2"></th>

                                        <?php if ($exam_info->show_division == 1) { ?>
                                            <th></th>
                                            <th></th>
                                        <?php } ?>
                                        <th class="col-sm-2"></th>
                                        <th></th>
                                        <?php
                                        if (isset($show_gender) && $show_gender == 1) {
                                        ?>
                                            <th class="col-sm-1"></th>
                                        <?php } ?>
                                        <!--<th class="col-sm-2">total_point') ?></th>-->
                                        <th class="col-sm-3" id="action_option"></th>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-1"></th>
                                        <!--<th class="col-sm-2">eattendance_photo') ?></th>-->
                                        <th class="col-sm-2">Rank</th>
                                        <?php
                                        if (isset($show_gender) && $show_gender == 1) {
                                        ?>
                                            <th class="col-sm-1"></th>
                                        <?php } ?>
                                        <th class="col-sm-1"></th>

                                        <?php
                                        if ($subjectID == 0) {
                                            //Loop in all subjects to show list of them here
                                            if (isset($subjects)) {
                                                foreach ($subjects as $subject) {
                                                    if (isset($subject_evaluations)) {
                                                        foreach ($subject_evaluations as $subject_class_ev) {

                                                            if ($subject->subjectID == $subject_class_ev->subjectID) {
                                                                echo '<th class="col-sm-2" ' . $col_span . '> ' . $subject_class_ev->ranking . '</th>';
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {

                                            echo '<th class="col-sm-2">Subject Name</th>';
                                        }
                                        ?>

                                        <th class="col-sm-2"></th>
                                        <?php if ($subjectID == 0) { ?>
                                            <!--<th class="col-sm-2">Subject Counted</th>-->
                                        <?php } ?>
                                        <th class="col-sm-2"></th>

                                        <?php if ($exam_info->show_division == 1) { ?>
                                            <th></th>
                                            <th></th>
                                        <?php } ?>
                                        <th class="col-sm-2"></th>
                                        <th></th>
                                        <?php
                                        if (isset($show_gender) && $show_gender == 1) {
                                        ?>
                                            <th class="col-sm-1"></th>
                                        <?php } ?>
                                        <!--<th class="col-sm-2">total_point') ?></th>-->
                                        <th class="col-sm-3" id="action_option"></th>
                                    </tr>
                                    {{-- <tr><th colspan="22">Subject Performance</th></tr> --}}
                                        <tr>
                                            <th class="col-sm-1">CODE</th>
                                            <th  colspan="2" class="col-sm-3">NAME</th>

                                            <th colspan="3" class="col-sm-3">TEACHER </th>
                                            <th  colspan="1" class="col-sm-3">TYPE</th>
                                            <?php
                                            if (count($grades)) {
                                                foreach ($grades as $grade) {
                                            ?>
                                                <th class="col-sm-2"><?= $grade->grade ?></th>

                                            <?php
                                                }
                                            }
                                            ?>
                                            <th class="col-sm-1">SAT</th>
                                            <th class="col-sm-1">ABSENT</th>
                                            <th class="col-sm-2">PASS</th>
                                            <th class="col-sm-2">FAIL</th>
                                            <th class="col-sm-2">Average</th>
                                            <th colspan="4" class="col-sm-2">Subj Average Grade</th>
                                            <?php
                                            if (((int)($section->classes->classlevel->school_level_id) == 3) || ((int)($section->classes->classlevel->school_level_id) == 4)) {
                                                ?>
                                                <th class="col-sm-2">GPA</th>
                                            <?php } ?>

                                        </tr>
                                    <?php
                                    $total_subjects = 0;
                                    $total_gpas = 0;
                                    $counts=1;
                                    if (count($subjects) > 0) {
                                       
                                        foreach ($subjects as $subject) {
                                            $subj = strtolower($subject->subject);
                                            $total_sat = $pass[$subj] + $fail[$subj];
                                    ?>
                                            <tr>
                                                <td><?= $counts++ ?></td>
                                                <td colspan="2"><?= ucwords($subject->subject) ?></td>
                                                <td colspan="3"><?= ucwords($subject->teacher) ?></td>
                                                <td colspan="1"><?= $subject->subject_type ?></td>
                                                <?php
                                            $total_points = 0;
                                                if (count($grades)) {
                                                    foreach ($grades as $grade) {
                                                ?>
                                                    <td><?= ${$subj . '_' . $grade->grade} ?></td>

                                                <?php
                                                        $total_points = $total_points + ((int)(${$subj . '_' . $grade->grade}) * (int)($grade->point));
                                                    }
                                                }
                                                ?>
                                                <td><?= $total_sat ?></td>
                                                <td><?= count($total_students) - $total_sat ?></td>

                                                <td><?= $pass[$subj] ?></td>
                                                <td><?= $fail[$subj] ?></td>
                                                <td><?php
                                                    if ($total_sat == 0) {
                                                        echo '0';
                                                        $subj_avg = 0;
                                                    } else {
                                                        $subj_avg = round($subject_sum[$subj] / $total_sat, 2);
                                                        echo $subj_avg;
                                                    }
                                                    ?></td>
                                                <td colspan="4">
                                                    <?php
                                                    foreach ($grades as $grade) {
                                                        if ($grade->gradefrom <= round($subj_avg, 0) && $grade->gradeupto >= round($subj_avg, 0)) {
                                                            echo $grade->grade;
                                                        }
                                                    }
                                                    ?>
                                                </td>
                                                <?php if (((int)($section->classes->classlevel->school_level_id) == 3) || ((int)($section->classes->classlevel->school_level_id) == 4)) {   ?>
                                                    <td colspan="2">
                                                        <?php
                                                        if ($total_sat == 0) {
                                                            echo '0';
                                                        } else {
                                                            echo  round(($total_points / $total_sat), 2);
                                                        }
                                                        ?>
                                                    </td>
                                                <?php } ?>
                                                <td></td>

                                            </tr>
                                        <?php
                                            if ($total_sat == 0) {
                                                $total_subjects = $total_subjects;
                                                $total_gpas = $total_gpas;
                                            } else {
                                                $total_subjects = $total_subjects + 1;
                                                $total_gpas = $total_gpas + ($total_points / $total_sat);
                                            }
                                        }
                                        if ($total_subjects == 0) {
                                            $average_gpa = 0;
                                        } else {
                                            $average_gpa = round(($total_gpas / $total_subjects), 2);
                                        }
                                        ?>
                                        <input type="hidden" value="<?= $average_gpa; ?>" id="gpa">
                                    <?php
                                    }
                                    ?>
                                </tfoot>

                            </table>

                        </div>

                    </div>

                    <?php foreach ($sections as $key => $section) { ?>
                        <div id="<?= $section->sectionID ?>" class="tab-pane">
                            <div id="hide-table">
                                <table class="table table-striped table-bordered table-hover dataTable no-footer">
                                    <thead>
                                        <tr>
                                            <th class="col-sm-1">slno') ?></th>

                                            <th class="col-sm-2">eattendance_name') ?></th>
                                        <?php  if (isset($show_gender) && $show_gender == 1 || set_schema_name() == 'stpeterclaver.') { ?>
                                            <th class="col-sm-1">sex') ?></th>
                                            <?php } ?>
                                            <?php  if (set_schema_name() != 'stpeterclaver.') { ?>
                                            <th class="col-sm-2">eattendance_roll') ?></th>

                                        <?php
                                            }
                                            if ($subjectID == 0) {
                                                $average = 'Student Average';
                                                //Loop in all subjects to show list of them here
                                                if (isset($subjects)) {
                                                    foreach ($subjects as $subject) {
                                                        $words = explode(' ', str_replace('/', '', $subject->subject));
                                                        $this_subject = "";
                                                        $check_ = str_replace('/', '', $subject->subject);
                                                        if(set_schema_name() == 'stpeterclaver.' || set_schema_name() == 'ibunjazarprimary'){
                                                            if(str_word_count($check_) > 1 && str_word_count($subject->subject) > 1){                                                            $this_subject .= substr(strtoupper($words[1]), 0, 4);
                                                            }else{
                                                           $this_subject = substr(strtoupper($subject->subject), 0, 4);
                                                        }
                                                        }else{
                                                            $this_subject = substr(strtoupper($subject->subject), 0, 4);
                                                        }
                                                        echo !empty($subject) ? '<th class="col-sm-2">' . $this_subject . '</th>' : '<th></th>';
                                                    }
                                                }
                                            } else {
                                                $average = 'Subject Average';
                                                echo '<th class="col-sm-2">Subject Name</th>';
                                            }
                                            ?>

                                            <th class="col-sm-2">Total Marks</th>
                                            <th class="col-sm-2"><?= $average ?></th>

                                            <?php if ($exam_info->show_division == 1 && $subjectID == '0') { ?>
                                                <th>Div</th>
                                                <th>Point</th>
                                            <?php } ?>
                                            <th class="col-sm-2">Rank</th>
                                            <!--<th class="col-sm-2">total_point') ?></th>-->
                                            <th class="col-sm-2">action') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (count($allsection[$section->section])) {
                                            $i = 1;

                                            foreach ($allsection[$section->section] as $student) {


                                                $student = is_object($student) ? (array) $student : $student;

                                                $point[$student['student_id']] = [];
                                        ?>
                                                <tr>
                                                    <td data-title="slno') ?>">
                                                        <?php echo $i; ?>
                                                    </td>

                                                    <td data-title="eattendance_name') ?>">
                                                        <?php echo $student['name']; ?>
                                                    </td>
                                                <?php  if (isset($show_gender) && $show_gender == 1 || set_schema_name() == 'stpeterclaver.') { ?>
                                                    <td data-title="sex') ?>">
                                                        <?php echo $student['sex']; ?>
                                                    </td>
                                                <?php } if(set_schema_name() != 'stpeterclaver.'){ ?>
                                                    <td data-title="eattendance_roll') ?>">
                                                        <?php echo $student['roll']; ?>
                                                    </td>
                                                    <?php
                                                    }
                                                    /*
                                                     * 
                                                     * -----------------------------------------
                                                     * This part show list of all subjects or ONE
                                                     *  subject depends on user selection
                                                     * -----------------------------------------
                                                     * 
                                                     */
                                                    if ($subjectID == 0) {


                                                        //print_r($total_marks); exit;
                                                        $point[$student['student_id']] = [];
                                                        foreach ($subjects as $subject) {

                                                            $subject_name = strtolower($subject->subject);
                                                            if (isset($student["$subject_name"])) {
                                                                if ($student["$subject_name"] < $classlevel->pass_mark) {

                                                                    $color = "pink";
                                                                } else {
                                                                    $color = "";
                                                                }

                                                                echo '<td data-title="' . $subject_name . '" style="background: ' . $color . ';">';
                                                                echo $student["$subject_name"];

                                                                echo '</td>';
                                                                foreach ($grades as $grade) {
                                                                    if ($grade->gradefrom <= round($student["$subject_name"], 0) && $grade->gradeupto >= round($student["$subject_name"], 0)) {
                                                                        // ${$subject_name . '_' . $grade->grade} ++;
                                                                        $sub = [
                                                                            'subject_mark' => round($student["$subject_name"], 0),
                                                                            'point' => $grade->point,
                                                                            'penalty' => $subject->is_penalty,
                                                                            'pass_mark' => $subject->pass_mark,
                                                                            'is_counted_indivision' => $subject->is_counted_indivision
                                                                        ];
                                                                        array_push($point[$student['student_id']], $sub);
                                                                    }
                                                                }
                                                            } else {
                                                                echo '<td></td>';
                                                            }
                                                        }
                                                    } else {

                                                        echo '<td>';

                                                        echo $student['subject'];
                                                        echo '</td>';
                                                    }

                                                    if ($subjectID == '0') {
                                                        echo '<td data-title="Total" >' . $student['total'] . '</td>';
                                                    } else {
                                                        echo '<td>' . $student['mark'] . '</td>';
                                                    }
                                                    ?>

                                                    <?php
                                                    if ($subjectID == '0') {

                                                        /**
                                                         * For the whole class, find total marks of that student
                                                         * and devide by total subjects taken by such student
                                                         */
                                                        $color = (isset($student) && $student['average'] < $classlevel->pass_mark) ? "pink" : "";
                                                        echo '<td data-title="AVG" style="background: ' . $color . ';">';
                                                        echo $student['average'];
                                                        echo '</td>';
                                                    } else {
                                                        /*
                                                         * For one subject, just find total marks 
                                                         * for that subject and divide by number of students
                                                         */

                                                        $total_marks = 0;
                                                        foreach ($allsection[$section->section] as $student_sum) {

                                                            $total_marks = $total_marks + $student_sum->mark;
                                                        }
                                                        $average_marks = $total_marks / count($allsection[$section->section]);

                                                        $color = $average_marks < $classlevel->pass_mark ? "pink" : "";
                                                        echo '<td data-title="AVG" style="background:' . $color . ' ;">';
                                                        echo round($average_marks, 2);
                                                        echo '</td>';
                                                    }
                                                    ?>

                                                    <?php if ($exam_info->show_division && $subjectID == '0') { ?>
                                                        <td data-title="DIV"><?php
                                                                                $division = getDivisionBySort($point[$student['student_id']], $classlevel->result_format);
                                                                                echo $division[0];
                                                                                ?></td>
                                                        <td data-title="Points"><?php
                                                                                echo $division[1];
                                                                                ?></td>
                                                    <?php } ?>

                                                    <td data-title="rank') ?>">
                                                        <?php
                                                        echo $student['rank'];
                                                        ?>
                                                    </td>

                                                    <td data-title="action') ?>">
                                                        <a href="<?= url('exam/singlereport/' . $student['student_id'] . '?exam=' . $examID . '&class_id=' . $classesID . '&year_no=' . $academic_year_id) ?>" class="btn btn-success btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Download Report"><i class="fa fa-download"></i>Preview</a>

                                                        <a href="<?= url('exam/singleReportAnalysis/' . $student['student_id'] . '/' . $examID . '/' . $academic_year_id) ?>" class="btn btn-info btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="View graph"><i class="fa fa-file-o"></i></a>

                                                    </td>
                                                </tr>
                                        <?php
                                                $i++;
                                            }
                                        }
                                        ?>
                                    </tbody>
                                    <?php
                                    if (isset($show_footer) && $show_footer == 1) {
                                    ?>
                                        <tfoot>
                                            <tr>
                                                <th class="col-sm-1"></th>
                                                <!--<th class="col-sm-2">eattendance_photo') ?></th>-->
                                                <th class="col-sm-2">Total</th>
                                                <th class="col-sm-1"></th>

                                                <?php
                                                if ($subjectID == 0) {
                                                    //Loop in all subjects to show list of them here
                                                    if (isset($subjects)) {
                                                        foreach ($subjects as $subject) {
                                                            if (isset($subject_section_evaluations[$section->section])) {
                                                                foreach ($subject_section_evaluations[$section->section] as $subject_sec_ev) {

                                                                    if ($subject->subjectID == $subject_sec_ev->subjectID) {
                                                                        echo '<th class="col-sm-2"> ' . $subject_sec_ev->sum . '</th>';
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                } else {

                                                    echo '<th class="col-sm-2">Subject Name</th>';
                                                }
                                                ?>

                                                <th class="col-sm-2"></th>
                                                <?php if ($subjectID == 0) { ?>
                                                    <!--<th class="col-sm-2">Subject Counted</th>-->
                                                <?php } ?>
                                                <th class="col-sm-2"></th>

                                                <?php if ($exam_info->show_division == 1) { ?>
                                                    <th></th>
                                                    <th></th>
                                                <?php } ?>
                                                <th class="col-sm-2"></th>
                                                <!--<th class="col-sm-2">total_point') ?></th>-->
                                                <th class="col-sm-3" id="action_option"></th>
                                            </tr>
                                            <tr>
                                                <th class="col-sm-1"></th>
                                                <!--<th class="col-sm-2">eattendance_photo') ?></th>-->
                                                <th class="col-sm-2">AVG</th>
                                                <th class="col-sm-1"></th>

                                                <?php
                                                if ($subjectID == 0) {
                                                    //Loop in all subjects to show list of them here
                                                    if (isset($subjects)) {
                                                        foreach ($subjects as $subject) {
                                                            if (isset($subject_section_evaluations[$section->section])) {
                                                                foreach ($subject_section_evaluations[$section->section] as $subject_sec_ev) {

                                                                    if ($subject->subjectID == $subject_sec_ev->subjectID) {
                                                                        echo '<th class="col-sm-2"> ' . $subject_sec_ev->round . '</th>';
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                } else {

                                                    echo '<th class="col-sm-2">Subject Name</th>';
                                                }
                                                ?>

                                                <th class="col-sm-2"></th>
                                                <?php if ($subjectID == 0) { ?>
                                                    <!--<th class="col-sm-2">Subject Counted</th>-->
                                                <?php } ?>
                                                <th class="col-sm-2"></th>

                                                <?php if ($exam_info->show_division == 1) { ?>
                                                    <th></th>
                                                    <th></th>
                                                <?php } ?>
                                                <th class="col-sm-2"></th>
                                                <!--<th class="col-sm-2">total_point') ?></th>-->
                                                <th class="col-sm-3" id="action_option"></th>
                                            </tr>
                                            <tr>
                                                <th class="col-sm-1"></th>
                                                <!--<th class="col-sm-2">eattendance_photo') ?></th>-->
                                                <th class="col-sm-2">Rank</th>
                                                <th class="col-sm-1"></th>

                                                <?php
                                                if ($subjectID == 0) {
                                                    //Loop in all subjects to show list of them here
                                                    if (isset($subjects)) {
                                                        foreach ($subjects as $subject) {
                                                            if (isset($subject_section_evaluations[$section->section])) {
                                                                foreach ($subject_section_evaluations[$section->section] as $subject_sec_ev) {

                                                                    if ($subject->subjectID == $subject_sec_ev->subjectID) {
                                                                        echo '<th class="col-sm-2"> ' . $subject_sec_ev->ranking . '</th>';
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                } else {

                                                    echo '<th class="col-sm-2">Subject Name</th>';
                                                }
                                                ?>

                                                <th class="col-sm-2"></th>
                                                <?php if ($subjectID == 0) { ?>
                                                    <!--<th class="col-sm-2">Subject Counted</th>-->
                                                <?php } ?>
                                                <th class="col-sm-2"></th>

                                                <?php if ($exam_info->show_division == 1) { ?>
                                                    <th></th>
                                                    <th></th>
                                                <?php } ?>
                                                <th class="col-sm-2"></th>
                                                <!--<th class="col-sm-2">total_point') ?></th>-->
                                                <th class="col-sm-3" id="action_option"></th>
                                            </tr>
                                        </tfoot>
                                    <?php } ?>
                                </table>

                            </div>
                        </div>
                    <?php } ?>
                    <div id="summary" class="tab-pane">
                        <div id="hide-table">
                            <?php if ($subjectID == '0') { ?>
                                <div class="row">
                                    <div class="title_left">
                                        <br />

                                        <h3><?php
                                            if (isset($sectionID) && $sectionID > 0) {
                                                //wrong gun, I dont wish to find DB, so I fake it
                                                foreach ($sections as $section) {
                                                    $sec = $section->section;
                                                }
                                                echo 'SECTION ' . $sec;
                                            } else {
                                                echo 'CLASS';
                                            }
                                            ?> PERFORMANCE</h3>
                                        <br />
                                    </div>

                                    <table class="table table-striped table-bordered dataTable table-hover no-footer">
                                        <thead>
                                            <tr>
                                                <th class="col-sm-1">TOTAL Students</th>
                                                <?php
                                                if (isset($sectionID) && $sectionID == 0) {
                                                    foreach ($sections as $section) {
                                                ?>
                                                        <th>Total students in <?= $section->section ?> Stream</th>
                                                <?php
                                                    }
                                                }
                                                ?>
                                                <th class="col-sm-2">ABSENT</th>
                                                <th class="col-sm-2">SAT</th>
                                                <th class="col-sm-2">PASS</th>
                                                <th class="col-sm-2">FAIL</th>
                                                <th class="col-sm-2">Class Exam Average</th>
                                                <th class="col-sm-2">Class Total</th>
                                                <th class="col-sm-2">Class Total Average</th>
                                                <?php
                                                if (((int)($section->classes->classlevel->school_level_id) == 3) || ((int)($section->classes->classlevel->school_level_id) == 4)) {
                                                ?>
                                                    <th class="col-sm-2">GPA</th>
                                                <?php
                                                } ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?= count($total_students) ?></td>
                                                <?php
                                                if ($sectionID == 0) {
                                                    foreach ($sections as $section) {
                                                ?>
                                                        <td><?= count($allsection[$section->section]) ?></td>
                                                <?php
                                                    }
                                                }
                                                ?>
                                                <td><?= count($total_students) - count($students) ?></td>
                                                <td><?= count($students) ?></td>
                                                <td><?= $student_pass ?></td>
                                                <td><?= count($students) - $student_pass ?></td>
                                                <td><?= $subjectID == '0' ? round($total_average / count($students), 2) : round($class_average_marks, 2) ?></td>
                                                <td><?= $total_of_total ?></td>
                                                <td><?= round($total_of_total / count($total_students), 1) ?></td>
                                                <?php
                                                if (((int)($section->classes->classlevel->school_level_id) == 3) || ((int)($section->classes->classlevel->school_level_id) == 4)) {
                                                ?>
                                                    <td id="gpa1"></td>
                                                <?php } ?>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>


                                <div class="row">
                                    <div class="title_left">
                                        <br /><br />
                                        <h3>SUBJECTS PERFORMANCE</h3>
                                        <br />
                                    </div>
                                    <script type="text/javascript">
                                        $(function() {

                                            $('#container').highcharts({
                                                chart: {
                                                    type: 'column'
                                                },
                                                title: {
                                                    text: "Subject Performance Evaluation"
                                                },
                                                subtitle: {
                                                    text: ''
                                                },
                                                xAxis: {
                                                    type: 'category'
                                                },
                                                yAxis: {
                                                    title: {
                                                        text: 'Avarage %'
                                                    }

                                                },
                                                legend: {
                                                    enabled: false
                                                },
                                                plotOptions: {
                                                    series: {
                                                        borderWidth: 0,
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '{point.y:.1f}%'
                                                        }
                                                    }
                                                },

                                                tooltip: {
                                                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                                                },

                                                series: [{
                                                    name: 'Avarage',
                                                    colorByPoint: true,
                                                    data: [
                                                        <?php
                                                        foreach ($subjects as $subject) {
                                                            $subj = strtolower($subject->subject);
                                                            $total_sat = $pass[$subj] + $fail[$subj];
                                                        ?> {
                                                                name: '<?= ucwords($subject->subject) ?>',
                                                                y: <?php
                                                                    if ($total_sat == 0) {
                                                                        echo '0';
                                                                    } else {
                                                                        echo round($subject_sum[$subj] / $total_sat, 2);
                                                                    }
                                                                    ?>,
                                                                drilldown: ''
                                                            },
                                                        <?php
                                                            $i++;
                                                        } //}  
                                                        ?>
                                                    ]
                                                }]
                                            });
                                        });
                                    </script>
                                    <script src="<?= url('public/assets/js/highchart.js') ?>"></script>
                                    <script src="<?= url('public/assets/js/exporting.js') ?>"></script>

                                    <div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
                                </div>
                                <table class="table table-striped table-bordered dataTable table-hover no-footer">
                                    <thead>
                                        <tr>
                                            <th class="col-sm-1">CODE</th>
                                            <th class="col-sm-3">NAME</th>

                                            <th class="col-sm-3">TEACHER </th>
                                            <th class="col-sm-3">TYPE</th>
                                            <!--<th class="col-sm-2">REGISTERED</th>-->
                                            <?php
                                            if (count($grades)) {
                                                foreach ($grades as $grade) {
                                            ?>
                                                    <th class="col-sm-2"><?= $grade->grade ?></th>

                                            <?php
                                                }
                                            }
                                            ?>
                                            <th class="col-sm-1">SAT</th>
                                            <th class="col-sm-1">ABSENT</th>
                                            <th class="col-sm-2">PASS</th>
                                            <th class="col-sm-2">FAIL</th>
                                            <th class="col-sm-2">Average</th>
                                            <th class="col-sm-2">Subj Average Grade</th>
                                            <?php
                                            if (((int)($section->classes->classlevel->school_level_id) == 3) || ((int)($section->classes->classlevel->school_level_id) == 4)) {
                                                ?>
                                                <th class="col-sm-2">GPA</th>
                                            <?php } ?>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $total_subjects = 0;
                                        $total_gpas = 0;
                            
                                        if (count($subjects) > 0) {
                                           

                                            foreach ($subjects as $subject) {
                                                $subj = strtolower($subject->subject);
                                                $total_sat = $pass[$subj] + $fail[$subj];
                                        ?>
                                                <tr>
                                                    <td><?= $subject->subject_code ?></td>
                                                    <td><?= ucwords($subject->subject) ?></td>
                                                    <td><?= ucwords($subject->teacher) ?></td>
                                                    <td><?= $subject->subject_type ?></td>
                                                    <!--<td>778</td>-->
                                                    <?php
                                                $total_points = 0;
                                                    if (count($grades)) {
                                                       
                                                        foreach ($grades as $grade) {
                                                    ?>
                                                            <td><?= ${$subj . '_' . $grade->grade} ?></td>

                                                    <?php
                                                            $total_points = $total_points + ((int)(${$subj . '_' . $grade->grade}) * (int)($grade->point));
                                                        }
                                                    }
                                                    ?>
                                                    <td><?= $total_sat ?></td>
                                                    <td><?= count($total_students) - $total_sat ?></td>

                                                    <td><?= $pass[$subj] ?></td>
                                                    <td><?= $fail[$subj] ?></td>
                                                    <td><?php
                                                        if ($total_sat == 0) {
                                                            echo '0';
                                                            $subj_avg = 0;
                                                        } else {
                                                            $subj_avg = round($subject_sum[$subj] / $total_sat, 2);
                                                            echo $subj_avg;
                                                        }
                                                        ?></td>
                                                    <td>
                                                        <?php
                                                        foreach ($grades as $grade) {
                                                            if ($grade->gradefrom <= round($subj_avg, 0) && $grade->gradeupto >= round($subj_avg, 0)) {
                                                                echo $grade->grade;
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <?php if (((int)($section->classes->classlevel->school_level_id) == 3) || ((int)($section->classes->classlevel->school_level_id) == 4)) {
                                                ?>
                                                        <td>
                                                            <?php
                                                            if ($total_sat == 0) {
                                                                echo '0';
                                                            } else {
                                                                echo  round(($total_points / $total_sat), 2);
                                                            }
                                                            ?>
                                                        </td>
                                                    <?php } ?>

                                                </tr>
                                            <?php
                                                if ($total_sat == 0) {
                                                    $total_subjects = $total_subjects;
                                                    $total_gpas = $total_gpas;
                                                } else {
                                                    $total_subjects = $total_subjects + 1;
                                                    $total_gpas = $total_gpas + ($total_points / $total_sat);
                                                }
                                            }
                                            if ($total_subjects == 0) {
                                                $average_gpa = 0;
                                            } else {
                                                $average_gpa = round(($total_gpas / $total_subjects), 2);
                                            }
                                            ?>
                                            <input type="hidden" value="<?= $average_gpa; ?>" id="gpa">
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                        </div>
                        <?php if ($exam_info->show_division) { ?>
                            <div class="row">
                                <div class="title_left">
                                    <br /><br />
                                    <h3>DIVISION</h3>
                                    <br />
                                </div>
                                <table class="table table-striped table-bordered dataTable table-hover no-footer">
                                    <thead>
                                        <tr>
                                            <th class="col-sm-1"></th>
                                            <th class="col-sm-2">DIV I</th>
                                            <th class="col-sm-2">DIV II</th>
                                            <th class="col-sm-1">DIV III</th>
                                            <th class="col-sm-2">DIV IV</th>
                                            <th class="col-sm-2">DIV 0</th>
                                            <th class="col-sm-2">ABS</th>
                                            <!--	<th class="col-sm-2">B</th>
                                                <th class="col-sm-2">C</th>
                                                <th class="col-sm-2">D</th>
                                                <th class="col-sm-2">E</th>
                                                <th class="col-sm-2">F</th>
                                                <th class="col-sm-2">GPA</th>-->
                                        </tr>

                                    </thead>
                                    <tbody>
                                        <?php ?>
                                        <tr>
                                            <td>Total</td>
                                            <td><?= $total_div_I ?></td>
                                            <td><?= $total_div_II ?></td>
                                            <td><?= $total_div_III ?></td>
                                            <td><?= $total_div_IV ?></td>
                                            <td><?= $total_div_0 ?></td>
                                            <td><?= count($total_students) - count($students) ?></td>
                                            <!--	<td>78</td>
                                                <td>65</td>	
                                                <td>65</td>
                                                <td>65</td>
                                                <td>65</td>
                                                <td>65</td>-->
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                    <?php
                                }
                            }
                    ?>

                    <!--    		    <h2 class="center">GENDER</h2>
                                            <table  class="table table-striped table-bordered table-hover no-footer">
                                                <thead>
                                                    <tr>
                                                        <th class="col-sm-1"></th>
                                                        <th class="col-sm-2">DIV I</th>
                                                        <th class="col-sm-2">DIV II</th>
                                                        <th class="col-sm-2">DIV III</th>
                                                        <th class="col-sm-2">DIV IV</th>
                                                        <th class="col-sm-2">DIV 0</th>
                                                        <th class="col-sm-2">ABS</th>
                                                        <th class="col-sm-2">TOTAL</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>M</td>
                                                        <td>0</td>
                                                        <td>778</td>
                                                        <td>778</td>
                                                        <td>0</td>
                                                        <td>500</td>
                                                        <td>25</td>
                                                        <td>14</td>
                        
                                                    </tr>
                                                    <tr>
                                                        <td>F</td>
                                                        <td>0</td>
                                                        <td>778</td>
                                                        <td>778</td>
                                                        <td>0</td>
                                                        <td>500</td>
                                                        <td>25</td>
                                                        <td>14</td>
                        
                                                    </tr>
                                                    <tr>
                                                        <td>T</td>
                                                        <td>0</td>
                                                        <td>778</td>
                                                        <td>778</td>
                                                        <td>0</td>
                                                        <td>500</td>
                                                        <td>25</td>
                                                        <td>14</td>
                        
                                                    </tr>
                                                </tbody>
                                            </table>-->
                    </div>
                </div>
            </div>

        </div> <!-- nav-tabs-custom -->

        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-6">
                <table class="table col-lg-6">
                    <tr>
                        <td>Color Meaning: </td>
                        <td style="background:pink">
                            xy
                        </td>
                        <td><a href="<?= url('setting#home') ?>">Below Average Pass mark</a></td>
                    </tr>
                </table>
            </div>
            <div class="col-lg-4"></div>
        </div>
    </div> <!-- col-sm-12 for tab -->

<?php
} else {
?>
    <div class="col-sm-12">

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#all" aria-expanded="true">eattendance_all_students") ?></a></li>
            </ul>

            <div class="tab-content">
                <div id="all" class="tab-pane active">
                    <div id="hide-table">
                        <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                            <thead>
                                <tr>
                                    <th class="col-sm-2">slno') ?></th>
                                    <th class="col-sm-2">eattendance_photo') ?></th>
                                    <th class="col-sm-2">eattendance_name') ?></th>
                                    <th class="col-sm-2">eattendance_roll') ?></th>
                                    <th class="col-sm-2">eattendance_phone') ?></th>
                                    <th class="col-sm-2">action') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (count($students)) {
                                    $i = 1;
                                    foreach ($students as $student) {
                                ?>
                                        <tr>
                                            <td data-title="slno') ?>">
                                                <?php echo $i; ?>
                                            </td>

                                            <td data-title="eattendance_photo') ?>">
                                                <?php
                                                $array = array(
                                                    "src" => url('storage/uploads/images/' . $student->photo),
                                                    'width' => '35px',
                                                    'height' => '35px',
                                                    'class' => 'img-rounded'
                                                );
                                                echo img($array);
                                                ?>
                                            </td>
                                            <td data-title="eattendance_name') ?>">
                                                <?php echo $student->name; ?>
                                            </td>
                                            <td data-title="eattendance_roll') ?>">
                                                <?php echo $student->roll; ?>
                                            </td>
                                            <td data-title="eattendance_phone') ?>">
                                                <?php echo $student->phone; ?>
                                            </td>
                                            <td data-title="action') ?>">
                                                <?php
                                                foreach ($eattendances as $eattendance) {
                                                    if ($eattendance->student_id == $student->student_id && $examID == $eattendance->examID && $classesID == $eattendance->classesID && $subjectID == $eattendance->subjectID) {

                                                        if ($eattendance->eattendance == "Present") {
                                                            echo "<button class='btn btn-success btn-xs'>" . $eattendance->eattendance . "</button>";
                                                        } elseif ($eattendance->eattendance == "") {
                                                            echo "<button class='btn btn-danger btn-xs'>" . "Absent" . "</button>";
                                                        } else {
                                                            echo "<button class='btn btn-danger btn-xs'>" . $eattendance->eattendance . "</button>";
                                                        }
                                                        break;
                                                    }
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div> <!-- nav-tabs-custom -->
    </div>
<?php }
?>
<!--<p align="center">This page took <?php echo (microtime(true) - LARAVEL_START) ?> seconds to render</p>-->



</div> <!-- col-sm-12 -->
</div><!-- row -->
</div><!-- Body -->
</div><!-- /.box -->

<!-- Modal content start here -->
<div class="modal fade" id="export_report">
    <div class="modal-dialog">
        <form action="#" method="post" class="form-horizontal" role="form">
            <input type="hidden" name="examID" value="<?= isset($examID) ? $examID : '' ?>" />
            <input type="hidden" name="classesID" value="<?= isset($post_class_id) ? $post_class_id : '' ?>" />
            <input type="hidden" name="sectionID" value="<?= isset($sectionID) ? $sectionID : '0' ?>" />
            <input type="hidden" name="subjectID" value="<?= isset($subjectID) ? $subjectID : '0' ?>" />
            <input type="hidden" name="academic_year_id" value="<?= isset($academic_year_id) ? $academic_year_id : '' ?>" />
            <input type="hidden" name="action" value="export" />
            <div class="modal-content">

                <div class="modal-header">
                    CA export Settings
                </div>

                <div class="modal-body">
                    <table class="table table-hover">


                        <tr style="border-bottom:1px solid whitesmoke">
                            <td style="padding-left:5px;">
                                <h4>Show Position in Class</h4>
                            </td>
                            <td>
                                <input type="checkbox" name="pos_in_class" checked="" />
                            </td>

                        </tr>
                        <tr style="border-bottom:1px solid whitesmoke">
                            <td style="padding-left:5px;">
                                <h4>Show Position in Section</h4>
                            </td>
                            <td>
                                <input type="checkbox" name="pos_in_section" checked="" />
                            </td>

                        </tr>
                        <tr style="border-bottom:1px solid whitesmoke">
                            <td style="padding-left:5px;">
                                <h4>Show Subject Performance Average</h4>
                            </td>
                            <td>
                                <input type="checkbox" name="show_subject_avg" checked="" />
                            </td>

                        </tr>
                        <tr style="border-bottom:1px solid whitesmoke">
                            <td style="padding-left:5px;">
                                <h4>Show Subject Grade per Each Mark</h4>
                            </td>
                            <td>
                                <input type="checkbox" name="show_grade" checked="" />
                            </td>

                        </tr>
                        <tr style="border-bottom:1px solid whitesmoke">
                            <td style="padding-left:5px;">
                                <h4>Show Subject Rank per Each Mark</h4>
                            </td>
                            <td>
                                <input type="checkbox" name="show_rank" checked="" />
                            </td>

                        </tr>
                        <tr style="border-bottom:1px solid whitesmoke">
                            <td style="padding-left:5px;">
                                <h4>Show Footer Message</h4>
                            </td>
                            <td>
                                <input type="checkbox" name="show_footer" checked="" />
                            </td>

                        </tr>
                        <tr style="border-bottom:1px solid whitesmoke">
                            <td style="padding-left:5px;">
                                <h4>Show Rank Per Gender</h4>
                            </td>
                            <td>
                                <input type="checkbox" name="show_gender" checked="" />
                            </td>

                        </tr>

                    </table>
                </div>


                <div class="modal-footer">
                    <button type="button" style="margin-bottom:0px;" class="btn btn-default" data-dismiss="modal" onclick="javascript:closeWindow()">close') ?></button>
                    <button type="submit" class="btn btn-success">Export</button>
                </div>
                <?= csrf_field() ?>
        </form>
    </div>
</div>
</div>
<!-- Modal content End here -->

<script type="text/javascript">


    $('.division').each(function() {
        var student_id = $(this).attr('student_id');
        var exam_id = $(this).attr('exam_id');
        var format = $(this).attr('format');
        var class_id = $(this).attr('class_id');
        // get_division(student_id,exam_id,format,class_id);
    });

    function get_division(student_id, exam_id, format, class_id) {
        $.ajax({
            type: 'POST',
            url: "<?= url('exam/getExamDivision') ?>",
            data: {
                "exam_id": exam_id,
                "class_id": class_id,
                "student_id": student_id,
                "format": format
            },
            dataType: "json",
            beforeSend: function(xhr) {
                jQuery('#loader-content').fadeOut('slow');
                $('#div' + student_id + exam_id + class_id).html('<a href="#/refresh"><i class="fa fa-spinner"></i> </a>');
                $('#points' + student_id + exam_id + class_id).html('<a href="#/refresh"><i class="fa fa-spinner"></i> </a>');
            },
            complete: function(xhr, status) {
                jQuery('#loader-content').fadeOut('slow');
            },
            success: function(data) {
                console.log(data);
                $('#div' + student_id + exam_id + class_id).html(data.division);
                $('#points' + student_id + exam_id + class_id).html(data.points);
            }
        });
    }


    $('#sectionID').change(function(event) {
        var sectionID = $(this).val();
        if (sectionID === '0' || sectionID === 'all') {
            //$('#subjectID').val(0);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('mark/getSubjectBySection') ?>",
                data: "id=" + sectionID,
                dataType: "html",
                success: function(data) {
                    $('#subjectID').html(data);
                }
            });
        }
    });

    function printDiv(divID) {
        //Get the HTML of div
        var divElements = document.getElementById(divID).innerHTML;
        //Get the HTML of whole page
        var oldPage = document.body.innerHTML;

        //Reset the page's HTML with div's HTML only
        document.body.innerHTML =
            "<html><head><title></title></head><body>" +
            divElements + "</body>";

        //Print Page
        window.print();

        //Restore orignal HTML
        document.body.innerHTML = oldPage;
    }

    function closeWindow() {
        location.reload();
    }

    $('#academic_year_id').change(function(event) {
        var academic_year_id = $(this).val();
        if (academic_year_id === '0') {
            $('#semester_id').val(0);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('semester/get_semester') ?>",
                data: "academic_year_id=" + academic_year_id,
                dataType: "html",
                success: function(data) {
                    if (data === '0') {
                        $('#semester_id').html('');
                        swal('warning', 'No semester(s) have been added in this level. Please define semester(s) first in this academic year', 'warning');
                        $('#sem_id').html();
                    } else {
                        $('#sem_id').html('');
                        $('#semester_id').html(data);
                    }
                }
            });
            var class_id = $('#classesID').val();
            $.ajax({
                type: 'POST',
                url: "<?= url('mark/getSubjectByClass') ?>",
                data: "year_id=" + academic_year_id + "&id=" + class_id,
                dataType: "html",
                success: function(data) {
                    $('#subjectID').html(data);
                }
            });
        }
    });
    $('#semester_id').change(function(event) {
        var academic_year_id = $('#academic_year_id').val();
        var semester_id = $(this).val();
        var classesID = $('#classesID').val();
        if (academic_year_id === '0') {
            $('#examID').val(0);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/getExamByClass') ?>",
                data: "id=" + classesID + "&academic_year_id=" + academic_year_id + '&semester_id=' + semester_id + '&done=1',
                dataType: "html",
                success: function(data) {
                    $('#examID').html(data);
                }
            });
        }
    });

    $('#classesID').change(function(event) {

        var classesID = $(this).val();
        if (classesID === '0') {
            $('#academic_year_id').val(0);
            $('#report_filter_div').hide();
        } else {
            $('#report_filter_div').show();
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/get_academic_years') ?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function(data) {
                    $('#academic_year_id').html(data);
                }
            });
        }
    });
    $('#academic_year_id').change(function(event) {
        var academic_year_id = $(this).val();
        var classesID = $('#classesID').val();
        if (academic_year_id === '0') {
            $('#sectionID').val(0);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('student/sectioncall') ?>",
                data: "id=" + classesID + '&academic_year_id=' + academic_year_id + '&all_section=1',
                dataType: "html",
                success: function(data) {
                    $('#sectionID').html(data);
                }
            });
        }
    });

    gpa_show = function() {
        var gpa = $('#gpa').val();
        $('#gpa1').html(gpa);
    }
    $(document).ready(gpa_show);
</script>