@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">

                        <div id="hide-table" style="margin-top: 6em;">
                        
                        <div style='break-after:always'>
                            <h3 align="center"> CLASS SUBJECTs PERFORMANCE</h3>
                        </div>
                        <hr>
                        <table class="table table-striped table-bordered dataTable table-hover no-footer">
                                    <thead>
                                        <tr>
                                            <th class="col-sm-1">CODE</th>
                                            <th class="col-sm-3">NAME</th>

                                            <th class="col-sm-3">TEACHER </th>
                                            <th class="col-sm-1">TYPE</th>
                                            <?php $total_avg = 'subject';
                                            if (count($grades)) {
                                                foreach ($grades as $grade) {
                                                    ${$total_avg . '_' . $grade->grade} = 0;
                                            ?>
                                                    <th class="col-sm-2"><?= $grade->grade ?></th>

                                            <?php
                                                }
                                            }
                                            ?>
                                            <th class="col-sm-1">SAT</th>
                                            <th class="col-sm-1">ABSENT</th>
                                            <th class="col-sm-1">PASS</th>
                                            <th class="col-sm-1">FAIL</th>
                                            <th class="col-sm-1">Average</th>
                                            <th class="col-sm-1">Grade</th>
                                            <th class="col-sm-2">GPA</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                         if (isset($subjects)) {
                                            foreach ($subjects as $subject) {
                                                $subj = strtolower(str_replace(' ', '', $subject->subject));
                                                $subject_sum[$subj] = 0;
                                                $sum_subj[$subj] = 0;
                                                $pass["$subj"] = 0;
                                                $fail["$subj"] = 0;
                                            }
                                        }
                                        foreach ($subjects as $subject) {
                                            $subj = strtolower(str_replace(' ', '', $subject->subject));
                                            $subject_sum["$subj"] = DB::table($this_schema.'.mark')->where('subjectID', $subject->subjectID)->where('examID', $exam_id)->where('classesID', $class_id)->where('academic_year_id', $academic_year_id)->sum('mark');
                                            
                                            $pass["$subj"] = DB::table($this_schema.'.mark')->where('subjectID', $subject->subjectID)->where('examID', $exam_id)->where('classesID', $class_id)->where('academic_year_id', $academic_year_id)
                                            ->where('mark',  '>=', $classlevel->pass_mark)->count();
                                            $fail["$subj"]  = DB::table($this_schema.'.mark')->where('subjectID', $subject->subjectID)->where('examID', $exam_id)->where('classesID', $class_id)->where('academic_year_id', $academic_year_id)
                                            ->where('mark', '<=',  $classlevel->pass_mark)->count();
                                            if (count($grades)) {
                                                foreach ($grades as $grade) {
                                                    ${$subj . '_' . $grade->grade} = DB::table($this_schema.'.mark')->where('subjectID', $subject->subjectID)->where('examID', $exam_id)->where('classesID', $class_id)->where('academic_year_id', $academic_year_id)
                                                    ->where('mark',  '>=', $grade->gradefrom)->where('mark', '<=', $grade->gradeupto)->count();
                                               // dd(${$subj . '_' . $grade->grade});
                                                }

                                            }
                                        }

                                        $total_sat =count($students);
                                       
                                        $total_subjects = 0;
                                        $sum_sub_average = 0;
                                        if (count($subjects) > 0) {
                                           

                                            foreach ($subjects as $subject) {
                                                $subj = strtolower(str_replace(' ', '', $subject->subject));
                                                $total_sat = $pass[$subj] + $fail[$subj];
                                        ?>
                                                <tr>
                                                    <td><?= $subject->subjectID ?></td>
                                                    <td><?= ucwords($subject->subject) ?></td>
                                                    <td><?= ucwords($subject->subject) ?></td>
                                                    <td><?= $subject->subject_type ?></td>
                                                    <?php
                                                $total_points = 0;
                                                $total_grades  = 0;
                                                $chek = [];
                                                    if (count($grades)) {
                                                       
                                                        foreach ($grades as $grade) {
                                                            //array_push($chek, ${$subj . '_' . $grade->grade})
                                                    ?>
                                                            <td style="text-align: center"><?= ${$subj . '_' . $grade->grade} ?></td>

                                                    <?php
                                                        $total_grades = $total_grades + ${$subj . '_' . $grade->grade};
                                                            ${$total_avg . '_' . $grade->grade} += ((int)(${$subj . '_' . $grade->grade}));
                                                            $total_points = $total_points + ((int)(${$subj . '_' . $grade->grade}) * (int)($grade->point));
                                                        }
                                                    }
                                                 //   dd($chek);
                                                    ?>
                                                    <td style="text-align: center"><?= $total_sat ?></td>
                                                    <td style="text-align: center"><?= $total_sat - $total_sat ?></td>

                                                    <td style="text-align: center"><?= $pass[$subj] ?></td>
                                                    <td style="text-align: center"><?= $fail[$subj] ?></td>
                                                    <td style="text-align: center"><?php
                                                        if ($total_sat == 0) {
                                                            echo '0';
                                                            $subj_avg = 0;
                                                        } else {
                                                            $subj_avg = round($subject_sum[$subj] / $total_sat, 2);
                                                            echo $subj_avg;
                                                        }
                                                       $sum_sub_average += $subj_avg;
                                                        ?></td>
                                                    <td style="text-align: center">
                                                        <?php
                                                        foreach ($grades as $grade) {
                                                            if ($grade->gradefrom <= round($subj_avg, 0) && $grade->gradeupto >= round($subj_avg, 0)) {
                                                                echo $grade->grade;
                                                            }
                                                        } 
                                                        ?>
                                                    </td>
                                                    <td style="text-align: center"> {{ (int)$total_grades> 0 ? round($total_points/$total_grades, 2) : 0 }}
                                                  

                                                </tr>
                                            <?php
                                               
                                            }
                                            
                                            ?>
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th class="col-sm-1"></th>
                                            <th class="col-sm-1"></th>

                                            <th class="col-sm-1"  style="text-align: center" colspan="2">Total Grade</th>
                                            <?php
                                            $overall_points = 0;
                                            $overall_grades = 0;
                                            if (count($grades)) {
                                                foreach ($grades as $grade) {
                                                    $overall_points += ${$total_avg . '_' . $grade->grade} * (int)$grade->point;
                                                    $overall_grades  += ${$total_avg . '_' . $grade->grade};

                                            ?>
                                                    <th class="col-sm-1" style="text-align: center"><b><?= ${$total_avg . '_' . $grade->grade} ?></b></th>

                                            <?php
                                                }
                                            }
                                            ?>
                                            <th class="col-sm-1" colspan="1"></th>
                                            <th class="col-sm-1" colspan="3">Class Average</th>
                                            <th class="col-sm-1"><?=count($subjects) > 0 ? round($sum_sub_average/count($subjects), 2) : round($sum_sub_average, 1) ?></th>
                                            <th class="col-sm-2" style="text-align: center; font-weight: bold;">  
                                                <?php
                                             $final_score = count($subjects) > 0 ? round($sum_sub_average/count($subjects), 2) : round($sum_sub_average, 1);
                                                        foreach ($grades as $grade) {
                                                            if ($grade->gradefrom <= $final_score && $grade->gradeupto >= $final_score) {
                                                                echo $grade->grade;
                                                            }
                                                        }
                                                        ?>
                                                </th>
                                                <td style="text-align: center;  font-weight: bold;"> {{ (int)$overall_grades> 0 ? round($overall_points/$overall_grades, 1) : 0 }}

                                        </tr>
                                    </tfoot>
                                </table>
                
                <?php  if(!isset($export)){ 
                    $i = 1;
                     if (count($subjects) > 0) { ?>
                                <div class="row">
                                   

                                <div class="row">
                                    <div class="title_left">
                                        <br /><br />
                                        <h3>SUBJECTS PERFORMANCE</h3>
                                        <br />
                                    </div>
                                    <div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>

                            <script src="https://code.highcharts.com/highcharts.js"></script>
                            <script src="https://code.highcharts.com/modules/data.js"></script>
                            <script type="text/javascript">
                                graph_disc = function() {

                                  $('#container').highcharts({
                                                chart: {
                                                    type: 'column'
                                                },
                                                title: {
                                                    text: "Subject Performance Evaluation"
                                                },
                                                subtitle: {
                                                    text: ''
                                                },
                                                xAxis: {
                                                    type: 'category'
                                                },
                                                yAxis: {
                                                    title: {
                                                        text: 'Avarage %'
                                                    }

                                                },
                                                legend: {
                                                    enabled: false
                                                },
                                                plotOptions: {
                                                    series: {
                                                        borderWidth: 0,
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '{point.y:.1f}%'
                                                        }
                                                    }
                                                },

                                                tooltip: {
                                                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                                                },

                                                series: [{
                                                    name: 'Avarage',
                                                    colorByPoint: true,
                                                    data: [
                                                        <?php
                                                        foreach ($subjects as $subject) {
                                                            $subj = strtolower(str_replace(' ', '', $subject->subject));
                                                            $total_sat = $pass[$subj] + $fail[$subj];
                                                        ?> {
                                                                name: '<?= ucwords($subject->subject) ?>',
                                                                y: <?php
                                                                    if ($total_sat == 0) {
                                                                        echo '0';
                                                                    } else {
                                                                        echo round($subject_sum[$subj] / $total_sat, 2);
                                                                    }
                                                                    ?>,
                                                                drilldown: ''
                                                            },
                                                        <?php
                                                            $i++;
                                                        } //}  
                                                        ?>
                                                    ]
                                                }]
                                            });
                                        }
                                    $(document).ready(graph_disc);
                            </script>
                                            
            <?php } ?>  </div>
                        <?php if (isset($exam_info)) { ?>
                            <div class="row">
                                <div class="title_left">
                                    <br /><br />
                                    <h3>DIVISION</h3>
                                    <br />
                                </div>
                                <table class="table table-striped table-bordered dataTable table-hover no-footer">
                                    <thead>
                                        <tr>
                                            <th class="col-sm-1"></th>
                                            <th class="col-sm-2">DIV I</th>
                                            <th class="col-sm-2">DIV II</th>
                                            <th class="col-sm-1">DIV III</th>
                                            <th class="col-sm-2">DIV IV</th>
                                            <th class="col-sm-2">DIV 0</th>
                                            <th class="col-sm-2">ABS</th>
                                            <!--	<th class="col-sm-2">B</th>
                                                <th class="col-sm-2">C</th>
                                                <th class="col-sm-2">D</th>
                                                <th class="col-sm-2">E</th>
                                                <th class="col-sm-2">F</th>
                                                <th class="col-sm-2">GPA</th>-->
                                        </tr>

                                    </thead>
                                    <tbody>
                                        <?php ?>
                                        <tr>
                                            <td>Total</td>
                                            <td><?= $total_div_I ?></td>
                                            <td><?= $total_div_II ?></td>
                                            <td><?= $total_div_III ?></td>
                                            <td><?= $total_div_IV ?></td>
                                            <td><?= $total_div_0 ?></td>
                                            <td><?= count($total_students) - count($students) ?></td>
                                            <!--	<td>78</td>
                                                <td>65</td>	
                                                <td>65</td>
                                                <td>65</td>
                                                <td>65</td>
                                                <td>65</td>-->
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                    <?php
                                }
                            }
                    ?>

         
            
         
        </div>
    </div>
    <!-- Server Side Processing table end -->
</div>



@endsection