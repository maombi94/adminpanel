@extends('layouts.app')

@section('content')
<?php $root = url('/public/'); ?>
<!-- Page-body start -->
<div class="page-body">
    <div class="row">

        <!-- Server Side Processing table start -->
        <div class="col-sm-12">
            <!-- Basic Inputs Validation start -->
            <div class="card">
                <div class="card-header">
                    <h5>Fill all fields for Validation</h5>
                    {{-- <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> --}}

                </div>
                <div class="card-block">
                    <form id="main" method="post" action="">
                    @csrf
                    <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Select School</label>
                            <div class="col-sm-10">
                            <select name="schema_name" id="schema_name" class="form-control form-control-danger">
                                <option value="opt1">Select One School</option>
                                @if (count(load_schemas()) > 0)
                                @foreach (load_schemas() as $schema)
                                    <option value="{{ $schema->username }}"> {{ $schema->username }}</option>
                                @endforeach
                                @endif
                               
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Select Class</label>
                            <div class="col-sm-10">
                            <select name="classesID" id="classesID" class="form-control form-control-info">
                                <option value="0">Select One Value Only</option>
                               
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Select Academic Year</label>
                            <div class="col-sm-10">
                            <select name="academic_year_id" id="academic_year_id" class="form-control form-control-primary">
                               
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Select Semester</label>
                            <div class="col-sm-10">
                            <select name="semester_id" id="semester_id" class="form-control form-control-info">
                             
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Select Exam</label>
                            <div class="col-sm-10">
                            <select name="examID" id="examID" class="form-control form-control-info">
                             
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                     
                        <div class="form-group row">
                            <label class="col-sm-2"></label>
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <?php 
            if (isset($exam_info) && !empty($exam_info) && !empty($students)) {  
                    if (count($students)) {
                        $i = 1;
                        $student_pass = 0;
                        $total_average = 0;
                        $total_div_I = 0;
                        $total_div_II = 0;
                        $total_div_III = 0;
                        $total_div_IV = 0;
                        $total_div_0 = 0;

                        foreach ($subjects as $subject) {
                            $subject_name = strtolower($subject->subject);
                            $subj = strtolower($subject->subject);
                            $subject_sum["$subj"] = 0;
                            $pass["$subj"] = 0;
                            $fail["$subj"] = 0;
                            foreach ($grades as $grade) {
                                ${$subject_name . '_' . $grade->grade} = 0;
                            }
                        }
                        foreach ($students as $student) {
                            $student = is_object($student) ? (array) $student : $student;
                            if ((isset($student) && $student['average'] < $classlevel->pass_mark)) {
                                $fail["$subject_name"]++;

                                $color = "pink";
                            } else {
                                $color = "";
                                $student_pass++;
                                $student["$subject_name"] != NULL ? $pass["$subject_name"]++ : '';

                            }
                            $subject_sum["$subject_name"] = $subject_sum["$subject_name"] + (float) $student["$subject_name"];
                            $total_average = $total_average + (float) $student['average'];
                        }
                }

                $total_of_total = 0;
                if ($subjectID == 0) {
                    //Loop in all subjects to show list of them here
                    if (isset($subjects)) {
                        foreach ($subjects as $subject) {
                            if (isset($subject_evaluations)) {
                                foreach ($subject_evaluations as $subject_ev) {

                                    if ($subject->subjectID == $subject_ev->subjectID) {
                                      //  echo '<th class="col-sm-2"> ' . $subject_ev->sum . '</th>';
                                        $total_of_total += $subject_ev->sum;
                                    }
                                }
                            }
                            $total_sat = $pass[$subj] + $fail[$subj];

                        }
                    }
                }
                    ?>


            <!-- Basic Inputs Validation end -->
            <div class="card">
                <div class="card-header">
                    <h5><?=$exam_info->exam?> CLASS PERFORMANCE</h5>
                    <!-- <span>The example below shows DataTables loading data for a table from arrays as the data source,
                        where the structure of the row's data source in this example is:</span> -->
                </div>
                <div class="card-block">
                
                            <?php if ($subjectID == '0') { ?>
                                <div class="dt-responsive table-responsive">
                                    <table id="simpletable" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th class="col-sm-1">CODE</th>
                                            <th class="col-sm-3">NAME</th>

                                            <th class="col-sm-3">TEACHER </th>
                                            <th class="col-sm-3">TYPE</th>
                                            <!--<th class="col-sm-2">REGISTERED</th>-->
                                            <?php
                                            if (count($grades)) {
                                                foreach ($grades as $grade) {
                                            ?>
                                                    <th class="col-sm-2"><?= $grade->grade ?></th>

                                            <?php
                                                }
                                            }
                                            ?>
                                            <th class="col-sm-1">SAT</th>
                                            <th class="col-sm-1">ABSENT</th>
                                            <th class="col-sm-2">PASS</th>
                                            <th class="col-sm-2">FAIL</th>
                                            <th class="col-sm-2">Average</th>
                                            <th class="col-sm-2">Subject Grade</th>
                                           

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $total_subjects = 0;
                                        $total_gpas = 0;
                            
                                        if (count($subjects) > 0) {
                                            foreach ($subjects as $subject) {
                                                $subj = strtolower($subject->subject);
                                                $total_sat = $pass[$subj] + $fail[$subj];
                                                $teacher =  DB::table($this_schema.'.section_subject_teacher')->where('classesID', $subject->classesID)->where('subject_id', $subject->subjectID)->count();
                                            ?>
                                                <tr>
                                                    <td><?= $subject->subject_code ?></td>
                                                    <td><?= ucwords($subject->subject) ?></td>
                                                    <td><?= ucwords($subject->teacher) ?></td>
                                                    <td><?= $subject->subject_type ?></td>
                                                    <!--<td>778</td>-->
                                                    <?php
                                                $total_points = 0;
                                                $failed =  DB::table($this_schema.'.mark')->where('examID', $exam_info->examID)->where('classesID', $subject->classesID)->where('subjectID', $subject->subjectID)->where('mark', '<', $classlevel->pass_mark+1)->count();
                                                $passed =  DB::table($this_schema.'.mark')->where('examID', $exam_info->examID)->where('classesID', $subject->classesID)->where('subjectID', $subject->subjectID)->where('mark', '>=', $classlevel->pass_mark)->count();

                                                if (count($grades)) {
                                                        foreach ($grades as $grade) {
                                                            $total_sub = DB::table($this_schema.'.mark')->where('examID', $exam_info->examID)->where('classesID', $subject->classesID)->where('subjectID', $subject->subjectID)->where('mark', '>=', $grade->gradefrom)->where('mark', '<', $grade->gradeupto+1)->count();
                                                            echo '<td>'.$total_sub.'</td>';
                                                            $total_points = $total_points + ((int)(${$subj . '_' . $grade->grade}) * (int)($grade->point));
                                                        }
                                                    }
                                                    ?>
                                                    <td><?= $total_sat ?></td>
                                                    <td><?= count($total_students) - $total_sat ?></td>

                                                    <td><?= $passed ?></td>
                                                    <td><?= $failed ?></td>
                                                    <td><?php
                                                           
                                                        if ($total_sat == 0) {
                                                            echo '0';
                                                            $subj_avg = 0;
                                                        } else {
                                                            $subj_avg = round($subject_sum[$subj] / $total_sat, 2);
                                                            echo $subj_avg;
                                                        }
                                                        ?></td>
                                                    <td>
                                                        <?php
                                                        foreach ($grades as $grade) {
                                                            if ($grade->gradefrom <= round($subj_avg, 0) && $grade->gradeupto >= round($subj_avg, 0)) {
                                                                echo $grade->grade;
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                

                                                </tr>
                                            <?php
                                            }
                                            ?>
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                                <th class="col-sm-1">TOTAL Student</th>
                                             
                                                <td><?= count($total_students) ?></td>
                                               <td></td>
                                               <td></td>
                                                <td><?= count($total_students) - count($students) ?></td>
                                                <td><?= count($students) ?></td>
                                               
                                                <td><?= $student_pass ?></td>
                                                <td><?= count($students) - $student_pass ?></td>
                                                <td><?= $subjectID == '0' ? round($total_average / count($students), 2) : round($class_average_marks, 2) ?></td>
                                                <th></th> <th></th>
                                                <td><?= $total_of_total ?></td>
                                                <td><?= round($total_of_total / count($total_students), 1) ?></td>
                                               
                                                <th></th>
                                                <th></th>
                                            </tr>
                                    </tfoot>
                                </table>
                        </div>
                        

                        <div class="row">
                                    <div class="title_left">
                                        <br /><br />
                                        <h3>SUBJECTS PERFORMANCE</h3>
                                        <br />
                                    </div>
                                    <script type="text/javascript">
                                        jQuery(function() {

                                            jQuery('#container').highcharts({
                                                chart: {
                                                    type: 'column'
                                                },
                                                title: {
                                                    text: "Subject Performance Evaluation"
                                                },
                                                subtitle: {
                                                    text: ''
                                                },
                                                xAxis: {
                                                    type: 'category'
                                                },
                                                yAxis: {
                                                    title: {
                                                        text: 'Avarage %'
                                                    }

                                                },
                                                legend: {
                                                    enabled: false
                                                },
                                                plotOptions: {
                                                    series: {
                                                        borderWidth: 0,
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '{point.y:.1f}%'
                                                        }
                                                    }
                                                },

                                                tooltip: {
                                                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                                                },

                                                series: [{
                                                    name: 'Avarage',
                                                    colorByPoint: true,
                                                    data: [
                                                        <?php
                                                        foreach ($subjects as $subject) {
                                                            $subj = strtolower($subject->subject);
                                                            $total_sat = $pass[$subj] + $fail[$subj];
                                                        ?> {
                                                                name: '<?= ucwords($subject->subject) ?>',
                                                                y: <?php
                                                                    if ($total_sat == 0) {
                                                                        echo '0';
                                                                    } else {
                                                                        echo round($subject_sum[$subj] / $total_sat, 2);
                                                                    }
                                                                    ?>,
                                                                drilldown: ''
                                                            },
                                                        <?php
                                                            $i++;
                                                        } //}  
                                                        ?>
                                                    ]
                                                }]
                                            });
                                        });
                                    </script>
                                    <div id="container" style="min-width: 310px; max-width: 800px; height: 500px; margin: 0 auto;"></div>
                                </div>
                                <hr>
                        <?php if ($exam_info->show_division) { ?>
                            <div class="row">
                                <div class="title_left">
                                    <br /><br />
                                    <h3>DIVISION</h3>
                                    <br />
                                </div>
                                <table class="table table-striped table-bordered dataTable table-hover no-footer">
                                    <thead>
                                        <tr>
                                            <th class="col-sm-1"></th>
                                            <th class="col-sm-2">DIV I</th>
                                            <th class="col-sm-2">DIV II</th>
                                            <th class="col-sm-1">DIV III</th>
                                            <th class="col-sm-2">DIV IV</th>
                                            <th class="col-sm-2">DIV 0</th>
                                            <th class="col-sm-2">ABS</th>

                                        </tr>

                                    </thead>
                                    <tbody>
                                        <?php ?>
                                        <tr>
                                            <td>Total</td>
                                            <td><?= $total_div_I ?></td>
                                            <td><?= $total_div_II ?></td>
                                            <td><?= $total_div_III ?></td>
                                            <td><?= $total_div_IV ?></td>
                                            <td><?= $total_div_0 ?></td>
                                            <td><?= count($total_students) - count($students) ?></td>
                                            
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                    <?php
                                }
                            }
                    ?>
                    </div>
                </div>
            </div>
            <!-- Server Side Processing table end -->
<?php } ?>
        </div>
    </div>
</div>
  <!-- Required Jquery -->
    <script type="text/javascript" src="<?=$root?>/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script src="<?= url('public/assets/js/highchart.js') ?>"></script>
    <script src="<?= url('public/assets/js/exporting.js') ?>"></script>

<script type="text/javascript">

    jQuery('#academic_year_id').change(function(event) {
        var academic_year_id = $(this).val();
        var schema_name = $('#schema_name').val();
        if (academic_year_id === '0') {
            $('#semester_id').val(0);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/get_semester') ?>",
                data: "_token=" + "{{ csrf_token() }}" + "&academic_year_id=" + academic_year_id + "&schema_name=" + schema_name,
                dataType: "html",
                success: function(data) {
                    if (data === '0') {
                        $('#semester_id').html('');
                        swal('warning', 'No semester(s) have been added in this level. Please define semester(s) first in this academic year', 'warning');
                        $('#sem_id').html();
                    } else {
                        $('#sem_id').html('');
                        $('#semester_id').html(data);
                    }
                }
            });
            var class_id = $('#classesID').val();
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/getSubjectByClass') ?>",
                data: "_token=" + "{{ csrf_token() }}" + "&year_id=" + academic_year_id + "&id=" + class_id + "&schema_name=" + schema_name,
                dataType: "html",
                success: function(data) {
                    $('#subjectID').html(data);
                }
            });
        }
    });

    $('#semester_id').change(function(event) {
        var schema_name = $('#schema_name').val();
        var academic_year_id = $('#academic_year_id').val();
        var semester_id = $(this).val();
        var classesID = $('#classesID').val();
        if (academic_year_id === '0') {
            $('#examID').val(0);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/getExamByClass') ?>",
                data: "_token=" + "{{ csrf_token() }}" + "&id=" + classesID + "&academic_year_id=" + academic_year_id + '&semester_id=' + semester_id + '&done=1' + "&schema_name=" + schema_name,
                dataType: "html",
                success: function(data) {
                    $('#examID').html(data);
                }
            });
        }
    });

    $('#classesID').change(function(event) {
        var schema_name = $('#schema_name').val();
        var classesID = $(this).val();
        if (classesID === '0') {
            $('#academic_year_id').val(0);
            $('#report_filter_div').hide();
        } else {
            $('#report_filter_div').show();
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/get_academic_years') ?>",
                data: "_token=" + "{{ csrf_token() }}" + "&id=" + classesID + "&schema_name=" + schema_name,
                dataType: "html",
                success: function(data) {
                    $('#academic_year_id').html(data);
                }
            });
        }
    });

    
    $('#schema_name').change(function(event) {
        var schema_name = $('#schema_name').val();
        var schema_name = $(this).val();
        if (schema_name === '0') {
            $('#academic_year_id').val(0);
            $('#report_filter_div').hide();
        } else {
            $('#report_filter_div').show();
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/getClasses') ?>",
                data: "_token=" + "{{ csrf_token() }}" + "&schema_name=" + schema_name,
                dataType: "html",
                success: function(data) {
                    $('#classesID').html(data);
                }
            });
        }
    });

/*    $('#academic_year_id').change(function(event) {
        var academic_year_id = $(this).val();
        var classesID = $('#classesID').val();
        if (academic_year_id === '0') {
            $('#sectionID').val(0);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('student/sectioncall') ?>",
                data: "id=" + classesID + '&academic_year_id=' + academic_year_id + '&all_section=1',
                dataType: "html",
                success: function(data) {
                    $('#sectionID').html(data);
                }
            });
        }
    }); */


</script>
</script>
@endsection