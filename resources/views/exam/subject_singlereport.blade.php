@extends('layouts.app')

@section('content')
<?php $root = url('/public/'); ?>
<!-- Page-body start -->
<div class="page-body">
    <div class="row">
        <!-- Server Side Processing table start -->
        <div class="col-sm-12">
            <!-- Basic Inputs Validation start -->
            <div class="card">
                <div class="card-header">
                    <h5>Basic Inputs Validation</h5>
                    <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                </div>
                <div class="card-block">
<?php
$usertype = session("usertype");
$exams_report_setting = \DB::table('exam_report_settings')->where('exam_type', 'single')->first();
?>

<link href="<?php echo url('public/assets/print.css'); ?>" rel="stylesheet" type="text/css">
<?php
if (can_access('print_exam_report')) {
    ?>
    <div class="well">
        <div class="row">
            <div class="col-sm-6">

            <?php
                if (!$exam_report) {
                    ?>

                    <button class="btn-cs btn-sm-cs" onclick="return false"  onmousedown="$('#validate_report').toggle()" data-target="#validate"><span class="fa fa-file-o"></span> Validate & Create Report</button>

                    <div class="col-sm-offset-2 col-lg-12" id="validate_report" style="display:none">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                        class="sr-only">Close</span></button>
                                <h4 class="modal-title">Validate and Create Report</h4>
                            </div>
                            <div class="body">


                                <p class="alert alert-info">
                                    If you click "Create Report", you will be able to print all reports.                       </p>
                                <form id="demo-form2" action="<?= url('exam/create_report') ?>" method="post" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" >

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Report Name 
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="first-name" placeholder="E.g Terminal Exam Results" required="required" class="form-control col-md-7 col-xs-12" name="name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">To Parents
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="checkbox" name="email_sent" id="send_email_option">Send Email
                                            &nbsp;<input type="checkbox" name="sms_sent" id="send_sms_option">Send SMS
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">send SMS/Email with Rank Per
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="radio" name="rank_per_stream" value="1" id="send_email_option"> Section/Stream
                                            &nbsp;<input type="radio" name="rank_per_stream" value="0" id="send_sms_option"> Class
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Date of reporting 
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="first-name" placeholder="Which day a school will open" required="required" class="calendar form-control col-md-7 col-xs" name="reporting_date">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        </label>
                                        <div class="col-md-6 text-center col-sm-6 col-xs-12">
                                            <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span
                                                    class="fa fa-print"></span> print') ?> Preview</button>

                                        </div>
                                    </div>

                                    <input type="hidden" name="classes_id" value="<?= $student->classesID ?>">
                                    <input type="hidden" name="student_id_return" value="<?= $student->student_id ?>">
                                    <input type="hidden" name="year_id" value="<?= request('year_no') ?>">
                                    <div class="ln_solid"></div>


                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" style="margin-bottom:0px;"
                                        data-dismiss="modal">close') ?></button>
                                <input type="submit" name="submit"  class="btn btn-success" value="Create Report"/>
                            </div>
                            <?= csrf_field() ?>
                            </form>
                        </div>
                    </div>

                <?php } else { ?>

                    <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span
                            class="fa fa-print"></span> print') ?> </button>




                                                                                                   <!-- <button class="btn-cs btn-sm-cs" data-toggle="modal" data-target="#mail"><span
                                                                                                                    class="fa fa-envelope-o"></span> mail') ?></button> -->
                <?php } ?>





            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= url("dashboard/index") ?>"><i
                                class="fa fa-laptop"></i> menu_dashboard') ?></a></li>
                    <li><a href="<?= url("exam/index") ?>">exam') ?></a></li>
                    <li class="active">view') ?></li>
                    <button class="btn-default btn-cs btn-sm-cs" data-toggle="modal" data-target="#idCard"><span class="fa fa-gear"></span> Options</button>
                </ol>
            </div>
        </div>

    </div>

<?php } else if (strtolower($usertype) == 'parent') { ?>
    <div class="well">
        <div class="row">
            <div class="col-sm-6">
                <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span
                        class="fa fa-print"></span> print') ?> </button>

            </div>
        </div>
    </div>
    <?php
}
if (empty($grades)) {
    echo '<div class="alert alert-warning">Report cannot be displayed because no grades have been defined in this class level. Please go to <a href="' . url('grade/index') . '">"Grade"</a> to define grades first</div>';
} else {
    ?>
    <div style="overflow: auto" class="overflow" oncontextmenu="return false" onselectstart="return false" ondragstart="return false">
        <div id="printablediv" class="page center sheet padding-10mm" >

            <section class=" subpage">


                <div id="p1" style="overflow: hidden; left: 5%;" class="">


                    <!-- Begin inline CSS -->
                    <style type="text/css">

                        .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
                            font-weight: bolder
                        }

                        #printablediv {
                            color: #000;
                        }

                        table.table.table-striped td, table.table.table-striped th, table.table {
                            margin-left: 10%;
                        }

                        @page {
                            margin: 0
                        }

                        .sheet {
                            margin: 0;
                            overflow: hidden;
                            position: relative;
                            box-sizing: border-box;
                            page-break-after: always;
                        }

                        /** Padding area **/
                        .sheet.padding-10mm {
                            padding: 10mm
                        }

                        /** For screen preview **/
                        @media screen {

                            .sheet {
                                background: white;
                                box-shadow: 0 .5mm 2mm rgba(0, 0, 0, .3);
                                margin: 5mm;
                                left:12%;
                            }
                        }
                    </style>
                    <!-- End inline CSS -->

                    <!-- Begin embedded font definitions -->
                    <style id="fonts1" type="text/css">


                        table.table.table-striped td, table.table.table-striped th, table.table {
                            border: 1px solid #000000;
                        }


                    </style>

                    <style type="text/css">
                        @font-face {
                            font-family: DejaVuSans_b;
                            src: url("../../public/assets/fonts/DejaVuSans_b.woff") format("woff");
                        }

                        table.table.table-striped td, table.table.table-striped th, table.table {
                            border: 1px solid #000000;
                        }

                        .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
                            font-weight: bolder
                        }

                        #printablediv {
                            color: #000;
                        }


                        @media print {
                            h3, h4 {
                                page-break-after: avoid;
                                line-height: 0;
                            }

                            .name_left{
                                margin-left: 0 !important;
                            }
                            .stream_right{
                                margin-right: 0 !important;
                            }
                            section.A4 {
                                width: 210mm
                            }

                            @page {
                                size: A4;
                                margin: 0;

                            }
                            table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
                                padding: 0.9%;
                            }

                            table {
                                width: 100%;

                            }
                            #signature_stamp_preview{
                                position:relative; margin:-11% 5% 0 0 !important;
                            }
                            .subpage {
                                padding: 1em 1em 0 1em;
                                /* border: 3px #f00 solid; */
                                font-size: .8em;
                            }


                            .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
                                padding: 0.2%;   border: 1px solid black !important;
                            }
                            table {
                                border-collapse: collapse !important;
                            }

                            .table-bordered > thead > tr > th, .table-bordered > tbody > tr > td {
                                font-size: 12px; padding: 1%;
                            }
                            .letterhead{

                                font-weight: 100;
                                font-family: "Adobe Garamond Pro";
                            }

                            .col-sm-6 , .col-lg-6{max-width: 50%}
                            .grading{float: left;}
                            .keys{float: right}
                            h3, h4 {
                                page-break-after: avoid;
                                font-size: 15px;
                                font-weight:100;
                            }
                            h2{
                                font-weight:bolder !important;
                            }


                        }

                        h3, h4 {
                            page-break-after: avoid;

                        }

                    </style>
                    <?php if ($classlevel->result_format == 'CSEE') { ?>
                        <style type="text/css">
                            table.table.table-striped td, table.table.table-striped th, table.table {
                                font-size: 11px !important;
                                margin-left: 0%;
                            }

                            @page {
                                margin: 0
                            }
                            .sheet {
                                margin: 0;
                                overflow: hidden;
                                position: relative;
                                box-sizing: border-box;
                                page-break-after: always;
                            }

                            /** Padding area **/
                            .sheet.padding-10mm {
                                padding: 10mm
                            }

                            /** For screen preview **/
                            @media screen {

                                .sheet {
                                    background: white;
                                    box-shadow: 0 .5mm 2mm rgba(0, 0, 0, .3);
                                    margin: 5mm;
                                    left:12%;
                                }
                            }

                        </style>
                    <?php } ?>
                    <!-- Begin page background -->


                    <?php
                    $array = array(
                        "src" => url('storage/uploads/images/' . $siteinfos->photo),
                        'width' => '126em',
                        'height' => '126em',
                        'class' => 'img-rounded',
                        'style' => 'margin-left:2em'
                    );
                    $image = is_file('storage/uploads/images/' . $student->photo) ? 'storage/uploads/images/' . $student->photo : 'storage/uploads/images/defualt.png';
                    $photo = array(
                        "src" => url($image),
                        'width' => '128em',
                        'height' => '128em',
                        'class' => 'img-rounded',
                        'style' => 'margin-left:2em'
                    );
                    ?>
                    <div class="">
                        <table class=" table-striped center" style="margin: 1px 2px 1px 0px;">
                            <thead>
                                <tr>
                                    <th class="col-md-2" style="padding-bottom:0px">
                                        <?= img($array); ?>
                                    </th>
                                    <th class="col-md-8 text-center letterhead" style="margin: 1% 0 0 16%; padding-top: 2%; ">

                                        <h2 style="font-weight: bolder !important; font-size: 24px; text-transform: uppercase;"><?= $siteinfos->sname ?></h2>
                                        <h4><?= 'P.O. BOX ' . $siteinfos->box . ', ' . ucfirst($siteinfos->address) ?></h4>
                                        <h3>cell') ?>: <?= str_replace(',', ' / ', $siteinfos->phone) ?></h3>
                                        <h3> email') ?>:<?= $siteinfos->email ?></h3>
                                        <h3>website') ?>: <?= $siteinfos->website ?></h3>
                                    </th>
                                    <th class="col-md-2" style="padding-bottom:0px">
                                        <?= img($photo); ?>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <hr/>
                </div>

                <div class="">
                    <h1 align='center' style="font-size: 1.9em; font-weight: bolder;padding-top: 0%; ">
                        <?= (count($exam_report) && $exam_report->name != '') ? strtoupper($exam_report->name) : $data->lang->line("single_report_title") ?> (<?= strtoupper($classes->classes) . ' ' . $academic_year->name ?>)</h1>
                </div>
                <div>

                    <?php //if ($classlevel->result_format != 'CSEE') {  
                    ?><!-- class="panel-body profile-view-dis" -->
                    <?php //}      ?>

                    <div class="panel-body container profile-view-dis" style="margin-top: .1em; padding: 0 5%;">
                        <div class="row">
                            <div class="col-sm-8 name_left" style="float:left; margin-left: 3%; color:black" >
                                <h3 style="font-weight: bolder;"> NAME: <?= strtoupper($student->name) ?></h3>
                                <h4 style="margin-top: 2%;"> Admission No: <?= $student->roll ?></h4>
                            </div>
                            <div class="col-sm-3 stream_right" style="float: right; font-weight: bolder; margin-right:-110px">
                                <h4></h4>
                                <?php if (strtoupper($classes->classes) == 'FORM SIX' || strtoupper($classes->classes) == 'FORM FIVE') { ?>
                                    <h4>COMB :<?=
                                        $student->section
                                        ?></h4>
                                <?php } else { ?>
                                    <h4>@lang('exam_report_lang.eattendance_section'): 
                                        <?php echo $student->section;
                                    }
                                    ?></h4>
                            </div>
                            <div style="clear:both;"></div>
                        </div>

                    </div>

                </div>

                <?php
                if (count($report) == 0) {
                    echo '<p class="alert alert-info">Sorry, this Exam report has not yet been published</p>';
                } else if (empty($marks)) {
                    echo '<p class="alert alert-info">Sorry, this Exam has not yet been published</p>';
                } elseif (count($exams) > 0 && count($exams)) {
                    ?>

                    <div class="row table" style="margin:0 2%;">
                        <?php
                        //print_r($marks);
                        if ($marks && $exams) {

                            $examController = new \App\Http\Controllers\mark();
                            ?>

                            <div class="col-lg-12">

                                <?php
                                $totalstudent_insection = $examController->get_student_per_class($classesID, $sectionID);
                                ?>
                                <div class="row" style="margin: 2%;font-family: verdana,arial,sans-serif; padding:.9%; ">
                                    <?php
                                    $point[$student->student_id] = [];
                                    if ($marks && $exams) {
                                        ?>

                                        <div class="col-lg-12 table-responsive" >


                                            <?php
                                            echo "<table  class=\"table table-striped table-bordered center\" style=\" margin-bottom: -.1%;margin-left:0\">";

                                            $f = 1;
                                            echo "<thead style='background-color:#b3b3b3; font-weight:bold !important'>";
                                            echo "<tr>";
                                            echo "<th>";
                                            echo strtoupper($data->lang->line("mark_subject"));
                                            echo "</th>";
                                            echo "<th>";

                                            echo 'Combined Exams';

                                            echo "</th>";
                                            if (count($grades) && $f == 1) {
                                                if ($exams_report_setting->show_subject_point == 1) {
                                                    echo "<th>";
                                                    echo strtoupper($data->lang->line("mark_point"));
                                                    echo "</th>";
                                                }
                                                if ($exams_report_setting->show_grade == 1) {
                                                    echo "<th>";
                                                    echo strtoupper($data->lang->line("mark_grade"));
                                                    echo "</th>";
                                                }
                                            }
                                            echo "<th>";
                                            echo strtoupper($data->lang->line("grade_status"));
                                            echo "</th>";
                                            if ($exams_report_setting->show_subject_pos_in_class == 9) {
                                                echo "<th>";
                                                echo 'P/C';
                                                echo "</th>";
                                            }
                                            if ($exams_report_setting->show_subject_pos_in_section == 9) {
                                                echo "<th>";
                                                echo 'P/S';
                                                echo "</th>";
                                            }
                                            echo "</tr>";

                                            echo "</thead>";

                                            echo "<tbody>";
                                            $mark_value = 0;

                                            foreach ($subjects as $subject) {
                                                $subject_name = strtolower($subject->subject);

                                                if (isset($marks[0]->{"$subject_name"})) {


                                                    $mark_value = $marks[0]->{"$subject_name"};


                                                    foreach ($grades as $grade) {
                                                        if ($grade->gradefrom <= round($mark_value, 0) && $grade->gradeupto >= round($mark_value, 0)) {

                                                            $sub = [
                                                                'subject_mark' => $mark_value,
                                                                'point' => $grade->point,
                                                                'penalty' => $subject->is_penalty,
                                                                'pass_mark' => $subject->pass_mark,
                                                                'is_counted_indivision' => $subject->is_counted_indivision
                                                            ];

                                                            array_push($point[$student->student_id], $sub);
                                                        }
                                                    }
                                                }

                                                echo "<tr style='font-weight:100'>";
                                                echo "<td style='font-weight:bold !important' data-title='" . $data->lang->line('mark_subject') . "'>";
                                                echo strtoupper($subject->subject);
                                                echo "</td>";
                                                echo "<td align='left' data-title='" . $data->lang->line('mark_mark') . "'>";
                                                echo $mark_value;
                                                echo "</td>";
                                                if (count($grades)) {
                                                    foreach ($grades as $grade) {
                                                        if ($grade->gradefrom <= round($mark_value, 0) && $grade->gradeupto >= round($mark_value, 0)) {
                                                            if ($exams_report_setting->show_subject_point == 1) {
                                                                echo "<td align='center' data-title='" . $data->lang->line('mark_point') . "'>";
                                                                echo $grade->point;
                                                                echo "</td>";
                                                            }
                                                            if ($exams_report_setting->show_grade == 1) {
                                                                echo "<td data-title='" . $data->lang->line('mark_grade') . "'>";
                                                                echo $grade->grade;
                                                                echo "</td>";
                                                            }

                                                            echo "<td data-title='" . $data->lang->line('mark_mark') . "'>";
                                                            echo $mark_value == '' ? '' : $grade->note;
                                                            echo "</td>";
                                                        }
                                                    }
                                                } else {
                                                    echo '<td></td><td></td>';
                                                }

                                                if ($exams_report_setting->show_subject_pos_in_class == 9) {
                                                    echo "<td align='center' data-title='" . $data->lang->line('mark_mark') . "'>";

//                                                        $subject_info = $examController->get_position($mark->student_id, $mark->examID, $mark->subjectID, $mark->classesID);
//                                                        echocount($subject_info) ? $subject_info->rank : '';
                                                    echo "</td>";
                                                }
                                                if ($exams_report_setting->show_subject_pos_in_section == 9) {
                                                    $subject_info_insection = [];
                                                    echo "<td align='center' data-title='" . $data->lang->line('mark_mark') . "'>";
                                                    echo count($subject_info_insection) ? $subject_info_insection->rank : '';
                                                    echo "</td>";
                                                }
                                                echo "</tr>";
                                            }

                                            echo "</tbody>";
                                            echo "</table>";
                                            ?>

                                            <table class="table table-striped table-bordered" style="margin-bottom: 0;margin-left:0">
                                                <thead>
                                                    <tr>
                                                        <th style="font-weight: bolder; width:19.2%;">total") ?>:
                                                            <strong><?php // $report->sum     ?></strong></th>

                                                        <th style="font-weight: bolder;width:26.6%;">avg_mark") ?>:
                                                            <strong><?= round($report->average, 1) ?></strong></th>
                                                        <?php if ($exams_report_setting->show_pos_in_class == 1) { ?>
                                                            <th style="font-weight: bolder">P/C: 
                                                                <strong><?= $report->rank ?></strong> out_of") ?> <?php echo $total_students->count ?></th>
                                                        <?php } ?>
                                                        <?php if ($exams_report_setting->show_pos_in_stream == 9) { ?>
                                                            <th style="font-weight: bolder">P/S: <strong>
                                                                <?= $report->rank . ' </strong> ' . $data->lang->line("out_of") . ' ' . count($total_students_section) ?></th> 

                                                        <?php } ?>
                                                        <?php
                                                        if ($exams_report_setting->show_acsee_division == 1 || $exams_report_setting->show_csee_division == 1) {
                                                            if ($classlevel->result_format == 'ACSEE' && $exams_report_setting->show_acsee_division) {

                                                                $division = getDivisionBySort($point[$student->student_id], $classlevel->result_format);
                                                                $div = $division[0];
                                                                $point = $division[1];
                                                                ?>
                                                                <th>Division: <?= $div ?></th>
                                                                <th>mark_point") ?>: <?= $point ?></th>

                                                                <?php
                                                            } else if ($exams_report_setting->show_csee_division == 1) {
                                                                $division = getDivisionBySort($point[$student->student_id], $classlevel->result_format);
                                                                $div = $division[0];
                                                                $point = $division[1];
                                                                ?>
                                                                <th>Division: <?= $div ?></th>
                                                                <th>mark_point") ?>: <?= $point ?></th>

                                                            <?php }
                                                            ?>

                                                        <?php } ?>
                                                    </tr>
                                                </thead>
                                            </table>

                                            <div style="float: left; width: 50%">
                                                <table class="table table-striped table-bordered" style="  float: left; margin: 1% 0 0 0;">
                                                    <thead>
                                                        <tr>
                                                            <th style="font-weight: bolder; text-align: center; border-bottom: 1px solid #fff;">
                                                                <?= strtoupper($data->lang->line('menu_grade')) ?>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                </table>

                                                <table class="table table-striped table-bordered" style=" margin-bottom: 1%;margin-left:0">
                                                    <thead>
                                                        <tr>
                                                            <?php foreach ($grades as $grade) { ?>
                                                                <th style="font-weight: bolder; text-align: center;"><?= $grade->grade ?>
                                                                </th>
                                                            <?php } ?>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <?php foreach ($grades as $grade) { ?>
                                                                <td style="font-weight: 100; font-size: smaller; text-align: center;background-color: white;color: #000;"><?= $grade->gradefrom ?>
                                                                    - <?= $grade->gradeupto ?>
                                                                </td>
                                                            <?php } ?>
                                                        </tr>
                                                    </tbody>

                                                </table>
                                            </div>
                                            <div style="float: right; width: 49.6%" >
                                                <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                                    <thead>
                                                        <tr>
                                                            <th style="font-weight: bolder; "><strong>key') ?>:</strong>
                                                                P/S:=pos_in_section") ?>, P/C:=pos_in_class") ?>
                                                                Combined Exams:= <?php
                                                                $exam_names = '';
                                                                foreach ($exams as $value) {

                                                                    if (isset($exam_percents[$value->exam]) && count($exam_percents[$value->exam])) {

                                                                        $percentage = '(' . ($exam_percents[$value->exam]) . '%)';
                                                                    } else {
                                                                        $percentage = '';
                                                                    }

                                                                    $exam_names .= $value->exam . $percentage . ' ,';
                                                                }
                                                                echo rtrim(str_replace('_', ' ', ucfirst($exam_names)), ' ,');
                                                                ?>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td style="background-color: white;color: #000;"><strong>formula') ?>:</strong> <span
                                                                    style="font-size: smaller"></span>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <?php
                                            /**
                                             * 
                                             * ----------------------------------------------
                                             * 
                                             * Report footer
                                             * ----------------------------------------------
                                             * 
                                             * 
                                             * 
                                             */
                                            if ($exams_report_setting->show_all_signature_on_the_footer == 0) {
                                                ?>        

                                                <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:56%;text-align: left">
                                                                <b style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px">
                                                                <?= $exams_report_setting->class_teacher_remark ?>:</b><br> <?php
                                                                if (count($character) > 0 && strlen($character->class_teacher_comment) > 1) {
                                                                    echo "<span class='' style='font-weight:100'>";
                                                                    echo $character->class_teacher_comment;
                                                                    echo "</span>";
                                                                } else {

                                                                    foreach ($grades as $grade) {
                                                                        if ($grade->gradefrom <= round($report->average, 0) && $grade->gradeupto >= round($report->average, 0)) {
                                                                            echo "<span class='' style='font-weight:100'>";
                                                                            echo $grade->overall_academic_note;
                                                                            echo "</span>";
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                            </th>
                                                            <th style="text-align: left;"><?php if (isset($report_setting->show_classteacher_name) && $report_setting->show_classteacher_name == 1) {
                                                                    ?>
                                                                    class_teacher') ?>: <?= $class_teacher_signature->name ?><br>
                                                                    <br>Phone: <u><?= $class_teacher_signature->phone?></u><br>
                                                                    <?php
                                                                    if ($class_teacher_signature->signature == NULL) {
                                                                        echo strtoupper($class_teacher_signature->name[0] . '.' . str_after($class_teacher_signature->name, ' ')[0]);
                                                                    } else {
                                                                        ?>
                                                                        <img src="<?= $class_teacher_signature->signature ?>" width="54"
                                                                             height="32">
                                                                             <?php
                                                                         }
                                                                     } else {
                                                                         ?>
                                                                    class_teacher_signature') ?><br>
                                                                    <?php
                                                                    if ($class_teacher_signature->signature == NULL) {
                                                                        echo strtoupper($class_teacher_signature->name[0] . '.' . str_after($class_teacher_signature->name, ' ')[0]);
                                                                    } else {
                                                                        ?>
                                                                        <img src="<?= $class_teacher_signature->signature ?>" width="54"
                                                                             height="32">
                                                                             <?php
                                                                         }
                                                                     }
                                                                     ?>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                                <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 56%; text-align: left; "><b style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px"><?= $exams_report_setting->head_teacher_remark ?>:</b><br> <?php
                                                                if (count($character) > 0 && strlen($character->head_teacher_comment) > 1) {
                                                                    echo "<span class='' style='font-weight:100'>";
                                                                    echo $character->head_teacher_comment;
                                                                    echo "</span>";
                                                                } else {
                                                                    foreach ($grades as $grade) {
                                                                        if (isset($report) && count($report)) {
                                                                            if ($grade->gradefrom <= round($report->average, 0) && $grade->gradeupto >= round($report->average, 0)) {
                                                                                echo "<span class='' style='font-weight:100'>";
                                                                                echo $grade->overall_note;
                                                                                echo "</span>";
                                                                                break;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                ?></th>
                                                            <th style="width: 50%; float: left;"> <?= $siteinfos->headname ?>'s<br/> Signature<br/></th>
                                                        </tr> 
                                                    </thead>
                                                </table>
                                                <?php
                                                /**
                                                 * 
                                                 * ----------------------------------------------
                                                 * 
                                                 * End of Report footer with buttom signature
                                                 * ----------------------------------------------
                                                 * 
                                                 * 
                                                 * 
                                                 */
                                            } else {
                                                ?>

                                                <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align: left;">
                                                                <b style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px"><?= $exams_report_setting->class_teacher_remark ?>:</b><br> <?php
                                                                if (count($character) > 0 && strlen($character->class_teacher_comment) > 1) {
                                                                    echo "<span class=''>";
                                                                    echo $character->class_teacher_comment;
                                                                    echo "</span>";
                                                                } else {

                                                                    foreach ($grades as $grade) {
                                                                        if ($grade->gradefrom <= round($report->average, 0) && $grade->gradeupto >= round($report->average, 0)) {
                                                                            echo "<span class=''>";
                                                                            echo $grade->overall_academic_note;
                                                                            echo "</span>";
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                                ?></th>

                                                        </tr>
                                                        <tr>
                                                            <th style=" text-align: left; ">
                                                                <b style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px"><?= $exams_report_setting->head_teacher_remark ?>:</b><br> <?php
                                                                if (count($character) > 0 && strlen($character->head_teacher_comment) > 1) {
                                                                    echo "<span class=''>";
                                                                    echo $character->head_teacher_comment;
                                                                    echo "</span>";
                                                                } else {
                                                                    foreach ($grades as $grade) {
                                                                        if (isset($report) && count($report)) {
                                                                            if ($grade->gradefrom <= round($report->average, 0) && $grade->gradeupto >= round($report->average, 0)) {
                                                                                echo "<span class='' >";
                                                                                echo $grade->overall_note;
                                                                                echo "</span>";
                                                                                break;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                ?></th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                                <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align: left;"><?php if (isset($report_setting->show_classteacher_name) && $report_setting->show_classteacher_name == 1) {
                                                                    ?>
                                                                    class_teacher') ?>: <?= $class_teacher_signature->name ?><br>
                                                                    <?php
                                                                    if ($class_teacher_signature->signature == NULL) {
                                                                        echo strtoupper($class_teacher_signature->name[0] . '.' . str_after($class_teacher_signature->name, ' ')[0]);
                                                                    } else {
                                                                        ?>
                                                                        <img src="<?= $class_teacher_signature->signature ?>" width="54"
                                                                             height="32">
                                                                             <?php
                                                                         }
                                                                     } else {
                                                                         ?>
                                                                    class_teacher_signature') ?><br>
                                                                    <?php
                                                                    if ($class_teacher_signature->signature == NULL) {
                                                                        echo strtoupper($class_teacher_signature->name[0] . '.' . str_after($class_teacher_signature->name, ' ')[0]);
                                                                    } else {
                                                                        ?>
                                                                        <img src="<?= $class_teacher_signature->signature ?>" width="54"
                                                                             height="32">
                                                                             <?php
                                                                         }
                                                                     }
                                                                     ?>
                                                            </th>

                                                            <th style="width: 50%; float:left;"> <?= $siteinfos->headname ?>'s<br/> Signature<br/></th>
                                                        </tr> 
                                                    </thead>
                                                </table>

                                                <?php
                                                /**
                                                 * 
                                                 * -----------------------------------------
                                                 * 
                                                 * End of footer with both signature at the bottom
                                                 * ------------------------------------------
                                                 */
                                            }
                                            ?>

                                            <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center;">

                                                            <?php if (count($exam_report) && date('dmY', strtotime($exam_report->reporting_date)) != '01011970') { ?>
                                                                <b><?php echo "Date of Reporting: " . date('jS M Y', strtotime($exam_report->reporting_date)) ?> </b>
                <?php } ?>		
                                                        </th>
                                                    </tr>
                                                </thead>
                                            </table>

                                        </div>
            <?php }
            ?>

                                </div>
                                <div style="padding-left:5%;">

                                    <div style="z-index: 4000">
                                        <div style="float: right; margin-right: 4%; margin-top: 4%;"></div>
                                        <div>

                                            <?php
                                            $path = "../../storage/uploads/images/" . $classlevel->stamp . "?v=1";
                                            ?>
                                            <?php
                                            $array = array(
                                                "src" => $path,
                                                'width' => '100',
                                                'height' => '100',
                                                'class' => '',
                                                'id' => "",
                                                'style' => 'position:relative; margin:-17% 15% 0 0; float:right;'
                                            );
                                            echo img($array);
                                            ?>


                                        </div>
                                    </div>

                                </div>

        <?php } ?>
                            </section>

                        </div>
                    </div>

                    <!-- CONTENT MAIL -->
                    <div class="col-sm-9 mail_view">
                        <div class="inbox-body">

                            <div>

                                <h2>exam_comment') ?></h2>

                                <!-- end of user messages -->
                                <ul class="messages"> 
                                    <?php
                                    $messages = \App\Model\ExamComment::where('student_id', $student->student_id)->where('academic_year_id', $_GET['year_no'])->whereNotNull('user_table')->get();
                                    //$messages = \DB::select('SELECT a.id,a.body, a.student_id, d.usertype, d.name, a.created_at FROM ' . set_schema_name() . 'exam_comments a,' . set_schema_name() . 'users d,' . set_schema_name() . 'student b WHERE a.student_id = b.student_id AND a.name = d.name AND a.student_id =' . $student->student_id . 'AND a.academic_year_id =' . $_GET['year_no'] . 'ORDER BY a.created_at');
                                    if(!empty($messages)){
                                    foreach ($messages as $message) {
                                        ?>
                                        <li>
                                            <img src="<?= url("storage/uploads/images/defualt.png"); ?>" class="avatar" alt="Avatar">
                                            <div class="message_date">
                                                <h3 class="date text-info"><?= timeAgo($message->created_at) ?></h3>
                                            </div>
                                            <div class="message_wrapper">
                                                <h4 class="heading"><?= $message->user()->name ?> - <?= $message->user()->usertype ?></h4>
                                                <blockquote class="message"><?= $message->body ?></blockquote>
                                                <br>                                                  
                                            </div>
                                        </li>

                                        <?php
                                    }
                                }
                                    ?>
                                    <div id="inbox_message"></div>
                                    <li>
                                        <p class="url" style="margin-left: 10%;">
                                            <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                            <textarea id="contents" required="required" class="form-control" name="contents" data-parsley-trigger="keyup" rows="3" data-parsley-minlength="20" data-parsley-maxlength="100" placeholder="exam_message') ?>"></textarea>
                                            <a href="#" id="reply_comment" onclick="return false" class="btn btn-sm btn-success"><i class="fa fa-comment"></i> comment') ?></a>
                                        </p>
                                    </li>
                                </ul>
                                <!-- end of user messages -->
                            </div>
                        </div>

                    </div>
                    <!-- /CONTENT MAIL -->

                    <script type="text/javascript">
                        $("#reply_comment").mousedown(function () {
                            var message = $('#contents').val();
                            var id = "<?= $student->student_id; ?>";
                            var name = "<?= $user_info->name; ?>";
                            var exam_id = $('#')
                            if (message == "") {
                                $("#message_error").html("Please Enter Your Review on This Report").css("text-align", "left").css("color", 'red');

                            } else {
                                $.ajax({
                                    type: 'POST',
                                    url: "<?= url('exam/examReviewCombined') ?>",
                                    data: {"message": message, "id": id, "name": name, "academic_year":<?= $_GET['year_no'] ?>},
                                    dataType: "html",
                                    success: function (data) {
                                        window.location.reload();
                                        console.log(data);
                                    }
                                });
                            }
                        });

                    </script>



        <?php if (can_access('add_subject')) { ?>
                        <!-- email modal starts here -->
                        <form class="form-horizontal" role="form" action="<?= url('exam/send_mail'); ?>" method="post">
                            <div class="modal fade" id="mail">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                                    class="sr-only">Close</span></button>
                                            <h4 class="modal-title">mail') ?></h4>
                                        </div>
                                        <div class="modal-body">

                                            <?php
                                            if (form_error($errors, 'to'))
                                                echo "<div class='form-group has-error' >";
                                            else
                                                echo "<div class='form-group' >";
                                            ?>
                                            <label for="to" class="col-sm-2 control-label">
            to") ?>
                                            </label>
                                            <div class="col-sm-6">
                                                <input type="email" class="form-control" id="to" name="to" value="<?= old('to') ?>">
                                            </div>
                                            <span class="col-sm-4 control-label" id="to_error">
                                            </span>
                                        </div>

                                        <?php
                                        if (form_error($errors, 'subject'))
                                            echo "<div class='form-group has-error' >";
                                        else
                                            echo "<div class='form-group' >";
                                        ?>
                                        <label for="subject" class="col-sm-2 control-label">
            subject") ?>
                                        </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="subject" name="subject"
                                                   value="<?= old('subject') ?>">
                                        </div>
                                        <span class="col-sm-4 control-label" id="subject_error">
                                        </span>

                                    </div>

                                    <?php
                                    if (form_error($errors, 'message'))
                                        echo "<div class='form-group has-error' >";
                                    else
                                        echo "<div class='form-group' >";
                                    ?>
                                    <label for="message" class="col-sm-2 control-label">
            message") ?>
                                    </label>
                                    <div class="col-sm-6">

                                        <textarea class="form-control" id="message" style="resize: vertical;" name="message"
                                                  value="<?= old('message') ?>"></textarea>
                                    </div>
                                </div>


                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" style="margin-bottom:0px;"
                                        data-dismiss="modal">close') ?></button>
                                <!--<input type="button" id="send_pdf" class="btn btn-success" value="send") ?>"/>-->
                            </div>
                            </div>
                            </div>
                            </div>
            <?= csrf_field() ?>
                        </form>
                        <!-- email end here -->

                        <!-- Modal content start here -->
                        <div class="modal fade" id="idCard">
                            <div class="modal-dialog">
                                <form action="#" method="post" class="form-horizontal" role="form">
                                    <input type="hidden" name="id" value="<?= $exams_report_setting->id ?>"/>
                                    <div class="modal-content">

                                        <div class="modal-header">
                                            Single Report Settings
                                        </div>
                                        <?php
                                        $vars = get_object_vars($exams_report_setting);
                                        ?>
                                        <div class="modal-body" > 
                                            <table class="table table-hover">
                                                <?php
                                                foreach ($vars as $key => $variable) {
                                                    if (!in_array($key, array('id', 'exam_type', 'show_teacher', 'show_teacher_sign', 'show_overall_division', 'show_division_by_exam', 'semester_average_name', 'single_semester_avg_name', 'show_pos_in_section', 'overall_semester_avg_name', 'average_column_name', 'show_remarks', 'updated_at', 'created_at'))) {
                                                        $name = ucfirst(str_replace('_', ' ', $key));
                                                        $final_name = str_replace('pos', 'position', $name);
                                                        $lname = str_replace('classteacher', 'class teacher ', $final_name);
                                                        ?>
                                                        <tr style="border-bottom:1px solid whitesmoke">
                                                            <td style="padding-left:5px;">
                                                                <h4><?= $lname ?></h4>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                if (is_integer($variable) && $variable == 1) {
                                                                    ?>
                                                                    <input type="checkbox" name="<?= $key ?>" checked="checked" onchange="this.value = this.checked ? 1 : 0" value="<?= $variable ?>"/>
                                                                <?php } else if ((is_integer($variable) && $variable == 0) || $variable == '') { ?>
                                                                    <input type="checkbox" onchange="this.value = this.checked ? 1 : 0" name="<?= $key ?>"  value="<?= $variable ?>"/>
                                                                <?php } else { ?>
                                                                    <input type="text" name="<?= $key ?>" value="<?= $variable ?>"/>
                    <?php } ?>
                                                            </td>

                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </table>   
                                        </div>


                                        <div class="modal-footer">
                                            <button type="button" style="margin-bottom:0px;" class="btn btn-default" data-dismiss="modal" onclick="javascript:closeWindow()">close') ?></button>
                                            <button type="submit" class="btn btn-success">Save</button>
                                        </div>
            <?= csrf_field() ?>
                                </form>
                            </div>
                        </div>
                </div>
                <!-- Modal content End here -->

                <script language="javascript" type="text/javascript">


                    function closeWindow() {
                        location.reload();
                    }

                    function check_email(email) {
                        var status = false;
                        var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
                        if (email.search(emailRegEx) == -1) {
                            $("#to_error").html('');
                            $("#to_error").html("mail_valid') ?>").css("text-align", "left").css("color", 'red');
                        } else {
                            status = true;
                        }
                        return status;
                    }


                    $("#send_pdf").click(function () {
                        var to = $('#to').val();
                        var subject = $('#subject').val();
                        var message = $('#message').val();

                        var error = 0;

                        if (to == "" || to == null) {
                            error++;
                            $("#to_error").html("");
                            $("#to_error").html("mail_to') ?>").css("text-align", "left").css("color", 'red');
                        } else {
                            if (check_email(to) == false) {
                                error++
                            }
                        }

                        if (subject == "" || subject == null) {
                            error++;
                            $("#subject_error").html("");
                            $("#subject_error").html("mail_subject') ?>").css("text-align", "left").css("color", 'red');
                        } else {
                            $("#subject_error").html("");
                        }

                        if (error == 0) {
                            $.ajax({
                                type: 'POST',
                                url: "<?= url('exam/send_mail') ?>",
                                data: 'to=' + to + '&subject=' + subject + "&id=" + id + "&message=" + message,
                                dataType: "html",
                                success: function (data) {
                                    location.reload();
                                }
                            });
                        }
                    });
                </script>

        <?php } ?>
            <script>
                function printDiv(divID) {

                    //Get the HTML of div
                    var divElements = document.getElementById(divID).innerHTML;
                    //Get the HTML of whole page
                    var oldPage = document.body.innerHTML;

                    //Reset the page's HTML with div's HTML only
                    document.body.innerHTML =
                            '<html><head><title></title></head><body>' +
                            divElements + '</body>';

                    //Print Page
                    window.print();
                    //Restore orignal HTML
                    document.body.innerHTML = oldPage;



                }

                function trigger_message(a) {
                    swal({
                        title: "Are you sure?",
                        text: "SMS will be sent to all parents in this class",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, I am sure!',
                        cancelButtonText: "No, cancel it!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {

                                if (isConfirm) {
                                    swal("Report!", "Message will be sent now!", "success");
                                    window.location.href = a;

                                } else {
                                    swal("Cancelled", "Message not sent :)", "error");
                                    e.preventDefault();
                                }
                            });
                }
            </script>

            <?php
        }
    } //close if no grades defined
    ?>
@endsection