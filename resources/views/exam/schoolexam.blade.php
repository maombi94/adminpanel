@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">

        <form style="" class="form-horizontal" role="form" method="post">
            <div class="row">
                <div class="col-sm-12 col-xl-4 m-b-30">
                      <h4 class="sub-title">School</h4>
                      <select id="schema" class="form-control form-control-primary">
                      <option value="opt1">Select One School</option>
                                @if (count(load_schemas()) > 0)
                                @foreach (load_schemas() as $schema)
                                    <option value="{{ $schema->username }}"> {{ school($schema->username)->name }}</option>
                                @endforeach
                                @endif
                      </select>
                  </div>
                  <div class="col-sm-12 col-xl-4 m-b-30" id="hide-form1">
                      <h4 class="sub-title">Class Level</h4>
                      <select name="select" id="classes_id" class="form-control form-control-primary">

                      </select>
                  </div>
                  <div class="col-sm-12 col-xl-4 m-b-30" id="hide-form">
                      <h4 class="sub-title">Academic Year</h4>
                      <select name="select" id="academic_year_id" class="form-control form-control-primary">
                         
                      </select>
                  </div>
                  </div>
                  <hr>

                  <?= csrf_field() ?>
              </form>

           <?php 
           if(isset($exams) && count($exams)>0)  { $e=1; ?>
                
                <div class="table-responsive dt-responsive text-center">
                <table id="simpletable" class="table table-striped table-bordered nowrap">
                       <thead>
                           <tr>
                               <th>S/N</th>
                               <th>Academic year</th>
                               <th>Boys</th>
                               <th>Girls</th>
                               <th>Total </th>
                               <th>Action </th>
                           </tr>
                         </thead>
                         <tbody>
                         @foreach ($exams as $exam)
                             
                             <tr>
                                <th>{{ $e++ }}</th>
                               <td ><?= $exam->exam  ?></td>
                               <td class="text-center"><?= $exam->abbreviation ?></td>
                               <td><?= $exam->semester ?></td>
                               <td><?= $exam->classes ?></td>
                               <td>
                                        <a type="button" class="btn btn-info btn-sm btn-out-dotted" 
                                            href="<?= url('exam/schoolreport/' .$this_schema .'/' . $exam->examID .'/'.$exam->class_id.'/'.$exam->academic_year_id) ?>">
                                            View
                                        </a>
                                        </td>
                             </tr>
                         @endforeach
                         </tbody>
                   </table>
                  </div>
             <?php  } ?>
            
         
        </div>
    </div>
    <!-- Server Side Processing table end -->
</div>


<script type="text/javascript">
       $('#schema').change(function (event) {
            var schema = $(this).val();
            if (schema === 'All') {
                window.location.href = "<?= url('Users/all_students/all') ?>";
              } else {
                $.ajax({ 
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '<?= url('Users/getClassLevel') ?>',
                    data: {schema: schema},
                    dataType: "html", 
                    cache: false,
                    success: function (data) { 
                       $('#classes_id').html(data);
                    }
                });
    
            }
        });

        $('#classes_id').change(function(event) {
        var schema_name = $('#schema').val();
        var classesID = $(this).val();
        if (classesID === '0') {
            $('#academic_year_id').val(0);
            $('#report_filter_div').hide();
        } else {
            $('#report_filter_div').show();
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/get_academic_years_bylevel') ?>",
                data: "_token=" + "{{ csrf_token() }}" + "&id=" + classesID + "&schema_name=" + schema_name,
                dataType: "html",
                success: function(data) {
                    $('#academic_year_id').html(data);
                }
            });
        }
    });

        $('#academic_year_id').change(function () {
        var academic_year_id = $(this).val();
        var schema = $('#schema').val();
        var classes_id = $('#classes_id').val();
        if (classes_id == 0 || academic_year_id == 0) {
            // $('#hide-table').hide();
            // $('.nav-tabs-custom').hide();
        } else {
            window.location.href = "<?= url('Exam/school') ?>/" + schema + '/' + academic_year_id + '/' + classes_id;
        }
    });

    </script>

@endsection