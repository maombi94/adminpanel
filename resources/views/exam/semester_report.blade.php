@extends('layouts.app')

@section('content')

<?php $root = url('/public/'); ?>
<!-- Page-body start -->
<div class="page-body">
    <div class="row">
        <!-- Server Side Processing table start -->
        <div class="col-sm-12">
            <div class="card">
               
                <div class="card-block">
   
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js" integrity="sha512-s/XK4vYVXTGeUSv4bRPOuxSDmDlTedEpMEcAQk0t/FMd9V6ft8iXdwSBxV0eD60c6w/tjotSlKu9J2AAW1ckTA==" crossorigin="anonymous"></script>
<link href="<?php echo url('public/assets/print.css'); ?>" rel="stylesheet" type="text/css" media="all">

<?php
$usertype = session("usertype");
$exams_report_setting = $report_setting = \DB::table($this_schema.'.exam_report_settings')->where('exam_type', 'combined')->where('classlevel_id', $classes->classlevel_id)->first();
if (!empty($exams_report_setting) && (strlen($exams_report_setting->class_teacher_remark) < 4 || strlen($exams_report_setting->head_teacher_remark) < 4)) {
    \DB::table($this_schema.'.exam_report_settings')->where('exam_type', 'combined')->where('classlevel_id', $classes->classlevel_id)->update([
        'class_teacher_remark' => 'Class Teacher Remark',
        'head_teacher_remark' => 'Head Teacher Remark'
    ]);
    $exams_report_setting = $report_setting = \DB::table($this_schema.'.exam_report_settings')->where('exam_type', 'combined')->where('classlevel_id', $classes->classlevel_id)->first();
}

//check exam if its combined or not
$exams_combined = explode('_', request('exam'));
$report_generated = 0;
foreach ($exam_report as $value) {
    $combined_exam_array = str_replace('{', '', str_replace('}', '', $value->combined_exam_array));
    $arr_exam = explode(',', $combined_exam_array);
    $status = identical_values($arr_exam, $exams_combined);

    if ($status == TRUE) {
        $report_generated = 1;

        $exam_report = $value;
    }
}
?>

    <!-- <div id="printablediv" class="page center sheet"> -->
        <section class="subpage ">
            <div id="p1" class="">


                <!-- Begin inline CSS -->
                <style type="text/css">

                    .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
                        font-weight: bolder
                    }

                    #printablediv {
                        color: #000;
                    }

                    table.table.table-striped td, table.table.table-striped th, table.table {
                        margin-left: 10%;
                    }

                    @page {
                        margin: 0
                    }

                    .sheet {
                        margin: 0;
                        overflow: hidden;
                        position: relative;
                        box-sizing: border-box;
                        page-break-after: always;
                    }

                    /** Padding area **/
                    .sheet.padding-10mm {
                        padding: 10mm
                    }

                    /** For screen preview **/
                    @media screen {

                        .sheet {
                            background: white;
                            box-shadow: 0 .5mm 2mm rgba(0, 0, 0, .3);
                            margin: 5mm;
                            left:12%;
                        }
                    }
                </style>
                <!-- End inline CSS -->

                <!-- Begin embedded font definitions -->
                <style id="fonts1" type="text/css">


                    table.table.table-striped td, table.table.table-striped th, table.table {
                        border: 1px solid #000000;
                    }


                </style>
                <!-- Begin inline CSS -->
                <style type="text/css">
                    #signature_stamp_preview{
                        position:relative; margin:-14% 5% 0 0;
                    }

                    .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
                        font-weight: bolder
                    }

                    #printablediv {
                        color: #000;
                    }

                    table.table.table-striped td, table.table.table-striped th, table.table {
                        border: 1px solid #000000;
                    }

                    @font-face {
                        font-family: DejaVuSans_b;
                        src: url("../../public/assets/fonts/DejaVuSans_b.woff") format("woff");
                    }

                    table.table.table-striped td, table.table.table-striped th, table.table {
                        border: 1px solid #000000;
                        margin: 1px auto !important;
                    }
                    @media print {

                        h3, h4 {
                            page-break-after: avoid;
                            /* line-height: 0; */
                        }

                        .name_left{
                            margin-left: 0 !important;
                        }
                        .stream_right{
                            margin-right: 0 !important;
                        }
                        section.A4 {
                            width: 210mm
                        }

                        @page {
                            size: A4;
                            margin: 0;

                        }
                        table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
                            padding: 0.9%;
                        }

                        table {
                            width: 100%;

                        }
                        #signature_stamp_preview{
                            position:relative; margin:-11% 5% 0 0 !important;
                        }
                        .subpage {
                            padding: 1em 1em 0 1em;
                            /* border: 3px #f00 solid; */
                            font-size: .8em;
                        }


                        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
                            padding: 0.2%;   border: 1px solid black !important;
                        }
                        table {
                            border-collapse: collapse !important;
                        }

                        .table-bordered > thead > tr > th, .table-bordered > tbody > tr > td {
                            font-size: 12px; padding: 1%;
                        }
                        .letterhead{

                            font-weight: 100;
                            font-family: "Adobe Garamond Pro";
                        }

                        .col-sm-6 , .col-lg-6{max-width: 50%}
                        .grading{float: left;}
                        .keys{float: right}
                        h3, h4 {
                            page-break-after: avoid;
                            font-size: 15px;
                            font-weight:100;
                        }
                        h2{
                            font-weight:bolder !important;
                        }

                    }

                    h3, h4 {
                        page-break-after: avoid;

                    }

                </style>
                <?php if ($level->result_format == 'CSEE') { ?>
                    <style type="text/css">
                        table.table.table-striped td, table.table.table-striped th, table.table {
                            font-size: 11px !important;
                            margin-left: 10%;
                        }

                        @page {
                            margin: 0
                        }
                        .letterhead{

                            font-family: "Adobe Garamond Pro";
                        }
                        .sheet {
                            margin: 0;
                            overflow: hidden;
                            position: relative;
                            box-sizing: border-box;
                            page-break-after: always;
                        }

                        /** Padding area **/
                        .sheet.padding-10mm {
                            padding: 10mm
                        }

                        /** For screen preview **/
                        @media screen {

                            .sheet {
                                background: white;
                                box-shadow: 0 .5mm 2mm rgba(0, 0, 0, .3);
                                margin: 5mm;
                                left:12%;
                            }
                        }


                    </style>
                <?php } ?>
                <!-- Begin page background -->


                <?php
                $array = array(
                    "src" => url('storage/uploads/images/' . $siteinfos->photo),
                    'width' => '126em',
                    'height' => '126em',
                    'class' => 'img-rounded',
                    'style' => 'margin-left:2em'
                );
                if(strpos($student->photo, 'http') !== false ){
                    $image = $student->photo;
                } else{
                    $image = is_file('storage/uploads/images/' .$student->photo) ? url('storage/uploads/images/' . $student->photo) : url('storage/uploads/images/defualt.png');
                }

                $photo = array(
                    "src" => $image,
                    'width' => '128em',
                    'height' => '128em',
                    'class' => 'img-rounded',
                    'style' => 'margin-left:2em'
                );
                ?>
                <div class="">
                    <table class=" table-striped center" style="margin: 1px 2px 1px 0px;">
                        <thead>
                            <tr>
                                <th class="col-md-2" style="padding-bottom:0px">
                                    <?= img($array); ?>
                                </th>
                                <th class="col-md-8 text-center letterhead" style="margin: 1% 0 0 16%; padding-top: 2%; ">

                                    <h2 style="font-weight: bolder; text-transform: uppercase;"><?= $siteinfos->sname ?></h2>
                                    <h3><?= 'P.O. BOX ' . $siteinfos->box . ', ' . ucfirst($siteinfos->address) ?>
                                        <br>
                                        cell: <?= str_replace(',', ' / ', $siteinfos->phone) ?>
                                        <br> email:<?= $siteinfos->email ?><br>
                                        <?= $siteinfos->website != '' ? 'website' . ' : ' . $siteinfos->website . '</h3>' : '</h3>' ?>
                                        </th>
                                        <th class="col-md-2" style="padding-bottom:0px">
                                            <?= img($photo); ?>
                                        </th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <hr/>
            </div>

            <div class="">
                <h1 align='center' style="font-size: 18px; margin-top: 0.7em; padding-bottom: 0.1em; font-weight: bold">
                    <?php
                    if (!empty($exam_report) && $report_generated == 1) {
                        echo $exam_report->name != '' ? strtoupper($exam_report->name) : ' STUDENT	ACADEMIC REPORT ';
                    } else {
                        echo ' STUDENT ACADEMIC REPORT ';
                    }
                    ?> 
                    (<?= strtoupper($classes->classes) . ' ' . $academic_year_name->name ?>)</h1>
            </div>
            <div <?php if ($level->result_format != 'CSEE') { ?> class="panel-body profile-view-dis" <?php } ?>>
                <div class="row" style="margin: 2%;">
                    <div class="col-sm-6" style="float:left;">
                        <h1 style="font-weight: bolder;font-size: 1.4em;"> name <?= $student->name ?></h1>
                        <h4 style="margin-top: 2%;"> roll: <?= $student->roll ?></h4>
                    </div>
                    <div class="col-sm-3" style="float: right;">
                        <?php if (strtoupper($classes->classes) == 'FORM SIX' || strtoupper($classes->classes) == 'FORM FIVE') { ?>
                            <h1 style="font-weight: bolder; font-size: 1.4em;">COMB :
                                <?= $section->section ?>
                            <?php } else { ?>
                                stream 

                                <?php
                                echo $section->section;
                            }
                            ?>
                        </h1> 


                    </div>
                    <div style="clear:both;"></div>
                </div>

            </div>

            <?php
            if (!empty($exams)) {
                $exam_percent = request('exam_percent');
                $percentage = json_decode($exam_percent);
                ?>

                <div class="row" style="margin:0 2%;font-family: verdana,arial,sans-serif; ">
                    <?php
                    if ($marks && $exams) {

                       $examController = new \App\Http\Controllers\exam();
                        ?>
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered center">
                                    <thead style="background-color: #b3b3b3">
                                        <tr>
                                            <th rowspan="2"><?= strtoupper('subject_name') ?></th>
                                            <?= $report_setting->show_teacher == 1 ? '<th rowspan="2">' . 'teacher_name' . '</th>' : '' ?>
                                            <?php
                                            $days_per_semester = 0;
                                            $semester_start_date = '';
                                            $s_c = 0;
                                            $count_semesters = count($semesters);
                                            foreach ($semesters as $semester) {
                                                if ($s_c == 0) {
                                                    $semester_start_date = $semester->start_date;
                                                }

                                                $days_per_semester += $semester->study_days;

                                                $add = $exams_report_setting->show_subject_total_across_exams == 1 ? 2 : 1;

                                                if ($exams_report_setting->show_marks == 1) {
                                                    ?>
                                                    <th colspan="<?= count($sem_exams[$semester->id]) + $add ?>"><?= strtoupper($semester->name) ?></th>
                                                    <?php
                                                    $s_c++;
                                                    if ($s_c == $count_semesters) {
                                                        $semester_end_date = $semester->end_date;
                                                    }
                                                }
                                                ?>
                                            <?php } if (count($semesters) > 1) { ?>
                                                <th rowspan="2"><?= $report_setting->average_column_name ?></th>
                                            <?php } ?>
                                            <th rowspan="2">GD</th>
                                            <?php if ($report_setting->show_subject_point == 1) { ?>
                                                <th rowspan="2" style="font-weight: bolder">points</th>
                                            <?php } ?>
                                            <th rowspan="2">remarks</th>
                                            <!--<th rowspan="2">P/S</th>-->
                                            <?= $report_setting->show_teacher_sign == 1 ? '<th rowspan="2">' . 'sign' . '</th>' : '' ?>
                                        </tr>
                                        <tr>
                                            <?php
                                            $is = 1;
                                            foreach ($semesters as $semester) {
                                                foreach ($sem_exams[$semester->id] as $exam) {
                                                    ?>
                                                    <th rowspan="2" style="font-weight: bolder"><?= strtoupper($exam->abbreviation) ?>
                                                        <?=
                                                        (isset($percentage->{$exam->examID}) && !empty($percentage->{$exam->examID}) && $exams_report_setting->show_percentage_on_mark_column == 1) ?
                                                                '<small style="font-size:80%">(' . $percentage->{$exam->examID} . '%)</small>' : ''
                                                        ?></th>
                                                        <?php
                                                }

                                                if ($exams_report_setting->show_subject_total_across_exams == 1) {
                                                    echo '<th rowspan="2">Total</th>';
                                                }
                                                echo '<th rowspan="2">' . $report_setting->semester_average_name . '' . $is . '</th>';

                                                $is++;
                                            }
                                            ?>

                                        </tr>
                                    </thead>
                                    <tbody style="text-align: left;">
                                        <?php
                                        $total_marks = 0;
                                        $total_subjects_done = 0;
                                        $formula = array();
                                        $subjects_no = 0;
                                        $point_ = [];
                                        foreach ($subjects as $subject) {
                                            ?>
                                            <tr>
                                                <td style="font-weight: bold !important"><?= strtoupper($subject->subject) ?></td>
                                                <?php
                                                $teacher = DB::table($this_schema.'.teacher')->where('teacherID', $subject->teacher_id)->first();
                                              //  dd($subject);
                                              !empty($teacher) ? $name = explode(' ', $teacher->name) : $name = [];
                                                $firstname = isset($name[0]) ? $name[0] : 'NILL';
                                                $lastname = isset($name[count($name) - 1]) ? $name[count($name) - 1] : 'NILL';
                                                preg_match('#^(\w+\.)?\s*([\'\’\w]+)\s+([\'\’\w]+)\s*(\w+\.?)?$#',  !empty($teacher) ? $teacher->name : null, $results);

                                                if ($report_setting->show_teacher == 1) {
                                                    ?>
                                                    <td style="font-weight: bolder; font-size: 8px !important" >
                                                    <?php
                                                        if (isset($results[3])) {
                                                            $res = $results[3];
                                                        } else if (isset($results[2])) {
                                                            $res = $results[2];
                                                        } else {
                                                            $res = 'NILL';
                                                        }

                                                            echo substr(ucfirst(trim($firstname)), 0, 1) . ' . ' . ucfirst(trim($res));
                                                    ?></td>

                                                <?php }
                                                ?>

                                                <?php
                                                // $penalty = 0;
                                                $sum_sum = 0;
                                                $x = 1;
                                                foreach ($semesters as $semester) {
                                                    $exam_done = 0;

                                                    $total = 0;

                                                    $formula[$x] = $report_setting->semester_average_name . '' . $x . '=(';
                                                    foreach ($sem_exams[$semester->id] as $exam) {


                                                        $mark = DB::table($this_schema.'.mark')->where("examID", $exam->examID)->where('student_id', $student->student_id)->where('classesID', $classesID)->where('subjectID', $subject->subject_id)->first();

                                                        $name = str_replace(' ', '_', strtolower($exam->exam));
                                                        //if (!empty($mark)) {						   
                                                        if (isset($percentage->{$exam->examID}) && !empty($percentage->{$exam->examID})) {

                                                            $times = $percentage->{$exam->examID} / 100; //Percent is within examID
                                                            $total += !empty($mark) ? $mark->mark * $times : 0;

                                                            $form_times = $exams_report_setting->show_percentage_on_mark_column == 1 ? '' : '*' . $times;
                                                            $formula[$x] .= $x <= count($exams) ? ' ' . $exam->abbreviation . $form_times . ' +' : "";
                                                        } else {
                                                            $times = 1;
                                                            $total += !empty($mark) ? $mark->mark : 0;
                                                            $x <= count($exams) ? $formula[$x] .= $exam->abbreviation . '+' : '';
                                                        }
                                                        // }
                                                        /*
                                                         * check penalty for that subject
                                                         */
                                                        if ($subject->is_penalty == 1) {
                                                            if (!empty($mark)) {
                                                                $penalty = $mark->mark < $subject->pass_mark ? 1 : 0;
                                                            }
                                                        }
                                                        ?>

                                                        <td align="center" style="font-size: 10px !important"><?php
                                                            if (!empty($mark)) {
                                                                if ($mark->mark == '' || (int)$mark->mark == 0) {
                                                                    echo $empty_mark;
                                                                } else {
                                                                    echo (isset($percentage->{$exam->examID}) && !empty($percentage->{$exam->examID}) && $exams_report_setting->show_percentage_on_mark_column == 1) ? $mark->mark * $percentage->{$exam->examID} / 100 : $mark->mark;
                                                                }
                                                            }else{
                                                                echo $empty_mark;
                                                            }
                                                        ?></td>

                                                        <?php
                                                        if (!empty($mark)) {
                                                            //if ($mark->total_mark == '') {
//later we will change this condition to match exactly case for student who do the subject
                                                            // } else {
                                                            //$exam_done++;
                                                            //  }
                                                        }
                                                        $exam_done++;
                                                    }

                                                    if ($exam_done != 0) {
                                                        $avg = $times == 1 ? round($total / $exam_done, 1) : round($total, 1);
                                                    } else {
                                                        $avg = 0;
                                                    }

                                                    $formula[$x] = $times == 1 ? '' . rtrim($formula[$x], '+') . ' ) / ' . $exam_done : rtrim($formula[$x], '+') . ')';
                                                    $x++;
                                                    if ($exams_report_setting->show_subject_total_across_exams == 1) {
                                                        echo '<td>' . $total . '</td>'; //total for subject across exams
                                                    }
                                                    echo '<td align="center">' . $avg . '</td>';

                                                    $sum_sum += (float) $avg;
                                                }
                                                ?>
                                                <?php
                                                if (count($semesters) > 1) {
                                                    $avg_more_semesters = round($sum_sum / count($semesters), 1);
                                                    ?>

                                                    <td align="center"><?= $avg_more_semesters ?></td>

                                                <?php } ?>


                                                <?php
                                                if ($avg == 0) {
                                                    //this is a short time solution, we put empty but
                                                    //later on we need to see if this student takes this subject or not
                                                    echo '<td></td><td></td>';
                                                    if ($report_setting->show_subject_point == 1) {
                                                        echo '<td></td>';
                                                    }
                                                } else {
                                                    $avg_mark = count($semesters) > 1 ? $avg_more_semesters : (float) $avg;

                                                    if ($subject->is_counted_indivision == 1) {
                                                        foreach ($grades as $grade) {
                                                            if ($grade->gradefrom <= round($avg_mark, 0) && $grade->gradeupto >= round($avg_mark, 0)) {
                                                                $sub = [
                                                                    'subject_mark' => $avg_mark,
                                                                    'point' => $grade->point,
                                                                    'penalty' => $subject->is_penalty,
                                                                    'pass_mark' => $subject->pass_mark,
                                                                    'is_counted_indivision' => $subject->is_counted_indivision
                                                                ];
                                                                array_push($point_, $sub);

                                                                $subjects_no++;
                                                            }
                                                        }
                                                    }

                                                    echo return_grade($grades, round($avg_mark), $report_setting->show_subject_point);
                                                    $total_subjects_done++;
                                                }
                                                $total_marks += count($semesters) > 1 ? $avg_more_semesters : (float) $avg;
                                                ?>
                                                                                                                                                                                                                                                                                                                                            <!--					    <td>
                                                <?php
                                                /**
                                                 * Find position of this student per subject per those combinations
                                                 */
                                                ?>
                                                                                                                                                                                                                                                                                                                                                </td>-->
                                                <?php if ($report_setting->show_teacher_sign == 1) { ?>
                                                    <td>
                                                        <?php
                                                        if ($avg != '-') {
                                                            if (isset($teacher->signature) && $teacher->signature == NULL) {
                                                                echo strtoupper($firstname[0] . '.' . $lastname[0]);
                                                            } else {
                                                                ?>
                                                                <img src="<?= $teacher->signature ?>" width="34"
                                                                     height="10">
                                                                     <?php
                                                                 }
                                                             }
                                                             ?></td>
                                                    <?php } ?>
                                            </tr>

                                            <?php
                                        }
                                        ?>

                                        <tr>
                                            <td style="font-weight: bolder">exam_avg</td>
                                            <?= $report_setting->show_teacher == 1 ? '<td></td>' : '' ?>

                                            <?php
                                            //$sectionID = $section == 'CLASS' ? NULL : $student->section_id;
                                            $exam_count = 0;
                                            $exam_avg_sum = 0;

                                            foreach ($semesters as $semester) {
                                                foreach ($sem_exams[$semester->id] as $exam) {
            
                                                    $rank_info = $examController->get_subject_rank($exam->examID, $section->classesID, $student->student_id, NULL, $this_schema);
                                                    if (!empty($rank_info)) {

                                                        $exam_avg_sum += (isset($percentage->{$exam->examID}) && !empty($percentage->{$exam->examID})) ? $rank_info->average * $percentage->{$exam->examID} / 100 : $rank_info->average;
                                                        $exam_count++;
                                                        ?>

                                                        <td><?= (isset($percentage->{$exam->examID}) && !empty($percentage->{$exam->examID}) && $exams_report_setting->show_percentage_on_mark_column == 1 ) ? $rank_info->average * $percentage->{$exam->examID} / 100 : $rank_info->average ?></td>
                                                        <?php
                                                    } else {
                                                        $exam_avg_sum += 0;
                                                        echo '<td></td>';
                                                        //if we count all exams done, otherwise we will skip this
                                                        $exam_count++;
                                                    }
                                                }
                                                if ($exams_report_setting->show_subject_total_across_exams == 1) {
                                                    echo '<td></td>'; //total for subject across exams
                                                }
                                                echo "<td></td>";
                                            }
                                            ?>
                                            <td></td>
                                            <td></td>
                                            <?= $report_setting->show_subject_point == 1 ? '<td></td>' : '' ?>
                                            <?php if (count($semesters) > 1) { ?>
                                                <td></td>
                                            <?php } ?>
                                            <?= $report_setting->show_teacher_sign == 1 ? '<td></td>' : '' ?>
                                        </tr>
                                        <?php
                                        /**
                                         *
                                         *
                                         * ----------------------------------------------------------------
                                         *
                                         * We show Division & points only to A-level students. Those with
                                         * grade format of ACSEE
                                         */
                                        if (($report_setting->show_acsee_division == 1 && $level->result_format == 'ACSEE') || ($level->result_format == 'CSEE' && $report_setting->show_csee_division == 1)) {

                                            if ($report_setting->show_overall_division == 1 && !in_array(strtoupper($level->result_format), ['PSLE', 'OTHER'])) {
                                                ?>

                                                <tr>
                                                    <td style="font-weight: bolder">DIVISION</td>

                                                    <?= $report_setting->show_teacher == 1 ? '<td></td>' : '' ?>

                                                    <?php
                                                    $semester_ids = 0;
                                                    $i = 0;
                                                    $total_points = 0;

                                                    foreach ($semesters as $semester) {

                                                        foreach ($sem_exams[$semester->id] as $exam) {


                                                            $division[$i] = $examController->getDivision($student->student_id, $exam->examID, $classesID);
                                                            ?>
                                                            <td><?= $report_setting->show_division_by_exam == 1 ? $division[$i][0] : '' ?></td>
                                                            <?php
                                                            $total_points += (int) $division[$i][1];
                                                            $i++;
                                                        }if ($exams_report_setting->show_subject_total_across_exams == 1) {
                                                            echo '<td></td>'; //total for subject across exams
                                                        }
                                                        echo "<td></td>";
                                                        $semester_ids++;
                                                    }
                                                    $total_i = $i;
                                                    $total_average_points = floor($total_points / $total_i);
                                                    $penalty = isset($penalty) ? $penalty : null;
                                                    $div = getDivisionBySort($point_, $level->result_format);


                                                    //echo $division[0];
                                                    ?>
                                                    <td><?= $div[0] ?></td>
                                                    <td></td>
                                                    <?= $report_setting->show_subject_point == 1 ? '<td></td>' : '' ?>
                                                    <?php if (count($semesters) > 1) { ?>
                                                        <td></td>
                                                    <?php } ?>
                                                    <?= $report_setting->show_teacher_sign == 1 ? '<td></td>' : '' ?>
                                                </tr>

                                                <tr>
                                                    <td style="font-weight: bolder">points</td>
                                                    <?= $report_setting->show_teacher == 1 ? '<td></td>' : '' ?>

                                                    <?php
                                                    $semester_ids = 0;
                                                    $i = 0;
                                                    $total_points = 0;
                                                    foreach ($semesters as $semester) {
                                                        foreach ($sem_exams[$semester->id] as $exam) {
                                                            ?>
                                                            <td><?= $report_setting->show_division_by_exam == 1 ? $division[$i][1] : '' ?></td>
                                                            <?php
                                                            $i++;
                                                        }
                                                        if ($exams_report_setting->show_subject_total_across_exams == 1) {
                                                            echo '<td></td>'; //total for subject across exams
                                                        }
                                                        echo "<td></td>";
                                                        $semester_ids++;
                                                    }
                                                    ?>

                                                    <?= $report_setting->show_subject_point == 1 ? '<td>' . $div[1] . '</td>' : '' ?>
                                                    <td></td>
                                                    <?php if (count($semesters) > 1) { ?>
                                                        <td style="font-weight: bolder"></td>
                                                    <?php } ?>
                                                    <td></td>
                                                    <?= $report_setting->show_teacher_sign == 1 ? '<td></td>' : '' ?>
                                                </tr>

                                                <?php
                                            }
                                        }


                                        /**
                                         * Otherwise, we don't show points and division
                                         * ------------------------------------------------------
                                         */
                                        $this_section = isset($section->sectionID) && $section->sectionID == '' ? 'CLASS' : 'STREAM';
                                        $sectionID = $this_section == 'CLASS' ? NULL : $section->sectionID;
                                        if ($report_setting->show_pos_in_stream == 1) {
                                            ?>
                                            <tr>
                                                <td style="font-weight: bolder">POS. IN <?= $this_section ?></td>
                                                <?= $report_setting->show_teacher == 1 ? '<td></td>' : '' ?>
                                                <?php
                                                foreach ($semesters as $semester) {
                                                    foreach ($sem_exams[$semester->id] as $exam) {

                                                        $rank_info = $examController->get_subject_rank($exam->examID, $section->classesID, $student->student_id, $sectionID, $this_schema);
                                                        ?>
                                                        <td><?= !empty($rank_info) ? $rank_info->rank : '' ?></td>
                                                        <?php
                                                    }

                                                    if ($exams_report_setting->show_subject_total_across_exams == 1) {
                                                        echo '<td></td>'; //total for subject across exams
                                                    }
                                                    if (count($semesters) < 2) {
                                                        echo '<td>' . $rank . '</td>';
                                                    } else {
                                                        echo '<td></td>';
                                                    }
                                                }
                                                ?>
                                                <?php if (count($semesters) > 1) { ?>
                                                    <td><?= $rank ?></td>
                                                <?php } ?>
                                                <?= $level->result_format == 'ACSEE' ? "" : '' ?>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                              
                                            </tr>
        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row" style="margin:0 0.1%;font-family: verdana,arial,sans-serif; ">
                            <?php
                                if ($exams_report_setting->show_average_row == 1) {
                                    ?>
                                    <table class="table table-striped table-bordered" style=" width: 100%; margin: 0%;">

                                        <thead>

                                            <tr>
                                                <?php
                                            }
                                            $avg_name = count($semesters) == 1 ? $report_setting->semester_average_name . '' . ($is - 1) : $report_setting->average_column_name;
                                            ?>
                                            <?php
                                            if ($exams_report_setting->show_average_row == 1) {
                                                ?>
                                                <th colspan="2" style="font-weight: bolder;"><?= 'Overall Total' . ' ' . $avg_name ?>:
                                                    <strong><?= $total_marks ?></strong></th>
                                            <?php }
                                            ?>
                                            <th style="font-weight: bolder;width:33.4%;">
                                                <?php
                                                if ($exams_report_setting->show_average_row == 1) {
                                                    ?>
                                                        <?= count($semesters) == 1 ? $report_setting->single_semester_avg_name : $report_setting->overall_semester_avg_name ?>: <?php } ?>
                                                <strong><?php
                                                    $total_average = round($exam_avg_sum / ((isset($percentage->{$exam->examID}) && !empty($percentage->{$exam->examID})) ? count($semesters) : $exam_count), 1);
                                                    if ($exams_report_setting->show_average_row == 1) {
                                                        echo $total_average;
                                                    }
                                                    foreach ($grades as $grade) {

                                                        if ($report_setting->show_overall_grade == 1 && $grade->gradefrom <= round($total_average, 0) && $grade->gradeupto >= round($total_average, 0) && $exams_report_setting->show_average_row == 1)
                                                            echo " ($grade->grade)";
                                                    }
                                                    ?></strong></th>
                                            <?php if (isset($rank_in_class) && $exams_report_setting->show_pos_in_section == 1) { ?>
                                                <th style="font-weight: bolder">P/S:
                                                    <strong><?= $rank_in_section ?></strong> out of <?= count($students_per_section) ?>
                                                </th>
                                            <?php } ?>
                                                <?php if (isset($rank_in_class) && $exams_report_setting->show_pos_in_class == 1) { ?>
                                                <th style="font-weight: bolder">P/C:
                                                    <strong><?= $rank_in_class ?></strong> out of <?= count($students_per_class) ?>
                                                </th>
                                            <?php } ?>
                                            <?php
                                            if ($exams_report_setting->show_average_row == 1) {
                                                ?>
                                            </tr>

                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>

                                <?php } ?>
                           
                                {{-- <div class="col-sm-12 col-lg-12"> --}}

                                    <table class="table table-striped table-bordered" style="">
                                        <tr>

                                        <tr>
                                            <th colspan="<?= count($grades) ?>" style="font-weight: bolder; text-align: center;">
                                                School Grading
                                            </th>
                                        </tr>

                                        <tr>
                                                <?php foreach ($grades as $grade) { ?>
                                                <th style="font-weight: bold; text-align: center;">
                                                <?= $grade->grade ?><?php //= $level->result_format != 'ACSEE' ? ' (' . $grade->note . ')' : ''   ?>
                                                </th>
                                    <?php } ?>
                                        </tr>

                                        <tr>
                                                <?php foreach ($grades as $grade) { ?>
                                                <td style="font-weight: 100;font-size: smaller; text-align: center;background-color: white;color: #000;">
                                                <?= $grade->gradefrom ?> - <?= $grade->gradeupto ?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        </tbody>

                                    </table>
                                    </td>
                                    <td colspan="3">
                                        <table class="table" style="width:100%">

                                            <tbody>
                                                <tr>
                                                    <th><strong style="font-weight: bolder; ">
                                                        key:</strong>
                                                            <?php foreach ($exams as $exam) { ?>
                                                            <span>
                                                            <?= strtoupper($exam->abbreviation) ?>=<?= $exam->exam ?></span>,
                                                            <?php
                                                        }
                                                        if ($report_setting->show_pos_in_section == 1) {
                                                            echo 'P/S = Pos in Stream';
                                                        }
                                                        if ($report_setting->show_pos_in_class == 1) {
                                                            echo 'P/C = Pos in Class';
                                                        }
                                                        if (isset($div[0]) && $div[0] == 'INC') {
                                                            echo 'INC= Incomplete Division';
                                                        }
                                                        ?>, GD=Grade

                                                    </th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    </tr>
                                    </table>



                                        <table class="table table-striped table-bordered" style="width:100%">
                                            <tr>
                                                <td style="background-color: white;color: #000;">

                                                    <strong>Formula:</strong>

                                                    <span style="font-size: smaller"><?php
                                                        $v = '';
                                                        foreach ($formula as $val) {
                                                            $v .= $val . ',';
                                                        }
                                                        echo rtrim($v, ',');
                                                        ?></span>

                                                    <?php
                                                    if (count($semesters) > 1) {
                                                        $t = '(';
                                                        $i = 1;
                                                        foreach ($semesters as $semester) {
                                                            $t .= $report_setting->semester_average_name . '' . $i . ' +';
                                                            $i++;
                                                        }
                                                        $avg_formula = rtrim($t, '+') . ' ) / ' . count($semesters);
                                                        ?>
                                                        <span style="font-size: smaller">, <?= $report_setting->average_column_name ?>=<?= rtrim($avg_formula, ',') ?></span>
                                                <?php } ?>

                                                </td>
                                            </tr>

                                        </table>
                                   
                       
                            <?php
                            /**
                             * 
                             * ----------------------------------
                             * 
                             * student attendance
                             * --------------------------------
                             */
                            if ($report_setting->show_student_attendance == 1) {
                                $att = \App\Model\Student::find($student->student_id);

                                $days = $att->sattendances()->where('present', 1)->where('date', '>=', date('Y-m-d', strtotime($semester_start_date)))->where('date', '<=', date('Y-m-d', strtotime($semester_end_date)))->count();
                                ?>
                                <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                        <thead>
                                            <tr>
                                                <th style="text-align: left;">
                                                    <b style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px">	Attendance:</b>
                                                </th>
                                                <th style="text-align: left;">
                                                    <?= $days ?> out of <?= $days_per_semester ?> days
                                                </th>
                                            </tr>

                                        </thead>
                                    </table>
                                <?php
                            }
                            /**
                             * 
                             * -----------------------------------
                             * end of attendance
                             * ---------------------------------
                             */
                            ?>

                            <?php
                     
                            if ($exams_report_setting->show_all_signature_on_the_footer == 0) {
                                ?>        

                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th style="width:56%; text-align: left; vertical-align: top;">

                                                    <b style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px"> <?= $exams_report_setting->class_teacher_remark ?>: </b><br> <?php
                                                    if (!empty($character) && $character->class_teacher_comment != '') {
                                                        echo "<span class=''>";
                                                        echo $character->class_teacher_comment;
                                                        echo "</span>";
                                                    } else {
                                                        foreach ($grades as $grade) {
                                                            if ($grade->gradefrom <= round($total_average, 0) && $grade->gradeupto >= round($total_average, 0)) {
                                                                echo "<span class=''>";
                                                                echo $grade->overall_academic_note;
                                                                echo "</span>";
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    ?></th>
                                                <th style="width:56%; text-align: left;">
                                                    <?php if ($report_setting->show_classteacher_name == 1) { ?>
                                                        class_teacher: <?= $class_teacher_signature->name ?><br>

                                                        <?php
                                                        if ($report_setting->show_classteacher_phone == 1) {
                                                            echo 'Phone: <u> ' . $class_teacher_signature->phone . '</u><br>';
                                                        }
                                                        if ($class_teacher_signature->signature == NULL) {
                                                            echo strtoupper($class_teacher_signature->name[0] . '.' . str_after($class_teacher_signature->name, ' ')[0]);
                                                        } else {
                                                            ?>
                                                            <img src="<?= $class_teacher_signature->signature ?>" width="54"
                                                                 height="32">
                                                                 <?php
                                                             }
                                                         } else {
                                                             ?>
                                                                 class_teacher_signature<br><img src="<?= $class_teacher_signature->signature ?>" width="54"
                                                                 height="32">
                                                                 <?php
                                                             }
                                                             ?>
                                                </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th style="width: 56%; text-align: left"><b style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px"><?= $exams_report_setting->head_teacher_remark ?>:</b><br> <?php
                                                    if (!empty($character) && $character->head_teacher_comment != '') {
                                                        echo "<span class=''>";
                                                        echo $character->head_teacher_comment;
                                                        echo "</span>";
                                                    } else {
                                                        foreach ($grades as $grade) {
                                                            if ($grade->gradefrom <= round($total_average, 0) && $grade->gradeupto >= round($total_average, 0)) {
                                                                echo "<span class=''>";
                                                                echo $grade->overall_note;
                                                                echo "</span>";
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    ?>
                                               
                                                </th>
                                                <th style="width: 50%; text-align: left"> <?= $siteinfos->headname ?>'s Signature<br></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <?php
                                /**
                                 * 
                                 * ----------------------------------------------
                                 * 
                                 * End of Report footer with buttom signature
                                 * ----------------------------------------------
                                 * 
                                 * 
                                 * 
                                 */
                            } else {
                                ?>


                                <div class="">
                                    <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                        <thead>
                                            <tr>
                                                <th style=" width:56%; text-align: left;"><b style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px"><?= $exams_report_setting->class_teacher_remark ?>:</b><br> <?php
                                                    if (!empty($character) && $character->class_teacher_comment != '') {
                                                        echo "<span class='' >";
                                                        echo $character->class_teacher_comment;
                                                        echo "</span>";
                                                    } else {
                                                        foreach ($grades as $grade) {
                                                            if ($grade->gradefrom <= round($total_average, 0) && $grade->gradeupto >= round($total_average, 0)) {
                                                                echo "<span class=''>";
                                                                echo $grade->overall_academic_note;
                                                                echo "</span>";
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    ?></th>

                                            </tr>
                                            <tr>
                                                <th style="width: 56%; text-align: left"><b style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px"><?= $exams_report_setting->head_teacher_remark ?>:</b><br> <?php
                                                    if (!empty($character) && $character->head_teacher_comment != '') {
                                                        echo "<span class='' >";
                                                        echo $character->head_teacher_comment;
                                                        echo "</span>";
                                                    } else {
                                                        foreach ($grades as $grade) {
                                                            if ($grade->gradefrom <= round($total_average, 0) && $grade->gradeupto >= round($total_average, 0)) {
                                                                echo "<span class=''>";
                                                                echo $grade->overall_note;
                                                                echo "</span>";
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    ?> <?php
                                                    if (set_schema_name() == 'enaboishu.') {
                                                        ?>
                                                        <p>
                                                        <hr/>
                                                        <b  style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px">Head Master comment: </b>Work Hard
                                                        </p>
            <?php } ?></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="">
                                    <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                        <thead>
                                            <tr>

                                                <th style="font-weight: bolder; width:56%; text-align: left;">
                                                    <?php if ($report_setting->show_classteacher_name == 1) { ?>
                                                        class_teacher: <?= $class_teacher_signature->name ?><br>
                                                        <?php
                                                        if ($report_setting->show_classteacher_phone == 1) {
                                                            echo 'Phone: <u> ' . $class_teacher_signature->phone . '</u><br>';
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($class_teacher_signature->signature == NULL) {
                                                            echo strtoupper($class_teacher_signature->name[0] . '.' . str_after($class_teacher_signature->name, ' ')[0]);
                                                        } else {
                                                            ?>
                                                            <img src="<?= $class_teacher_signature->signature ?>" width="54"
                                                                 height="32">
                                                                 <?php
                                                             }
                                                         } else {
                                                             ?>
                                                                 class_teacher_signature<br><img src="<?= $class_teacher_signature->signature ?>" width="54"
                                                                 height="32">
                                                                 <?php
                                                             }
                                                             ?>
                                                </th>
                                                <th style="width: 50%; text-align: left"> <?= $siteinfos->headname ?>'s Signature<br></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>

                                <?php
                                /**
                                 * 
                                 * -----------------------------------------
                                 * 
                                 * End of footer with both signature at the bottom
                                 * ------------------------------------------
                                 */
                            }
                            /**
                             * 
                             * ----------------------------------------------
                             * 
                             * start attendance
                             * ----------------------------------------------
                             */
                            ?>
        <?php if ($level->stamp != '') { ?>
                                <div style="padding-left:5%;">

                                    <div style="z-index: 4000">
                                        <?php
                                        $path = "storage/uploads/images/" . $level->stamp . "?v=2";
                                        ?>
                                        <?php
                                        $array = array(
                                            "src" => $path,
                                            'width' => '120em',
                                            'height' => '120em',
                                            'class' => 'img-rounded',
                                            'id' => "signature_stamp_preview",
                                            'style' => 'float:right;'
                                        );
                                        echo img($array);
                                        ?>

                                    </div>
                                </div>
                            <?php } ?>
        <?php if (isset($report_setting->show_parent_comment) && $report_setting->show_parent_comment == 1) { ?>

                                <div style="float: left; width: 100%">
                                    <table class="table table-striped table-bordered" style="  float: left; margin: -4% 0 0 0;">
                                        <thead>
                                            <tr>
                                                <th colspan="4" style="font-weight: bolder; text-align: left;">
                                                    Parent Comment:
                                                    <br><br><br><br><br><br>

                                                </th>                                                
                                                </th>                                                
                                                </th>                                                
                                                </th>                                                
                                                </th>                                                
                                            </tr>
                                            <tr>
                                                <th colspan="2">
                                                    <u> Parent Name</u>
                                                    <br> <br>.................................................................
                                                </th> 
                                                </th> 
                                                </th> 
                                                </th> 
                                                </th> 
                                                <th>
                                                    <u> Signature</u>
                                                    <br> <br>.....................................
                                                </th> 
                                                </th> 
                                                </th> 
                                                </th> 
                                                </th> 
                                                <th>
                                                    <u>  Date</u>
                                                    <br> <br> ...../...../...........
                                                </th> 
                                                </th> 
                                                </th> 
                                                </th> 
                                                </th> 

                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            <?php } ?>
        <?php if (!empty($exam_report) && isset($exam_report->reporting_date) && date('Y', strtotime($exam_report->reporting_date)) <> 1970) { ?>
                                <div class="">
                                    <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center;">


                                                    <b><?php
                                                        if ((int) strtotime($exam_report->reporting_date) > 0) {
                                                            if (date('Y', strtotime($exam_report->reporting_date)) <> 1970) {
                                                                echo 'date_of_reporting' . ': ' . date('jS M Y', strtotime($exam_report->reporting_date));
                                                            }
                                                        }
                                                        ?> </b>

                                                </th>
                                            </tr>

                                        </thead>
                                    </table>
                                </div>
                        <?php } ?>
                        </div>
    <?php } ?>

                </div>


        </div>

<?php } ?>
</section>
<div class="page-break"></div>
</div>
</div>


<!-- Modal content start here -->
<div class="modal fade" id="report_setting_model">
    <div class="modal-dialog">
        <form action="#" method="post" class="form-horizontal" role="form">
            <input type="hidden" name="id" value="<?= $exams_report_setting->id ?>"/>
            <div class="modal-content">

                <div class="modal-header">
                    Single Report Settings
                </div>
                <?php
                $vars = get_object_vars($exams_report_setting);
                ?>
                <div class="modal-body" > 
                    <table class="table table-hover">
                        <?php
                        foreach ($vars as $key => $variable) {
                            if (!in_array($key, array('id', 'exam_type', 'average_column_name', 'semester_average_name', 'single_semester_avg_name', 'classlevel_id', 'overall_semester_avg_name', 'average_column_name', 'show_remarks', 'show_subject_pos_in_section', 'show_subject_pos_in_class', 'created_at', 'updated_at'))) {
                                $name = ucfirst(str_replace('_', ' ', $key));
                                $final_name = str_replace('pos', 'position', $name);
                                $lname = str_replace('classteacher', 'class teacher ', $final_name);
                                ?>
                                <tr style="border-bottom:1px solid whitesmoke">
                                    <td style="padding-left:5px;">
                                        <h4><?= $lname ?></h4>
                                    </td>
                                    <td>
                                        <?php
                                        if (is_integer($variable) && $variable == 1) {
                                            ?>
                                            <input type="checkbox" name="<?= $key ?>" checked="checked" onchange="this.value = this.checked ? 1 : 0" value="1"/>
                                        <?php } else if ((is_integer($variable) && $variable == 0) || $variable == '') { ?>
                                            <input type="checkbox" onchange="this.value = this.checked ? 1 : 0" name="<?= $key ?>"  value="1"/>
                                        <?php } else { ?>
                                            <input type="text" name="<?= $key ?>" value="<?= $variable ?>"/>
        <?php } ?>
                                    </td>

                                </tr>
                                <?php
                            }
                        }
                        ?>
                        <tr style="border-bottom:1px solid whitesmoke">
                            <td style="padding-left:5px;">
                                <h4>Average Column Name</h4>
                            </td>
                            <td>
                                <input type="text" name="average_column_name" value="<?= $exams_report_setting->average_column_name ?>"/>
                            </td>
                        </tr>
                        <tr style="border-bottom:1px solid whitesmoke">
                            <td style="padding-left:5px;">
                                <h4>Semester Average Name</h4>
                            </td>
                            <td>
                                <input type="text" name="semester_average_name" value="<?= $exams_report_setting->semester_average_name ?>"/>
                            </td>
                        </tr>
                        <tr style="border-bottom:1px solid whitesmoke">
                            <td style="padding-left:5px;">
                                <h4>One Semester Average Name</h4>
                            </td>
                            <td>
                                <input type="text" name="single_semester_avg_name" value="<?= $exams_report_setting->single_semester_avg_name ?>"/>
                            </td>
                        </tr>
                        <tr style="border-bottom:1px solid whitesmoke">
                            <td style="padding-left:5px;">
                                <h4>Overall Semester Average Name</h4>
                            </td>
                            <td>
                                <input type="text" name="overall_semester_avg_name" value="<?= $exams_report_setting->overall_semester_avg_name ?>"/>
                            </td>
                        </tr>
                    </table>   
                </div>


                <div class="modal-footer">
                    <button type="button" style="margin-bottom:0px;" class="btn btn-default" data-dismiss="modal" onclick="javascript:closeWindow()">close</button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
<?= csrf_field() ?>
        </form>
    </div>
</div>
</div>
<!-- Modal content End here -->

<script language="javascript" type="text/javascript">

    function closeWindow() {
        location.reload();
    }

    function check_email(email) {
        var status = false;
        var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
        if (email.search(emailRegEx) == -1) {
            $("#to_error").html('');
            $("#to_error").html("mail_valid").css("text-align", "left").css("color", 'red');
        } else {
            status = true;
        }
        return status;
    }


    $("#send_pdf").click(function () {
        var to = $('#to').val();
        var subject = $('#subject').val();
        var message = $('#message').val();
        var id = "<?php // $exams->examID                                                                                ?>";
        var error = 0;

        if (to == "" || to == null) {
            error++;
            $("#to_error").html("");
            $("#to_error").html("mail_to").css("text-align", "left").css("color", 'red');
        } else {
            if (check_email(to) == false) {
                error++
            }
        }

        if (subject == "" || subject == null) {
            error++;
            $("#subject_error").html("");
            $("#subject_error").html("mail_subject").css("text-align", "left").css("color", 'red');
        } else {
            $("#subject_error").html("");
        }

        if (error == 0) {
            $.ajax({
                type: 'POST',
                url: "<?= url('parents/send_mail') ?>",
                data: 'to=' + to + '&subject=' + subject + "&id=" + id + "&message=" + message,
                dataType: "html",
                success: function (data) {
                    location.reload();
                }
            });
        }
    });
</script>

<script type="text/javascript">
    function printDiv(divID) {
        //Get the HTML of div
        var divElements = document.getElementById(divID).innerHTML;
        //Get the HTML of whole page
        var oldPage = document.body.innerHTML;

        //Reset the page's HTML with div's HTML only
        document.body.innerHTML =
                "<html><head><title></title></head><body>" +
                divElements + "</body>";

        //Print Page
        window.print();

        //Restore orignal HTML
        document.body.innerHTML = oldPage;
    }

    $(document).ready(function () {
        var element = $("#printablediv"); // global variable
        var getCanvas; // global variable

        html2canvas(element, {
            onrendered: function (canvas) {
                $("#previewImage").append(canvas);
                getCanvas = canvas;
            }
        });

        $("#downloadimage").on('click', function () {
            var imgageData = getCanvas.toDataURL("image/png");
            // Now browser starts downloading it instead of just showing it
            var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
            $("#downloadimage").attr("download", "Report_<?= $student->name ?>.png").attr("href", newData);
        });
    });
</script>
@endsection