@extends('layouts.app')

@section('content')
<?php $root = url('/public/'); ?>
<!-- Page-body start -->
<div class="page-body">
    <div class="row">
        <!-- Server Side Processing table start -->
        <div class="col-sm-12">
            <!-- Basic Inputs Validation start -->
            <div class="card">
                <div class="card-header">
                    <h5>Basic Inputs Validation</h5>
                    <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                </div>
                <div class="card-block">
        <div class="row">
            <div class="col-sm-12">
            <form id="main" method="post" action="">
                    @csrf
                <?php
                $usertype = session("usertype");
                ?>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Select School</label>
                            <div class="col-sm-10">
                            <select name="schema_name" id="schema_name" class="form-control form-control-danger">
                                <option value="opt1">Select One School</option>
                                @if (count(load_schemas()) > 0)
                                @foreach (load_schemas() as $schema)
                                    <option value="{{ $schema->username }}"> {{ $schema->username }}</option>
                                @endforeach
                                @endif
                               
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Select Class</label>
                            <div class="col-sm-10">
                            <select name="classesID" id="classesID" class="form-control form-control-info">
                                <option value="0">Select One Value Only</option>
                               
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Select Academic Year</label>
                            <div class="col-sm-10">
                            <select name="academic_year_id" id="academic_year_id" class="form-control form-control-primary">
                               
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Select class section</label>
                            <div class="col-sm-10">
                            <select name="sectionID" id="sectionID" class="form-control form-control-primary">
                               
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Select Subject</label>
                            <div class="col-sm-10">
                            <select name="subjectID" id="subjectID" class="form-control form-control-primary">
                               
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>

                        <div class="form-group" id="show_division" style="display:none">
                            <div class="col-sm-offset-4 col-sm-8">
                                <p><input type="checkbox" name="combine_subject" /> Do you want to combine similar subject?</p>
                            </div> 
                            <div class="col-sm-offset-4 col-sm-8">
                                <p><input type="checkbox" name="show_division" /> Show Division</p>
                            </div>
                            <div class="col-sm-offset-4 col-sm-8">
                                <p><input type="checkbox" name="show_type" /> Show NECTA Format with Grades</p>
                            </div>
                            <div class="col-sm-offset-4 col-sm-8">
                                <p><input type="checkbox" name="show_type_avarage" /> Show NECTA Format With Average</p>
                            </div>
                            <div class="col-sm-offset-4 col-sm-8">
                                <p><input type="checkbox" name="show_rank" /> Show Rank Per Each Subject</p>

                            </div>
                            <div class="col-sm-offset-4 col-sm-8">
                                <p><input type="checkbox" name="show_grade" /> Show Grade Per Each Subject</p>

                            </div>
                        </div>

                <div id="examID"></div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6 col-xs-12">
                        <input type="submit" class="btn btn-success btn-block" style="margin-bottom:0px" value="<?= "view_combined_report" ?>">
                    </div>
                </div>
                <?= csrf_field() ?>
                </form>
            </div>
        </div>
<?php
      
 if (isset($combined_exams_array) && !empty($combined_exams_array)) { ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-6 col-xs-12 col-sm-offset-3 list-group">
                <div class="list-group-item">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Class Name</th>
                                <th>Exams Combined</th>
                                <th>Selection Type</th>
                                <th>Total Students</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?= $class->classes . ' - ' . $academic->name ?></td>
                                <td><?php

                                    $exam_names = '';
                                    $exam_percentage = array();
                                    foreach ($combined_exams_array as $value) {

                                        if (isset($exam_percents[$value]) && !empty($exam_percents[$value])) {
                                            //$perc=   
                                            $percentage = '(' . ($exam_percents[$value]) . '%)';
                                            
                                        } else {
                                            $percentage = '';
                                        }

                                        $exam_names .= $value = $exam_name[$value] . $percentage . ' ,';
                                    }
                                    echo rtrim(str_replace('_', ' ', ucfirst($exam_names)), ' ,');
                                    ?></td>
                                <td><?= (int) $subjectID > 0 ? strtoupper(\collect($subjects_info)->first()->subject_name) . ' SUBJECT' : 'Exams Average' ?></td>
                                <td><?= sizeof($combine_exams) ?></td>
                            </tr> 
                        </tbody>
                    </table>
                </div>
                <?php
                //count students in this class
                $section_ids = array();
                foreach ($sections as $section) {
                    array_push($section_ids, $section->sectionID);
                }
                if (!empty($exams_title)) {
                    //find exams done
                    $id_ar = [];
                    foreach ($exams_title as $exam) {
                        array_push($id_ar, $exam->examID);
                    }
                }
                $not_in_students = \DB::table($this_schema.'.mark')->whereIn('examID', $id_ar)->where('academic_year_id', $academic_year_id)->get(['student_id']);
                $s_ids = [];
                foreach ($not_in_students as $val) {
                    array_push($s_ids, $val->student_id);
                }
                $total_students_in_class = \DB::table($this_schema.'.student_archive')->whereIn($this_schema.'.student_archive.section_id', $section_ids)->where($this_schema.'.student_archive.academic_year_id', $academic_year_id)->whereIn($this_schema.'.student_archive.student_id', $s_ids)->join($this_schema.'.student', $this_schema.'.student.student_id', $this_schema.'.student_archive.student_id')->where($this_schema.'.student.status', 1)->get();
               
                if (sizeof($total_students_in_class) != sizeof($combine_exams) && $section_id == 0) {
                    if (!empty($exams_title) ) {
                        //find exams done
                        $ids = '';
                        foreach ($exams_title as $exam) {
                            $ids .= $exam->examID . '_';
                        }
                    }
                    $number_missing = sizeof($total_students_in_class) - sizeof($combine_exams);
                ?>
                    <!--                    <div class="alert alert-warning">
                    <?= $number_missing ?> student<?= $number_missing == 1 ? '' : 's' ?> miss in the list because there are not exam results published for one or more exams done. <a href="<?= url('exam/missing/' . $classesID . '/' . $academic_year_id . '/' . rtrim($ids, '_')) ?>">Click here to see the list and resolve</a>
                                        </div>-->
                <?php } ?>
            </div>
        </div>

    <?php }
if (isset($combine_exams)) {

    ?>

        <div class="col-sm-12">
        <ul class="nav nav-tabs md-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#all" role="tab">All students</a>
                <div class="slide"></div>
            </li>
            <?php
                    foreach ($sections as $key => $section) {
                    
                    echo '<li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#' . $section->sectionID . '" role="tab"> Section - ' . " ( " . $section->section . " )" . '</a>
                            <div class="slide"></div>
                        </li>';
            }
            ?>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#summary" role="tab"><?= (int) $subjectID > 0 ? strtoupper(\collect($subjects_info)->first()->subject_name) : '' ?> Summary </a>
                <div class="slide"></div>
            </li>
        </ul>               
                <div class="tab-content">
                    <div id="all" class="tab-pane active">
                            <?php
                            /*
                             * ------------------------------------------------------------------
                             * Here we load report for the whole class
                             * 
                             * ------------------------------------------------------------------
                             * 
                             */
                            $is_percent = array_values($exam_percents);
                                
                            ?>
                <div class="dt-responsive table-responsive">
                        <table id="simpletable" class="table table-striped table-bordered nowrap">                                <thead>
                        <thead>
                                    <tr>
                                        <th class="col-sm-1"><?= 'slno' ?></th>
                                        <th class="col-sm-2"><?= 'eattendance_name' ?></th>
                                        <th class="col-sm-1"><?= 'sex' ?></th>
                                        <th class="col-sm-1"><?= 'eattendance_roll' ?></th>
                                        <th class="col-sm-1"><?= 'section' ?></th>
                                        <?php
                                        if (!empty($exams_title)) {
                                            foreach ($exams_title as $exam) {
                                                echo '<th class="col-sm-1">' . $exam->exam . '</th>';
                                            }
                                        }
                                        ?>

                                        <th class="col-sm-2">Total</th>
                                        <?php
                                        // if ((int) $is_percent[0] == 0) {
                                        ?>
                                        <th class="col-sm-1">Average <span class="col-sm-4 control-label"><a class="right"><i class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="left" data-trigger="hover" data-content="sum of exam's average devide by total number of exams" title="" data-original-title=" Average"></i>
                                                </a>
                                            </span></th>
                                        <?php //} 
                                        ?>
                                        <th class="col-sm-1">Grade</th>
                                        <th class="col-sm-1">Rank</th>
                                        <th class="col-sm-4"><?= 'action' ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($sections as $key => $section) {
                                        foreach ($allsection[$section->section] as $student_ranks) {
                                            $section_rank[$student_ranks->student_id] = $student_ranks->rank;
                                        }
                                    }
                                    ?>

                                    <?php
                                    if (sizeof($combine_exams)>0) {
                                        $i = 1;
                                        foreach ($combine_exams as $combined) {
                                            $combine = is_object($combined) ? (array) $combined : $combined;
                                    ?>
                                            <tr>
                                                <td data-title="<?= 'slno' ?>">
                                                    <?php echo $i ?>
                                                </td>

                                                <td data-title="<?= 'eattendance_name' ?>">
                                                    <a href="<?= url('/student/view/' . $combine['student_id'] . '/' . $classesID) ?>"> <?php echo $combine['name']; ?></a>
                                                </td>
                                                <td data-title="<?= 'sex' ?>">
                                                    <?php echo $combine['sex']; ?>
                                                </td>
                                                <td data-title="<?= 'eattendance_roll' ?>">
                                                    <?php echo $combine['roll']; ?>
                                                </td>
                                                <td data-title="<?= 'section' ?>">
                                                    <?php echo $combine['section']; ?>
                                                </td>
                                                <?php
                                                if (!empty($exams_title)) {
                                                    $url_ids = '';
                                                    foreach ($exams_title as $exam) {

                                                        $name = preg_replace('/[^a-z0-9]/', '', strtolower($exam->exam . $exam->examID));
                                                        $url_ids .= $exam->examID . '_';
                                                        echo (int) $combine[$name] == 0 || $combine[$name] == null ?
                                                            '<td  data-title="' . $exam->exam . '"><b href="#" style="border-bottom: dashed 1px #0088cc;"  data-toggle="modal" data-target="#idCard"  onclick="open_edit_model(' . $combine['student_id'] . ', ' . $exam->examID . ',\'' . implode('_', $combined_exams_array) . '\',' . $classesID . ')" id="Card">' . $combine[$name] . '..</b></td>' :
                                                            '<td  data-title="' . $exam->exam . '">' . $combine[$name] . '</td>';
                                                    }
                                                }
                                                ?>

                                                <td data-title="<?= 'total' ?>">
                                                    <?= $combine['total'] ?>
                                                </td>
                                                <?php
                                                // if ((int) $is_percent[0] == 0) {
                                                ?>
                                                <td data-title="<?= 'average' ?>">
                                                    <?= ((int) $is_percent[0] <> 0) ? $combine['total'] / sizeof($semesters) : $combine['average'] ?>
                                                </td>
                                                <?php //} 
                                                ?>
                                                <td>
                                                    <?php
                                                    if ((int) $is_percent[0] == 0) {
                                                        $average = $combine['average'];
                                                    } else {
                                                        $average = $combine['total'] / sizeof($semesters);
                                                    }
                                                    foreach ($grades as $grade) {
                                                        if ($grade->gradefrom <= round($average, 0) && $grade->gradeupto >= round($average, 0)) {
                                                            echo $grade->grade;
                                                        }
                                                    }
                                                    ?>
                                                </td>
                                                <td data-title="<?= 'rank' ?>">
                                                    <?= $combine['rank'] ?>
                                                </td>
                                                <td data-title="<?= 'action' ?>">
                                                    <?php
                                                    if ($subjectID != 0 || $subjectID != NULL) {
                                                    ?>
                                                        <a href="<?= url('exam/singlecombined/'.$this_schema.'/' . $combine['student_id'] . '?year_no=' . $academic_year_id . '&exam=' . rtrim($url_ids, '_') . '&exam_percent=' . urlencode(json_encode($exam_percents))) ?>&tag=all" class="btn btn-success btn-sm btn-out-dotted" data-placement="top" data-toggle="tooltip" data-original-title="View Student Report"><i class="fa fa-file"> </i> Preview</a>
                                                        <a href="<?php echo url('exam/evaluation/'.$this_schema.'/' . $combine['student_id'] . '?year_no=' . $academic_year_id . '&exam=' . rtrim($url_ids, '_') . '&exam_percent=' . urlencode(json_encode($exam_percents))) ?>&tag=all" class="btn btn-info btn-sm btn-out-dotted" data-placement="top" data-toggle="tooltip" data-original-title="View Graphical Report"><i class="fa fa-file-text-o"> </i> Graph</a>

                                                    <?php } else { ?>

                                                    <?php } ?>
                                                </td>
                                            </tr>
                                    <?php
                                            $class_rank[$combine['student_id']] = $combine['rank'];
                                            $i++;
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>

                    </div>

                    <?php foreach ($sections as $key => $section) { ?>
                        <div id="<?= $section->sectionID ?>" class="tab-pane">
                            <div id="hide-table">
                                <?php
                                /**
                                 * ----------------------------------------------------------------
                                 * 
                                 * Here we load report for specific section only
                                 * ---------------------------------------------------------------
                                 */
                                ?>
                                <table class="table table-striped table-bordered table-hover dataTable no-footer">
                                    <thead>
                                        <tr>
                                            <th class="col-sm-1"><?= 'slno' ?></th>

                                            <th class="col-sm-2"><?= 'eattendance_name' ?></th>
                                            <th class="col-sm-1"><?= 'sex' ?></th>
                                            <th class="col-sm-1"><?= 'eattendance_roll' ?></th>

                                            <?php
                                            if (!empty($exams_title)) {
                                                foreach ($exams_title as $exam) {
                                                    echo '<th class="col-sm-1">' . $exam->exam . '</th>';
                                                }
                                            }
                                            ?>

                                            <th class="col-sm-2">Total Marks</th>
                                            <th class="col-sm-2">Average <span class="col-sm-4 control-label"><a class="right"><i class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="left" data-trigger="hover" data-content="sum of exam's average devide by total number of exams" title="" data-original-title=" Average"></i></th>
                                            <th class="col-sm-1">Rank</th>
                                            <th class="col-sm-4"><?= 'action' ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($allsection[$section->section])) {
                                            $i = 1;
                                            foreach ($allsection[$section->section] as $student_info) {
                                                $student = is_object($student_info) ? (array) $student_info : $student_info;
                                        ?>
                                                <tr>
                                                    <td data-title="<?= 'slno' ?>">
                                                        <?php echo $i ?>
                                                    </td>
                                                    <td data-title="<?= 'eattendance_name' ?>">
                                                        <?php echo $student['name']; ?>
                                                    </td>
                                                    <td data-title="<?= 'sex' ?>">
                                                        <?php echo $combine['sex']; ?>
                                                    </td>
                                                    <td data-title="<?= 'eattendance_roll' ?>">
                                                        <?php echo $student['roll']; ?>
                                                    </td>
                                                    <?php
                                                    if (!empty($exams_title)) {
                                                        $url_ids = '';
                                                        foreach ($exams_title as $exam) {

                                                            $name = preg_replace('/[^a-z0-9]/', '', strtolower($exam->exam . $exam->examID));
                                                            $url_ids .= $exam->examID . '_';
                                                            echo '<td>' . $student[$name] . '</td>';
                                                        }
                                                    }
                                                    ?>

                                                    <td data-title="<?= 'total' ?>">
                                                        <?= $student['total'] ?>
                                                    </td>
                                                    <td data-title="<?= 'average' ?>">
                                                        <?= $student['average'] ?>
                                                    </td>
                                                    <td data-title="<?= 'rank' ?>">
                                                        <?= $student['rank'] ?>
                                                    </td>
                                                    <td data-title="<?= 'action' ?>">
                                                        <?php
                                                        if ($subjectID == 0 || $subjectID == NULL) {
                                                        ?>
                                                            <a href="<?= url('exam/singlecombined/' . $student['student_id'] . '?section_rank=' . $student['rank'] . '&class_rank=' . $class_rank[$student['student_id']] . '&exam=' . rtrim($url_ids, '_') . '&exam_percent=' . urlencode(json_encode($exam_percents)) . '&section_id=true&year_no=' . $academic_year_id) ?>" class="btn btn-success  btn-sm btn-out-dotted" data-placement="top" data-toggle="tooltip" data-original-title="Download Report"><i class="fa fa-file"></i>Preview</a>
                                                            <a href="<?php echo url('exam/evaluation/' . $student['student_id'] . '?year_no=' . $academic_year_id . '&exam=' . rtrim($url_ids, '_') . '&exam_percent=' . urlencode(json_encode($exam_percents))) ?>&tag=all" class="btn btn-info  btn-sm btn-out-dotted" data-placement="top" data-toggle="tooltip" data-original-title="Download Report"><i class="fa fa-file-text-o"></i> View Graph</a>

                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                        <?php
                                                $i++;
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php
                    }
                    /**
                     * ------------------------------------------------------------
                     * Here we end report by section
                     * -----------------------------------------------------------
                     */

                    ?>
                    <div id="summary" class="tab-pane">
                        <!-- top tiles -->
                        <div class="row tile_count">
                            <?php
                            $all_avg = 0;
                            $e = 0;
                            if (!empty($exams_average)) {

                                foreach ($exams_average as $exam_avg) {
                                    $all_avg += $exam_avg->average;
                                    $e++;
                                }
                            }
                            ?>
                            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                                <span class="count_top"><i class="fa fa-book"></i> Exams Average</span>
                                <?php if ($e > 0) { ?>
                                    <div class="count"><?= round($all_avg / $e, 2) ?></div>
                                <?php } else { ?>
                                    <div class="count"></div>
                                    <?php } ?>
                                    <span class="count_bottom"> </span>
                                    </div>
                                    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                                        <span class="count_top"><i class="fa fa-users"></i>Passed Students</span>
                                        <div class="count"><?= $passed_students->count ?></div>
                                        <span class="count_bottom"> </span>
                                    </div>
                                    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                                        <span class="count_top"><i class="fa fa-users"></i>Failed Students</span>
                                        <div class="count"><?= sizeof($combine_exams) - $passed_students->count ?></div>
                                        <span class="count_bottom"> </span>
                                    </div>
                                  


                            </div>
                            <div class="title_left">
                                <br /><br />
                                <h3><?= (int) $subjectID > 0 ? strtoupper(\collect($subjects_info)->first()->subject_name) : '' ?> Exam Averages</h3>
                                <div class="clearfix"></div>
                            </div>

                            <div class="row">
                                <script type="text/javascript">
                                    $(function() {
                                        $('#container').highcharts({
                                            data: {
                                                table: 'datatable'
                                            },
                                            series: [{
                                                name: '',
                                                colorByPoint: true
                                            }],
                                            chart: {
                                                type: 'column'
                                            },
                                            title: {
                                                text: '<?= (int) $subjectID > 0 ? strtoupper(\collect($subjects_info)->first()->subject_name) : '' ?> Exams Average'
                                            },
                                            yAxis: {
                                                allowDecimals: false,
                                                title: {
                                                    text: 'Average'
                                                }
                                            },
                                            legend: {
                                                enabled: false
                                            },
                                            plotOptions: {
                                                series: {
                                                    borderWidth: 0,
                                                    dataLabels: {
                                                        enabled: true,
                                                        format: ''
                                                    }
                                                }
                                            },
                                            tooltip: {
                                                formatter: function() {
                                                    return '<b>' + this.series.name + '</b><br/>' +
                                                        this.point.y + ' ' + this.point.name.toUpperCase();
                                                }
                                            }
                                        });
                                    });
                                </script>

                                <script src="<?= url('public/assets/js/highchart.js') ?>"></script>
                                <script src="<?= url('public/assets/js/exporting.js') ?>"></script>
                                <script src="<?= url('public/assets/js/data.js') ?>"></script>


                                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

                                <table id="datatable" style="display:none">
                                    <thead>
                                        <tr>
                                            <th>Exam Name</th>
                                            <th>Average</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php

                                        $exams_name = array();
                                        if (!empty($exams_average)) {
                                           
                                            foreach ($exams_average as $exam_avg) {
                                                array_push($exams_name, $exam_avg->exam);
                                        ?>
                                                <tr>
                                                    <th><?= $exam_avg->exam ?></th>
                                                    <td><?= $exam_avg->average ?></td>
                                                </tr>
                                        <?php
                                            }
                                        }

                                        ?>
                                    </tbody>
                                </table>

                            </div>
                            <hr />

                            <div class="row">
                                <div class="title_left">
                                    <br /><br />
                                    <h3 class="heading"><?= (int) $subjectID > 0 ? strtoupper(\collect($subjects_info)->first()->subject_name) : '' ?> Subjects Average</h3>
                                    <br />
                                </div>
                                <script type="text/javascript">
                                    $(function() {
                                        $('#subject_container').highcharts({
                                            title: {
                                                text: '<?= (int) $subjectID > 0 ? strtoupper(\collect($subjects_info)->first()->subject_name) : '' ?> Subjects Performance Average Per Exam',
                                                x: -20 //center
                                            },
                                            subtitle: {
                                                text: '',
                                                x: -20
                                            },
                                            xAxis: {
                                                categories: <?= json_encode($exams_name) ?>
                                            },
                                            yAxis: {
                                                title: {
                                                    text: 'Avarage  (%)'
                                                },
                                                plotLines: [{
                                                    value: 0,
                                                    width: 1,
                                                    color: '#808080'
                                                }]
                                            },
                                            tooltip: {
                                                valueSuffix: '%'
                                            },
                                            legend: {
                                                layout: 'vertical',
                                                align: 'right',
                                                verticalAlign: 'middle',
                                                borderWidth: 0
                                            },
                                            series: [
                                                <?php
                                                foreach ($subjects_info as $subject) {
                                                ?> {
                                                        name: "<?= $subject->subject_name ?>",
                                                        data: [<?php
                                                                foreach ($exams_average as $exam) {
                                                                    echo isset($subject_average[$exam->examID][$subject->subjectID][0]->average) ? $subject_average[$exam->examID][$subject->subjectID][0]->average . ',' : '0,';
                                                                }
                                                                ?>]
                                                    },
                                                <?php }
                                                ?>
                                            ]
                                        });
                                    });
                                </script>

                                <div id="subject_container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                                <hr />

                                <script type="text/javascript">
                                    $(function() {
                                        $('#container_bar_graph').highcharts({
                                            chart: {
                                                type: 'column'
                                            },
                                            title: {
                                                text: '<?= (int) $subjectID > 0 ? strtoupper(\collect($subjects_info)->first()->subject_name) : '' ?> Subjects Performance Average Per Exam'
                                            },
                                            subtitle: {
                                                text: ''
                                            },
                                            xAxis: {
                                                categories: <?= json_encode($exams_name) ?>,
                                                crosshair: true
                                            },
                                            yAxis: {
                                                min: 0,
                                                title: {
                                                    text: 'Avarage (%)'
                                                }
                                            },
                                            credits: {
                                                enabled: false
                                            },
                                            tooltip: {
                                                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                                    '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                                                footerFormat: '</table>',
                                                shared: true,
                                                useHTML: true
                                            },
                                            plotOptions: {
                                                column: {
                                                    pointPadding: 0.2,
                                                    borderWidth: 0
                                                },
                                                series: {
                                                    borderWidth: 0,
                                                    dataLabels: {
                                                        enabled: true,
                                                        format: ''
                                                    }
                                                }

                                            },
                                            series: [<?php
                                                        foreach ($subjects_info as $subject) {
                                                        ?> {
                                                        name: "<?= $subject->subject_name ?>",
                                                        data: [<?php
                                                                foreach ($exams_average as $exam) {
                                                                    echo isset($subject_average[$exam->examID][$subject->subjectID][0]->average) ? $subject_average[$exam->examID][$subject->subjectID][0]->average . ',' : '0,';
                                                                }
                                                                ?>]
                                                    },
                                                <?php }
                                                ?>
                                            ]
                                        });
                                    });
                                </script>

                                <div id="container_bar_graph" style="min-width: 310px; height: 400px; margin: 0 auto"></div>


                                <div class="x_content">
                                    <br />
                                    <br />
                                    <h3 class="heading"><?= (int) $subjectID > 0 ? strtoupper(\collect($subjects_info)->first()->subject_name) : '' ?> Exam Grades Evaluation</h3>
                                    <br />
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Exam Name</th>
                                                <?php foreach ($grades as $grade) { ?>
                                                    <th><?= $grade->grade ?></th>
                                                <?php }
                                                ?>

                                                <th>Average</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $e = 1;
                                            $avg_table = ($siteinfos->exam_avg_format == NULL || $siteinfos->exam_avg_format == 0) ? 'sum_exam_average_done' : 'sum_exam_average';
                                            $total_gpas = 0;
                                            $num_raw = 0;
                                            
                                            $points = 0;
                               
                                            if (!empty($exams_taken)) {

                                                foreach ($exams_taken as $single) {
                                            ?>
                                                    <tr>
                                                        <th scope="row"><?= $e ?></th>
                                                        <th><?= $single->exam ?></th>
                                                        <?php
                                                        $total_points = 0;
                                    
                                                        if (sizeof($grades)>0) {
                                                            foreach ($grades as $grade) {
                                                                if ((int) $subjectID > 0) {
                                                                    $point = DB::table($this_schema.'.mark')->where('examID', $single->examID)->whereBetween('mark', [$grade->gradefrom - 1, $grade->gradeupto + 1])->where('classesID', $classesID)->where('academic_year_id', $academic_year_id)->where('subjectID', $subjectID)->count(); ?>
                                                                    <th><?= DB::table($this_schema.'.mark')->where('examID', $single->examID)->whereBetween('mark', [$grade->gradefrom - 1, $grade->gradeupto + 1])->where('classesID', $classesID)->where('academic_year_id', $academic_year_id)->where('subjectID', $subjectID)->count() ?></th>
                                                                <?php
                                                                } else {
                                                                    $point = DB::table($this_schema.'.mark')->where('examID', $single->examID)->whereBetween('mark', [$grade->gradefrom - 1, $grade->gradeupto + 1])->where('classesID', $classesID)->where('academic_year_id', $academic_year_id)->count(); ?>
                                                                    <th><?= DB::table($this_schema.'.mark')->where('examID', $single->examID)->whereBetween('mark', [$grade->gradefrom - 1, $grade->gradeupto + 1])->where('classesID', $classesID)->where('academic_year_id', $academic_year_id)->count() ?></th>
                                                            <?php
                                                                }
                                                                $points = $points + (int) $point;
                                                               $total_points = $total_points + (int) ( (int) $point * (int)$grade->point);
                                                            } ?>
                                                            <td><?= round(DB::table($this_schema.'.mark')->where('examID', $single->examID)->where('classesID', $classesID)->where('academic_year_id', $academic_year_id)->avg('mark')) ?></td>

                                                            <?php
                                                          
                                                        }
                                                    }
                                                    
                                                    ?>
                                                    </tr>
                                                <?php
                                                       }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>

                </div> <!-- nav-tabs-custom -->
            </div> <!-- col-sm-12 for tab -->

        <?php
    } else {
        /**
         * -------------------------------------------------------------------
         * Here we load default table if there is no any report generated yet
         * --------------------------------------------------------------------
         */
        ?>

        <?php }
        ?>



        </div> <!-- col-sm-12 -->
    </div><!-- row -->
    </div><!-- Body -->
    </div><!-- /.box -->

    <?php if (isset($url_ids)) { ?>

    <?php } ?>
    <script>
        $('#subjectID').change(function(event) {

            var subjectID = $(this).val();
            if (subjectID === 'all') {
                $('#show_division').show();
            } else {
                $('#show_division').hide();
            }
        });

        $('#schema_name').change(function(event) {
        var schema_name = $('#schema_name').val();
        var schema_name = $(this).val();
        if (schema_name === '0') {
            $('#academic_year_id').val(0);
            $('#report_filter_div').hide();
        } else {
            $('#report_filter_div').show();
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/getClasses') ?>",
                data: "_token=" + "{{ csrf_token() }}" + "&schema_name=" + schema_name,
                dataType: "html",
                success: function(data) {
                    $('#classesID').html(data);
                }
            });
        }
    });

    $('#classesID').change(function(event) {
        var schema_name = $('#schema_name').val();
        var classesID = $(this).val();
        if (classesID === '0') {
            $('#academic_year_id').val(0);
            $('#report_filter_div').hide();
        } else {
            $('#report_filter_div').show();
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/get_academic_years') ?>",
                data: "_token=" + "{{ csrf_token() }}" + "&id=" + classesID + "&schema_name=" + schema_name,
                dataType: "html",
                success: function(data) {
                    $('#academic_year_id').html(data);
                }
            });
        }
    });

        $('#academic_year_id').change(function(event) {
            var schema_name = $('#schema_name').val();
            var classesID = $('#classesID').val();
            var academic_year_id = $(this).val();
            if (academic_year_id === '0') {
                $('#examID').val(0);
            } else {
                $.ajax({
                    type: 'POST',
                    url: "<?= url('exam/getClassExam') ?>",
                    data: "_token=" + "{{ csrf_token() }}" + "&id=" + classesID + '&year_id=' + academic_year_id  + "&schema_name=" + schema_name + '&done=1',
                    dataType: "html",
                    success: function(data) {
                        $('#examID').html(data);
                    }
                });
            }

        });
        $('#academic_year_id').change(function(event) {
            var classesID = $('#classesID').val();
            var academic_year_id = $(this).val();
            var schema_name = $('#schema_name').val();
            if (academic_year_id === '0') {
                $('#subjectID').val(0);
            } else {
                $.ajax({
                    type: 'POST',
                    url: "<?= url('exam/getSubjectByClass') ?>",
                    data: "_token=" + "{{ csrf_token() }}" + "&id=" + classesID + "&schema_name=" + schema_name + '&year_id=' + academic_year_id,
                    dataType: "html",
                    success: function(data) {
                        $('#subjectID').html(data);
                    }
                });
            }
        });
        open_edit_model = function(a, b, c, d) {
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/optimize') ?>",
                data: {
                    "student_id": a,
                    "exam_id": b,
                    'exams': c,
                    'class_id': d
                },
                dataType: "html",
                success: function(data) {
                    $('#optimize_data').html(data);
                }
            });
        }
        $('#academic_year_id').change(function(event) {
            var academic_year_id = $(this).val();
            var schema_name = $('#schema_name').val();
            var classesID = $('#classesID').val();
            if (academic_year_id === '0') {
                $('#sectionID').val(0);
            } else {
                $.ajax({
                    type: 'POST',
                    url: "<?= url('exam/sectioncall') ?>",
                    data: "_token=" + "{{ csrf_token() }}" + "&id=" + classesID + "&schema_name=" + schema_name + '&academic_year_id=' + academic_year_id,
                    dataType: "html",
                    success: function(data) {
                        if (data === '0') {
                            $('#sectionID').html('');
                            $('#section_id').html('<span class="red"><?= "no_sections" ?> <a href="<?= url('section/add') ?>" class="btn btn-primary" role="button"><?= "add_section" ?> </a> <?= "for_class" ?></span>');
                        } else {
                            $('#section_id').html('');
                            $('#sectionID').html(data);
                        }
                    }
                });
            }
        });

      
    </script>
    <!--<p align="center">This page took <?php echo (microtime(true) - LARAVEL_START) ?> seconds to render</p>-->
    <div class="modal fade bs-example-modal-lg" id="idCard" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <div id="idCardPrint">
                    <div class="modal-header">
                        Standardize Report
                    </div>
                    <div class="modal-body" id="optimize_data">


                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" style="margin-bottom:0px;" data-dismiss="modal"><?= 'close' ?></button>
                    <button type="button" class="btn btn-success" style="margin-bottom:0px;" onclick="$('#Card').html($('#total_amount').text())" data-dismiss="modal"><?= 'submit' ?></button>

                </div>
            </div>
        </div>
    </div>
    @endsection