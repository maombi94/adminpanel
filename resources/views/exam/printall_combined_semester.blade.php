<?php
ini_set('max_execution_time', 360);
/**
 * Description of print_combined
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
$usertype = session("usertype");
$times = 0;
$exams_report_setting = $report_setting = \DB::table('exam_report_settings')->where('exam_type', 'combined')->first();
?>


<html lang="en">
    <head>
        <meta charset="utf-8">
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
        <title>Report</title>

        <link href="<?php echo url('public/assets/css/print_all/print.css'); ?>" rel="stylesheet" type="text/css" media="print">
        <!--<link href="<?php echo url('public/assets/bootstrap/bootstrap.min.css'); ?>" rel="stylesheet">-->
        <link href="<?php echo url('public/assets/css/print_all/certificate.css'); ?>" rel="stylesheet" type="text/css">

        <link rel="shortcut" href="<?= url(); ?>public/assets/images/favicon.ico">
        <!-- Begin shared CSS values -->
        <style type="text/css" >
            @font-face {
                font-family: "Adobe Garamond Pro";
                /*src: url("../../public/assets/fonts/DejaVuSans_b.woff") format("woff");*/
            }
            table.table.table-striped td,table.table.table-striped th, table.table {
                border: 1px solid #000000;
                margin: 0px auto !important;
            }

            #report_footer{
                margin-left: 2em;
            }
            .headings{
                margin-left: 2em;
            }
            .school_logo{
                float: left; height: 129px; width: 15%
            }
            .head_letter_photo{
                float: right; height: 129px; width: 81%
            }
            .school_info_heading{
                float: left; text-align: center; width: 70%; height: 149px;
            }
            .student_photo{
                float: right; width: 22%; height: 129px;
            }
            @media print {
                /*.panel{*/
                /*padding-left: 10%;*/
                /*}*/
                /*table.table.table-striped td,table.table.table-striped th, table.table {*/
                /*border: 1px solid #000000 !important;*/
                /*margin: 1px auto !important;*/
                /*}*/
                /*.table-stripe {*/
                /*background-color: #dedede !important;*/
                /*border: 1px solid #000000 !important;*/
                /*}*/
                /*body, .panel {*/
                /*margin: 0;*/
                /*box-shadow: 0;*/
                /*}*/
                /*.headings{*/
                /*margin-left:2em;*/
                /*}*/
                /*#t7_1{*/
                /*width: 23em !important;*/
                /*}*/
                #wrappers{
                    page-break-before: always;
                }
                h3, h4 {
                    page-break-after: avoid;
                    /* line-height: 0; */
                }

            }
            h3, h4 {
                page-break-after: avoid;
                font-size: 15px;
                /* line-height: 16px; */
            }

        </style>

        <?php
        $days_per_semester = 0;
        $no_of_exams = 0;
        $s_c = 0;
        $count_semesters = count($semesters);
        foreach ($semesters as $semester) {
            if ($s_c == 0) {
                $semester_start_date = $semester->start_date;
            }
            $days_per_semester += $semester->study_days;
            if (isset($sem_exams[$semester->id])) {
                foreach ($sem_exams[$semester->id] as $exam) {
                    $no_of_exams++;
                }
            }
            $s_c++;
            if ($s_c == $count_semesters) {
                $semester_end_date = $semester->end_date;
            }
        }
        if ($no_of_exams > 5) {
            $font_size = '8.0px !important';
        } else if ($no_of_exams > 4 && $no_of_exams < 5) {
            $font_size = '8.0px !important';
        } else {
            $font_size = '9.0px !important';
        }
        ?>

        <?php if ($level->result_format == 'CSEE' || $level->result_format == 'ACSEE') { ?>
            <style  type="text/css" >
                .panel{
                    color: #000000 !important;

                }
                table.table.table-striped td,table.table.table-striped th, table.table {
                    font-size: <?= $font_size ?>;
                    margin-left: 10%;
                }
            </style>
        <?php } ?>
    </head>

    <body onload="window.print()">
        <?php
        //$examController = new \App\Http\Controllers\exam();
        $percent = request('percent');
        $exam_percent = (array) (json_decode($percent));
        foreach ($students as $student_info) {
            $student = (object) $student_info;
            ?>


            <div class="rows" style="font-size: 80%;">
                <div class="col-md-12">

                    <div id="wrappers" style="overflow:hidden;  height: 255mm;  border: 1px solid white; margin: 10mm auto; line-break: auto;" >

                        <section class="panel">
                            <div>
                                <?php
                                $array = array(
                                    "src" => url('storage/uploads/images/' . $siteinfos->photo),
                                    'width' => '90%',
                                    'height' => '80%',
                                    'class' => 'img-rounded',
                                );
                                if(strpos($student->photo, 'http') !== false ){
                                    $image = $student->photo;
                                }else{
                                    $image = is_file('storage/uploads/images/' . $student->photo) ? url('storage/uploads/images/' . $student->photo) : url('storage/uploads/images/defualt.png');
                                }

                                $photo = array(
                                    "src" => $image,
                                    'width' => '90%',
                                    'height' => '80%',
                                    'class' => 'img-rounded',
                                );
                                ?>
                                <div class="row">
                                    <div class="school_logo">
                                        <?php echo img($array);
                                        ?>
                                    </div>
                                    <div class="head_letter_photo">
                                        <div class="school_info_heading">
                                            <br/>
                                            <h2 style="font-weight: bolder; font-size: 20px; text-transform: uppercase;" class="text-uppercase">
                                                <?= ucfirst($siteinfos->sname) ?></h2>
                                            <h3><?= 'P.O. BOX ' . $siteinfos->box . ', ' . ucfirst($siteinfos->address) ?><br>
                                            Cell: <?= str_replace(',', ' / ', $siteinfos->phone) ?><br>
                                             Email:<?= $siteinfos->email ?><br>
                                            <?=$siteinfos->website != '' ? $data->lang->line('website') .' : '. $siteinfos->website .'</h3>' : '</h3>' ?>

                                        </div>
                                        <div class="student_photo">
                                            <?php echo img($photo);
                                            ?>
                                        </div>
                                        <div style="clear: both"></div>
                                    </div>
                                    <div style="clear: both"></div>
                                </div>
                                <hr/>
                            </div>

                            <div class="">

                                <h1 align='center' style="font-size: 17px; font-weight: bolder">
                                    <?= (!empty($exam_report) && $exam_report->name != '') ? strtoupper($exam_report->name) : ' STUDENT ACADEMIC REPORT ' ?>  (<?= strtoupper($classes->classes) . ' ' . $academic_year_name->name ?>)</h1>

                            </div>
                            <div class="panel-body profile-view-dis" style="max-height: 40px; margin-top: 0.8em; padding: 0 5% 0 14px;">
                                <div class="row">
                                    <div class="col-sm-9" style="float:left;" >
                                        <h1 style="font-weight: bolder; font-size: 14px;"> @lang('exam_report_lang.name') <?= strtoupper($student->name) ?></h1>
                                        <h4 style="margin-top: 2%;"> @lang('exam_report_lang.admission_no') <?= $student->roll ?></h4>
                                    </div>
                                    <div class="col-sm-3" style="float: right;">
                                        <h1 style="font-weight: bolder; font-size: 14px;"><?PHP
                                            echo (isset($section_id) && $section_id == '') ?
                                                    strtoupper($data->lang->line('class')) . ' ' . strtoupper($classes->classes) . '' :
                                                    strtoupper($data->lang->line('stream')) . ' ' . $student->section
                                            ?>
                                        </h1>
                                    </div>
                                    <div style="clear:both;"></div>
                                </div>

                            </div>

                            <?php
                            if (!empty($exams)) {
                                $exam_percent = request('exam_percent');
                                $percentage = json_decode($exam_percent);
                                $examController = new \App\Http\Controllers\exam();
                                ?>

                                <div class="row"  style="margin: 0 2% 2% 2%;">
                                    <?php
                                    if (!empty($exams)) {
                                        ?>
                                        <div class="col-lg-12">
                                            <table class="table table-striped table-bordered" style="font-size: 92%; padding: 0;">
                                                <thead style="background-color: #b3b3b3; font-weight: bold;">
                                                    <tr>
                                                        <th rowspan="2">@lang('exam_report_lang.subject')</th>
                                                        <?= $report_setting->show_teacher == 1 ? '<th rowspan="2">' . $data->lang->line('teacher') . '</th>' : '' ?>
                                                        <?php
                                                        foreach ($semesters as $semester) {
                                                            if (isset($sem_exams[$semester->id])) {
                                                                $add = (int) $exams_report_setting->show_subject_total_across_exams == 1 ? 2 : 1;
                                                                ?>
                                                                <th colspan="<?= count($sem_exams[$semester->id]) + $add ?>" style="text-align: center;"><?= strtoupper($semester->name) ?></th>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                        <?php if (count($semesters) > 1) { ?>
                                                            <th rowspan="2"><?= $report_setting->average_column_name ?></th>
                                                        <?php } ?>
                                                        <th rowspan="2">GD</th>
                                                        <?php if ($report_setting->show_subject_point == 1) { ?>
                                                            <th style="font-weight: bolder">points') ?></th>
                                                        <?php } ?>
                                                        <th rowspan="2">remarks') ?></th>
                                                        <!--<th rowspan="2">P/S</th>-->
                                                        <?= $report_setting->show_teacher_sign == 1 ? '<th rowspan="2">' . $data->lang->line('sign') . '</th>' : '' ?>
                                                    </tr>
                                                    <tr>
                                                        <?php
                                                        $i = 1;
                                                        $is = 1;

                                                        foreach ($semesters as $semester) {
                                                            if (isset($sem_exams[$semester->id])) {
                                                                foreach ($sem_exams[$semester->id] as $exam) {
                                                                    ?>
                                                                    <th style="font-weight: bolder"><?= strtoupper($exam->abbreviation) ?>
                                                                        <?=
                                                                        (isset($percentage->{$exam->examID}) && !empty($percentage->{$exam->examID}) && $exams_report_setting->show_percentage_on_mark_column == 1) ?
                                                                                '<small style="font-size:100%">(' . $percentage->{$exam->examID} . '%)</small>' : ''
                                                                        ?>
                                                                    </th>
                                                                    <?php
                                                                }
                                                            }
                                                            if ((int) $exams_report_setting->show_subject_total_across_exams == 1) {
                                                                echo '<th>Total</th>';
                                                            }
                                                            echo '<th>' . $report_setting->semester_average_name . '' . $i . '</th>';
                                                            $i++;
                                                            $is++;
                                                        }
                                                        ?>

                                                    </tr>
                                                </thead>

                                                <tbody style="text-align: left; font-weight:bold">
                                                    <?php
                                                    $total_marks = 0;
                                                    $total_subjects_done = 0;

                                                    $total_points_all = 0;
                                                    $total_subjects_done_ = 0;
                                                    $point_ = [];
                                                    $subject_lists = isset($subjects[$student->student_id]) ? $subjects[$student->student_id] : array();
                                                    // $total_subjects_done_ = count($subject_lists);

                                                    if (isset($subjects[$student->student_id])) {
                                                        foreach ($subjects[$student->student_id] as $subject_info) {
                                                            $subject = (object) $subject_info;
                                                            ?>
                                                            <tr>
                                                                <td style="font-weight: bolder"><?= strtoupper($subject->subject) ?></td>
                                                                <?php
                                                                $name = explode(' ', $subject->teacher);
                                                                $firstname = isset($name[0]) ? $name[0] : 'NIL';
                                                                $lastname = isset($name[count($name) - 1]) ? $name[count($name) - 1] : 'NIL';
                                                                preg_match('#^(\w+\.)?\s*([\'\�\w]+)\s+([\'\�\w]+)\s*(\w+\.?)?$#', $subject->teacher, $results);
                                                                if ($report_setting->show_teacher == 1) {
                                                                    ?>
                                                                    <td style="font-weight: bolder; font-size: 7px !important;"><?php
                                                                        if (isset($results[3])) {
                                                                            $res = $results[3];
                                                                        } else if (isset($results[2])) {
                                                                            $res = $results[2];
                                                                        } else {
                                                                            $res = 'NIL';
                                                                        }

                                                                        echo substr(ucfirst(trim($firstname)), 0, 1) . ' . ' . ucfirst(trim($res))
                                                                        ?></td>

                                                                    <?php
                                                                }
                                                                $sum_sum = 0;
                                                                $x = 1;
                                                                $formula = array();
                                                                foreach ($semesters as $semester) {
                                                                    $exam_done = 0;

                                                                    $total = 0;

                                                                    $formula[$x] = $report_setting->semester_average_name . '' . $x . '=(';
                                                                    if (isset($sem_exams[$semester->id]) && !empty($sem_exams[$semester->id])) {
                                                                        foreach ($sem_exams[$semester->id] as $exam) {

                                                                            //$mark = $examController->get_total_marks($student->student_id, $exam->examID, $classesID, $subject->subject_id);

                                                                            $mark = isset($marks[$student->student_id][$exam->examID][$subject->subject_id][0]) ?
                                                                                    (object) $marks[$student->student_id][$exam->examID][$subject->subject_id][0] : array();

                                                                            $name = str_replace(' ', '_', strtolower($exam->exam));
                                                                            if (!empty($mark)) {

                                                                                if (isset($percentage->{$exam->examID}) && !empty($percentage->{$exam->examID})) {
                                                                                    $times = $percentage->{$exam->examID} / 100;
                                                                                    $total += !empty($mark) ? (float) $mark->total_mark * $times : 0;
                                                                                    $form_times = $exams_report_setting->show_percentage_on_mark_column == 1 ? '' : '*' . $times;
                                                                                    $formula[$x] .= $x <= count($exams) ? ' ' . $exam->abbreviation . $form_times . ' +' : "";
                                                                                } else {
                                                                                    $times = 1;

                                                                                    $total += !empty($mark) ? (float) $mark->total_mark : 0;
                                                                                    $x <= count($exams) ? $formula[$x] .= $exam->abbreviation . '+' : '';
                                                                                }
                                                                            } else {
                                                                                $formula[$x] .= $exam->abbreviation . '+';
                                                                            }
                                                                            /*
                                                                             * check penalty for that subject
                                                                             */
                                                                            if ($subject->is_penalty == 1) {
                                                                                if (!empty($mark)) {
                                                                                    $penalty = $mark->total_mark < $subject->pass_mark ? 1 : 0;
                                                                                }
                                                                            }
                                                                            ?>
                                                                            <td style="font-size: 9px !important;
                                                                                padding: 2px;     text-align: center; "><?php
                                                                                if (!empty($mark)) {
                                                                                    if ($mark->total_mark == 0.1) {

                                                                                        echo 0;
                                                                                    } else {

                                                                                        if ($mark->total_mark == '') {
                                                                                            echo $empty_mark;
                                                                                        } else {

                                                                                            echo (isset($percentage->{$exam->examID}) && !empty($percentage->{$exam->examID}) && $exams_report_setting->show_percentage_on_mark_column == 1) ? $mark->total_mark * $percentage->{$exam->examID} / 100 : $mark->total_mark;
                                                                                        }
                                                                                    }
                                                                                }else{
                                                                                    echo $empty_mark;
                                                                                }
                                                                                ?></td>

                                                                            <?php
                                                                            if (!empty($mark)) {
                                                                                // if ($mark->total_mark == '') {
//later we will change this condition to match exactly case for student who do the subject
                                                                                // } else {
                                                                                // $exam_done++;
                                                                                // }
                                                                            }
                                                                            $exam_done++;
                                                                        }
                                                                    }

                                                                    if ($exam_done != 0 && !empty($mark)) {

                                                                        $avg = $times == 1 ? round($total / $exam_done, 1) : round($total, 1);
                                                                    } else {
                                                                        $avg = 0;
                                                                    }
                                                                    if (isset($formula[$x])) {
                                                                        $formula[$x] = $times == 1 ? '' . rtrim($formula[$x], '+') . ' ) / ' . $exam_done : rtrim($formula[$x], '+') . ')';
                                                                    }
                                                                    $x++;
                                                                    if ((int) $exams_report_setting->show_subject_total_across_exams == 1) {
                                                                        echo '<td>' . $total . '</td>'; //total for subject across exams
                                                                    }
                                                                    echo $avg == 0 ? '<td></td>' :
                                                                            '<td style="font-size: 11px !important;padding: 2px; text-align: center; font-weight: bold;">' . trim($avg) . '</td>';
                                                                    $sum_sum += (float) $avg;
                                                                }
                                                                ?>
                                                                <?php
                                                                if (count($semesters) > 1) {
                                                                    $avg_more_semesters = round($sum_sum / count($semesters), 1);
                                                                    ?>
                                                                    <td style="font-size: 11px !important; font-weight: bold;
                                                                        padding: 2px;"><?= $avg_more_semesters ?></td>
                                                                    <?php } ?>


                                                                <?php
                                                                if ($avg == 0) {
                                                                    //this is a short time solution, we put empty but
                                                                    //later on we need to see if this student takes this subject or not
                                                                    echo '<td></td><td></td>';
                                                                    if ($report_setting->show_subject_point == 1) {
                                                                        echo '<td></td>';
                                                                    }
                                                                } else {
                                                                    $total_subjects_done_++;
                                                                    $avg_mark = count($semesters) > 1 ? $avg_more_semesters : $avg;
                                                                    if ($subject->is_counted_indivision == 1) {
                                                                        foreach ($grades as $grade) {
                                                                            if ($grade->gradefrom <= round($avg_mark, 0) && $grade->gradeupto >= round($avg_mark, 0)) {
                                                                                $sub = [
                                                                                    'subject_mark' => $avg_mark,
                                                                                    'point' => $grade->point,
                                                                                    'penalty' => $subject->is_penalty,
                                                                                    'pass_mark' => $subject->pass_mark,
                                                                                    'is_counted_indivision' => $subject->is_counted_indivision
                                                                                ];
                                                                                array_push($point_, $sub);
                                                                                $total_points_all = $total_points_all + (int) return_grade_point($grades, round($avg_mark, 0))['point'];
                                                                            }
                                                                        }
                                                                    }
                                                                    echo return_grade($grades, round($avg_mark, 0), $report_setting->show_subject_point);
                                                                    $total_subjects_done++;
                                                                }
                                                                $total_marks += count($semesters) > 1 ? $avg_more_semesters : $avg;
                                                                //last print
                                                                ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <!--					    <td>
                                                                <?php
                                                                /**
                                                                 * Find position of this student per subject per those combinations
                                                                 */
                                                                ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </td>-->
                                                                <?php if ($report_setting->show_teacher_sign == 1) { ?>
                                                                    <td>
                                                                        <?php
                                                                        if ($avg != 0) {
                                                                            if ($subject->signature == NULL) {

                                                                                $tname = isset($results[3][0]) ? $results[3][0] : '';
                                                                                echo strtoupper($firstname[0] . '.' . $tname);
                                                                            } else {
                                                                                ?>
                                                                                <img src="<?= $subject->signature ?>" width="35"
                                                                                     height="10">
                                                                                     <?php
                                                                                 }
                                                                             }
                                                                             ?>
                                                                    </td>
                                                                <?php } ?>
                                                            </tr>
                                                            <?php
                                                            is_float($avg) ? $total_subjects_done++ : 0;
                                                        }
                                                    }
                                                    ?>



                                                    <tr>
                                                        <td style="font-weight: bolder">exam_avg') ?></td>
                                                        <?= $report_setting->show_teacher == 1 ? '<td></td>' : '' ?>

                                                        <?php
                                                        //$sectionID = $section == 'CLASS' ? NULL : $student->section_id;
                                                        $exam_avg_sum = 0;
                                                        $exam_count = 0;
                                                        foreach ($semesters as $semester) {

                                                            foreach ($sem_exams[$semester->id] as $exam) {

                                                                $examname = preg_replace('/[^a-z0-9]/', '', strtolower($exam->exam . $exam->examID));

                                                                if (isset($percentage->{$exam->examID}) && !empty($percentage->{$exam->examID})) {
                                                                    $times = $percentage->{$exam->examID} / 100;
                                                                    $exam_avg_full = $student->{$examname} / $times;
                                                                } else {
                                                                    $exam_avg_full = $student->{$examname};
                                                                }

                                                                $exam_avg_sum += $student->{$examname};
                                                                $exam_count++;
                                                                ?>
                                                                <td><?= (isset($percentage->{$exam->examID}) && !empty($percentage->{$exam->examID}) && $exams_report_setting->show_percentage_on_mark_column == 1 ) ? $exam_avg_full * $percentage->{$exam->examID} / 100 : $exam_avg_full ?></td>
                                                                <?php
                                                            }
                                                            if ((int) $exams_report_setting->show_subject_total_across_exams == 1) {
                                                                echo '<td></td>'; //total for subject across exams
                                                            }
                                                            echo "<td></td>";
                                                        }
                                                        ?>
                                                        <td></td>
                                                        <td></td>
                                                        <?= $report_setting->show_subject_point == 1 ? '<td></td>' : '' ?>
                                                        <?php if (count($semesters) > 1) { ?>
                                                            <td></td>
                                                        <?php } ?>
                                                        <?= $report_setting->show_teacher_sign == 1 ? '<td></td>' : '' ?>
                                                    </tr>

                                                    <?php
                                                    /**
                                                     * 
                                                     * 
                                                     * ----------------------------------------------------------------
                                                     * 
                                                     * We show Division & points only to A-level students. Those with
                                                     * grade format of ACSEE
                                                     */
                                                    if (($report_setting->show_acsee_division == 1 && $level->result_format == 'ACSEE') || ($level->result_format == 'CSEE' && $report_setting->show_csee_division == 1)) {
                                                        if ($report_setting->show_overall_division == 1 && !in_array(strtoupper($level->result_format), ['PSLE', 'OTHER'])) {
                                                            ?>
                                                            <tr>
                                                                <td style="font-weight: bolder">DIVISION</td>

                                                                <?= $report_setting->show_teacher == 1 ? '<td></td>' : '' ?>

                                                                <?php
                                                                $semester_ids = 0;
                                                                $i = 0;
                                                                $total_points = 0;
                                                                $division = array();
                                                                foreach ($semesters as $semester) {
                                                                    if (isset($sem_exams[$semester->id])) {
                                                                        foreach ($sem_exams[$semester->id] as $exam) {
                                                                            $division[$i] = (new \app\Http\Controllers\exam())->getDivision($student->student_id, $exam->examID, $classesID);
                                                                            ?>
                                                                            <td><?= $report_setting->show_division_by_exam == 1 ? $division[$i][0] : '' ?></td>
                                                                            <?php
                                                                            $total_points += (int)$division[$i][1];
                                                                            $i++;
                                                                        }
                                                                    }
                                                                    if ((int) $exams_report_setting->show_subject_total_across_exams == 1) {
                                                                        echo '<td></td>'; //total for subject across exams
                                                                    }
                                                                    echo "<td></td>";
                                                                    $semester_ids++;
                                                                }
                                                                $total_i = $i;
                                                                $total_average_points = floor($total_points / $total_i);
                                                                $penalty = isset($penalty) ? $penalty : null;
                                                                if ($level->result_format == 'CSEE') {
                                                                    $div = cseeDivision($total_points_all, $penalty);
                                                                } elseif ($level->result_format == 'ACSEE') {
                                                                    $div = acseeDivision($total_points_all, $penalty);
                                                                }
                                                                $div = getDivisionBySort($point_, $level->result_format);
                                                                ?>
                                                                <td><?= $div[0] ?></td>
                                                                <td></td>
                                                                <?= $report_setting->show_subject_point == 1 ? '<td></td>' : '' ?>
                                                                <?php if (count($semesters) > 1) { ?>
                                                                    <td></td>
                                                                <?php } ?>

                                                                <?= $report_setting->show_teacher_sign == 1 ? '<td></td>' : '' ?>
                                                            </tr>





                                                            <tr>
                                                                <td style="font-weight: bolder">POINTS</td>
                                                                <?= $report_setting->show_teacher == 1 ? '<td></td>' : '' ?>

                                                                <?php
                                                                $semester_ids = 0;
                                                                $i = 0;
                                                                $total_points = 0;
                                                                foreach ($semesters as $semester) {
                                                                    if (isset($sem_exams[$semester->id])) {
                                                                        foreach ($sem_exams[$semester->id] as $exam) {
                                                                            ?>
                                                                            <td><?= $report_setting->show_division_by_exam == 1 ? (int)$division[$i][1] : '' ?></td>
                                                                            <?php
                                                                            $i++;
                                                                        }
                                                                    }
                                                                    if ((int) $exams_report_setting->show_subject_total_across_exams == 1) {
                                                                        echo '<td></td>'; //total for subject across exams
                                                                    }
                                                                    echo "<td></td>";
                                                                    $semester_ids++;
                                                                }
                                                                ?>
                                                                <?= $report_setting->show_subject_point == 1 ? '<td>' . $div[1] . '</td>' : '' ?>

                                                                <td></td>
                                                                <?php if (count($semesters) > 1) { ?>
                                                                    <td style="font-weight: bolder"></td>
                                                                <?php } ?>
                                                                <td></td>
                                                                <?= $report_setting->show_teacher_sign == 1 ? '<td></td>' : '' ?>

                                                            </tr>

                                                            <?php
                                                        }
                                                    }

                                                    /**
                                                     * Otherwise, we don't show points and division
                                                     * ------------------------------------------------------
                                                     */
                                                    $section = isset($section_id) && $section_id == '' ? 'CLASS' : 'STREAM';
                                                    $sectionID = $section == 'CLASS' ? NULL : $sectionID;
                                                    if ($report_setting->show_pos_in_stream == 1) {
                                                        ?>
                                                        <tr>
                                                            <td style="font-weight: bolder">P/S</td>
                                                            <?php echo $report_setting->show_teacher == 1 ? '<td></td>' : '' ?>
                                                            <?php
                                                            foreach ($semesters as $semester) {
                                                                if (isset($sem_exams[$semester->id])) {
                                                                    foreach ($sem_exams[$semester->id] as $exam) {

                                                                        $rank_info = $examController->get_rank_per_all_subjects($exam->examID, $classesID, $student->student_id, $sectionID);
                                                                        ?>
                                                                        <td><?= !empty($rank_info) ? $rank_info->rank : '' ?></td>
                                                                        <?php
                                                                    }
                                                                }
                                                                if ((int) $exams_report_setting->show_subject_total_across_exams == 1) {
                                                                    echo '<td></td>'; //total for subject across exams
                                                                }
                                                                if (count($semesters) < 2) {
                                                                    echo '<td style="font-size: 13px !important;
                                                                    padding: 2px;     text-align: center">' . $student->rank . '</td>';
                                                                } else {
                                                                    echo '<td></td>';
                                                                }
                                                            }
                                                            ?>
                                                            <?php if (count($semesters) > 1) { ?>
                                                                <td><?= $student->rank ?></td>
                                                            <?php } ?>
                                                            <?= $level->result_format == 'ACSEE' ? "" : '' ?>
                                                            <td></td>
                                                            <td></td>
                                                            {{-- <td></td> --}}
                                                            <?php // $report_setting->show_teacher == 1 ? '<td></td>' : '' ?>
                                                        </tr> 
                                                    <?php } ?>
                                                </tbody>
                                            </table>

                                            <?php
                                            if ($exams_report_setting->show_average_row == 1) {
                                                ?>
                                                <table class="table table-striped table-bordered" style=" width: 100%; margin: 0%;">
                                                    <thead>
                                                        <tr>
                                                            <?php
                                                        }
                                                        $avg_name = count($semesters) == 1 ? $report_setting->semester_average_name . '' . ($is - 1) : $report_setting->average_column_name;
                                                        ?>
                                                        <?php
                                                        if ($exams_report_setting->show_average_row == 1) {
                                                            ?>
                                                            <th style="font-weight: bolder; width:25.4%;">total_of') . ' ' . $avg_name ?>:
                                                                <strong><?= $total_marks ?></strong></th>
                                                        <?php } ?>
                                                        <th style="font-weight: bolder;width:33.4%;">
                                                            <?php
                                                            if ($exams_report_setting->show_average_row == 1) {
                                                                ?>
                                                                <?= count($semesters) == 1 ? $report_setting->single_semester_avg_name : $report_setting->overall_semester_avg_name ?>: <?php } ?>
                                                            <strong><?php
                                                                //avoid division by 0 in case
                                                                $tt = $total_subjects_done_ == 0 ? 1 : $total_subjects_done_;
                                                                $percent = json_decode(request('exam_percent'));
                                                                $exams_info = explode('_', request('exam'));

                                                                if (isset($percent->{$exams_info[0]}) && $percent->{$exams_info[0]} > 1) {
                                                                    $total_average = round($exam_avg_sum / count($semesters), 1);
                                                                } else {
                                                                    $total_average = round($exam_avg_sum / $exam_count, 1);
                                                                }
                                                                if ($exams_report_setting->show_average_row == 1) {
                                                                    echo $total_average;
                                                                }
                                                                foreach ($grades as $grade) {

                                                                    if ($report_setting->show_overall_grade == 1 && $grade->gradefrom <= round($total_average, 0) && $grade->gradeupto >= round($total_average, 0))
                                                                        echo " ($grade->grade)";
                                                                }
                                                                ?></strong></th>
                                                        <?php if (isset($class_rank) && $exams_report_setting->show_pos_in_section == 1) { ?>
                                                            <th style="font-weight: bolder; font-size: 13px !important; padding: 2px;   text-align: center">P/S:
                                                                <strong><?= $student->rank ?></strong> out of <?= $students_per_section ?>
                                                            </th>
                                                        <?php } if (isset($class_rank) && $exams_report_setting->show_pos_in_class == 1) { ?>
                                                            <th style="font-weight: bolder; font-size: 13px !important; padding: 2px;  text-align: center">P/C:
                                                                <strong><?= $class_rank[$student->student_id] ?></strong> out of <?= $students_per_class ?>
                                                            </th>
                                                        <?php } ?>
            <!--<th style="font-weight: bolder">Points</th>-->
            <!--								<th style="font-weight: bolder">Division</th>-->

                                                        <?php
                                                        if ($exams_report_setting->show_average_row == 1) {
                                                            ?>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                </table>
                                            <?php } ?>
                                            <div style="float: left; width: 56%">
                                                <table class="table table-striped table-bordered" style="float: left; margin: 0 0 0 0; text-align:left;">
                                                    <thead>
                                                        <tr>
                                                            <th style="font-weight: bolder; text-align: center; border-bottom: 1px solid #fff;">
                                                                GRADING
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                </table>

                                                <table class="table table-striped table-bordered text-left" style=" margin-bottom: 0; text-align:left; font-size:74%;">
                                                    <thead>
                                                        <tr>
                                                            <?php foreach ($grades as $grade) { ?>
                                                                <th style="font-weight: bolder; text-align: left;"><?= $grade->grade ?><?= $level->result_format != 'ACSEE' ? ' (' . $grade->note . ')' : '' ?>
                                                                </th>
                                                            <?php } ?>
                                                        </tr>
                                                    </thead>
                                                    <tbody style="text-align:left; font-size: 13px;">
                                                        <tr>
                                                            <?php foreach ($grades as $grade) { ?>
                                                                <td style="font-size: 13px; text-align: left; background-color: white;color: #000;"><?= $grade->gradefrom ?>-<?= $grade->gradeupto ?>
                                                                </td>
                                                            <?php } ?>
                                                        </tr>
                                                    </tbody>

                                                </table>
                                            </div>

                                            <div style="float: right; width: 43.6%">
                                                <table class="table table-striped table-bordered" style=" margin: 0 0 0 0;">
                                                    <thead>
                                                        <tr>
                                                            <th style="font-weight: bold; "><strong>KEY:</strong>
                                                                <?php foreach ($exams as $exam) { ?>
                                                                    <span>
                                                                        <?= strtoupper($exam->abbreviation) ?>=<?= $exam->exam ?></span>,
                                                                    <?php
                                                                }
                                                                if ($report_setting->show_pos_in_section == 1) {
                                                                    echo 'P/S=Pos in Stream, ';
                                                                }
                                                                if ($report_setting->show_pos_in_class == 1) {
                                                                    echo 'P/C=Pos in Class';
                                                                }
                                                                if (isset($div[0]) && $div[0] == 'INC') {
                                                                    echo 'INC= Incomplete Division';
                                                                }
                                                                ?>, GD=Grade
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                <?php    if(set_schema_name() !='rosmini.'){ ?>
                                                    <tbody>
                                                        <tr>
                                                            <td style="background-color: white;color: #000;">
                                                                <div class="row">
                                                                    <div class="col-lg-3"><strong> FORMULAS:</strong></div>
                                                                    <div class="col-lg-9">
                                                                        <span style=""><?php
                                                                            $v = '';
                                                                            foreach ($formula as $val) {
                                                                                $v .= $val . ',';
                                                                            }
                                                                            echo rtrim($v, ',');
                                                                            ?></span>

                                                                        <?php
                                                                        if (count($semesters) > 1) {
                                                                            $t = '(';
                                                                            $i = 1;
                                                                            foreach ($semesters as $semester) {
                                                                                $t .= $report_setting->semester_average_name . '' . $i . '  +';
                                                                                $i++;
                                                                            }
                                                                            $avg_formula = rtrim($t, '+') . ' ) / ' . count($semesters);
                                                                            ?>
                                                                            <span style="">, <?= $report_setting->average_column_name ?>=<?= rtrim($avg_formula, ',') ?></span>
                                                                        <?php } ?>

                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                        <?php } ?>
                                                </table>
                                            </div>

                                            <?php
                                            /**
                                             * 
                                             * ----------------------------------
                                             * 
                                             * student attendance
                                             * --------------------------------
                                             */
                                            if ($exams_report_setting->show_student_attendance == 1) {
                                                $att = \App\Model\Student::find($student->student_id);

                                                $days = $att->sattendances()->where('present', 1)->where('date', '>=', date('Y-m-d', strtotime($semester_start_date)))->where('date', '<=', date('Y-m-d', strtotime($semester_end_date)))->count();
                                                ?>
                                                <div class="">
                                                    <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                                        <thead>
                                                            <tr>
                                                                <th style="text-align: left;">
                                                                    <b style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px">	attendance') ?></b>
                                                                </th>
                                                                <th style="text-align: left;">
                                                                    <?= $days ?> out of <?= $days_per_semester ?> days
                                                                </th>
                                                            </tr>

                                                        </thead>
                                                    </table>
                                                </div>


                                                <?php
                                            }

                                            /**
                                             * 
                                             * -----------------------------------
                                             * end of attendance
                                             * ---------------------------------
                                             */
                                            /**
                                             * 
                                             * ----------------------------------------------
                                             * 
                                             * Report footer
                                             * ----------------------------------------------
                                             * 
                                             * 
                                             * 
                                             */
                                            if ($exams_report_setting->show_all_signature_on_the_footer == 0) {
                                                ?>

                                                <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                                    <thead>

                                                        <tr>
                                                        <th style="width:56%; text-align: left; vertical-align: top;">
                                                            <b style="vertical-align: top; font-style: italic; font-size:11px"><?= $exams_report_setting->class_teacher_remark ?>:</b><br>

                                                                <?php
                                                                if (isset($character[$student->student_id]) && !empty($character[$student->student_id]) && $character[$student->student_id]->class_teacher_comment != '') {
                                                                    echo "<span class='bolder' style='font-size:11px'>&nbsp;";
                                                                    echo $character[$student->student_id]->class_teacher_comment;
                                                                    echo "</span>";
                                                                } else {
                                                                    foreach ($grades as $grade) {
                                                                        if ($grade->gradefrom <= round($total_average, 0) && $grade->gradeupto >= round($total_average, 0)) {
                                                                            echo "<span class='bolder' style='font-size:11px'>&nbsp;";
                                                                            echo $grade->overall_academic_note;
                                                                            echo "</span>";
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                                ?></th>

                                                            <th style="font-weight: bolder; width:56%; text-align: left;">
                                                                <?php if ($report_setting->show_classteacher_name == 1) { ?>
                                                                    <?= strtoupper($data->lang->line('class_teacher')) ?> <?= $class_teacher_signature->name ?>
                                                                    <br>
                                                                    <?php
                                                                    if ($report_setting->show_classteacher_phone == 1) {
                                                                        echo 'Phone: <u> ' . $class_teacher_signature->phone . '</u><br>';
                                                                    }
                                                                    ?>
                                                                    <?php
                                                                    if ($class_teacher_signature->signature == NULL) {


                                                                        echo strtoupper($class_teacher_signature->name[0] . '.' . str_after($class_teacher_signature->name, ' ')[0]);
                                                                    } else {
                                                                        ?>
                                                                        <img src="<?= $class_teacher_signature->signature ?>" width="54"
                                                                             height="32">
                                                                             <?php
                                                                         }
                                                                     } else {
                                                                         ?>
                                                                    Class Teacher's Signature<br><img src="<?= $class_teacher_signature->signature ?>" width="54" height="32">
                                                                    <?php
                                                                }
                                                                ?>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                </table>

                                                <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 56%; text-align: left"><b style="vertical-align: top; font-style: italic; font-size:10px "><?= $exams_report_setting->head_teacher_remark ?>:</b><br> <?php
                                                                if (!empty($character[$student->student_id]) && isset($character[$student->student_id]) && $character[$student->student_id] != '') {
                                                                    echo "<span class='bolder' style='font-size:13px'>";
                                                                    echo $character[$student->student_id]->head_teacher_comment;
                                                                    echo "</span>";
                                                                } else {
                                                                    foreach ($grades as $grade) {
                                                                        if ($grade->gradefrom <= round($total_average, 0) && $grade->gradeupto >= round($total_average, 0)) {
                                                                            echo "<span class='bolder' style='font-size:13px'>";
                                                                            echo $grade->overall_note;
                                                                            echo "</span>";
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                                ?> <?php
                                                                if (set_schema_name() == 'enaboishu.') {
                                                                    ?>
                                                                    <p>
                                                                    <hr/>
                                                                    <b  style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px">Head Master comment: </b>Work Hard
                                                                    </p>
                                                                <?php } ?></th>
                                                            <th style="width: 50%; text-align: left"><?= $siteinfos->headname ?>'s<br/> Signature<br/></th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                                <?php
                                                /**
                                                 * 
                                                 * ----------------------------------------------
                                                 * 
                                                 * End of Report footer with buttom signature
                                                 * ----------------------------------------------
                                                 * 
                                                 * 
                                                 * 
                                                 */
                                            } else {
                                                ?>
                                                <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                                    <thead>
                                                        <tr>
                                                            <th style=" padding:0;"><br/><b style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px">&nbsp;<?= $exams_report_setting->class_teacher_remark ?>:</b><br> <?php
                                                                if (isset($character[$student->student_id]) && !empty($character[$student->student_id]) && $character[$student->student_id]->class_teacher_comment != '') {
                                                                    echo "<span class='' style='font-size:11px'>&nbsp;";
                                                                    echo $character[$student->student_id]->class_teacher_comment;
                                                                    echo "</span>";
                                                                } else {
                                                                    foreach ($grades as $grade) {
                                                                        if ($grade->gradefrom <= round($total_average, 0) && $grade->gradeupto >= round($total_average, 0)) {
                                                                            echo "<span class='' style='font-size:11px'>&nbsp;";
                                                                            echo $grade->overall_academic_note;
                                                                            echo "</span>";
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                                ?></th>

                                                        </tr>
                                                        <tr>

                                                            <th style=" text-align: left"><b style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px"><?= $exams_report_setting->head_teacher_remark ?>:</b><br> <?php
                                                                if (!empty($character[$student->student_id]) && isset($character[$student->student_id]) && $character[$student->student_id] != '') {
                                                                    echo "<span class='bolder' style='font-size:13px'>";
                                                                    echo $character[$student->student_id]->head_teacher_comment;
                                                                    echo "</span>";
                                                                } else {
                                                                    foreach ($grades as $grade) {
                                                                        if ($grade->gradefrom <= round($total_average, 0) && $grade->gradeupto >= round($total_average, 0)) {
                                                                            echo "<span class='bolder' style='font-size:13px'>";
                                                                            echo $grade->overall_note;
                                                                            echo "</span>";
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                                ?> <?php
                                                                if (set_schema_name() == 'enaboishu.') {
                                                                    ?>
                                                                    <p>
                                                                    <hr/>
                                                                    <b  style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px">Head Master comment: </b>Work Hard
                                                                    </p>
                                                                <?php } ?></th>


                                                        </tr>

                                                    </thead>
                                                </table>
                                                <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                <?php if ($report_setting->show_classteacher_name == 1) { ?>
                                                                    Class Teacher: <?= $class_teacher_signature->name ?>
                                                                    <br>

                                                                    <?php
                                                                    if ($report_setting->show_classteacher_phone == 1) {
                                                                        echo 'Phone: <u> ' . $class_teacher_signature->phone . '</u><br>';
                                                                    }
                                                                    if ($class_teacher_signature->signature == NULL) {


                                                                        echo strtoupper($class_teacher_signature->name[0] . '.' . str_after($class_teacher_signature->name, ' ')[0]);
                                                                    } else {
                                                                        ?>
                                                                        <img src="<?= $class_teacher_signature->signature ?>" width="54"
                                                                             height="32">
                                                                             <?php
                                                                         }
                                                                     } else {
                                                                         ?>
                                                                    Class Teacher's Signature<br><img src="<?= $class_teacher_signature->signature ?>" width="54" height="32">
                                                                    <?php
                                                                }
                                                                ?>
                                                            </th>
                                                            <th style="width: 50%; text-align: left"><?= $siteinfos->headname ?>'s<br/> Signature<br/></th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                                <?php
                                                /**
                                                 * 
                                                 * -----------------------------------------
                                                 * 
                                                 * End of footer with both signature at the bottom
                                                 * ------------------------------------------
                                                 */
                                            }
                                            ?>

                                            <div style="padding-left:5%;">

                                                <div style="">

                                                    <?php
                                                    $path = "storage/uploads/images/" . $level->stamp . "?v=1";
                                                    ?>
                                                    <?php
                                                    $array = array(
                                                        "src" => $path,
                                                        'width' => '120em',
                                                        'height' => '120em',
                                                        'class' => 'img-rounded',
                                                        'id' => "signature_stamp_preview",
                                                        'style' => 'position:relative; margin:-16% 10% 0 0; float:right;'
                                                    );
                                                    echo img($array);
                                                    ?>

                                                </div>
                                            </div>
                                            <?php if (isset($report_setting->show_parent_comment) && $report_setting->show_parent_comment == 1) { ?>
                                                <div style="float: left; width: 100%">
                                                    <table class="table table-striped table-bordered" style="  float: left; margin: -13% 0 0 0;">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="4" style="font-weight: bolder; text-align: left;">
                                                                    Parent Comment:
                                                                    <br><br><br><br><br><br>

                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th colspan="2">
                                                                    <u> Parent Name</u>
                                                                    <br> <br>....................................................................................
                                                                </th>
                                                                <th>
                                                                    <u> Signature</u>
                                                                    <br> <br>.....................................
                                                                </th>
                                                                <th>
                                                                    <u> Date</u>
                                                                    <br> <br> ...../...../...........
                                                                </th>

                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                            <?php
                                            }
                                            
                                            if (!empty($exam_report) && date('Y', strtotime($exam_report->reporting_date)) <> 1970) {
                                                ?>

                                                <table class="table table-striped table-bordered" style=" margin: 0 0 0 0;">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align: center; font-weight: bold; font-size: 11px !important;">
                                                                <?php
                                                                if (date('Y', strtotime($exam_report->reporting_date)) <> 1970) {
                                                                    ?>
                                                                    <b><?php
                                                                        echo "Date of Reporting: " . date('jS M Y', strtotime($exam_report->reporting_date));
                                                                    }
                                                                    ?> </b>

                                                            </th>

                                                        </tr>
                                                    </thead>
                                                </table>
                                            <?php }  ?>	

                                        </div>
                                    <?php } ?>
                                    <div style="margin-left: 3%; margin-top: 2%; font-size: 110%; line-height: 21px;">

                                        <div style="padding-left:5%;">

                                            <div style="z-index: 4000">
                                                <div style="float: right; margin-right: 4%; margin-top: 4%;"></div>
                                                <div>


                                                <?php } ?>

                                                </section>

                                            </div>


                                        </div>
                                    </div>
                                    <?php
                                }
                                /*      $html = ob_get_clean();
                                  $dompdf = new DOMPDF();
                                  $dompdf->load_html($html);
                                  $dompdf->render();
                                  $dompdf->stream('output.pdf'); */
                                ?>
                                </body>
                                </html>
                                 <!--<p align="center">Page rendered in <?php // echo (microtime(true) - LARAVEL_START)                   ?> seconds.</p>--> 
