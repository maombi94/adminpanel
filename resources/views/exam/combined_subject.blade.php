@extends('layouts.app')

@section('content')
<?php $root = url('/public/'); ?>
<!-- Page-body start -->
<div class="page-body">
    <div class="row">
        <!-- Server Side Processing table start -->
        <div class="col-sm-12">
            <!-- Basic Inputs Validation start -->
            <div class="card">
                <div class="card-header">
                    <h5>Basic Inputs Validation</h5>
                    <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                </div>
                <div class="card-block">
        <div class="row">
            <div class="col-sm-12">
            <form id="main" method="post" action="">
                    @csrf
                <?php
                $usertype = session("usertype");
                ?>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Select School</label>
                            <div class="col-sm-10">
                            <select name="schema_name" id="schema_name" class="form-control form-control-danger">
                                <option value="opt1">Select One School</option>
                                @if (count(load_schemas()) > 0)
                                @foreach (load_schemas() as $schema)
                                    <option value="{{ $schema->username }}"> {{ $schema->username }}</option>
                                @endforeach
                                @endif
                               
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Select Class</label>
                            <div class="col-sm-10">
                            <select name="classesID" id="classesID" class="form-control form-control-info">
                                <option value="0">Select One Value Only</option>
                               
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Select Academic Year</label>
                            <div class="col-sm-10">
                            <select name="academic_year_id" id="academic_year_id" class="form-control form-control-primary">
                               
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Select class section</label>
                            <div class="col-sm-10">
                            <select name="sectionID" id="sectionID" class="form-control form-control-primary">
                               
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Select Subject</label>
                            <div class="col-sm-10">
                            <select name="subjectID" id="subjectID" class="form-control form-control-primary">
                               
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>

                        <div class="form-group" id="show_division" style="display:none">
                            <div class="col-sm-offset-4 col-sm-8">
                                <p><input type="checkbox" name="combine_subject" /> Do you want to combine similar subject?</p>
                            </div> 
                            <div class="col-sm-offset-4 col-sm-8">
                                <p><input type="checkbox" name="show_division" /> Show Division</p>
                            </div>
                            <div class="col-sm-offset-4 col-sm-8">
                                <p><input type="checkbox" name="show_type" /> Show NECTA Format with Grades</p>
                            </div>
                            <div class="col-sm-offset-4 col-sm-8">
                                <p><input type="checkbox" name="show_type_avarage" /> Show NECTA Format With Average</p>
                            </div>
                            <div class="col-sm-offset-4 col-sm-8">
                                <p><input type="checkbox" name="show_rank" /> Show Rank Per Each Subject</p>

                            </div>
                            <div class="col-sm-offset-4 col-sm-8">
                                <p><input type="checkbox" name="show_grade" /> Show Grade Per Each Subject</p>

                            </div>
                        </div>

                <div id="examID"></div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6 col-xs-12">
                        <input type="submit" class="btn btn-success btn-block" style="margin-bottom:0px" value="<?= "view_combined_report" ?>">
                    </div>
                </div>
                <?= csrf_field() ?>
                </form>
            </div>
        </div>
    <?php if (isset($combined_exams_array) && $combined_exams_array) { ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-6 col-sm-offset-3 list-group">
                    <div class="list-group-item">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Class Name</th>
                                    <th>Exams Combined</th>
                                    <th>Selection Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $singleclass->classes ?></td>
                                    <td><?php
                                        $exam_names = '';
                                        foreach ($combined_exams_array as $key => $value) {
                                            if (isset($exam_percents[$value]) && $exam_percents[$value]) {
                                                //$perc=  
                                                $percentage = '(' . ($exam_percents[$value]) . '%)';
                                            } else {
                                                $percentage = '';
                                            }

                                            $exam_names .= $value = $exam_name[$value] . $percentage . ' ,';
                                        }
                                        echo rtrim(str_replace('_', ' ', ucfirst($exam_names)), ' ,');
                                        ?></td>
                                    <td>Subjects Average </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        <?php
    }

    if ($students) {
        $global_div = [];
        ?>

            <div class="col-sm-12">

                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#all" aria-expanded="true">eattendance_all_students</a></li>
                        <?php
                        foreach ($sections as $key => $section) {
                            echo '<li class=""><a data-toggle="tab" href="#' . $section->sectionID . '" aria-expanded="false">' . "student_section" . " " . $section->section . " ( " . $section->category . " )" . '</a></li>';
                        }
                        ?>
                        <li class=""><a data-toggle="tab" href="#summary" aria-expanded="false">Summary</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="all" class="tab-pane active">
                            <div id="hide-table" class="table-responsive">
                                <?php
                                $sub_rank = isset($show_rank) && $show_rank == 1 && $show_grade == 1 ? 3 : 2;

                                $col_span = isset($show_grade) && $show_grade == 1 || $show_rank == 1 ? 'colspan="' . $sub_rank . '"' : '';
                                $row_span = isset($show_grade) && $show_grade == 1 || $show_rank == 1 ? 'rowspan="2"' : '';
                                ?>
                                <table class="table table-striped table-bordered table-hover dataTable no-footer">
                                    <thead>
                                        <tr>
                                            <th class="col-sm-1" <?= $row_span ?>>S/N</th>

                                            <th class="col-sm-2" <?= $row_span ?>>Nnme</th>
                                            <th class="col-sm-2" <?= $row_span ?>>Roll</th>

                                            <?php
                                            $average = 'Student Average';
                                            //Loop in all subjects to show list of them here
                                            foreach ($subjects as $subject) {

                                                $subject_name = strtolower($subject->subject);
                                                $pass["$subject_name"] = 0;
                                                $fail["$subject_name"] = 0;
                                                $subject_sum["$subject_name"] = 0;
                                               
                                                $words = explode(' ', str_replace('/', '', $subject->subject));
                                                
                                                $this_subject = "";
                                                $check_ = str_replace('/', '', $subject->subject);
                                                if(str_word_count($check_) > 1 && str_word_count($subject->subject) > 1){                                                    $this_subject .= substr(strtoupper($words[1]), 0, 4);
                                                }else{
                                                   $this_subject = substr(strtoupper($subject->subject), 0, 4);
                                                }
                                                echo $subject ?
                                                    '<th class="col-sm-1 verticalTableHeader" ' . $col_span . '>'
                                                    . '<p>' . $this_subject . ''
                                                    . '</p></th>' : '<th></th>';
                                            }
                                            ?>

                                            <th class="col-sm-2" <?= $row_span ?>>Total Marks</th>
                                            <th class="col-sm-2" <?= $row_span ?>><?= $average ?> <span class="col-sm-4 control-label"><a class="right"><i class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="left" data-trigger="hover" data-content="This average is calculated based on exams combined and percentages if specified" title="" data-original-title=" Report Average"></i>
                                                    </a>
                                                </span></th>
                                            <th class="col-sm-2" <?= $row_span ?>>GD</th>
                                            <?php if (request('show_division') == 'on') { ?>
                                                <th <?= $row_span ?>>Div</th>
                                                <th <?= $row_span ?>>Points</th>
                                            <?php } ?>
                                            <th class="col-sm-2" <?= $row_span ?>>Rank <span class="col-sm-4 control-label"><a class="right"><i class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="left" data-trigger="hover" data-content="This rank is calculated based on exams combined and percentages if specified" title="" data-original-title=" Report Rank Option"></i>
                                                    </a>
                                                </span></th>
                                            <!--<th class="col-sm-2">total_point') ?></th>-->
                                            <th class="col-sm-2" id="action_option" <?= $row_span ?>>action') ?></th>
                                        </tr>
                                        <?php if (isset($subjects) && isset($show_grade) && $show_grade == 1 || $show_rank == 1) { ?>
                                            <tr>
                                                <?php
                                                $subject_th = $show_rank == 1 ? '<th class="col-sm-2">Rank</th>' : '';
                                                $grade_th = $show_grade == 1 ? '<th class="col-sm-1">GD</th>' : '';
                                                foreach ($subjects as $subject) {
                                                    if ($subject) {
                                                ?>
                                                        <th class="col-sm-1"><?= substr(strtoupper($subject->subject), 0, 3) ?></th>
                                                        <?= $grade_th ?>
                                                        <?= $subject_th ?>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </tr>
                                        <?php } ?>
                                    </thead>
                                    <tbody>
                                        <?php

                                        if ($students) {
                                            $i = 1;
                                            $student_pass = 0;
                                            $total_average = 0;
                                            $s = 0;
                                            foreach ($subjects as $subject) {
                                                $subject_name = strtolower($subject->subject);
                                                foreach ($grades as $grade) {
                                                    ${$subject_name . '_' . $grade->grade} = 0;
                                                }
                                            }
                                            $total_div_I = 0;
                                            $total_div_II = 0;
                                            $total_div_III = 0;
                                            $total_div_IV = 0;
                                            $total_div_0 = 0;
                                            foreach ($students as $student) {
                                                $student = is_object($student) ? (array) $student : $student;
                                        ?>
                                                <tr>
                                                    <td data-title="slno">
                                                        <?php echo $i ?>
                                                    </td>

                                                    <td data-title="eattendance_name">
                                                        <?php echo $student['name']; ?>
                                                    </td>
                                                    <td data-title="eattendance_roll">
                                                        <?php echo $student['roll']; ?>
                                                    </td>
                                                    <?php
                                                    /*
                                                     * 
                                                     * -----------------------------------------
                                                     * This part show list of all subjects or ONE
                                                     *  subject depends on user selection
                                                     * -----------------------------------------
                                                     * 
                                                     */
                                                    $total_mark_class = 0;
                                                    $point[$student['student_id']] = [];
                                                    $subject_count = 1;
                                                    foreach ($subjects as $subject) {
                                                        $subject_name = strtolower($subject->subject);
                                                        $percent_values = array();
                                                        $percent = request('exam_percent');


                                                        foreach ($percent as $key => $value) {
                                                            isset($value) ? array_push($percent_values, $value) : '';
                                                        }

                                                        if (count($percent_values)==0) {
                                                            if (isset($student["$subject_name"])) {
                                                                if ($student["$subject_name"] < $classlevel->pass_mark && $student["$subject_name"] != NULL) {

                                                                    $color = "pink";
                                                                    $fail["$subject_name"]++;
                                                                } else {
                                                                    $color = "";
                                                                    $student["$subject_name"] != NULL ? $pass["$subject_name"]++ : '';
                                                                }
                                                                $subject_sum["$subject_name"] += $student["$subject_name"];
                                                                $total_mark_class += $student["$subject_name"];

                                                                echo '<td class="mark" subject_id="' . $subject->subjectID . '" student_id="' . $student['student_id'] . '" data-title="" style="background: ' . $color . ';">';
                                                                $mark = $student["$subject_name"];
                                                                echo $mark;
                                                                echo '</td>';

                                                                if (isset($show_grade) && $show_grade == 1) {
                                                                    foreach ($grades as $grade) {
                                                                        if ($grade->gradefrom <= round($student["$subject_name"], 0) && $grade->gradeupto >= round($student["$subject_name"], 0)) {


                                                                            echo ' <td>' . $grade->grade . '</td>';
                                                                        }
                                                                    }
                                                                }
                                                                $s_rank = isset($subject_rank[$student['student_id']][$subject->subjectID]) ? $subject_rank[$student['student_id']][$subject->subjectID] : '';
                                                                echo $show_rank == 1 ? '<th class="col-sm-2">' . $s_rank . '</th>' : '';

                                                                //$sum_subj[$subject_name] += $student["$subject_name"];

                                                                foreach ($grades as $grade) {
                                                                    if ($grade->gradefrom <= round($student["$subject_name"], 0) && $grade->gradeupto >= round($student["$subject_name"], 0)) {
                                                                        ${$subject_name . '_' . $grade->grade}++;

                                                                        $sub = [
                                                                            'subject_mark' => $mark,
                                                                            'point' => $grade->point,
                                                                            'penalty' => $subject->is_penalty,
                                                                            'pass_mark' => $subject->pass_mark,
                                                                            'is_counted_indivision' => $subject->is_counted_indivision
                                                                        ];
                                                                        array_push($point[$student['student_id']], $sub);
                                                                    }
                                                                }
                                                            } else {
                                                                //here you will check if this student subscribe to this subject or not . if yes, place a yellow box, if not disable the input if result format is based on the subject counted otherwise, place yellow and content editable in all cases
                                                                echo '<td  class="mark" subject_id="' . $subject->subjectID . '" student_id="' . $student['student_id'] . '"></td>';
                                                                echo isset($show_grade) && $show_grade == 1 ? '<td></td>' : '';
                                                                echo $show_rank == 1 ? '<th class="col-sm-2"></th>' : '';
                                                            }
                                                        } else {
                                                            $marks_avg = isset($student_average["$subject_name"][$student['student_id']]) ? $student_average["$subject_name"][$student['student_id']] : 0;
                                                            if ($marks_avg < $singleclass->classlevel->pass_mark && $marks_avg != NULL) {
                                                                $color = "pink";
                                                                $fail["$subject_name"]++;
                                                            } else {
                                                                $color = "";
                                                                $marks_avg != NULL ? $pass["$subject_name"]++ : '';
                                                            }

                                                            $total_mark_class += $marks_avg / $total_semesters;


                                                            echo '<td data-title="" style="background: ' . $color . ';">';
                                                            $mark_sb = $marks_avg == 0 ? '' : round($marks_avg / $total_semesters, 1);
                                                            echo $mark_sb;
                                                            $subject_sum["$subject_name"] += (float) $mark_sb;
                                                            echo '</td>';
                                                            if (isset($show_grade) && $show_grade == 1) {
                                                                foreach ($grades as $grade) {
                                                                    if ($grade->gradefrom <= round($mark_sb, 0) && $grade->gradeupto >= round($mark_sb, 0)) {


                                                                        echo ' <td>' . $grade->grade . '</td>';
                                                                    }
                                                                }
                                                            }


                                                            $s_rank = isset($subject_prank[$subject_name][$student['student_id']]) ? (int)$subject_prank[$subject_name][$student['student_id']] : '';
                                                            echo $show_rank == 1 ? '<th class="col-sm-2">' . $s_rank . '</th>' : '';
                                                            foreach ($grades as $grade) {
                                                                if ($grade->gradefrom <= round($mark_sb, 0) && $grade->gradeupto >= round($mark_sb, 0)) {
                                                                    if (round($mark_sb, 0) == 0) {
                                                                    } else {
                                                                        ${$subject_name . '_' . $grade->grade}++;
                                                                    }
                                                                    $sub = [
                                                                        'subject_mark' => $mark_sb,
                                                                        'point' => $grade->point,
                                                                        'penalty' => $subject->is_penalty,
                                                                        'pass_mark' => $subject->pass_mark,
                                                                        'is_counted_indivision' => $subject->is_counted_indivision
                                                                    ];
                                                                    array_push($point[$student['student_id']], $sub);
                                                                }
                                                            }
                                                        }
                                                        $subject_count++;
                                                    }

                                                    echo '<td>' . $total_mark_class . '</td>';
                                                    ?>

                                                    <?php
                                                    /**
                                                     * For the whole class, find total marks of that student
                                                     * and devide by total subjects taken by such student
                                                     */
                                                    $avrg = empty($percent_values) ? $student['average'] : $student['total'];
                                                    if ((isset($student) && $avrg < $classlevel->pass_mark)) {

                                                        $color = "pink";
                                                    } else {
                                                        $color = "";
                                                        $student_pass++;
                                                    }

                                                    $smrk = round($avrg / $total_semesters, 1);
                                                    echo '<td data-title="" style="background: ' . $color . ';">';

                                                    echo $smrk;
                                                    $total_average += $smrk;
                                                    echo '</td>';
               
                                                    $grade_column = '';
                                                    foreach ($grades as $grade) {

                                                        if ($grade->gradefrom <= round($smrk, 0) && $grade->gradeupto >= round($smrk, 0)) {
                                                            $grade_column = '<td>' . $grade->grade . '</td>';
                                                        }
                                                    }
                                                    if ($grade_column == '') {
                                                        echo '<td></td>';
                                                    } else {
                                                        echo $grade_column;
                                                    }
                                                    ?>

                                                    <?php
                                                    if (request('show_division') == 'on') {

                                                        $global_div[$student['student_id']] = $division = getDivisionBySort($point[$student['student_id']], $singleclass->classlevel->result_format);
                                                        $total_div_0 += $division[0] == '0' ? 1 : 0;
                                                        $total_div_I += $division[0] == 'I' ? 1 : 0;
                                                        $total_div_II += $division[0] == 'II' ? 1 : 0;
                                                        $total_div_III += $division[0] == 'III' ? 1 : 0;
                                                        $total_div_IV += $division[0] == 'IV' ? 1 : 0;
                                                    ?>
                                                        <td><?= $division[0] ?></td>
                                                        <td><?= $division[1] ?></td>
                                                    <?php } ?>
                                                    <td data-title="eattendance_roll">
                                                        <?php echo $student['rank']; ?>
                                                    </td>
                                                    
                                                    <td data-title="action" class="action_btn">
                                                        <a href="<?php echo url('exam/evaluation/' . $student['student_id'] . '?year_no=' . $academic_year_id . '&exam=' . $exams_combined) ?>&tag=all" class="btn btn-info btn-sm btn-out-dotted" data-placement="top" ><i class="fa fa-file-text-o"></i>  Report</a>
                                                        <a href="<?= url('exam/singleReportAnalysis/' . $student['student_id'] . '/' . $exams_combined . '/' . $academic_year_id . '?type=combined') ?>" class="btn btn-info btn-sm btn-out-dotted" data-placement="top" ><i class="fa fa-file-o"></i>  Graph</a>
                                                    </td>
                                                </tr>
                                        <?php
                                                $i++;
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>

                            </div>

                        </div>

                        <?php                        

         foreach ($sections as $key => $section) { ?>
                            <div id="<?= $section->sectionID ?>" class="tab-pane">
                                <div id="hide-table">
                                    <table class="table table-striped table-bordered table-hover dataTable no-footer">
                                        <thead>
                                            <tr>
                                                <th class="col-sm-1">slno') ?></th>

                                                <th class="col-sm-2">eattendance_name') ?></th>
                                                <th class="col-sm-2">eattendance_roll') ?></th>

                                                <?php
                                                $average = 'Student Average';
                                                //Loop in all subjects to show list of them here
                                                foreach ($section_subjects[$section->section] as $subject) {
                                                    $words = explode(' ', str_replace('/', '', $subject->subject));
                                                    $this_subject = "";
                                                    $check_ = str_replace('/', '', $subject->subject);
                                                    if(str_word_count($check_) > 1 && str_word_count($subject->subject) > 1){                                                        $this_subject .= substr(strtoupper($words[1]), 0, 4);
                                                    }else{
                                                       $this_subject = substr(strtoupper($subject->subject), 0, 4);
                                                    }
                                                    echo $subject ? '<th class="col-sm-2">' . $this_subject . '</th>' : '<th></th>';
                                                }
                                                ?>

                                                <th class="col-sm-2">Total Marks</th>
                                                <th class="col-sm-2"><?= $average ?></th>
                                                <?php if (request('show_division') == 'on') { ?>
                                                    <th>Div</th>
                                                    <th>Points</th>
                                                <?php } ?>
                                                <th class="col-sm-2">Rank</th>
                                                <!--<th class="col-sm-2">total_point') ?></th>-->
                                                <th class="col-sm-2">action') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (isset($allsection) && $allsection[$section->section]) {
                                                $i = 1;
                                                foreach ($allsection[$section->section] as $student) {
                                                    $student = is_object($student) ? (array) $student : $student;
                                            ?>
                                                    <tr>
                                                        <td data-title="slno">
                                                            <?php echo $i; ?>
                                                        </td>
                                                        <td data-title="eattendance_name">
                                                            <?php echo $student['name']; ?>
                                                        </td>
                                                        <td data-title="eattendance_roll">
                                                            <?php echo $student['roll']; ?>
                                                        </td>
                                                        <?php
                                                        /*
                                                         * 
                                                         * -----------------------------------------
                                                         * This part show list of all subjects or ONE
                                                         *  subject depends on user selection
                                                         * -----------------------------------------
                                                         * 
                                                         */

                                                        $total_mark_section = 0;
                                                        foreach ($section_subjects[$section->section] as $subject) {

                                                            $subject_name = strtolower($subject->subject);
                                                            $percent_values = array();
                                                            $percent = request('exam_percent');
                                                            foreach ($percent as $key => $value) {
                                                                isset($value) ? array_push($percent_values, $value) : '';
                                                            }

                                                            if (empty($percent_values)) {
                                                                if (isset($student["$subject_name"])) {
                                                                    if ($student["$subject_name"] < $classlevel->pass_mark) {

                                                                        $color = "pink";
                                                                    } else {
                                                                        $color = "";
                                                                    }
                                                                    $total_mark_section += $student["$subject_name"];
                                                                    echo '<td data-title="" style="background: ' . $color . ';">';
                                                                    echo $student["$subject_name"];
                                                                    echo '</td>';
                                                                } else {
                                                                    echo '<td></td>';
                                                                }
                                                            } else {
                                                                $marks_avg = isset($student_average["$subject_name"][$student['student_id']]) ? $student_average["$subject_name"][$student['student_id']] : 0;
                                                                if ($marks_avg < $classlevel->pass_mark && $marks_avg != NULL) {
                                                                    $color = "pink";
                                                                } else {
                                                                    $color = "";
                                                                }
                                                                $total_mark_section += $marks_avg / $total_semesters;;

                                                                echo '<td data-title="" style="background: ' . $color . ';">';
                                                                echo $marks_avg == 0 ? '' : round($marks_avg / $total_semesters, 1);
                                                                echo '</td>';
                                                            }
                                                        }



                                                        echo '<td>' . $total_mark_section . '</td>';
                                                        ?>

                                                        <?php
                                                        /**
                                                         * For the whole class, find total marks of that student
                                                         * and devide by total subjects taken by such student
                                                         */
                                                        $avrg = empty($percent_values) ? $student['average'] : $student['total'];
                                                        if ((isset($student) && $avrg < $classlevel->pass_mark)) {

                                                            $color = "pink";
                                                        } else {
                                                            $color = "";
                                                        }
                                                        echo '<td data-title="" style="background: ' . $color . ';">';
                                                        echo round($avrg / $total_semesters, 1);
                                                        echo '</td>';
                                                        ?>

                                                        <?php if (request('show_division') == 'on') { ?>
                                                            <td data-title="eattendance_phone">
                                                                <?php
                                                                echo isset($global_div[$student['student_id']][0]) ? $global_div[$student['student_id']][0] : '';
                                                                ?>
                                                            </td>
                                                            <td data-title="eattendance_phone">
                                                                <?php
                                                                echo isset($global_div[$student['student_id']][1]) ? $global_div[$student['student_id']][1] : '';
                                                                ?>
                                                            </td>
                                                        <?php } ?>
                                                        <td data-title="eattendance_roll">
                                                            <?php
                                                            echo $student['rank'];
                                                            ?>
                                                        </td>
                                                        <td data-title="action">
                                                            <a href="<?php echo url('exam/evaluation/' . $student['student_id'] . '?year_no=' . $academic_year_id . '&exam=' . $exams_combined) ?>&tag=all" class="btn btn-info btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Analyse Report"><i class="fa fa-file-text-o"></i></a>
                                                        </td>
                                                    </tr>
                                            <?php
                                                    $i++;
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        <?php }

                         ?>
                        <div id="summary" class="tab-pane">
                            <div id="hide-table">
                                <div class="row">
                                    <div class="title_left">
                                        <br />

                                        <h3>CLASS PERFORMANCE</h3>
                                        <br />
                                    </div>

                                    <table class="table table-striped table-bordered dataTable table-hover no-footer">
                                        <thead>
                                            <tr>
                                                <th class="col-sm-1">TOTAL Students</th>
                                                <?php foreach ($sections as $section) { ?>
                                                    <th>Total students in <?= $section->section ?> Stream</th>
                                                <?php } ?>
                                                <th class="col-sm-2">ABSENT</th>
                                                <th class="col-sm-2">SAT</th>
                                                <th class="col-sm-2">PASS</th>
                                                <th class="col-sm-2">FAIL</th>
                                                <th class="col-sm-2">Class Exam Average</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?= count($total_students) ?></td>
                                                <?php foreach ($sections as $section) { ?>
                                                    <td><?= count($allsection[$section->section]) ?></td>
                                                <?php } ?>
                                                <td><?= count($total_students) - count($students) ?></td>
                                                <td><?= count($students) ?></td>
                                                <td><?= $student_pass ?></td>
                                                <td><?= count($students) - $student_pass ?></td>
                                                <td><?=round($total_average / count($students), 2) ?></td>
                                                
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                
                                <div class="row">
                                    <div class="title_left">
                                        <br /><br />
                                        <h3>SUBJECTS PERFORMANCE</h3>
                                        <br />
                                    </div>
                                    <script type="text/javascript">
                                        $(function() {

                                            $('#container').highcharts({
                                                chart: {
                                                    type: 'column'
                                                },
                                                title: {
                                                    text: "Combined Subject Performance Evaluation"
                                                },
                                                subtitle: {
                                                    text: ''
                                                },
                                                xAxis: {
                                                    type: 'category'
                                                },
                                                yAxis: {
                                                    title: {
                                                        text: 'Avarage %'
                                                    }

                                                },
                                                legend: {
                                                    enabled: false
                                                },
                                                plotOptions: {
                                                    series: {
                                                        borderWidth: 0,
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '{point.y:.1f}%'
                                                        }
                                                    }
                                                },

                                                tooltip: {
                                                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                                                },

                                                series: [{
                                                    name: 'Avarage',
                                                    colorByPoint: true,
                                                    data: [
                                                        <?php
                                                        foreach ($subjects as $subject) {
                                                            $subj = strtolower($subject->subject);
                                                            $total_sat = $pass[$subj] + $fail[$subj];
                                                        ?> {
                                                                name: '<?= ucwords($subject->subject) ?>',
                                                                y: <?php
                                                                    if ($total_sat == 0) {
                                                                        echo '0';
                                                                    } else {
                                                                        echo round($subject_sum[$subj] / $total_sat, 2);
                                                                    }
                                                                    ?>,
                                                                drilldown: ''
                                                            },
                                                        <?php
                                                            $i++;
                                                        } //}  
                                                        ?>
                                                    ]
                                                }]
                                            });
                                        });
                                    </script>
                                    <script src="<?= url('public/assets/js/highchart.js') ?>"></script>
                                    <script src="<?= url('public/assets/js/exporting.js') ?>"></script>

                                    <div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
                                </div>
                                <div class="row">
                                    <div class="title_left">
                                        <br /><br />
                                        <h3>SUBJECTS PERFORMANCE</h3>
                                        <br />
                                    </div>
                                    <table class="table table-striped table-bordered dataTable table-hover no-footer">
                                        <thead>
                                            <tr>
                                                <th class="col-sm-1">CODE</th>
                                                <th class="col-sm-3">NAME</th>
                                                <th class="col-sm-3">TEACHER </th>
                                                <th class="col-sm-3">TYPE</th>
                                                <?php
                                                if ($grades) {
                                                    foreach ($grades as $grade) {
                                                ?>
                                                        <th class="col-sm-2"><?= $grade->grade ?></th>

                                                <?php
                                                    }
                                                }
                                                ?>
                                                <th class="col-sm-1">SAT</th>
                                                <th class="col-sm-2">PASS</th>
                                                <th class="col-sm-2">FAIL</th>
                                                <th class="col-sm-2">Average</th>
                                                <th class="col-sm-2">Subj Average Grade</th>
                                                <th class="col-sm-2">GPA</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php

                                            $total_gpas = 0;
                                            $total_subjects = 0;
                                            if (count($subjects) > 0) {
                                                foreach ($subjects as $subject) {
                                                    $subj = strtolower($subject->subject);
                                                    $total_sat = $pass["$subj"] + $fail["$subj"]; ?>
                                                <tr>
                                                    <td><?= $subject->subject_code ?></td>
                                                    <td><?= ucwords($subject->subject) ?></td>
                                                    <td><?= ucwords($subject->teacher_name) ?></td>
                                                    <td><?= $subject->subject_type ?></td>
                                                    <!--<td>778</td>-->
                                                    <?php
                                                    if ($grades) {
                                                        $total_points = 0;
                                                        foreach ($grades as $grade) {
                                                            ?>
                                                            <td><?= ${$subj . '_' . $grade->grade} ?></td>

                                                    <?php
                                                            $total_points = $total_points + ((int)(${$subj . '_' . $grade->grade}) * (int)($grade->point));
                                                        }
                                                    } ?>
                                                    <td><?= $total_sat ?></td>
                                                    <td><?= $pass["$subj"] ?></td>
                                                    <td><?= $fail["$subj"] ?></td>
                                                    <td><?php
                                                        $ovr_subj_avg = 0;
                                                    if ($total_sat == 0) {
                                                    $total_sat=1;
                                                    } else {
                                                        $ovr_subj_avg = round($subject_sum["$subj"] / $total_sat, 2);
                                                    }
                                                    echo $ovr_subj_avg; ?></td>
                                                    <td>
                                                        <?php
                                                        foreach ($grades as $grade) {
                                                            if ($grade->gradefrom <= round($ovr_subj_avg, 0) && $grade->gradeupto >= round($ovr_subj_avg, 0)) {
                                                                echo $grade->grade;
                                                            }
                                                        } ?>
                                                    </td>
                                                    <td><?=round(($total_points / $total_sat), 2) ?></td>
                                                </tr>
                                            <?php
                                             
                                                $total_subjects = $total_subjects + 1;
                                                }
                                            }
                                              
                                                ?>
        
                                        </tbody>
                                    </table>
                                </div>

                                <?php if (request('show_division') == 'on') { ?>

                                    <div class="row">
                                        <div class="title_left">
                                            <br /><br />
                                            <h3>DIVISION</h3>
                                            <br />
                                        </div>
                                        <table class="table table-striped table-bordered dataTable table-hover no-footer">
                                            <thead>
                                                <tr>
                                                    <th class="col-sm-1"></th>
                                                    <th class="col-sm-2">DIV I</th>
                                                    <th class="col-sm-2">DIV II</th>
                                                    <th class="col-sm-1">DIV III</th>
                                                    <th class="col-sm-2">DIV IV</th>
                                                    <th class="col-sm-2">DIV 0</th>
                                                    <th class="col-sm-2">ABS</th>

                                                </tr>

                                            </thead>
                                            <tbody>
                                                
                                                <tr>
                                                    <td>Total</td>
                                                    <td><?= $total_div_I ?></td>
                                                    <td><?= $total_div_II ?></td>
                                                    <td><?= $total_div_III ?></td>
                                                    <td><?= $total_div_IV ?></td>
                                                    <td><?= $total_div_0 ?></td>
                                                    <td><?= count($total_students) - count($students) ?></td>

                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                </div> <!-- nav-tabs-custom -->
            </div> <!-- col-sm-12 for tab -->

        <?php
    } else {
        ?>

        <?php } ?>



        </div> <!-- col-sm-12 -->
</div><!-- row -->
<div class="row">
    <div class="col-lg-2"></div>
    <div class="col-lg-6">
        <table class="table col-lg-6">
            <tr>
                <td>Color Meaning: </td>
                <td style="background:pink">
                    xy
                </td>
                <td><a href="<?= url('setting#home') ?>">Below Average Pass mark</a></td>
            </tr>
        </table>
    </div>
    <div class="col-lg-4"></div>
</div>
</div><!-- Body -->
</div><!-- /.box -->

<script>
        $('#subjectID').change(function(event) {

            var subjectID = $(this).val();
            if (subjectID === 'all') {
                $('#show_division').show();
            } else {
                $('#show_division').hide();
            }
        });

        $('#schema_name').change(function(event) {
        var schema_name = $('#schema_name').val();
        var schema_name = $(this).val();
        if (schema_name === '0') {
            $('#academic_year_id').val(0);
            $('#report_filter_div').hide();
        } else {
            $('#report_filter_div').show();
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/getClasses') ?>",
                data: "_token=" + "{{ csrf_token() }}" + "&schema_name=" + schema_name,
                dataType: "html",
                success: function(data) {
                    $('#classesID').html(data);
                }
            });
        }
    });

    $('#classesID').change(function(event) {
        var schema_name = $('#schema_name').val();
        var classesID = $(this).val();
        if (classesID === '0') {
            $('#academic_year_id').val(0);
            $('#report_filter_div').hide();
        } else {
            $('#report_filter_div').show();
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/get_academic_years') ?>",
                data: "_token=" + "{{ csrf_token() }}" + "&id=" + classesID + "&schema_name=" + schema_name,
                dataType: "html",
                success: function(data) {
                    $('#academic_year_id').html(data);
                }
            });
        }
    });

        $('#academic_year_id').change(function(event) {
            var schema_name = $('#schema_name').val();
            var classesID = $('#classesID').val();
            var academic_year_id = $(this).val();
            if (academic_year_id === '0') {
                $('#examID').val(0);
            } else {
                $.ajax({
                    type: 'POST',
                    url: "<?= url('exam/getClassExam') ?>",
                    data: "_token=" + "{{ csrf_token() }}" + "&id=" + classesID + '&year_id=' + academic_year_id  + "&schema_name=" + schema_name + '&done=1',
                    dataType: "html",
                    success: function(data) {
                        $('#examID').html(data);
                    }
                });
            }

        });
        $('#academic_year_id').change(function(event) {
            var classesID = $('#classesID').val();
            var academic_year_id = $(this).val();
            var schema_name = $('#schema_name').val();
            if (academic_year_id === '0') {
                $('#subjectID').val(0);
            } else {
                $.ajax({
                    type: 'POST',
                    url: "<?= url('exam/getSubjectByClass') ?>",
                    data: "_token=" + "{{ csrf_token() }}" + "&id=" + classesID + "&schema_name=" + schema_name + '&year_id=' + academic_year_id,
                    dataType: "html",
                    success: function(data) {
                        $('#subjectID').html(data);
                    }
                });
            }
        });
        open_edit_model = function(a, b, c, d) {
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/optimize') ?>",
                data: {
                    "student_id": a,
                    "exam_id": b,
                    'exams': c,
                    'class_id': d
                },
                dataType: "html",
                success: function(data) {
                    $('#optimize_data').html(data);
                }
            });
        }
        $('#academic_year_id').change(function(event) {
            var academic_year_id = $(this).val();
            var schema_name = $('#schema_name').val();
            var classesID = $('#classesID').val();
            if (academic_year_id === '0') {
                $('#sectionID').val(0);
            } else {
                $.ajax({
                    type: 'POST',
                    url: "<?= url('exam/sectioncall') ?>",
                    data: "_token=" + "{{ csrf_token() }}" + "&id=" + classesID + "&schema_name=" + schema_name + '&academic_year_id=' + academic_year_id,
                    dataType: "html",
                    success: function(data) {
                        if (data === '0') {
                            $('#sectionID').html('');
                            $('#section_id').html('<span class="red"><?= "no_sections" ?> <a href="<?= url('section/add') ?>" class="btn btn-primary" role="button"><?= "add_section" ?> </a> <?= "for_class" ?></span>');
                        } else {
                            $('#section_id').html('');
                            $('#sectionID').html(data);
                        }
                    }
                });
            }
        });

      
    </script>

@endsection