@extends('layouts.app')

@section('content')
<?php $root = url('/public/'); ?>
<!-- Page-body start -->
<div class="page-body">
    <div class="row">
        <!-- Server Side Processing table start -->
        <div class="col-sm-12">
            <div class="card">
               
                <div class="card-block">
                <div class="dt-responsive table-responsive">
                        <table id="simpletable" class="table table-striped table-bordered nowrap">
                        <thead>
                        <?php
                            $usertype = session("usertype");
                            $exams_report_setting = \DB::table($schema.'.exam_report_settings')->where('exam_type', 'single')->where('classlevel_id', $classes->classlevel_id)->first();
                            if (!empty($exams_report_setting) && (strlen($exams_report_setting->class_teacher_remark) < 4 || strlen($exams_report_setting->head_teacher_remark) < 4)) {
                                \DB::table($schema.'.exam_report_settings')->where('exam_type', 'single')->where('classlevel_id', $classes->classlevel_id)->update([
                                    'class_teacher_remark' => 'Class Teacher Remark',
                                    'head_teacher_remark' => 'Head Teacher Remark'
                                ]);
                                $exams_report_setting = \DB::table($schema.'.exam_report_settings')->where('exam_type', 'single')->where('classlevel_id', $classes->classlevel_id)->first();
                            }
                           
                if (empty($exam_report)) {
                    ?>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-6">


                                    <span class="share_social"></span>

                            <?php
                            if (isset($allsections) && !empty($allsections)) {
                                foreach ($allsections as $section_info) {
                                    ?>

                                    <a href="<?= url("exam/printall/" . $exams->examID . '?class_id=' . $class_id . '&section_id=' . $section_info->sectionID) . '&year_no=' . request('year_no') ?>"
                                       target="_blank">
                                        <i class="fa fa-print"></i>Print all (<?= $section_info->section ?>)</a>
                                    <span>&nbsp;</span>
                                    <?php
                                }
                            }
                            ?>
                            </div>
                        </div>

                    <?php
                     } if (empty($grades)) {
                        echo '<div class="alert alert-warning">Report cannot be displayed because no grades have been defined in this class level. Please go to <a href="' . url('grade/index') . '">"Grade"</a> to define grades first</div>';
                    } else {    ?>
                <section class=" subpage">

                        <!-- Begin inline CSS -->
                        <style type="text/css">

                            .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
                                font-weight: bolder
                            }

                            #printablediv {
                                color: #000;
                            }

                            table.table.table-striped td, table.table.table-striped th, table.table {
                                margin-left: 10%;
                            }

                            @page {
                                margin: 0
                            }

                            .sheet {
                                margin: 0;
                                overflow: hidden;
                                position: relative;
                                box-sizing: border-box;
                                page-break-after: always;
                            }

                            /** Padding area **/
                            .sheet.padding-10mm {
                                padding: 10mm
                            }

                            /** For screen preview **/
                            @media screen {

                                .sheet {
                                    background: white;
                                    box-shadow: 0 .5mm 2mm rgba(0, 0, 0, .3);
                                    margin: 5mm;
                                    left:12%;
                                }
                            }
                        </style>
                        <!-- End inline CSS -->

                        <!-- Begin embedded font definitions -->
                        <style id="fonts1" type="text/css">


                            table.table.table-striped td, table.table.table-striped th, table.table {
                                border: 1px solid #000000;
                            }


                        </style>

                        <style type="text/css">
                            @font-face {
                                font-family: DejaVuSans_b;
                                src: url("../../public/assets/fonts/DejaVuSans_b.woff") format("woff");
                            }

                            table.table.table-striped td, table.table.table-striped th, table.table {
                                border: 1px solid #000000;
                            }

                            .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
                                font-weight: bolder
                            }

                            #printablediv {
                                color: #000;
                            }


                            @media print {
                                h3, h4 {
                                    page-break-after: avoid;
                                    /* line-height: 0; */
                                }

                                .name_left{
                                    margin-left: 0 !important;
                                }
                                .stream_right{
                                    margin-right: 0 !important;
                                }
                                section.A4 {
                                    width: 210mm
                                }

                                @page {
                                    size: A4;
                                    margin: 0;

                                }
                                table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
                                    padding: 0.9%;
                                }

                                table {
                                    width: 100%;

                                }
                                #signature_stamp_preview{
                                    position:relative; margin:-11% 5% 0 0 !important;
                                }
                                .subpage {
                                    padding: 1em 1em 0 1em;
                                    /* border: 3px #f00 solid; */
                                    font-size: .8em;
                                }


                                .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
                                    padding: 0.2%;   border: 1px solid black !important;
                                }
                                table {
                                    border-collapse: collapse !important;
                                }

                                .table-bordered > thead > tr > th, .table-bordered > tbody > tr > td {
                                    font-size: 12px; padding: 1%;
                                }
                                .letterhead{

                                    font-weight: 100;
                                    font-family: "Adobe Garamond Pro";
                                }

                                .col-sm-6 , .col-lg-6{max-width: 50%}
                                .grading{float: left;}
                                .keys{float: right}
                                h3, h4 {
                                    page-break-after: avoid;
                                    font-size: 15px;
                                    font-weight:100;
                                }
                                h2{
                                    font-weight:bolder !important;
                                }


                            }

                            h3, h4 {
                                page-break-after: avoid;

                            }

                        </style>
                        <?php if ($classlevel->result_format == 'CSEE') { ?>
                            <style type="text/css">
                                table.table.table-striped td, table.table.table-striped th, table.table {
                                    font-size: 11px !important;
                                    margin-left: 0%;
                                }

                                @page {
                                    margin: 0
                                }
                                .sheet {
                                    margin: 0;
                                    overflow: hidden;
                                    position: relative;
                                    box-sizing: border-box;
                                    page-break-after: always;
                                }

                                /** Padding area **/
                                .sheet.padding-10mm {
                                    padding: 10mm
                                }

                                /** For screen preview **/
                                @media screen {

                                    .sheet {
                                        background: white;
                                        box-shadow: 0 .5mm 2mm rgba(0, 0, 0, .3);
                                        margin: 5mm;
                                        left:12%;
                                    }
                                }

                            </style>
                        <?php } ?>
                        <!-- Begin page background -->


                        <?php
                        $array = array(
                            "src" => url('storage/uploads/images/' . $siteinfos->photo),
                            'width' => '126em',
                            'height' => '126em',
                            'class' => 'img-rounded',
                            'style' => 'margin-left:2em'
                        );
                        if(strpos($student->photo, 'https:') !== false ){
                            $image = $student->photo;

                        } else{
                            $image = is_file('storage/uploads/images/' . $student->photo) ? url('storage/uploads/images/' . $student->photo) : url('storage/uploads/images/defualt.png');
                        }
                      
                        ?>
                        <div class="row">
                            <br>
                                        <div class="col-md-2" style="padding-bottom:0px">
                                            <img src='<?=url('storage/uploads/images/' . $siteinfos->photo)?>' width ='128em' height= '128em' class='img-rounded' style ='margin-left:2em' alt=""> 
                                        </div>
                                        <div class="col-md-8 text-center" style="">

                                            <h2 style="font-weight: bolder !important; font-size: 24px; text-transform: uppercase;"><?= $siteinfos->sname ?></h2>
                                            <h3><?= 'P.O. BOX ' . $siteinfos->box . ', ' . ucfirst($siteinfos->address) ?>
                                                <br>Phone: <?= str_replace(',', ' / ', $siteinfos->phone) ?>
                                                <br> Email: <?= $siteinfos->email ?><br>
                                                <?= $siteinfos->website != '' ? 'Website' . ' : ' . $siteinfos->website . '</h3>' : '</h3>' ?>
                                                </div>
                                                <div class="col-md-2" style="padding-bottom:0px">
                                                <img src= <?=$image?> width ='128em' height= '128em' class='img-rounded' style ='margin-left:2em' alt=""> 

                                                </div>
                                 
                        </div>
                        <hr/>
                    </div>

                        <h1 align='center' style="font-size: 1.9em; font-weight: bolder;padding-top: 0%; ">
                            <?php
                          
                                $report_name = 'Exam Student Report';
                            ?>
                            <?= $report_name ?> (<?= strtoupper($classes->classes) . ' ' . $academic_year->name ?>)
                        </h1>
                    <div>

                    

                        <div class="panel-body container profile-view-dis" style="text-align: center; padding-top: .5%;">
                            <div class="row">
                                <div class="col-sm-5" style="float:left; color:black" >
                                    <p style="font-weight: bolder;">  <?='Name' ?>: <?= strtoupper($student->name) ?></p>
                                </div>
                                <?php if ($schema != 'stjohnbosco.') { ?>
                                    <div class="col-sm-4" style=" color:black" >
                                        <p style="font-weight: bolder;"> S/N: <?= $student->roll ?></p>
                                    </div>
                                <?php } ?>
                                <div class="col-sm-3" style="float:right; color:black" >
                                    <p style="font-weight: bolder;">
                                        <?php if (strtoupper($classes->classes) == 'FORM SIX' || strtoupper($classes->classes) == 'FORM FIVE') { ?>
                                            COMB :<?= $student_archive->section->section ?>
                                        <?php } else { ?>
                                       STREAM: 
                                            <?php
                                            echo 'A';
                                        }
                                        ?></p>
                                </div>
                      
                    </div>

                    <?php
                    if (empty($report)) {
                        echo '<p class="alert alert-info">Sorry, this Exam report has not yet been published</p>';
                    } else if (empty($marks)) {
                        echo '<p class="alert alert-info">Sorry, this Exam has not yet been published</p>';
                    } elseif (!empty($exams)) {
                        ?>

                        <div class="row table" style="margin:0 2%;">
                            <?php
                            //print_r($marks);
                            if ($marks && $exams) {

                                ?>

                                <div class="col-lg-12">

                                    <?php
                                    $sectionreport = $report;

                                    ?>
                                    <div class="row" style="margin: 2%;font-family: verdana,arial,sans-serif; padding:.9%; ">
                                        <?php
                                        if ($marks && $exams) {
                                            ?>

                                            <div class="col-lg-12 table-responsive" >


                                                <?php
                                                echo "<table  class=\"table table-striped table-bordered center\" style=\" margin-bottom: -.1%;margin-left:0\">";

                                                $f = 1;
                                                echo "<thead style='background-color:#b3b3b3; font-weight:bold !important'>";
                                                echo "<tr>";
                                                echo "<th>";
                                                echo strtoupper('Subject');
                                                echo "</th>";
                                                if ($exams_report_setting->show_marks == 1) {
                                                    echo "<th>";

                                                    echo strtoupper($exams->exam);
                                                    echo "</th>";
                                                }
                                                if (!empty($grades) && $f == 1) {
                                                    if ($exams_report_setting->show_subject_point == 1) {
                                                        echo "<th>";
                                                        echo 'Point';
                                                        echo "</th>";
                                                    }
                                                    if ($exams_report_setting->show_grade == 1) {
                                                        echo "<th>";
                                                        echo "grade";
                                                        echo "</th>";
                                                    }
                                                }
                                                echo "<th>";
                                                echo 'Grade Point';
                                                echo "</th>";




                                                if ($exams_report_setting->show_subject_pos_in_class == 1) {
                                                    echo "<th>";
                                                    echo 'P/C';
                                                    echo "</th>";
                                                }
                                                if ($exams_report_setting->show_subject_pos_in_section == 1) {
                                                    echo "<th>";
                                                    echo 'P/S';
                                                    echo "</th>";
                                                }
                                                echo "</tr>";

                                                echo "</thead>";

                                                echo "<tbody>";
                                                $point = [];

                                                foreach ($marks as $mark) {
                                                    if ($schema == 'motherofmercy.') {
                                                        $new_mark = "$mark->mark != 0 && $mark->mark != ''";
                                                    } else {
                                                        $new_mark = "$mark->mark != ''";
                                                    }

                                                    if ($exams->examID == $mark->examID && $new_mark) {
                                                        echo "<tr style='font-weight:bold'>";
                                                        echo "<td style='font-weight:bold !important'>";
                                                        echo strtoupper($mark->subject);
                                                        echo "</td>";
                                                        if ($exams_report_setting->show_marks == 1) {
                                                            echo "<td align='left'>";
                                                                echo $mark->mark;
                                                            echo "</td>";
                                                        }
                                                        if (!empty($grades)) {
                                                            $max_grade = [];
                                                            foreach ($grades as $grade) {
                                                                array_push($max_grade, $grade->gradeupto);
                                                                if ($grade->gradefrom <= round($mark->mark, 0) && $grade->gradeupto >= round($mark->mark, 0)) {
                                                                    if ($exams_report_setting->show_subject_point == 1) {
                                                                        echo "<td align='center'>";
                                                                        echo $grade->point;
                                                                        echo "</td>";
                                                                    }
                                                                    if ($exams_report_setting->show_grade == 1) {
                                                                        echo "<td>";
                                                                        echo $grade->grade;
                                                                        echo "</td>";
                                                                    }

                                                                    echo "<td>";
                                                                    echo $mark->mark == '' ? '' : $grade->note;
                                                                    echo "</td>";
                                                                    $sub = [
                                                                        'subject_mark' => round($mark->mark, 0),
                                                                        'point' => $grade->point,
                                                                        'penalty' => $mark->is_penalty,
                                                                        'pass_mark' => $mark->pass_mark,
                                                                        'is_counted_indivision' => $mark->is_counted_indivision
                                                                    ];
                                                                    array_push($point, $sub);
                                                                }
                                                            }
                                                        } else {
                                                            echo '<td></td><td></td>';
                                                        }

                                                        if ($exams_report_setting->show_subject_pos_in_class == 1) {
                                                            echo "<td align='center'>";

                                                            $subject_info = get_position($mark->student_id, $mark->examID, $mark->subjectID, $mark->classesID, null, $schema);
                                                          //  print_r($subject_info[0]->['rank']);
                                                            echo !empty($subject_info) ? $subject_info->rank : '';
                                                        echo "</td>";
                                                        }
                                                        if ($exams_report_setting->show_subject_pos_in_section == 1) {
                                                            $subject_info_insection = get_position($mark->student_id, $mark->examID, $mark->subjectID, $mark->classesID, $sectionID, $schema);
                                                            echo "<td align='center'>";
                                                            echo!empty($subject_info_insection) ? $subject_info_insection->rank : '';
                                                            echo "</td>";
                                                        }
                                                        echo "</tr>";
                                                    }
                                                }
                                                echo "</tbody>";
                                                echo "</table>";
                                                ?>

                                                <table class="table table-striped table-bordered" style="margin-bottom: 0;margin-left:0">
                                                    <thead>
                                                        <tr>
                                                            <th style="font-weight: bolder; width:29%;">Total:
                                                                <strong><?= $report->sum ?></strong> 
                                                                <?php
                                                                asort($max_grade);
                                                                $offset = isset($max_grade[0]) ? $max_grade[0] : 100;
                                                                $total_max = \collect(DB::select('select (subject_count * ' . $offset . ') as total_max from '.$schema.'.student_total_subject where student_id=' . $student->student_id . ' and academic_year_id=' . $academic_year->id))->first();
                                                                if ((int) $exams_report_setting->show_total_marks == 1 && !empty($total_max)) {
                                                                    ?>
                                                                    / <strong><?= $total_max->total_max ?></strong>
                                                            <?php } ?>
                                                            </th>

                                                            <th style="font-weight: bolder;width:26.6%;"> Average:
                                                                <strong><?= round($report->average, 1) ?></strong></th>
                                                            <?php if ($exams_report_setting->show_pos_in_class == 1) { ?>
                                                                <th style="font-weight: bolder">P/C: 
                                                                    <strong><?= $report->rank ?></strong> Out of <?= $total_students->count ?></th>
                                                                <?php } ?>
                                                                <?php if ($exams_report_setting->show_pos_in_stream == 1) { ?>
                                                                <th style="font-weight: bolder">P/S: <strong>
                                                                <?= $sectionreport->rank . ' </strong> Out Of ' . count($total_students_section) ?></th> 

                                                            <?php } ?>
                                                            <?php
                                                        if(preg_match('/level/i', strtolower($classlevel->name))){
                                                            if ($exams->show_division == 1 || $exams_report_setting->show_acsee_division == 1 || $exams_report_setting->show_csee_division == 1) {
                                                                $division = getDivisionBySort($point, $classlevel->result_format);
                                                                if (strtolower(trim($classlevel->result_format)) == 'cambridge') {
                                                                    echo '<th>' . cambridgeDivision($division[1]) . '</th>';
                                                                } else {
                                                                    ?>
                                                                    <th>Division: <?= $division[0] ?></th>
                                                                    <th>Point: <?= $division[1] ?></th>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                            ?>
                                                        </tr>
                                                    </thead>
                                                </table>

                                                <div style="float: left; width: 100%">
                                                    <table class="table table-striped table-bordered" style="  float: left; margin: 1% 0 0 0;">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="<?= count($grades) ?>" style="font-weight: bolder; text-align: center;">
                                                                    Grade
                                                                </th>
                                                            </tr>


                                                        <tbody>
                                                            <tr>
                                                                <?php foreach ($grades as $grade) { ?>
                                                                    <th style="font-weight: bolder; text-align: center;"><?= $grade->grade ?>
                                                                    </th>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <?php foreach ($grades as $grade) { ?>
                                                                    <td style="font-weight: bold; font-size: smaller; text-align: center;background-color: white;color: #000;"><?= $grade->gradefrom ?>
                                                                        - <?= $grade->gradeupto ?>
                                                                    </td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <th colspan="<?= count($grades) ?>" style="font-weight: bolder; text-align: center; "><strong>KEY:</strong>
                                                                    P/S:=Position in Section , P/C:=Position in Class
                                                                </th>
                                                            </tr>
                                                            <?php if ($schema == 'stjohnbosco.') { ?>
                                                                <tr>
                                                                    <th colspan="5" style="font-weight: bolder; text-align: center; "><strong>Grade Status:</strong>
                                                                        <?php
                                                                        $i = 1;
                                                                        foreach ($grades as $grade) {
                                                                            echo ' ' . $grade->grade . ' - ' . $grade->note . ' | ';
                                                                        }
                                                                        ?>

                                                                    </th>
                                                                </tr>


                                                        <?php } ?>

                                                        </tbody>

                                                    </table>
                                                </div>

                                                <?php
                                                /**
                                                 * 
                                                 * ----------------------------------------------
                                                 * 
                                                 * Report footer
                                                 * ----------------------------------------------
                                                 * 
                                                 * 
                                                 * 
                                                 */
                                                if ($exams_report_setting->show_all_signature_on_the_footer == 0) {
                                                    ?>        

                                                    <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                                        <thead>
                                                            <tr>
                                                                <th style="width:56%;text-align: left">
                                                                    <b style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px">
                                                                    <?= $exams_report_setting->class_teacher_remark ?>:</b><br> <?php
                                                                    if (!empty($character) && strlen($character->class_teacher_comment) > 1) {
                                                                        echo "<span class='' style='font-weight:100'>";
                                                                        echo $character->class_teacher_comment;
                                                                        echo "</span>";
                                                                    } else {

                                                                        foreach ($grades as $grade) {
                                                                            if ($grade->gradefrom <= round($report->average, 0) && $grade->gradeupto >= round($report->average, 0)) {
                                                                                echo "<span class='' style='font-weight:100'>";
                                                                                echo $grade->overall_academic_note;
                                                                                echo "</span>";
                                                                                break;
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </th>
                                                                <th style="text-align: left;"><?php if (isset($report_setting) && $report_setting->show_classteacher_name == 1) {
                                                                        ?>
                                                                        Class Teacher: <?= $class_teacher_signature->name ?>
                                                                        <br>Phone: <u><?= $class_teacher_signature->phone ?></u><br>
                                                                        <?php
                                                                        if ($class_teacher_signature->signature == NULL) {
                                                                            echo '<u>' . strtoupper($class_teacher_signature->name[0] . '.' . str_after($class_teacher_signature->name, ' ')[0]) . '</u>';
                                                                        } else {
                                                                            ?>
                                                                            <img src="<?= $class_teacher_signature->signature ?>" width="54"
                                                                                 height="32">
                                                                                 <?php
                                                                             }
                                                                         } else {
                                                                             ?>
                                                                        Shinature <br>
                                                                        <?php
                                                                        if ($class_teacher_signature->signature == NULL) {
                                                                            echo strtoupper($class_teacher_signature->name[0] . '.' . str_after($class_teacher_signature->name, ' ')[0]);
                                                                        } else {
                                                                            ?>
                                                                            <img src="<?= $class_teacher_signature->signature ?>" width="54"
                                                                                 height="32">
                                                                                 <?php
                                                                             }
                                                                         }
                                                                         ?>

                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 56%; text-align: left; "><b style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px"><?= $exams_report_setting->head_teacher_remark ?>:</b><br> <?php
                                                                    if (!empty($character) && strlen($character->head_teacher_comment) > 1) {
                                                                        echo "<span class='' style='font-weight:100'>";
                                                                        echo $character->head_teacher_comment;
                                                                        echo "</span>";
                                                                    } else {
                                                                        foreach ($grades as $grade) {
                                                                            if (isset($report) && !empty($report)) {
                                                                                if ($grade->gradefrom <= round($report->average, 0) && $grade->gradeupto >= round($report->average, 0)) {
                                                                                    echo "<span class='' style='font-weight:100'>";
                                                                                    echo $grade->overall_note;
                                                                                    echo "</span>";
                                                                                    break;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <?php
                                                                    if ($schema == 'enaboishu.') {
                                                                        ?>
                                                                        <p>
                                                                        <hr/>
                                                                        <b  style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px">Head Master comment: </b>Work Hard
                                                                        </p>
                                                                        <?php
                                                                        echo'';
                                                                    }
                                                                    ?>
                                                                </th>
                                                                <th> <?= $siteinfos->headname ?>'s<br/> Signature<br/></th>
                                                            </tr> 
                                                        </thead>
                                                    </table>
                                                    <?php
                                                    /**
                                                     * 
                                                     * ----------------------------------------------
                                                     * 
                                                     * End of Report footer with buttom signature
                                                     * ----------------------------------------------
                                                     * 
                                                     * 
                                                     * 
                                                     */
                                                } else {
                                                    ?>

                                                    <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                                        <thead>
                                                            <tr>
                                                                <th style="text-align: left;">
                                                                    <b style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px"><?= $exams_report_setting->class_teacher_remark ?>:</b><br> <?php
                                                                    if (!empty($character) && strlen($character->class_teacher_comment) > 1) {
                                                                        echo "<span class=''>";
                                                                        echo $character->class_teacher_comment;
                                                                        echo "</span>";
                                                                    } else {

                                                                        foreach ($grades as $grade) {
                                                                            if ($grade->gradefrom <= round($report->average, 0) && $grade->gradeupto >= round($report->average, 0)) {
                                                                                echo "<span class=''>";
                                                                                echo $grade->overall_academic_note;
                                                                                echo "</span>";
                                                                                break;
                                                                            }
                                                                        }
                                                                    }
                                                                    ?></th>

                                                            </tr>
                                                            <tr>
                                                                <th style=" text-align: left; ">
                                                                    <b style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px"><?= $exams_report_setting->head_teacher_remark ?>:</b><br> <?php
                                                                    if (!empty($character) && strlen($character->head_teacher_comment) > 1) {
                                                                        echo "<span class=''>";
                                                                        echo $character->head_teacher_comment;
                                                                        echo "</span>";
                                                                    } else {
                                                                        foreach ($grades as $grade) {
                                                                            if (isset($report) && !empty($report)) {
                                                                                if ($grade->gradefrom <= round($report->average, 0) && $grade->gradeupto >= round($report->average, 0)) {
                                                                                    echo "<span class='' >";
                                                                                    echo $grade->overall_note;
                                                                                    echo "</span>";
                                                                                    break;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                            <?php
                                                                if ($schema == 'enaboishu.' || $schema == 'jifunze.') {
                                                            ?>
                                                                        <p>
                                                                        <hr/>
                                                                        <b  style="vertical-align: top; color: black; font-weight: bolder; font-size: 14px">Head Master comment: </b>Work Hard
                                                                        </p>
                                                                <?php } ?>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                    <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                                        <thead>
                                                            <tr>
                                                                <th style="text-align: left;">
                                                                <?php 
                                                                    if (isset($report_setting->show_classteacher_name) && $report_setting->show_classteacher_name == 1) {
                                                                ?>
                                                                        Class Teacher: <?= $class_teacher_signature->name ?><br>
                                                                        <?php
                                                                        if ($class_teacher_signature->signature == NULL) {
                                                                            echo strtoupper($class_teacher_signature->name[0] . '.' . str_after($class_teacher_signature->name, ' ')[0]);
                                                                        } else {
                                                                            ?>
                                                                            <img src="<?= $class_teacher_signature->signature ?>" width="54"
                                                                                 height="32">
                                                                                 <?php
                                                                             }
                                                                         } else {
                                                                             ?>
                                                                        Signature<br>
                                                                        <?php
                                                                        if ($class_teacher_signature->signature == NULL) {
                                                                            echo strtoupper($class_teacher_signature->name[0] . '.' . str_after($class_teacher_signature->name, ' ')[0]);
                                                                        } else {
                                                                            ?>
                                                                            <img src="<?= $class_teacher_signature->signature ?>" width="54"
                                                                                 height="32">
                                                                                 <?php
                                                                             }
                                                                         }
                                                                         ?>
                                                                </th>

                                                                <th style="width: 50%; float:left;"> <?= $siteinfos->headname ?>'s<br/> Signature<br/></th>
                                                            </tr> 
                                                        </thead>
                                                    </table>

                                                    <?php
                                                    /**
                                                     * 
                                                     * -----------------------------------------
                                                     * 
                                                     * End of footer with both signature at the bottom
                                                     * ------------------------------------------
                                                     */
                                                }
                                                ?>

                                                <div style="padding-left:5%;">

                                                    <div style="z-index: 4000">
                                                        <div>

                                                            <?php
                                                            $path = "storage/uploads/images/" . $classlevel->stamp . "?v=1";
                                                            ?>
                                                            <?php
                                                            echo "<img src=".$path." width= '100' height = '100' style = 'position:relative; margin:-12% 10% 0 0; float:right;'>";
                                                            ?>

                                                        </div>
                                                    </div>

                                                </div>
                                    <?php if (isset($report_setting->show_parent_comment) && $report_setting->show_parent_comment == 1) { ?>
                                                    <div style=" width: 100%">
                                                        <table class="table table-striped table-bordered" style="  float: left; margin: 1% 0 0 0;">
                                                            <thead>
                                                                <tr>
                                                                    <th colspan="4" style="font-weight: bolder; text-align: left;">
                                                                        Parent Comment:
                                                                        <br><br><br><br><br><br>

                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <th colspan="2">
                                                                        <u> Parent Name</u>
                                                                        <br> <br>.............................................................................
                                                                    </th> 
                                                                    <th>
                                                                        <u> Signature</u>
                                                                        <br> <br>.....................................
                                                                    </th> 
                                                                    <th>
                                                                        <u>  Date</u>
                                                                        <br> <br> ...../...../...........
                                                                    </th> 

                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                            <?php } ?>
      
                                            <?php 
                                            /*
                                             if(!empty($invoice)){
                                                $unpaid = $total_amount - $total_paid;
                                                $bank_accounts = \App\Model\BankAccount::limit(2)->get();
                                                if(!empty($invoice) && $unpaid > 0 && $schema == 'shulesoft.'){
                                            ?>
                                                <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                                    <thead>
                                                        <tr>
                                                            <th>Total {{ $academic_year->name }} Invoice Amount: is <?= money($total_amount) ?></th>
                                                            <th colspan="1">Total Paid Amount is: <?= money($total_paid) ?></th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="2" style="text-align: center">
                                                                <strong>Total Unpaid Amount on {{ $academic_year->name }} is: <?= $unpaid > 0 ? money($unpaid) : '0' ?>/=</strong>
                                                            </th>      
                                                        </tr>
                                                        <tr>
                                                            <th colspan="2"><b>Please pay before opening date, use this control number #<?=$invoice->reference?> </b> </th>
                                                        </tr>
                                                       
                                                        <tr>
                                                            @foreach($bank_accounts as $account)
                                                            <th><?=$account->name?> (<?=$account->number?>) - <?=$account->referBank->abbreviation?></th>
                                                            @endforeach
                                                        </tr>
                                                         
                                                    </thead>
                                                </table>     
                                                <?php } } */ ?>

                                                <?php if (!empty($exam_report) && date('dmY', strtotime($exam_report->reporting_date)) != '01011970') { ?>

                                                    <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                                                        <thead>
                                                            <tr>
                                                                <th style="text-align: center;">
                                                                        <b><?php echo "Date of Reporting: " . date('jS M Y', strtotime($exam_report->reporting_date)) ?> </b>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                    <?php } ?>
                                                </div>
                                    <?php } ?>                                        
                                    </div>
                                </div>
                        <?php } ?>
                    </section>
                      
                        <?php
                    }
                } //close if no grades defined
                ?>

                    </div>
                </div>
            </div>
            <!-- Server Side Processing table end -->
        </div>
    </div>
</div>
  <!-- Required Jquery -->
  <script type="text/javascript" src="<?=$root?>/bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?=$root?>/bower_components/jquery-ui/js/jquery-ui.min.js"></script>

<script>

</script>
@endsection