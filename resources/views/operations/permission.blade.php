@extends('layouts.app')
@section('content')

<div class="page-body">
<div class="row">
 <div class="col-sm-12">


        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                <div class="card-header">
    
                <div class="form-group has-success row">
                    <div class="col-sm-5">
                        <label class="col-form-label" for="inputSuccess1">Select user Role
                        </label>
                    </div>
                    <div class="col-sm-7">
                        <select class="form-control"  id='permission'>
                        <option></option>
                        <?php foreach ($user_roles as $u_role) { ?>
                        <option value="<?= $u_role->id ?>" <?= (int) request('id') > 0 && request('id') == $u_role->id ? 'selected' : '' ?> ><?= $u_role->name  ?></option>
                        <?php } ?>
                    </select>
                    </div>
                </div>

                    </div>
                    <div class="card-block accordion-block">
                        <div id="accordion" role="tablist" aria-multiselectable="true">

                        @foreach ($Permissionsgroup as $key => $group)
                            <div class="accordion-panel">
                                <div class="accordion-heading" role="tab" id="headingOne">
                                    <h3 class="card-title accordion-title">
                                    <a class="accordion-msg" data-toggle="collapse" data-parent="#accordion" href="#collapseOne_{{ $group->id }}" aria-expanded="true" aria-controls="collapseOne">
                                        <?= $group->name ?>
                                    </a>
                                </h3>
                                </div>
                                <div id="collapseOne_{{ $group->id }}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">

                                    @foreach($group->permissions()->get() as $sub)
                                    <div class="accordion-content accordion-desc">
                                        <p>
                                            <?php
                                            $check = \App\Models\PermissionRole::where('role_id', $set)->where('permission_id', $sub->id)->first();
                                                    !empty($check) ? $checked = 'checked' : $checked = '';
                                                ?>
                                            <div class="checkbox-fade fade-in-primary">
                                                <label>
                                                <input type="checkbox" {{ $checked }} value="{{ $sub->id }}" id="permission_{{ $sub->id }}" onclick="submit_role(this)">
                                                <span class="cr">
                                                    <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                                </span>
                                                <span> {{ $sub->description }}</span>
                                            </label>
                                        </div>

                                        
                                        </p>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            @endforeach
                        
                
                        </div>
                    </div>
                </div>
            </div>
        
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        
                          <div class="d-flex justify-content-between">
                            <h5 class="card-header-text">Roles</h5>
                                <?php if(can_access('add_role')){?>
                                    <button type="button" class="btn btn-info waves-effect" data-toggle="modal" 
                                       data-target="#default-Modal"> Add role</button>
                                <?php } ?>

                                   <div class="modal fade" id="default-Modal" tabindex="-1" role="dialog">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Create role</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>
                                            <form method="POST" action="<?= url('users/storeroles') ?>">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="name" placeholder="Name" required>
                                                     @if ($errors->has('name'))
                                                       <span class="text-danger">{{ $errors->first('name') }}</span>
                                                      @endif
                                                </div>
                                            
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="description" placeholder="Description" required>
                                                     @if ($errors->has('description'))
                                                          <span class="text-danger">{{ $errors->first('description') }}</span>
                                                      @endif
                                                </div>
                                                      
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary waves-effect waves-light ">Save changes</button>
                                            </div>
                                             <?= csrf_field() ?>
                                          </form>
                                        </div>
                                       </div>
                                  </div>
                        </div>

                    </div>
                    <div class="card-bltock taccordion-block">
                        <div class="accordion-boxt" id="single-open">
                              <div class="card-block">
                                <div class="dt-responsive table-responsive">
                                <table id="lang-dt" class="table table-striped table-bordered table-sm">
                                    <thead>
                                     <tr>
                                        <th>#</th>
                                        <th>Role</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1;foreach ($roles as $key => $value) { ?>
                                        <tr>
                                          <td><?= $i ?></td>          
                                          <td><?= $value->name ?></td>          
                                        </tr>
                                    <?php $i++; } ?>
                                   </tbody>
                                
                                </table>
                                </div>
                             </div>
                        
                        </div>
                    </div>
                </div>
            </div>
        
        </div>







     



  </div>
 </div>
</div>

<script type="text/javascript">

$('#permission').change(function(event) {
    var id = $(this).val();
    if (id === '') {} else {
        window.location.href = '<?= url('users/permissions') ?>/' + id;
    }
});


function submit_role(permission) {
    var perm_id = permission.value;
    var role = '<?=$set?>';
    if(!permission.checked){
        var url_obj = "<?= url('users/removepermission') ?>";
    } else {
        var url_obj = "<?= url('users/storepermission') ?>";
    }
    $.ajax({
        url: url_obj,
        method: 'post',
        data: { "_token": "{{ csrf_token() }}", perm_id: perm_id,role_id: role},
        success: function(data) {
           // alert(data);
        }
    });
}

</script>

@endsection