@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">

        <form style="" class="form-horizontal" role="form" method="post">
            <div class="row">
                <div class="col-sm-12 col-xl-4 m-b-30">
                      <h4 class="sub-title">School</h4>
                      <select id="schema" class="form-control form-control-primary">
                        <option value="opt1">Select One School</option>
                                  @if (count(load_schemas()) > 0)
                                  @foreach (load_schemas() as $schema)
                                      <option value="{{ $schema->username }}"> {{ school($schema->username)->name }}</option>
                                  @endforeach
                                  @endif
                        </select>
                  </div>
               
                  <div class="col-sm-12 col-xl-4 m-b-30">
                      <h4 class="sub-title">Class</h4>
                      <select name="select" id="classes_id" class="form-control form-control-primary">

                      </select>
                  </div>
                  <?= csrf_field() ?>
              </form>
              </div>

             <?php if( isset($male) && isset($female) && isset($all))   {   ?>
              <div class="">
                <div class="table-responsive dt-responsive">
                     <table id="" class="table table-striped table-bordered nowrap">
                       <thead>
                           <tr>
                               <th>School name</th>
                               <?php if(isset($class_name)) { ?>
                               <th>Class name</th>
                               <?php } else { ?>
                               <th>All classes</th>
                                <?php } ?>
                               <th>Men</th>
                               <th>Women</th>
                               <th>Total </th>
                           </tr>
                         </thead>
                         <tbody>
                             <tr>
                               <th><?= isset($school_name) ? $school_name : ''  ?></th>
                               <?php if(isset($class_name)) { ?>
                               <th><?=$class_name ?></th>
                               <?php } else { ?>
                               <th><?=$class_name ?></th>
                                <?php } ?>
                               <th><?=$male ?></th>
                               <th><?=$female ?></th>
                               <th><?=$all?></th>
                             </tr>
                         </tbody>
                   </table>
                  </div>
                </div>
             <?php  } ?>
             
             <?php if( isset($parents) && (!empty($parents))) { ?>
                <div class="table-responsive dt-responsive">
                    <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Photo</th> 
                                <th>Parent name</th>
                                <th>Relation</th>                               
                                <th>Email</th>
                                <th>Phone</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                                <?php $i = 1; foreach ($parents as $value) { ?>
                                    <tr>
                                        <td><?= $i ?></td>
                                        <td>
                                            <?php
                                            $array = array(
                                                "src" => url('storage/uploads/images/' . $value->photo),
                                                'width' => '35px',
                                                'height' => '35px',
                                                'class' => 'img-rounded rotate'
                                            );
                                            echo img($array);
                                            ?>
                                        </td>
                                        <td><?= $value->name ?></td>
                                        <td><?= $value->relation ?></td>
                                        <td><?= $value->email ?></td>
                                        <td><?= $value->phone ?></td>
                                        <td>
                                        <a type="button" class="btn btn-info btn-sm btn-out-dotted" 
                                            href="<?= url('Users/prt_profile/' .$schema_name .'/'.$value->parentID) ?>">
                                            View
                                        </a>
                                        </td>
                                    </tr>
                              <?php $i++; } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Photo</th> 
                                <th>Parent name</th>
                                <th>Relation</th>                               
                                <th>Email</th>
                                <th>Phone</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
             <?php } ?>
        </div>
    </div>
    <!-- Server Side Processing table end -->

<script type="text/javascript">
       $('#schema').change(function (event) {
            var schema = $(this).val();
            if (schema === '0') {
            } else {
                $.ajax({ 
                    // method: 'get',
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '<?= url('Users/getClasses') ?>',
                    data: {schema: schema},
                    dataType: "html", 
                    cache: false,
                    success: function (data) { 
                       $('#classes_id').html(data);
                    }
                });

                $('#classes_id').change(function () {
                var classes_id = $(this).val();
                var schema = $('#schema').val();
                if (classes_id == 0 || schema == '') {
                    $('#hide-table').hide();
                    $('.nav-tabs-custom').hide();
                  } else {
                     window.location.href = "<?= url('Users/parent') ?>/" + schema + '/' + classes_id;
                 }
              });
            }
        });

    </script>

@endsection