
@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>

                          <div class="page-body">
                                    <!--profile cover start-->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="cover-profile">
                                                <div class="profile-bg-img"> 
                                                    <img class="profile-bg-img img-fluid" style="height: 280px;" src="<?=$root?>/assets/images/shulesoft.JPG" alt="bg-img">
                                                    <div class="card-block user-info">
                                                      
                                                        <div class="col-md-12">
                                                            <div class="media-left">
                                                                <?php 
                                                                if (strpos($parent->photo, 'https:') !== false) {
                                                                    $src = $parent->photo;
                                                                } elseif (is_file('storage/uploads/images/' . $parent->photo)) {
                                                                    $image = 'storage/uploads/images/' . $parent->photo;
                                                                    $src = url($image);
                                                                } else {
                                                                    $image = 'storage/uploads/images/defualt.png';
                                                                    $src = url($image);
                                                                }
                                    
                                                                ?>
                                                                <a href="#" class="profile-image">
                                                                    <img class="user-img img-radius" style="height: 150px;" src="<?php echo $src; ?>"  alt="ShuleSoft" title="{{$parent->name}}">
                                                                </a>
                                                            </div>
                                                         
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--profile cover end-->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <!-- tab header start -->
                                            <div class="tab-header card">
                                                <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Basic Info</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#students" role="tab">
                                                           Students</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#activities" role="tab">
                                                            Payments</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#sms" role="tab">SMS</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="tab-content">
                                                <div class="tab-pane active" id="personal" role="tabpanel">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <h3 class="card-header-text">Parent profile</h3>
                                                        </div>
                                                        <div class="card-block">
                                                            <div class="view-info">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="general-info">
                                                                            <div class="row">
                                                                                <div class="col-lg-12 col-xl-6">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table table-responsive m-b-0">
                                                                                            <tr>
                                                                                                <th class="">Parent full Name
                                                                                                </th>
                                                                                                <td class=""><?= $parent->name ?></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <th class="">Relation</th>
                                                                                                <td class=""><?=$parent->relation?></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <th class="">Proffesion</th>
                                                                                                <td class=""><?= $parent->relation == 'Father' ? $parent->father_profession : $parent->mother_profession ?></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <th class="">Employer Name</th>
                                                                                                <td class=""><?=$parent->employer ?></td>
                                                                                            </tr>
                                                                                             <tr>
                                                                                                <th class="">Phone</th>
                                                                                                <td class=""><?=$parent->phone ?></td>
                                                                                               </tr>
                                                                                            
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- end of table col-lg-6 -->
                                                                                <div class="col-lg-12 col-xl-6">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <th class="">Email</th>
                                                                                                      <td class=""><?=$parent->email ?></td>
                                                                                                   </tr>
                                                                                                   <tr>
                                                                                                     <th class="">Address</th>
                                                                                                      <td class=""> <?=$parent->address ?> </td>
                                                                                                 </tr>
                                                                                                  <tr>
                                                                                                     <th class="">Preferred Language</th>
                                                                                                      <td class=""><?=$parent->language?> </td>
                                                                                                 </tr>
                                                                                                   <tr>
                                                                                                     <th class="">Sex</th>
                                                                                                      <td class=""> <?=$parent->sex?>  </td>
                                                                                                 </tr>
                                                                                              
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- end of table col-lg-6 -->
                                                                            </div>
                                                                            <!-- end of row -->
                                                                        </div>
                                                                        <!-- end of general info -->
                                                                    </div>
                                                                    <!-- end of col-lg-12 -->
                                                                </div>
                                                                <!-- end of row -->
                                                            </div>                                                         
                                                        </div>
                                                    </div>
                                                </div>


                                                 <div class="tab-pane" id="students" role="tabpanel">
                                                     <div class="card">
                                                        <div class="card-header">
                                                            <h4 class="card-header-text">Students belongs to </h5>
                                                        </div>
                                                        <div class="card-block">
                                                            <div class="view-info">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="general-info">
                                                                            <div class="row">
                                                                                <div class="col-lg-12 col-xl-12">
                                                                                    <div class="table-responsive">
                                                                                        <?php if(isset($students)) { ?>
                                                                                        <table id="" class="table table-striped table-bordered nowrap">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th>#</th>
                                                                                                    <th>Photo</th>
                                                                                                    <th>Name</th>
                                                                                                    <th>Class </th>
                                                                                                    <th>Roll</th>
                                                                                                    <th>Action</th>
                                                                                                </tr>
                                                                                              </thead>
                                                                                             <tbody>
                                                                                               
                                                                                                <?php $i=1; foreach ($students as $student) { ?>
                                                                                                  <tr>
                                                                                                    <th><?=$i?></th>
                                                                                                    <th><?=$student->photo?></th>
                                                                                                    <th><?=$student->name?></th>
                                                                                                    <th><?=$student->classes?></th>
                                                                                                    <th><?=$student->roll?></th>
                                                                                                    <th>
                                                                                                        <a  type="button" href="<?= url('Users/std_profile/' .$schema .'/'.$student->student_id) ?>" class="btn btn-sm btn-primary waves-effect waves-light f-right">
                                                                                                            View   
                                                                                                       </a>
                                                                                                    </th>
                                                                                                  </tr>
                                                                                                <?php $i++;} ?>
                                                                                            </tbody>
                                                                                       </table>
                                                                                       <?php } ?>
                                                                                    </div>
                                                                             
                                                                            </div> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                                <div class="tab-pane" id="activities" role="tabpanel">
                                                    <div class="row">
                                                        <div class="col-xl-12">
                                                            <div class="row">

                                                                <div class="col-sm-12">
                                                                    <!-- contact data table card start -->
                                                                    <div class="card">
                                                                        <div class="card-header">
                                                                            <h4 class="card-header-text">View All invoices</h5>
                                                                        </div>
                                                                        <div class="card-block contact-details">
                                                                           <?php $i = 1; foreach ($students as $stud) { ?>
                                                                            <h5> <?= $stud->name ?> </h5>
                                                                            <div class="data_table_main table-responsive dt-responsive">
                                                                                <table id="" class="table  table-striped table-bordered nowrap">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>#</th>
                                                                                            <th>Invoice No</th>
                                                                                            <th>Total amount </th>
                                                                                            <th>Total Paid</th>
                                                                                            <th>Unpaid</th>
                                                                                            <th>Payment status</th>
                                                                                            <th>Payment due</th>
                                                                                            <th>Action</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php
                                                                                        $f = 1;
                                                                                        
                                                                                        $student_archives = DB::table($schema.'.student_archive')->where('student_id', $stud->student_id)->get();
                                                                                        foreach ($student_archives as $archive) {
                                
                                                                                            $invoices = \DB::table($schema.'.invoice_balances')->join($schema.'.invoices', 'invoices.id', 'invoice_balances.invoice_id')->where($schema.'.invoice_balances.student_id', $archive->student_id)->where($schema.'.invoice_balances.academic_year_id', $archive->academic_year_id)->select(DB::raw('coalesce(coalesce(sum(total_amount),0)-sum(discount_amount),0) as amount, coalesce(coalesce(sum(total_payment_invoice_fee_amount),0)+ coalesce(sum(total_advance_invoice_fee_amount)),0) as paid_amount, sum(balance) as balance,invoices.id, invoice_id,invoice_balances.student_id,reference'))->groupBy('invoice_id', 'invoice_balances.student_id', 'reference', 'invoices.id')->get();
                                                                                            foreach ($invoices as $invoice) {
                                                                                                ?>
                                                                                                <tr> <td><?= $i ?></td>
                                                                                                    <th scope="row"><?= $invoice->reference ?></th>
                                                                                                    <!--<td><?php // date('d M Y', strtotime($invoice->date))  ?></td>-->
                                                                                                    <td>  
                                                                                                        <?php
                                //                                                                    $fees = $invoice->invoicesFeesInstallments()->get();
                                //                                                                    $total_amount = 0;
                                //                                                                    foreach ($fees as $amount) {
                                //                                                                        $total_amount += $amount->amount;
                                //                                                                    }
                                                                                                        // echo money($total_amount);
                                                                                                        $total_amount = $invoice->amount;
                                                                                                        echo money($total_amount);
                                                                                                        ?></td>
                                                                                                       <td>
                                                                                                        <?php
                                //                                                                   
                                                                                                        $paid_total_amount = $invoice->paid_amount;
                                                                                                        echo money($paid_total_amount);
                                                                                                        ?>
                                                                                                      </td>
                                                                                                     <td>
                                                                                                        <?php
                                                                                                        echo money($total_amount - $paid_total_amount);
                                                                                                        ?>
                                                                                                     </td>
                                                                                                    <td> <?php
                                                                                                        $status = '1';
                                                                                                        $setstatus = '';
                                                                                                        $btn_class = 'success';
                                                                                                        if ($paid_total_amount == '0') {
                                                                                                            $check_status = '0';
                                                                                                            $status = 'invoice not paid';
                                                                                                            $btn_class = 'danger';
                                                                                                        } elseif ($paid_total_amount > 0 && $paid_total_amount < $total_amount) {
                                                                                                            $check_status = '2';
                                                                                                            $status = 'Invoice partially paid';
                                                                                                            $btn_class = 'warning';
                                                                                                        } elseif ($paid_total_amount >= $total_amount) {
                                                                                                            $check_status = '3';
                                                                                                            $status = 'invoice fully paid';
                                                                                                            $btn_class = 'info';
                                                                                                        }
                                
                                                                                                        echo "<button class='btn btn-" . $btn_class . " btn-sm'>" . $status . "</button>";
                                                                                                        ?>
                                                                                                      </td>
                                                                                                      <td>
                                                                                                        <?php 
                                                                                                            $payment = \DB::table($schema.'.invoices')->where('id',$invoice->id)->select('invoices.*')->first();
                                                                                                               $i_due_date = date('Y-m-d', strtotime($payment->due_date));
                                                                                                            if(is_null($payment->due_date)){  
                                                                                                                //Update due date by adding 30 days to a date
                                                                                                                $due_date =  date('Y-m-d', strtotime($payment->date. ' + 30 days'));  
                                                                                                                \DB::table($schema.'.invoices')->where('id',$invoice->id)->update(['invoices.due_date' => $due_date]);
                                                                                                                $payment = \DB::table($schema.'.invoices')->where('id',$invoice->id)->select('invoices.due_date')->first();
                                                                                                                $i_due_date = date('Y-m-d', strtotime($payment->due_date));
                                                                                                              }
                                                                                                            
                                                                                                            ?>
                                                                                                           
                                                                                                         <?= $i_due_date ?>                                                                                                        
                                                                                                    </td>
                                                                                                    <td> 
                                                                                                        {{-- <a  type="button" href="<?= url('invoices/show/' .$schema .'/'. $invoice->id .'/'. $archive->academic_year_id) ?>" class="btn btn-sm btn-primary waves-effect waves-light f-right">
                                                                                                            View   
                                                                                                       </a> --}}
                                                                                                    </td>
                                                                                                  </tr>
                                                                                                <?php
                                                                                                $f++;
                                                                                            }
                                                                                        }
                                                                                        ?>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <?php } ?>
                                                                        </div>
                                                                        
                                                                    </div>

                                                                </div>
                                                            </div>
                                              
                                                        </div>
                                                    </div>
                                                </div>


                                               
                                                <div class="tab-pane" id="sms" role="tabpanel">
                                                    <div class="row">
                                                              <div class="col-sm-12">
                                                              <div class="card">
                                                            <div class="card-header">
                                                                <h5 class="card-header-text">Sent SMS</h5>
                                                            </div>
                                                            <div class="card-block">
                                                                <ul class="media-list">
                                                                <?php
                                                                    if (isset($messages)) {
                                                                        foreach ($messages as $message) {
                                                                            if ($message->is_sent == 1) {
                                                                ?>
                                                                    <li class="media">
                                                                        <div class="media-left">
                                                                            <a href="#">
                                                                                <img class="media-object img-radius comment-img" src="<?= url("storage/default.png"); ?>" alt="Generic placeholder image">
                                                                            </a>
                                                                        </div>
                                                                        <div class="media-body">
                                                                            <h6 class="media-heading"><?= isset($message->user_id) ? parent_name($message->user_id,$schema) : '' ?><span class="f-12 text-muted m-l-5"><?= timeAgo($message->created_at) ?></span></h6>
                                                                            
                                                                            <p class="m-b-0"><?= $message->body ?></p>
                                                                            <div class="m-b-25">
                                                                                <span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span>
                                                                            </div>
                                                                            <hr>
                                                                           
                                                                        </div>
                                                                    </li>
                                                                    <?php
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                    </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                                          
                                                              </div>
                                                        </div>
                                                   </div>

                                          </div>
                                       </div>
                                    </div>
                                </div>
                         @endsection