@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">

        <form style="" class="form-horizontal" role="form" method="post">
            <div class="row">
                <div class="col-sm-12 col-xl-4 m-b-30">
                      <h4 class="sub-title">School</h4>
                      <select id="schema" class="form-control form-control-primary">
                        <option value="opt1">Select One School</option>
                                  @if (count(load_schemas()) > 0)
                                  @foreach (load_schemas() as $schema)
                                      <option value="{{ $schema->username }}"> {{ school($schema->username)->name }}</option>
                                  @endforeach
                                  @endif
                        </select>
                  </div>
               
                  <div class="col-sm-12 col-xl-4 m-b-30" id="hide-form1">
                      <h4 class="sub-title">Class</h4>
                      <select name="select" id="classes_id" class="form-control form-control-primary">

                      </select>
                  </div>
                  
                  <div class="col-sm-12 col-xl-4 m-b-30" id="hide-form">
                      <h4 class="sub-title">Academic Year</h4>
                      <select name="select" id="academic_year_id" class="form-control form-control-primary">
                         
                      </select>
                  </div>
                  
                  <?= csrf_field() ?>
              </form>
            </div>

        
          <?php if(!isset($all_class) && (empty($all_class)))  { ?>

             <?php if(isset($male) && isset($female) && isset($all))   {   ?>
                <div class="table-responsive dt-responsive">
                     <table id="" class="table table-striped table-bordered nowrap">
                       <thead>
                           <tr>
                               <th>School name</th>
                               <th>Academic year</th>
                               <th>Class</th>
                               <th>Boys</th>
                               <th>Girls</th>
                               <th>Total </th>
                           </tr>
                         </thead>
                         <tbody>
                             <tr>
                               <th class="text-center"><?= isset($school_name) ? $school_name : ''  ?></th>
                               <th class="text-center"><?=$year ?></th>
                               <th class="text-center"><?=$class_name ?></th>
                               <th class="text-center"><?=$male ?></th>
                               <th class="text-center"><?=$female ?></th>
                               <th class="text-center"><?=$all?></th>
                             </tr>
                         </tbody>
                   </table>
                  </div>
             <?php  } ?>

             <?php if( isset($students) && (!empty($students))) { ?>
                <div class="table-responsive dt-responsive">
                    <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Photo</th>
                                <th>Name</th>
                                <th>Roll/RegNo</th>
                                <th>Class</th>
                                <th>Section/Stream</th>
                                <th>Date of birth</th>
                                <th>Gender</th>
                                <th>Academic year</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                                <?php $i = 1; foreach ($students as $value) { ?>
                                    <tr>
                                        <td><?= $i ?></td>
                                        <td>
                                            <?php
                                            $array = array(
                                                "src" => url('https://'.$schema_name.'.shulesoft.com/storage/uploads/images/' . $value->photo),
                                                'width' => '35px',
                                                'height' => '35px',
                                                'class' => 'img-rounded rotate'
                                            );
                                            echo img($array);
                                            ?>
                                        </td>
                                   
                                        <td><?= $value->name ?></td>
                                        <td><?= $value->roll ?></td>
                                        <td><?= $value->classes_name ?></td>
                                        <td><?= $value->section_name ?></td>
                                        <td><?= $value->dob ?></td>
                                        <td><?= $value->sex ?></td>
                                        <td><?= $value->year ?></td>
                                        <td>
                                          <a type="button" class="btn btn-info btn-sm btn-out-dotted" 
                                            href="<?= url('users/std_profile/'.$schema_name . '/'. $value->student_id) ?>">
                                            View
                                        </a>
                                       
                                        </td>
                                    </tr>
                              <?php $i++; } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Photo</th>
                                <th>Name</th>
                                <th>RegNo</th>
                                <th>Class</th>
                                <th>Section/Stream</th>
                                <th>Date of birth</th>
                                <th>Gender</th>
                                <th>Academic year</th>
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
              <?php } ?>
            <?php }  ?>
    
        </div>
    </div>
    <!-- Server Side Processing table end -->
</div>


<script type="text/javascript">
       $('#schema').change(function (event) {
            var schema = $(this).val();
            if (schema === 'All') {
               // window.location.href = "<?= url('Users/all_students/all') ?>";
              } else {
                $.ajax({ 
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '<?= url('Users/getClasses') ?>',
                    data: {schema: schema},
                    dataType: "html", 
                    cache: false,
                    success: function (data) { 
                       $('#classes_id').html(data);
                    }
                });
    
            }
        });

        $('#classes_id').change(function(event) {
        var schema_name = $('#schema').val();
        var classesID = $(this).val();
        if (classesID === '0') {
            $('#academic_year_id').val(0);
            $('#report_filter_div').hide();
        } else {
            $('#report_filter_div').show();
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/get_academic_years') ?>",
                data: "_token=" + "{{ csrf_token() }}" + "&id=" + classesID + "&schema_name=" + schema_name,
                dataType: "html",
                success: function(data) {
                    $('#academic_year_id').html(data);
                }
            });
        }
    });

        $('#academic_year_id').change(function () {
        var academic_year_id = $(this).val();
        var schema = $('#schema').val();
        var classes_id = $('#classes_id').val();
        if (classes_id == 0 || academic_year_id == 0) {
            // $('#hide-table').hide();
            // $('.nav-tabs-custom').hide();
        } else {
            window.location.href = "<?= url('Users/student') ?>/" + schema + '/' + academic_year_id + '/' + classes_id;
        }
    });

    </script>

@endsection