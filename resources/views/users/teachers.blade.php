@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">

        <form style="" class="form-horizontal" role="form" method="post">
            <div class="row">
                <div class="col-sm-12 col-xl-4 m-b-30">
                      <h4 class="sub-title">School</h4>
                      <select id="schema" class="form-control form-control-primary">
                        <option value="opt1">Select One School</option>
                                  @if (count(load_schemas()) > 0)
                                  @foreach (load_schemas() as $schema)
                                      <option value="{{ $schema->username }}"> {{ school($schema->username)->name }}</option>
                                  @endforeach
                                  @endif
                        </select>
                  </div>
    
                  <?= csrf_field() ?>
              </form>
              </div>

             <?php if( isset($male) || isset($female) || isset($all))   {   ?>
              <div class="">
                <div class="table-responsive dt-responsive">
                     <table id="" class="table table-striped table-bordered nowrap">
                       <thead>
                           <tr>
                            <th>School name</th>
                            <th>Men</th>
                            <th>Women</th>
                            <th>Total </th>
                           </tr>
                         </thead>
                         <tbody>
                             <tr>
                               <th><?= isset($school_name) ? $school_name : ''  ?></th>
                               <th><?=$male ?></th>
                               <th><?=$female ?></th>
                               <th><?=$all?></th>
                             </tr>
                         </tbody>
                   </table>
                  </div>
                </div>
             <?php  } ?>

                <div class="table-responsive dt-responsive">
                    <?php if( isset($teachers) && (!empty($teachers))) { ?>
                    <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Photo</th>                                  
                                <th>Name</th>
                                <!-- <th>ShuleSoft ID</th>                      -->
                                <th>Employment type</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; foreach ($teachers as $value) { ?>
                                <tr>
                                    <td><?= $i ?></td>
                                    <td>
                                        <?php
                                        $array = array(
                                            "src" => url('https://'.$this_schema.'.shulesoft.com/storage/uploads/images/' . $value->photo),
                                            'width' => '35px',
                                            'height' => '35px',
                                            'class' => 'img-rounded rotate'
                                        );
                                        echo img($array);
                                        ?>
                                    </td>
                                    <td><?= $value->name ?></td>
                                    <td><?= $value->designation ?></td>
                                    <td><?= $value->email ?></td>
                                    <td><?= $value->phone ?></td>
                                    <td>
                                    <a type="button" class="btn btn-info btn-sm btn-out-dotted" 
                                        href="<?= url('Users/teacherprofile/' .$this_schema .'/'.$value->teacherID) ?>">
                                        View
                                    </a>
                                    </td>
                                </tr>
                        <?php $i++; } ?>
                         
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Photo</th>                                  
                                <th>Name</th>                 
                                <th>Employment type</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </tfoot>
                    </table>
                    <?php } ?>
                </div>
        </div>
    </div>
    <!-- Server Side Processing table end -->
</div>


<script type="text/javascript">
     
       $('#schema').change(function (event) {
            var schema = $(this).val();
            if (schema === '0') {
            } else {
                window.location.href = "<?= url('Users/teacher') ?>/" + schema;
            }
        });


    </script>

@endsection