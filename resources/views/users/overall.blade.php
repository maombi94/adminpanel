@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">

          <form style="" class="form-horizontal" role="form" method="post">
            <div class="row">
                <div class="col-sm-12 col-xl-4 m-b-30">
                      <h4 class="sub-title">Overall <?= isset($name) ? $name : '' ?></h4>
                      <select id="overall" class="form-control form-control-primary">
                            <option value=""> Select </option>
                             <?php $i = 0; foreach($all as $value) { ?>
                                 <option value="<?= $value?>"><?=$value?></option>
                             <?php } ?>
                      </select>
                  </div>
                </div>
                  <?= csrf_field() ?>
              </form>
            </div>


            <?php if(isset($students) && (!empty($students))) {  ?>
                <div class="col-lg-12 col-xl-12">
                  <div class="row">
                        <div class="card-block">
                            <div class="row">
                                <div class="col-lg-12 col-xl-12">
                                    
                                    <ul class="nav nav-tabs  tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">Home</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">Summary</a>
                                        </li>
                                    </ul>

                                    <div class="tab-content tabs card-block">
                                        <div class="tab-pane active" id="home1" role="tabpanel">
                                            <div class="table-responsive dt-responsive">
                                                <table id="dom-jqry" class="table table-striped table-bordered nowrap table-md">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>                                                      
                                                            <th>Name</th>                                                     
                                                            <th>Roll/RegNo</th>
                                                            <th>Date of birth</th>
                                                            <th>Gender</th>
                                                            <th>School Name</th>
                                                            <th class="text-center">Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $i = 1; foreach ($students as $value) { ?>
                                                             <tr>
                                                                <td><?= $i ?></td>
                                                                <td><?= $value->name ?></td>
                                                                <td><?= $value->roll ?></td>
                                                                <td><?= dobdate($value->dob) ?></td>
                                                                <td><?= $value->sex ?></td>
                                                                <td><?= school($value->schema_name)->name ?></td>
                                                                <td>
                                                                <a type="button" class="btn btn-info btn-sm btn-out-dotted" 
                                                                    href="<?= url('Users/std_profile/' .$value->schema_name .'/'.$value->student_id) ?>">
                                                                    View
                                                                </a>
                                                             </td>
                                                         </tr>
                                                      <?php $i++; } ?>
                                                      </tbody>
                                                    </table>
                                                 </div>
                                            </div>

                                        <div class="tab-pane" id="profile1" role="tabpanel">
                                            <div class="table-responsive dt-responsive text-center">
                                                <table id="simpletable" class="table table-striped table-bordered nowrap">                        
                                                 <thead>
                                                    <tr>
                                                        <th class="text-center">#</th>
                                                        <th>Class</th>
                                                        <th class="text-center">Male</th>
                                                        <th class="text-center">Female</th>
                                                        <th class="text-center">Total</th>
                                                        <th >School</th>
                                                        
                                                    </tr>
                                                   </thead>
                                                 <tbody>
                                                    <?php $i = 1;  foreach ($all_classes as $value) { ?>
                                                        <tr>
                                                            <td><?= $i ?></td>
                                                            <td class="text-left"><?=$value->name?></td>
                                                            <td> <?= load_sexoverall($value->id,'M',$value->schema_name) ?></td>
                                                            <td> <?= load_sexoverall($value->id,'F',$value->schema_name) ?></td>
                                                            <td> <?=$value->total?> </td>
                                                            <td><?=strtoupper(school($value->schema_name)->name)?></td>
                                                         </tr>
                                                    <?php $i++; } ?>
                                                   </tbody>
                                                   <tfoot>
                                                       <tr>
                                                           <td colspan="2"></td>
                                                           <td></td>
                                                           <td></td>
                                                           <td></td>
                                                       </tr>
                                                   </tfoot>
                                                 </table>
                                              </div>
                                
                                                 <?php if(isset($overall_students)) { ?>
                                                     <div class="table-responsive dt-responsive">
                                                          <table id="" class="table table-striped table-bordered nowrap">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-center">Boys</th>
                                                                    <th class="text-center">Girls</th>
                                                                    <th class="text-center">Undefined</th>
                                                                    <th class="text-center">Total </th>
                                                                </tr>
                                                              </thead>
                                                              <tbody>
                                                                 <?php $boys = [];$girls = [];$null = [];foreach ($overall_students as $value) 
                                                                if($value->sex == 'M'|| $value->sex == 'Male'||$value->sex == 'MALE'||$value->sex == 'male'||
                                                                    $value->sex == 'm'){array_push($boys, $value->total);
                                                                } else if($value->sex == 'F'|| $value->sex == 'Female'|| $value->sex == 'FEMALE'|| $value->sex == 'female'||$value->sex == 'f'){
                                                                    array_push($girls, $value->total);
                                                                } else{array_push($null, $value->total); }
                                                              {  ?>
                                                                 <tr>
                                                                 <th class="text-center"><?= $a = array_sum($boys) ?></th>
                                                                 <th class="text-center"><?= $b = array_sum($girls) ?></th>
                                                                 <th class="text-center"><?= $c = array_sum($null)?></th>
                                                                 <th class="text-center"><?= $a+$b+$c ?></th>
                                                                 </tr>
                                                                 <?php } ?>
                                                             </tbody>
                                                         </table>
                                                       </div>
                                                     <?php  } ?>
                             
                                                     <?php if(isset($students_per_school)) { ?>
                                                         <?php foreach($students_per_school as $value) { ?>
                                                           <div class="table-responsive dt-responsive">
                                                             <table class="table table-striped table-bordered nowrap">
                                                                 <thead>
                                                                     <tr>
                                                                         <th class="text-">School Name</th>
                                                                         <th class="text-">Total Students</th>
                                                                     </tr>
                                                                 </thead>
                                                                 <tbody>
                                                                     <tr>
                                                                         <td> <?= strtoupper(school($value->schema_name)->name) ?></td>
                                                                         <td class="text-left"> <?= $value->total ?></td>
                                                                     </tr>
                                                                 </tbody>
                                                             </table>
                                                           </div>
                                                       <?php } ?>
                                                      <?php } ?>
                                               </div>
                                      </div>
                                </div>
                            </div>
                        </div>
                   </div>
               </div>
            <?php } ?>


         
            <?php if(isset($teachers) && (!empty($teachers))) {  ?>
                <div class="row">
                    <div class="col-sm-12">
                     <div class="">
                      <div class="card-block">
                          <div class="row">
                              <div class="col-lg-12 col-xl-12">
                                      
                                      <ul class="nav nav-tabs  tabs" role="tablist">
                                          <li class="nav-item">
                                              <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">Home</a>
                                          </li>
                                          <li class="nav-item">
                                              <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">Summary</a>
                                          </li>
                                      </ul>
  
                                      <div class="tab-content tabs card-block">
                                          <div class="tab-pane active" id="home1" role="tabpanel">
                                              
                                             <div class="table-responsive dt-responsive">
                                                <table id="dom-jqry" class="table table-striped table-bordered nowrap table-md">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>                                
                                                            <th>Name</th>
                                                            <th>ShuleSoft ID</th>                     
                                                            <th>Phone</th>
                                                            <th>School name</th>
                                                            <th class="text-center">Actions</th>
                                                        </tr>
                                                    </thead>
                                                     <tbody>
                                                        <?php $i = 1; foreach ($teachers as $value) { ?>
                                                            <tr>
                                                                <td><?= $i ?></td>
                                                                <td><?= $value->name ?></td>
                                                                <td><?= $value->sid ?></td>
                                                                <td><?= $value->username ?></td>
                                                                <td><?= school($value->schema_name)->name ?></td>
                                                                <td>
                                                                <a type="button" class="btn btn-info btn-sm btn-out-dotted" 
                                                                    href="<?= url('Users/teacherprofile/' .$value->schema_name .'/'.$value->teacherID) ?>">
                                                                    View
                                                                </a>
                                                                </td>
                                                            </tr>
                                                           <?php $i++; } ?>
                                                      </tbody>
                                                   </table>
                                               </div>
                                            </div>
  
                                             <div class="tab-pane" id="profile1" role="tabpanel">
                                                <?php if(isset($total_teachers))   {   ?>
                                                    <div class="table-responsive dt-responsive">
                                                       <table id="" class="table table-striped table-bordered nowrap">
                                                         <thead>
                                                             <tr>
                                                                 <th class="text-center">Men</th>
                                                                 <th class="text-center">Women</th>
                                                                 <th class="text-center">Undefined</th>
                                                                 <th class="text-center">Total </th>
                                                             </tr>
                                                           </thead>
                                                           <tbody>
                                                              <?php $men = [];$women = [];$null = [];foreach ($total_teachers as $value) 
                                                              if($value->sex == 'M'|| $value->sex == 'Male'||$value->sex == 'MALE'||$value->sex == 'male'||
                                                                  $value->sex == 'm'){array_push($men, $value->total);
                                                              } else if($value->sex == 'F'|| $value->sex == 'Female'|| $value->sex == 'FEMALE'|| $value->sex == 'female'||$value->sex == 'f'){array_push($women, $value->total);
                                                              } else{array_push($null, $value->total); }
                                                              {  ?>
                                                              <tr>
                                                              <th class="text-center"><?= $a = array_sum($men) ?></th>
                                                              <th class="text-center"><?= $b = array_sum($women) ?></th>
                                                              <th class="text-center"><?= $c = array_sum($null)?></th>
                                                              <th class="text-center"><?= $a+$b+$c ?></th>
                                                              </tr>
                                                              <?php } ?>
                                                           </tbody>
                                                     </table>
                                                    </div>
                                               <?php  } ?>

                                               <?php if(isset($school_teachers)) { ?>
                                                <?php foreach($school_teachers as $value) { ?>
                                                  <div class="table-responsive dt-responsive">
                                                    <table class="table table-striped table-bordered nowrap">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-">School Name</th>
                                                                <th class="text-">Total teachers</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td> <?= strtoupper(school($value->schema_name)->name) ?></td>
                                                                <td class="text-left"> <?= $value->total ?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                  </div>
                                                <?php } ?>
                                             <?php } ?>
                                              
                                            </div>
                                        </div>
                                  </div>
                              </div>
                          </div>
                     </div>
                 </div>
               </div>
              <?php } ?>


         


              <?php  if(isset($parents) && (!empty($parents))) { ?>
                <div class="row">
                    <div class="col-sm-12">
                     <div class="">
                      <div class="card-block">
                          <div class="row">
                              <div class="col-lg-12 col-xl-12">
                                      
                                      <ul class="nav nav-tabs  tabs" role="tablist">
                                          <li class="nav-item">
                                              <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">Home</a>
                                          </li>
                                          <li class="nav-item">
                                              <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">Summary</a>
                                          </li>
                                      </ul>
  
                                      <div class="tab-content tabs card-block">
                                          <div class="tab-pane active" id="home1" role="tabpanel">
                                            <div class="table-responsive dt-responsive">
                                                <table id="dom-jqry" class="table table-striped table-bordered nowrap table-md">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Photo </th>
                                                            <th>Parent name</th>
                                                            <th>Relation</th>                               
                                                            <th>School</th>
                                                            <th class="text-center">Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $i = 1; foreach ($parents as $value) { ?>
                                                            <tr>
                                                                <td><?= $i ?></td>
                                                                <td>
                                                                    <?php
                                                                    $array = array(
                                                                        "src" => url('storage/uploads/images/' . $value->photo),
                                                                        'width' => '35px',
                                                                        'height' => '35px',
                                                                        'class' => 'img-rounded rotate'
                                                                    );
                                                                    echo img($array);
                                                                    ?>
                                                                </td>
                                                                <td><?= $value->name ?></td>
                                                                <td><?= $value->relation ?></td>
                                                                <td><?= school($value->schema_name)->name ?></td>
                                                                <td>
                                                                <a type="button" class="btn btn-info btn-sm btn-out-dotted" 
                                                                     href="<?= url('Users/prt_profile/' .$value->schema_name .'/'.$value->parentID) ?>">
                                                                    View
                                                                </a>
                                                                </td>
                                                            </tr>
                                                          <?php $i++; } ?>
                                                      </tbody>
                                                    </table>
                                                 </div>
                                            </div>
  
                                             <div class="tab-pane" id="profile1" role="tabpanel">
                                                <?php if(isset($total_parents))  { ?>
                                                    <div class="table-responsive dt-responsive">
                                                         <table id="" class="table table-striped table-bordered ">
                                                           <thead>
                                                               <tr>
                                                                   <th class="text-center">Men</th>
                                                                   <th class="text-center">Women</th>
                                                                   <th class="text-center">Undefined</th>
                                                                   <th class="text-center">Total </th>
                                                               </tr>
                                                             </thead>
                                                             <tbody>
                                                 <?php $men = [];$women = [];$null = [];foreach ($total_parents as $value) 
                                                if($value->sex == 'M'|| $value->sex == 'Male'||$value->sex == 'MALE'||$value->sex == 'male'||
                                                    $value->sex == 'm'){array_push($men, $value->total);
                                                } else if($value->sex == 'F'|| $value->sex == 'Female'|| $value->sex == 'FEMALE'|| $value->sex == 'female'||$value->sex == 'f'){array_push($women, $value->total);
                                                } else{array_push($null, $value->total); }
                                                                {  ?>
                                                                 <tr>
                                                                   <th class="text-center"><?= $a = array_sum($men) ?></th>
                                                                   <th class="text-center"><?= $b = array_sum($women) ?></th>
                                                                   <th class="text-center"><?= $c = array_sum($null)?></th>
                                                                   <th class="text-center"><?= $a+$b+$c ?></th>
                                                                 </tr>
                                                                  <?php } ?>
                                                             </tbody>
                                                       </table>
                                                      </div>
                                                 <?php  } ?>
                                
                                                 <?php if(isset($school_parents)) { ?>
                                                    <?php foreach($school_parents as $value) { ?>
                                                      <div class="table-responsive dt-responsive">
                                                        <table class="table table-striped table-bordered nowrap">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-">School Name</th>
                                                                    <th class="text-">Total Parents</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td> <?= strtoupper(school($value->schema_name)->name) ?></td>
                                                                    <td class="text-left"> <?= $value->total ?></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                      </div>
                                                  <?php } ?>
                                                 <?php } ?>
                                            </div>
                                        </div>
                                  </div>
                              </div>
                          </div>
                     </div>
                 </div>
               </div>
              <?php } ?>





              <?php if(isset($staffs) && (!empty($staffs))) { ?>
                <div class="row">
                    <div class="col-sm-12">
                     <div class="">
                      <div class="card-block">
                          <div class="row">
                              <div class="col-lg-12 col-xl-12">
                                      <ul class="nav nav-tabs  tabs" role="tablist">
                                          <li class="nav-item">
                                              <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">Home</a>
                                          </li>
                                          <li class="nav-item">
                                              <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">Summary</a>
                                          </li>
                                      </ul>
                                      <div class="tab-content tabs card-block">
                                        <div class="tab-pane active" id="home1" role="tabpanel">
                                         <div class="table-responsive">
                                            <table id="dom-jqry" class="table table-striped table-bordered nowrap table-md">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Photo</th>   
                                                        <th>Name</th>                                                  
                                                        <th>Phone</th>
                                                        <th>Type</th>
                                                        <th>School Name</th>
                                                        <th class="text-center">Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i = 1;  foreach ($staffs as $value) { ?>
                                                        <tr>
                                                            <td><?= $i ?></td>
                                                            <td>
                                                                <?php
                                                                $array = array(
                                                                    "src" => url('storage/uploads/images/' . $value->photo),
                                                                    'width' => '35px',
                                                                    'height' => '35px',
                                                                    'class' => 'img-rounded rotate'
                                                                );
                                                                echo img($array);
                                                                ?>
                                                            </td>
                                                            <td><?= $value->name ?></td>
                                                            <td><?= $value->username ?></td>
                                                            <td><?= $value->usertype ?></td>
                                                            <td><?= school($value->schema_name)->name ?></td>
                                                            <td>
                                                            <a type="button" class="btn btn-info btn-sm btn-out-dotted" 
                                                                href="<?= url('Users/staff_profile/' .$value->schema_name .'/'.$value->id) ?>">
                                                                View
                                                            </a>
                                                            </td>
                                                        </tr>
                                                     <?php $i++; } ?>
                                                  </tbody>
                                                </table>
                                             </div>
                                            </div>
  
                                             <div class="tab-pane" id="profile1" role="tabpanel">
                                                <?php if(isset($count_school_staffs)) { ?>
                                                    <div class="table-responsive">
                                                        <table id="dom-jqry" class="table table-striped table-bordered nowrap table-md">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>School name</th>
                                                                    <th>Staff Usertype</th>
                                                                    <th>Total</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php $sum = 0;$i=1;foreach($count_school_staffs as $value) { ?>
                                                                <tr>
                                                                    <td><?= $i ?> </td>
                                                                    <td><?= strtoupper(school($value->schema_name)->name) ?></td>
                                                                    <td><?= strtoupper($value->usertype) ?></td>
                                                                    <td><?php $sum = $sum+ $value->total; echo $value->total; ?></td>
                                                                </tr>
                                                                <?php $i++;} ?>
                                                            </tbody>
                                                            <tr>
                                                                <th colspan="3">TOTAL STAFFS</th>
                                                                <th><?= $sum?></th>
                                                            </tr>
                                                        </table>
                                                      </div>
                                                <?php } ?>

                                                <?php if(isset($school_staffs)) { ?>
                                                    <?php foreach($school_staffs as $value) { ?>
                                                      <div class="table-responsive dt-responsive">
                                                        <table class="table table-striped table-bordered nowrap">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-">School Name</th>
                                                                    <th class="text-">Total Staff</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td> <?= strtoupper(school($value->schema_name)->name) ?></td>
                                                                    <td class="text-left"> <?= $value->total ?></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                      </div>
                                                  <?php } ?>
                                                <?php } ?>

                                            </div>
                                        </div>
                                  </div>
                              </div>
                          </div>
                     </div>
                  </div>
                </div>
              <?php } ?>




              
              <?php  if(isset($schools) && (!empty($schools))) { ?>
                
                    <div class="row">
                        <div class="col-sm-12">
                         <div class="">
                          <div class="card-block">
                              <div class="row">
                                  <div class="col-lg-12 col-xl-12">
                                      
                                      <ul class="nav nav-tabs  tabs" role="tablist">
                                          <li class="nav-item">
                                              <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">Home</a>
                                          </li>
                                      </ul>
  
                                      <div class="tab-content tabs card-block">
                                        <div class="tab-pane active" id="home1" role="tabpanel">
                                         <div class="table-responsive dt-responsive">
                                            <table id="dom-jqry" class="table table-bordered  table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>School name</th>                                  
                                                        <th>School email</th>                     
                                                        <th>School Address</th>                     
                                                        <th>School Phone</th>                     
                                                        {{-- <th>Action</th> --}}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                   <?php $i = 1; foreach ($schools as $school) { ?>
                                                    <tr>
                                                        <td><?=$i?></td>
                                                        <td><?=$school['name'] ?></td>
                                                        <td><?=$school['email'] ?></td>
                                                        <td><?=$school['address'] ?></td>
                                                        <td><?=$school['phone'] ?></td>
                                                        {{-- <td> 
                                                          <a type="button" class="btn btn-info btn-sm" 
                                                            href="<?= url('dashboard/profile/'.$school['username'])  ?>">
                                                            View
                                                          </a>
                                                        </td> --}}
                                                    </tr>
                                                   <?php $i++; } ?>
                                                 </tbody>
                                               </table>
                                             </div>
                                           </div>
  
                                            <div class="tab-pane" id="profile1" role="tabpanel">
                                            
                                            </div>
                                        </div>
                                  </div>
                              </div>
                          </div>
                     </div>
                  </div>
                 
               </div>
              <?php } ?>    
         </div>
      </div>
        

<script type="text/javascript">
$('#overall').change(function (event) {
    var overall = $(this).val();
    if (overall === '0') {
    } else {
        window.location.href = "<?= url('Users/overall') ?>/" + overall.replace(/\s+/g, '');
    }
});
</script>

@endsection