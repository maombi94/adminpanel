@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>

<div class="page-body">
<div class="card">
                <center>
        
                <div class="col-sm-8 col-xs-12 col-sm-offset-3 list-group">
                    <div class="list-group-item list-group-item-warning">
                        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                            <?php
                            if (form_error($errors, 'classlevel'))
                                echo "<div class='form-group row has-error'>";
                            else
                                echo "<div class='form-group row row'>";
                            ?>
                            <div for="class_level_id" class="col-sm-2 control-label">
                                classlevel*
                            </div>
                            <div class="col-sm-8">
                                <?php
                                $array = array("0" => 'classlevel');
                                if (count($classlevels)) {
                                    foreach ($classlevels as $level) {
                                        $array[$level->classlevel_id] = $level->name;
                                    }
                                }
                                echo form_dropdown("class_level_id", $array, old("class_level_id"), "id='class_level_id' class='form-control'");
                                ?>
                            </div>
                            <span class="col-sm-2 control-label">
                                <?php echo form_error($errors, 'classlevel'); ?>
                            </span>
                    </div>
                <div class='form-group row'>
                    <label for="academic_year_id" class="col-sm-2 control-label">
                        academic_year
                    </label>
                    <div class="col-sm-8">
                        <?php
                        $array = array("0" => "academic_year");
                       if (isset($academic_years) && !empty($academic_years)) {
                            foreach ($academic_years as $academic) {
                                $array[$academic->id] = $academic->name;
                            }
                        }

                        echo form_dropdown("academic_year_id", $array, old("academic_year_id"), "id='academic_year_id' class='form-control'");
                        ?>
                    </div>
                    <span class="col-sm-2 control-label">
                        <?php echo form_error($errors, 'academic_year'); ?>
                    </span>
                </div>
            <div class='form-group row'>
                <label for="classes_id" class="col-sm-2 control-label">
                    Select class
                </label>
                <div class="col-sm-8">

                    <div class="select2-wrapper">
                        <select class="form-control" id="classes_id" name="classes_id">

                        </select>
                    </div>
                </div>
            </div>
            <div class='form-group row'>
            <label for="semester_id" class="col-sm-2 control-label">
               Select semester
            </label>
            <div class="col-sm-8">
            <select class="form-control" id="semester_id" name="semester_id">
                @if (isset($semesters)) {                        
                    @foreach ($semesters as $semester) {
                        <option value="{{ $semester->id }}"> {{$semester->name}} </option>
                  @endforeach
                  @endif
            </select>
                
            </div>
        </div>
      <div class='form-group row'>
        <label for="examID" class="col-sm-2 control-label">
            Select Exam
        </label>

        <div class="col-sm-8 ">
            <select class="form-control" id="examID" name="examID">
            </select>
            <span id="exam_warning"></span>
        </div>
    </div>

    <div class='form-group row'>
    <label for="section_id" class="col-sm-2 control-label">
       Select section
    </label>
    <div class="col-sm-8">
        <div class="select2-wrapper">
            <select class="form-control" id="section_id" name="section_id">
            </select>
        </div>
    </div>
</div>
<?= csrf_field() ?>
</form>
</div>
</div>
</div>
<?php if (isset($exam) && isset($section)) { ?>
    <div class="col-sm-12">
        <div class="col-sm-6 col-sm-offset-3 list-group">
            <div class="list-group-item">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Class Name</th>
                            <th>Number of Students</th>
                            <th>Exam Name</th>
                            <th>Semester Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?= $section->classes->classes ?></td>
                            <td><?= count($students) ?></td>
                            <td><?= $exam->exam ?></td>
                            <td><?= $exam->semester->name ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php
}
if (isset($students) && count($students) > 0) {
    ?>
    <div class="col-sm-12">

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#all" aria-expanded="true">Students</a></li>
                <li class=""><a data-toggle="tab" href="#summary" aria-expanded="false">Summary</a></li>
            </ul>
            <div class="tab-content">

                <div id="all" class="tab-pane active">
                    <div id="hide-table">
                        <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                            <thead>
                                <tr>
                                    <th class="col-lg-1">slno</th>

                                    <th class="col-lg-2">student_name</th> 
                                    <th class="col-lg-2">roll</th> 
                                    <th class="col-lg-2">class</th>
                                    <th class="col-lg-2">section</th>
                                    <th class="col-lg-6">number_character_assessed</th>
                                    <th class="col-lg-1">action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($students)) {
                                    $i = 1;
                                    $total_characters_per_class = \App\Model\Character::join('character_classes', 'character_classes.character_id', '=', 'characters.id')->where('class_id', $class_id)->orderBy('position', 'asc')->count();
                                    foreach ($students as $student) {
                                        ?>
                                        <tr>
                                            <td data-title="slno">
                                                <?php echo $i; ?>
                                            </td>
                                            <td data-title="student_name">
                                                <?php echo $student->student->name; ?>
                                            </td>
                                            <td data-title="roll">
                                                <?php echo $student->student->roll; ?>
                                            </td>
                                            <td data-title="classes">
                                                <?php echo $student->student->classes->classes; ?>
                                            </td>
                                            <td data-title="stream">
                                                <?php echo $student->student->section->section; ?>
                                            </td>
                                                 

                                            <td data-title="number_character_assessed">
                                                <?php $total_assessed = $student->student->studentCharacters()->where('exam_id',$exam_id)->whereNotNull('grade')->count(); 
                                                echo  $total_assessed==$total_characters_per_class ?'<b class="label label-success">complete</b>':$total_assessed.' '.$data->lang->line('message').'  '.$total_characters_per_class;   ?>
                                            </td>
                                            <td data-title="action">
                                                <?php echo btn_view('general_character_assessment/report/' . $exam_id . '/' . $student->section_id . '/' . $student->student_id . '/' . $student->academic_year_id, $data->lang->line('view')); ?>

                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div id="summary" class="tab-pane">
                    <div class="row  col-lg-offset-2  center-block">
                        <div class="title_left">
                            <br/>

                            <h3>General Summary </h3>
                            <br/>
                        </div>
                        <div id="hide-table">



                            <table  class="table table-striped table-bordered table-hover no-footer">
                                <thead>
                                    <tr>
                                        <th class="col-sm-1">TOTAL Students</th>

                                        <th class="col-sm-2">Student Assessed</th>
                                        <th class="col-sm-2">Remained Students</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?= count($students) ?></td>

                                        <td><?= $assessed ?></td>
                                        <td><?= count($students) - $assessed ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="row col-lg-offset-2  center-block">
                        <div class="title_left col-lg-12">
                            <br/><br/>
                            <h3>Statistical Evaluation of Characters assessed </h3>
                            <br/>
                        </div>
                         <div class="col-lg-12">

                        <table id="example1" class="table table-bordered">
                            <thead>
                                <?php
                                if (isset($character_categories) && count($character_categories) > 0) {
                                    $tt = 1;
                                    foreach ($character_categories as $character_category) {
                                        ?>


                                        <tr>
                                            <th colspan="<?= count($character_gradings) + 2 ?>"><?php echo '<h4 align="left" style="font-size: 12px; font-weight: bolder;padding-top: 0%; margin-top: 2%; margin-bottom:1%; color:black ">' . ucwords($character_category->character_category) . '</h4>'; ?></th>
                                        </tr>
                                        <?php
                                        if ($tt == 1) {
                                            ?>
                                            <tr>
                                                <th class="col-lg-1">#</th>
                                                <th class="col-lg-6">character</th>
                                                <?php foreach ($character_gradings as $grade) { ?>
                                                    <th class="col-lg-2"><?= $grade->grade ?></th>
                                                <?php } ?>

                                            </tr>
                                        <?php } ?>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $all_characters = \App\Model\Character::where('character_category_id', $character_category->id)->join('character_classes', 'character_classes.character_id', '=', 'characters.id')->where('class_id', $class_id)->orderBy('position', 'asc')->get();

                                        $x = 1;

                                        foreach ($all_characters as $all_character) {


                                            $character_id = $all_character->character_id;
                                            $student_character = \App\Model\StudentCharacter::where('character_id', $character_id)->where('exam_id', $exam_id)->first();
                                            ?>
                                            <tr>
                                                <td data-title="code">
                                                    <?php echo $x; ?>
                                                </td>
                                                <td data-title="description">
                                                    <?php
                                                    echo $all_character->description;
                                                    ?>
                                                </td>
                                                <?php foreach ($character_gradings as $grade) { ?>
                                                    <td data-title="description">
                                                        <?php
                                                    echo   \App\Model\StudentCharacter::where('exam_id', $exam_id)->whereIn('student_id',$active_section_students)->where('grade', $grade->id)->where('character_id',$character_id)->count();
                                                        ?>
                                                    </td>
                                                    <?php
                                                }
                                                ?>

                                            </tr>
                                            <?php
                                            $x++;
                                        }
                                        $tt++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                           
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>


<script type="text/javascript">
    $('#classes_id').change(function (event) {
        var classes_id = $(this).val();
        if (classes_id == '') {
            //
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('general_character_assessment/getSectionByClass') ?>",
                data: "classes_id=" + classes_id + "&schema_name=" + <?= $this_schema?>, 
                dataType: "html",
                success: function (data) {
                    $('#section_id').html(data);
                }
            });
        }
    })

    $('#section_id').change(function (event) {
        var section_id = $(this).val();
        var semester_id = $('#semester_id').val();
        var academic_year_id = $('#academic_year_id').val();
        var class_level_id = $('#class_level_id').val();
        var classes_id = $('#classes_id').val();
        var exam_id = $('#examID').val();
        if (classes_id == "") {

        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('general_character_assessment/fullAssessedStudent') ?>",
                data: "classes_id=" + classes_id + "&semester_id=" + semester_id + "&academic_year_id=" + academic_year_id + "&class_level_id=" + class_level_id + "&section_id=" + section_id + "&exam_id=" + exam_id + "&schema_name=" + <?= $this_schema?>, 
                dataType: "html",
                success: function (data) {
                    window.location.href = data;
                }
            });
        }
    });
    $('#class_level_id').change(function (event) {
        var class_level_id = $(this).val();
        if (class_level_id === '0') {
            $('#academic_year_id').val(0);
            $('#classes_id').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/get_academic_years_bylevel') ?>",
                data: "class_level_id=" + class_level_id + "&schema_name=" + <?= $this_schema?>, 
                dataType: "html",
                success: function (data) {
                    $('#academic_year_id').html(data);
                }
            });
            $('#classes_id').show();
            $('.checkbox_tag, .checkbox_name').hide();
            $('.class' + class_level_id).show();
        }
        $.ajax({
            type: 'POST',
            url: "<?= url('classes/getClasses') ?>",
            data: "class_level_id=" class_level_id  + "&schema_name=" + <?= $this_schema?>,
            dataType: "html",
            success: function (data) {
                $('#classes_id').html(data);
            }
        });
    });
    $('#academic_year_id').change(function (event) {
        var academic_year_id = $(this).val();
        if (academic_year_id === '0') {
            $('#semester_id').val(0);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('semester/get_semester') ?>",
                data: "academic_year_id=" + academic_year_id + "&schema_name=" + <?= $this_schema?>, 
                dataType: "html",
                success: function (data) {
                    if (data === '0') {
                        $('#semester_id').after('<span class="red small">No semesters have been defined in this academic year. Please <a href="<?= url('semester/add') ?>"class="btn btn-primary btn-sm" role="button">add semester </a>first</span>');
                    } else {
                        $('#semester_id').html(data);
                    }
                }
            });
        }
    });
    $('#semester_id').change(function (event) {
        var academic_year_id = $('#academic_year_id').val();
        var semester_id = $(this).val();
        var classesID = $('#classes_id').val();
        if (academic_year_id === '0') {
            $('#examID').val(0);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/getExamByClass') ?>",
                data: "id=" + classesID + "&academic_year_id=" + academic_year_id + '&semester_id=' + semester_id + "&schema_name=" + <?= $this_schema?>, 
                dataType: "html",
                success: function (data) {
                    $('#exam_warning').html('');
                    if (data === "<option value='0'>Exam Name</option>") {
                        $('#exam_warning').html('<div class="alert alert-danger">No exams defined. Please <a href="<?= url('exam/index') ?>">click here </a> define exam in exam section</div>');
                    } else {
                        $('#examID').html(data);
                    }
                }
            });
        }
    });

</script>

@endsection
