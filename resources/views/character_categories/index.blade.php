@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>

<div class="page-body">
<div class="card">
                <div class="card-header">
                    <a href="{{ url('users/create') }}" class="btn btn-success">Add User</a>
                </div>
                <div class="card-block">

                <?php 
                    $usertype = session("usertype");
                    if(can_access('add_character')) {
                ?>
                    <h5 class="page-header">
                        <a class="btn btn-success" href="<?php echo url('character_categories/add') ?>">
                            <i class="fa fa-plus"></i> 
                           Add
                        </a>
                    </h5>
                <?php } ?>

                <center>
                <div class="col-sm-12 col-xl-4 m-b-30"> 
                      <select id="schema" class="form-control form-control-primary">
                        <option value="opt1">Select One School</option>
                                  @if (count(load_schemas()) > 0)
                                  @foreach (load_schemas() as $schema)
                                      <option value="{{ $schema->username }}"> {{ school($schema->username)->name }}</option>
                                  @endforeach
                                  @endif
                        </select>
                  </div>
                  <hr>
                  <h4>List of <?= default_school(Auth::user()->id)->name ?> Assessment Categories</h4>
                  </center>

                <div class="dt-responsive table-responsive">
                        <table id="simpletable" class="table table-striped table-bordered nowrap">
                        <thead>
                            <tr>
                                <th class="col-lg-1">slno</th>
                                <th class="col-lg-4">character_category</th>
                                <th class="col-lg-1">position</th>
                                <th class="col-lg-1">action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($all_character_categories)) {$i = 1; foreach($all_character_categories as $character_category) { ?>
                                <tr>
                                    <td data-title="slno">
                                        <?php echo $i; ?>
                                    </td>
                                    <td data-title="character_category">
                                        <?php echo $character_category->character_category; ?>
                                    </td>
                                    <td data-title="position">
                                        <?php echo $character_category->position; ?>
                                    </td>
                                    <td data-title="action">
                                        <?php echo btn_edit('character_categories/edit/'.$character_category->id, 'Edit') ?>
                                        <?php echo btn_delete('character_categories/delete/'.$character_category->id, 'Delete') ?>
                                    </td>
                                </tr>
                            <?php $i++; }} ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
     
       $('#schema').change(function (event) {
            var schema = $(this).val();
            if (schema === '0') {
            } else {
                window.location.href = "<?= url('character_categories/index') ?>/" + schema;
            }
        });


    </script>

@endsection
