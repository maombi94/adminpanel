
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-list"></i> panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=url("dashboard/index")?>"><i class="fa fa-laptop"></i> menu_dashboard')?></a></li>
            <li><a href="<?=url("character_categories/index")?>"></i> menu_character_categories')?></a></li>
            <li class="active">menu_edit')?> menu_character_categories')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8">
                <form class="form-horizontal" role="form" method="post">
                    
                    <?php 
                        if(form_error($errors,'subject_based')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="classes" class="col-sm-2 control-label">
                            <?=$data->lang->line("subject_based")?>
                        </label>
                    <div class="form-group">
                        
                         <input type="radio" name="subject_based" value="0" required="required"> Achievement Only
                          <input type="radio" name="subject_based" value="1" required="required">Both Achievement and Effort 
                     </div>
                     <span class="col-sm-4 control-label">
                            <?php echo form_error($errors,'character_category'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error($errors,'character_category')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="classes" class="col-sm-2 control-label">
                            <?=$data->lang->line("character_category")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="character_category" name="character_category" placeholder="character category" value="<?=old('character_category', $character_categories->character_category)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error($errors,'character_category'); ?>
                        </span>
                    </div>
          <?php 
                        if(form_error($errors,'position')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="classes" class="col-sm-2 control-label">
                            <?=$data->lang->line("position")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="position" name="position" placeholder="<?=$data->lang->line("position")?>" value="<?=old('position',$character_categories->position)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error($errors,'position'); ?>
                        </span>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-6">
                            <input type="submit" class="btn btn-success btn-block" value="<?=$data->lang->line("save")?>" >
                        </div>
                    </div>

                <?= csrf_field() ?>
</form>


            </div>
        </div>
    </div>
</div>
