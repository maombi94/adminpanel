<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>

<link href="https://demo.shulesoft.com/public/assets/shulesoft/dashboard.min.css" rel="stylesheet">
@extends('layouts.app')

@section('content')
<?php $root = url('/public/'); ?>
<!-- Page-body start -->
<div class="page-body">
    <div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Accounts Dashboard for {{ ucfirst($set_schema) . ' - '. $year }}</h1>
        <div class="dropdown no-arrow">
            <a class="dropdown-toggle  btn btn-success" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                Select School <i class="fa fa-chevron-down fa-sm fa-fw text-gray-400"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                aria-labelledby="dropdownMenuLink">
                <div class="dropdown-header">Schools:</div>
                
                @if (count(load_schemas()) > 0)
                @foreach (load_schemas() as $this_schema)
                    <a class="dropdown-item" href="{{ url('dashboard/accounts/'.$this_schema->username.'/'.$year) }}"> {{ ucfirst($this_schema->username) }} </a>
                @endforeach
                @endif

            </div>
        </div>
    </div>

    <!-- Content Row -->
    <div class="row">

        <!-- Total Revenue card  -->
        <div class="col-md-12 mb-4">
            <div class="card border-left-primary shadow ">

                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Revenue</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            Select Year <i class="fa fa-chevron-down fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                            aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">Installment:</div>
                            <a class="dropdown-item" href="{{ url('dashboard/accounts/'.$set_schema.'/'.date('Y')) }}"> Year - {{ date('Y') }}</a>
                            <a class="dropdown-item" href="{{ url('dashboard/accounts/'.$set_schema.'/'.date('Y')-1) }}"> Year - {{ date('Y')-1 }}</a>
                            <a class="dropdown-item" href="{{ url('dashboard/accounts/'.$set_schema.'/'.date('Y')-2) }}"> Year - {{ date('Y')-2 }}</a>
                            <a class="dropdown-item" href="{{ url('dashboard/accounts/'.$set_schema.'/'.date('Y')-3) }}"> Year - {{ date('Y')-3 }}</a>
                            <a class="dropdown-item" href="{{ url('dashboard/accounts/'.$set_schema.'/'.date('Y')-4) }}"> Year - {{ date('Y')-4 }}</a>

                        </div>
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <figure class="highcharts-figure">
                        <div id="container" style="height: 350px;"></div>

                    </figure>

                </div>
            </div>
        </div>
    </div>

    <!-- Content Row -->

    <div class="row">

        <!-- Area Chart -->
        <div class="col-xl-6 col-lg-6">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Revenue</h6>
                   
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <figure class="highcharts-figure">
                        <div id="expenserevenue"></div>
                    </figure>


                </div>
            </div>
        </div>




        <!-- Content Column -->
        <div class="col-lg-6 mb-4">

            <!-- Bar Chart -->
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Payment Overview</h6>
             
                </div>
                <div class="card-body">

                    <figure class="highcharts-figure">
                        <div id="payment"></div>
                    </figure>
                </div>
            </div>


        </div>

    </div>

    <!-- Content Row -->

    <div class="row">


        <!-- Content Column -->

      

        <!-- Total Revenue card  -->
        <div class="col-xl-12 col-md-12 mb-4">
            <div class="card border-left-primary shadow ">

                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Collections</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            Installments <i class="fa fa-chevron-down fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                            aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">Installment:</div>
                            <a class="dropdown-item" href="#">First Installment</a>
                            <a class="dropdown-item" href="#">Second Installment</a>

                        </div>
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <figure class="highcharts-figure">
                        <div id="collections" style="height: 450px;"></div>

                    </figure>

                </div>
            </div>
        </div>

        <!-- Total Revenue card  -->
        <div class="col-xl-12 col-md-12 mb-4">
            <div class="card border-left-secondary shadow ">

                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Collections</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            Installments <i class="fa fa-chevron-down fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                            aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">Installment:</div>
                            <a class="dropdown-item" href="#">First Installment</a>
                            <a class="dropdown-item" href="#">Second Installment</a>

                        </div>
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <figure class="highcharts-figure">
                        <div id="installments" style="height: 450px;"></div>

                    </figure>

                </div>
            </div>
        </div>

          <!-- Area Chart -->
          <div class="col-xl-12 col-lg-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Expense Overview</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            Installments <i class="fa fa-chevron-down fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                            aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">Installment:</div>
                            <a class="dropdown-item" href="#">First Installment</a>
                            <a class="dropdown-item" href="#">Second Installment</a>

                        </div>
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <figure class="highcharts-figure">
                        <div id="expenseoverview" style="height: 450px;"></div>
                    </figure>
                </div>
            </div>
        </div>

        
        <div class="col-xs-12 col-md-12 mb-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Expense Vs Revenues</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            Installment <i class="fa fa-chevron-down fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                            aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">Installment:</div>
                            <a class="dropdown-item" href="#">First Installment</a>
                            <a class="dropdown-item" href="#">Second Installment</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">

                    <figure class="highcharts-figure">
                        <div id="container55" style="height: 350px;"></div>
                </div>
            </div>

        </div>
    </div>

</div>

<!-- /.container-fluid -->
<script>
Highcharts.chart('container55', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Expense Vs Revenue Dashboard'
    },

    xAxis: {
        categories: [
            <?php foreach($revenue as $class){  ?> '<?=date("M", strtotime($class->date))?>', <?php } ?>
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Amounts'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [

        {
            name: 'Revenue',
            color: '#ea4335',
            data: [
                <?php
            foreach($revenue as $class){ 
                if($class->revenue != ''){
                    echo $class->revenue.',';
                }
            }
            ?>
            ]
        },
        {
            name: 'Expenses',
            color: '#000999',
            data: [
                <?php
            foreach($revenue as $class1){ 
                if($class1->expense != ''){
                    echo $class1->expense.',';
                }
            }
            ?>
            ]
        },

    ]
});


Highcharts.chart('container', {

    title: {
        text: 'Total  Revenue Collection Trend'
    },

    subtitle: {
        text: 'Based on Fee Collection and other sources.'
    },

    yAxis: {
        title: {
            text: 'Amount'
        }
    },

    xAxis: {
        categories: [
            <?php foreach($total_transactions as $class){  ?> '<?=date("F", strtotime($class->month))?>',
            <?php } ?>
        ]
    },

    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },



    series: [{
        name: 'Revenue',
        data: [<?php foreach($total_transactions as $teacher){ echo $teacher->amount.','; }?>]
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});


Highcharts.chart('expenserevenue', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Targeted Revenue (Annual): <?=money($collections->amount)?>'
    },
    subtitle: {
        text: 'Fees Collection Status'
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Average %'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
    },

    series: [{
        name: 'Subjects',
        colorByPoint: true,
        data: [
            {
                name: 'Total Amount Fee',
                y: <?=$collections->amount?>,
                drilldown: '<?=$collections->amount?>'
            },
            {
                name: 'Collected Amount',
                y: <?=$collections->paid_amount?>,
                drilldown: '<?=$collections->paid_amount?>'
            },
            {
                name: 'Not Collected',
                y: <?=$collections->sum?>,
                drilldown: '<?=$collections->sum?>'
            },
        ]
    }]
});


Highcharts.chart('payment', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Student Payments Overview'
    },
    xAxis: {
        categories: ['Invoiced Student', 'Fully Paid Students', 'Partially Paid Students', 'Not Paid Students']
    },
    yAxis: {
        title: {
            text: 'Total Students'
        }
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Total Student ',
        data: [500]
    }, {
        name: 'Fully Paid',
        data: [250]
    }, {
        name: 'Partially Paid',
        data: [350]
    }, {
        name: 'Not Paid',
        data: [400]
    }]
});


Highcharts.chart('expenseoverview', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'School Expense Overview on <?=date("Y")?>'
    },
    subtitle: {
        text: 'Top Account Group Expenses'
    },
    xAxis: {
        categories: [<?php foreach($expenses as $expense){  ?> '<?=$expense->name?>',
            <?php } ?>],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Amounts(tsh)',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },

    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        colorByPoint: true,
        name: 'Expenses',
        data: [<?php foreach($expenses as $expense){ echo $expense->amount.','; } ?>]
    }]
});


Highcharts.chart('installments', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Fee Installment Collection on this Year'
    },
    subtitle: {
        text: 'Based On Current Academic Year'
    },
    xAxis: {
        categories: [
            <?php foreach($installments as $class){  ?> '<?=$class->name?><?='<br>'.$class->level?>',
            <?php } ?>
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Amount (tshs)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Total',
        data: [
            <?php
            foreach($installments as $installment){ 
                if($installment->amount){
                    echo $installment->amount.',';
                }
            }
            ?>
        ]
    }, {
        name: 'Paid',
        data: [
            <?php
            foreach($installments as $installment){ 
                if($installment->paid_amount){
                    echo $installment->paid_amount.',';
                }
            }
            ?>
        ]
    }, {
        name: 'UnPaid',
        data: [
            <?php
            foreach($installments as $installment){ 
                if($installment->sum){
                    echo $installment->sum.',';
                }
            }
            ?>
        ]
    }]
});


Highcharts.chart('collections', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Fee Collection Status on this Year'
    },
    subtitle: {
        text: 'Based On Current Academic Year'
    },
    xAxis: {
        categories: [
            <?php foreach($fees as $fee){  ?> '<?=$fee->name?>', <?php } ?>
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Amount (tshs)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Total',
        data: [
            <?php
            foreach($fees as $fe){ 
                if($fe->amount){
                    echo $fe->amount.',';
                }
            }
            ?>
        ]
    }, {
        name: 'Paid',
        data: [
            <?php
            foreach($fees as $fe){ 
                if($fe->paid_amount){
                    echo $fe->paid_amount.',';
                }
            }
            ?>
        ]
    }, {
        name: 'UnPaid',
        data: [
            <?php
            foreach($fees as $fe){ 
                if($fe->sum){
                    echo $fe->sum.',';
                }
            }
            ?>
        ]
    }]
});

Highcharts.chart('collection66s', {
    chart: {
        type: 'column'
    },
    title: {
        text: "A-level Subjects Average Performance Evaluation"
    },
    subtitle: {
        text: 'Each Subject Average'
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Average %'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}%'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
    },

    series: [{
        name: 'Subjects',
        colorByPoint: true,
        data: [{
                name: 'CHEMISTRY ONE',
                y: 64.06,
                drilldown: '64.06'
            },
            {
                name: 'MATHEMATICS ONE',
                y: 62.94,
                drilldown: '62.94'
            },
            {
                name: 'GENERAL STUDIES',
                y: 66.68,
                drilldown: '66.68'
            },
            {
                name: 'PHYSICS ONE',
                y: 49.17,
                drilldown: '49.17'
            },
        ]
    }]
});
</script>
<!-- select DISTINCT a.student_id
from shulesoft.invoice_balances a join shulesoft.student b on a.student_id=b.student_id join shulesoft.invoices c on c.id=a.invoice_id where a.student_id in (select student_id from shulesoft.student_archive where academic_year_id=15 )
group by a.invoice_id,a.created_at,b.name ,b.roll,a.student_id,c.reference,c.sync,c.academic_year_id,c.date,c.due_date;

select  coalesce(coalesce(sum(a.total_amount),0)-sum(a.discount_amount),0) as amount, coalesce(coalesce(sum(a.total_payment_invoice_fee_amount),0)+ coalesce(sum(a.total_advance_invoice_fee_amount)),0) as paid_amount, sum(a.balance), a.invoice_id as id,a.student_id, c.reference,c.sync, b.name as student_name,b.roll, a.created_at,c.academic_year_id,c.date,c.due_date
from shulesoft.invoice_balances a join shulesoft.student b on a.student_id=b.student_id join shulesoft.invoices c on c.id=a.invoice_id where a.student_id in (select student_id from shulesoft.student_archive where academic_year_id=15 )
group by a.invoice_id,a.created_at,b.name ,b.roll,a.student_id,c.reference,c.sync,c.academic_year_id,c.date,c.due_date; -->

@endsection