@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>

<div class="page-body">
   
    <!--profile cover end-->
    <div class="row">
        <div class="col-lg-12">
            <!-- tab header start -->
            <div class="tab-header card">
                <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Basic Info</a>
                        <div class="slide"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#binfo" role="tab">
                            SMS/Email</a>
                        <div class="slide"></div>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#payments" role="tab">
                            Activity</a>
                        <div class="slide"></div>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#contract" role="tab">School List</a>
                        <div class="slide"></div>
                    </li>
                </ul>
            </div>

            <div class="tab-content">
                <div class="tab-pane active" id="personal" role="tabpanel">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-header-text">Personal Information</h3>
                        </div>
                        <div class="card-block">
                            <div class="view-info">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="general-info">
                                            <div class="row">
                                                <div class="col-lg-12 col-xl-6">
                                                    <div class="table-responsive">
                                                        <table class="table m-0">
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">Full Name</th>
                                                                    <td><?= $staff->name ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Phone Number</th>
                                                                    <td><?= $staff->phone ?></td>
                                                                </tr>

                                                                <tr>
                                                                    <th scope="row">Address</th>
                                                                    <td><?= $staff->address ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Email</th>
                                                                    <td><?= $staff->email; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row"></th>
                                                                    <td></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- end of table col-lg-6 -->
                                                <div class="col-lg-12 col-xl-6">
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">Staff Role</th>
                                                                    <td><?= $staff->role->name ?></td>
                                                                </tr>
                                                                
                                                                <tr>
                                                                    <th scope="row">Status</th>
                                                                    <td><?= $staff->status == '1'? 'Active' : 'Inactive' ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Country</th>
                                                                    <td><?= $staff->country->country ?></td>
                                                                </tr>

                                                                <tr>
                                                                    <th scope="row">Added Date</th>
                                                                    <td><?= customdate($staff->created_at) ?></td>
                                                                </tr>

                                                                <tr>
                                                                    <th scope="row"></th>
                                                                    <td></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- end of table col-lg-6 -->
                                            </div>
                                            <!-- end of row -->
                                        </div>
                                        <!-- end of general info -->
                                    </div>
                                    <!-- end of col-lg-12 -->
                                </div>
                                <!-- end of row -->
                            </div>
                        </div>
                    </div>
                </div>


                <div class="tab-pane" id="binfo" role="tabpanel">
                    <div>
                        <div class="bg-white p-relative">
                            <div class="input-group wall-elips">
                                <span class="dropdown-toggle addon-btn text-muted f-right wall-dropdown"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
                                    role="tooltip"></span>
                                <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                    <a class="dropdown-item" href="#">Remove tag</a>
                                    <a class="dropdown-item" href="#">Report Photo</a>
                                    <a class="dropdown-item" href="#">Hide From Timeline</a>
                                    <a class="dropdown-item" href="#">Blog User</a>
                                </div>
                            </div>
                            <div class="card-block user-box">
                                <div class="p-b-20">
                                    <span class="f-14"><a href="#">Comments (110)</a></span>
                                    <span class="f-right">see all comments</span>
                                </div>
                                <div class="media">
                                    <a class="media-left" href="#">
                                        <img class="media-object img-radius m-r-20"
                                            src="<?=$root?>\assets\images\avatar-1.jpg" alt="Generic placeholder image">
                                    </a>
                                    <div class="media-body b-b-theme social-client-description">
                                        <div class="chat-header">About Marta Williams<span class="text-muted">Jane 04,
                                                2015</span></div>
                                        <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                            typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                            text ever since the 1500s, when an unknown printer
                                            took a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                </div>
                                <div class="media">
                                    <a class="media-left" href="#">
                                        <img class="media-object img-radius m-r-20"
                                            src="<?=$root?>\assets\images\avatar-2.jpg" alt="Generic placeholder image">
                                    </a>
                                    <div class="media-body b-b-theme social-client-description">
                                        <div class="chat-header">About Marta Williams<span class="text-muted">Jane 10,
                                                2015</span></div>
                                        <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and
                                            typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                            text ever since the 1500s, when an unknown printer
                                            took a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                </div>
                                <div class="media">
                                    <a class="media-left" href="#">
                                        <img class="media-object img-radius m-r-20" src="<?=$root?>\assets\images\user.png" alt="Generic placeholder image">
                                    </a>
                                    <div class="media-body">
                                        <form class="">
                                            <div class="">
                                                <textarea class="f-13 form-control msg-send" rows="3" cols="10"
                                                    required="" placeholder="Write something....."></textarea>
                                                <div class="text-right m-t-20"><a href="#"
                                                        class="btn btn-primary waves-effect waves-light">Post</a></div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="tab-pane" id="contract" role="tabpanel">
                    <div class="row">

                        <div class="col-xl-12">
                            <div class="row">
                                <div class="col-sm-12">
                                    <!-- contact data table card start -->
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-header-text"><?=$staff->name?> School List</h4>
                                        </div>
                                        <div class="card-block contact-details">
                                            <div class="data_table_main table-responsive dt-responsive">
                                                <?php if(!empty($group)) { ?>
                                                <table id="simpletable"
                                                    class="table  table-striped table-bordered nowrap">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Full name</th>
                                                            <th>Username</th>
                                                            <th>Email</th>
                                                            <th>Phone</th>
                                                            <th>Address</th>
                                                            <th>On Date</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <?php
                                                                $i=1; 
                                                                $schools = $group->group->clients()->get();
                                                                foreach ($schools as $contract) {
                                                        ?>
                                                        <tr>
                                                            <td><?= $i;?></td>
                                                            <td><?= $contract->client->name ?></td>
                                                            <td><?= $contract->client->username ?></td>
                                                            <td><?= $contract->client->phone ?></td>
                                                            <td><?= $contract->client->email ?></td>
                                                            <td><?= $contract->client->address ?></td>
                                                            <td><?= date('d M Y', strtotime($contract->created_at)) ?></td>
                                                            <td>
                                                                <a class="btn btn-info btn-sm " href="<?= url('users/profile//'.$contract->client->username) ?>"> View   </a>
                                                            </td>
                                                        </tr>
                                                        <?php $i++; } ?>
                                                        <?php } else { ?>
                                                        <h4 class="text-center">Contract of <?= $staff->name ?> Not
                                                            Defined</h4>
                                                        <?php } ?>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>

@endsection
