@extends('layouts.app')

@section('content')
<?php $root = url('/public/'); ?>
<!-- Page-body start -->
<div class="page-body">
    <div class="row">
        <!-- customar project  start -->

        @if (count(load_users()) > 0)
        @foreach (load_users() as $user)
        <div class="col-xl-3 col-md-6">
            <div class="card">
                <div class="card-block">
                    <div class="row align-items-center m-l-0">
                        <div class="col-auto">
                            <i class="feather icon-users f-30 text-c-pink"></i>
                        </div>
                        <div class="col-auto">
                            <a href="{{ url('users/'.$user->table) }}">
                                <h6 class="text-muted m-b-10"> Total {{ ucfirst($user->table) }}s </h6>
                                <h2 class="m-b-0"> {{ $user->count }}</h2>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        @endif

        <!-- user start -->
        <div class="col-xl-6 col-md-12">
            <div class="card table-card">
                <div class="card-header">
                    <h5>Number of Students per School</h5>
                </div>
                <div class="card-block">
                    <div class="table-responsive">
                        <table class="table table-hover table-borderless">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Schools</th>
                                    <th>Male</th>
                                    <th>Female</th>
                                    <th class="text-center">Total</th>
                                    <th class="text-center"> Profile </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $total_male = 0;
                                $total_female = 0;
                            ?>
                                @if (count(load_schemas()) > 0)
                                @foreach (load_schemas() as $this_schema)
                                <tr>
                                    <td><img src="<?=$root?>/images/shulesoft_logo.png" alt="" class="img-fluid img-30">
                                    </td>
                                    <td>{{ $this_schema->username }}</td>
                                    <td>
                                        <?php
                                     $total_male = 0;
                                     $total_female = 0;
                                    $total_male += find_total_sex($this_schema->username, null, null, 'M');
                                    echo find_total_sex($this_schema->username, null, null, 'M');
                                     ?>
                                    </td>
                                    <td>
                                        <?php
                                    $total_female += find_total_sex($this_schema->username, null, null, 'F');
                                    echo find_total_sex($this_schema->username, null, null, 'F');
                                     ?>
                                    </td>
                                    <td><?= $total_male + $total_female ?></td>
                                    <td class="text-center"> <a
                                            href="{{ url('users/profile/'.$this_schema->username) }}"
                                            class="btn btn-info btn-sm btn-out-dotted"> View </a> </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                            <tfoot>
                                <!-- <tr>
                                <th class="text-center">Total</th>
                                <th class="text-center">4</th>
                                <th class="text-center">5</th>
                                <th class="text-center">6</th>
                                <th class="text-center">7</th>
                                <th></th>
                            </tr> -->
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-md-12">
            <div class="row">
                <!-- order-visitor start -->
                <div class="col-md-6">
                    <div class="card text-center text-white bg-c-green">
                        <div class="card-block">
                            <h6 class="m-b-0">Fee Collections</h6>
                            <h4 class="m-t-10 m-b-10"><i class="feather icon-arrow-up m-r-15"></i>{{number_format(all_schema_total_payments())}}</h4>
                            <p class="m-b-0">{{ number_format(monthly_payments(),2) }} This Months</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card text-center text-white bg-c-pink">
                        <div class="card-block">
                            <h6 class="m-b-0">Total Revenues</h6>
                            <h4 class="m-t-10 m-b-10"><i class="feather icon-arrow-up m-r-15"></i>{{number_format(all_schema_total_revenues(),2)}}</h4>
                            <p class="m-b-0">{{ number_format(monthly_revenues(),2) }} This Months</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card text-center text-white bg-c-lite-green">
                        <div class="card-block">
                            <h6 class="m-b-0">Total Expenses</h6>
                            <h4 class="m-t-10 m-b-10"><i class="feather icon-arrow-down m-r-15"></i>{{ number_format(all_schema_total_expenses(),2) }}/=</h4>
                            <p class="m-b-0">{{ number_format(monthly_expenses(),2) }}/= This Months</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card text-center text-white bg-c-yellow">
                        <div class="card-block">
                            <h6 class="m-b-0">Salary Payments</h6>
                            <h4 class="m-t-10 m-b-10"><i class="feather icon-arrow-up m-r-15"></i>{{ number_format(all_total_salary(),2) }}/=</h4>
                            <p class="m-b-0">{{ number_format(monthly_salary(),2) }}/= This Months</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- user end -->
    <?php
        $year = (int)request()->segment(3) > 0 ? request()->segment(3) : date('Y');

        $where =  (int)$year > 0 ? " WHERE c.date>'".date($year.'-01-01')."' " : " WHERE c.date>'".date('Y-01-01')."' ";

        ?>

     
        @if (count(load_schemas()) > 0)
        @foreach (load_schemas() as $this_schema)
      <?php
        $sql = 'SELECT coalesce(coalesce(sum(a.total_amount),0)-sum(a.discount_amount),0) as amount, coalesce(coalesce(sum(a.total_payment_invoice_fee_amount),0)+ coalesce(sum(a.total_advance_invoice_fee_amount)),0) as paid_amount, sum(a.balance) from ' . $this_schema->username . '.invoice_balances a join ' . $this_schema->username . '.student b on a.student_id=b.student_id join ' . $this_schema->username . '.invoices c on c.id=a.invoice_id' . $where;
        $collections = \collect(DB::select($sql))->first();
        ?>
        <div class="col-md-12 col-lg-4">
            <div class="card">
                <div class="card-block text-center">
                    <p class="m-b-20">{{ ucfirst($this_schema->username) }} - Fee Collection</p> 
                    <h4 class="m-t-20">Total - <span class="text-c-lite-blue"><?=money($collections->amount)?></span> </h4>
                    <h4 class="m-t-20"><span class="text-c-lite-green">Paid - <?=money($collections->paid_amount)?></span> </h4>
                    <h4 class="m-t-20">Unpaid - <span class="text-c-lite-orange"><?=money($collections->amount)?></span> </h4>
                    <a href="{{ url('dashboard/accounts/'.$this_schema->username.'/'.$year) }}" class="btn btn-primary btn-sm btn-round">View {{ $this_schema->username }}</a>
                </div>
            </div>
        </div>
        @endforeach
        @endif
        <!-- subscribe start
        <div class="col-md-6 col-lg-4">
            <div class="card">
                <div class="card-block text-center">
                    <i class="feather icon-twitter text-c-green d-block f-40"></i>
                    <h4 class="m-t-20"><span class="text-c-blgreenue">+40</span> Followers</h4>
                    <p class="m-b-20">Your main list is growing</p>
                    <button class="btn btn-success btn-sm btn-round">Check them out</button>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4">
            <div class="card">
                <div class="card-block text-center">
                    <i class="feather icon-briefcase text-c-pink d-block f-40"></i>
                    <h4 class="m-t-20">Business Plan</h4>
                    <p class="m-b-20">This is your current active plan</p>
                    <button class="btn btn-danger btn-sm btn-round">Upgrade to VIP</button>
                </div>
            </div>
        </div>
        <!-- subscribe end -->


        <!-- order  start 
        <div class="col-md-12 col-xl-4">
            <div class="card bg-c-yellow order-card">
                <div class="card-block">
                    <h6>Revenue</h6>
                    <h2>$42,562</h2>
                    <p class="m-b-0">$5,032 <i class="feather icon-arrow-up m-l-10"></i></p>
                    <i class="card-icon feather icon-filter"></i>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-4">
            <div class="card bg-c-blue order-card">
                <div class="card-block">
                    <h6>Orders Received</h6>
                    <h2>486</h2>
                    <p class="m-b-0">$5,032 <i class="feather icon-arrow-up m-l-10 m-r-10"></i>$1,055 <i class="feather icon-arrow-down"></i></p>
                    <i class="card-icon feather icon-users"></i>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-4">
            <div class="card bg-c-green order-card">
                <div class="card-block">
                    <h6>Total Sales</h6>
                    <h2>1641</h2>
                    <p class="m-b-0">$5,032 <i class="feather icon-arrow-down m-l-10 m-r-10"></i>$1,055 <i class="feather icon-arrow-up"></i></p>
                    <i class="card-icon feather icon-radio"></i>
                </div>
            </div>
        </div>
        <!-- order  end -->
    </div>
</div>
<!-- Page-body end -->

@endsection
