@extends('layouts.app')

@section('content')
<?php $root = url('/public/'); ?>
<!-- Page-body start -->
        <div class="page-body">
            <!-- Server Side Processing table start -->
            <div class="card">
                <div class="card-header">
                    <h5>Lists of Registered Portal Users</h5>
                    <!-- <span>The example below shows DataTables loading data for a table from arrays as the data source, </span> -->
                    <a href="{{ url('users/create') }}" class="btn btn-success">Add User</a>
                </div>
                <div class="card-block">
                <div class="dt-responsive table-responsive">
                        <table id="simpletable" class="table table-striped table-bordered nowrap">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Full name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th>Nationality</th>
                                    <th>Role</th>
                                    <th class="col-lg-2"> Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td> <strong>{{ $user->name }}</strong> </td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->phone }}</td>
                                    <td>{{ $user->address }}</td>
                                    <td>{{ $user->country->country }}</td>
                                    <td>{{ $user->role->name }}</td>
                                    <td>
                                        <a class="btn btn-info btn-sm btn-out-dotted" href="<?= url('Users/userinfo/'.$user->id) ?>"> View   </a>
                                        <a class="btn btn-danger btn-sm btn-out-dotted" href="<?= url('Users/prt_profile/'.$user->id) ?>"> Remove </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
 
                        </table>
                    </div>
                </div>
            </div>
            <!-- Server Side Processing table end -->
          
        </div>
 

@endsection