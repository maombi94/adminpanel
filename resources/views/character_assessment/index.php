
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-users"></i> panel_title') ?></h3>

        <ol class="breadcrumb">
            <li><a href="<?= url("dashboard/index") ?>"><i class="fa fa-laptop"></i> menu_dashboard') ?></a></li>
            <li class="active">menu_characters') ?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <?php
                $usertype = session("usertype");
                if (can_access('general_character_assessment')) {
                    ?>
                    <!--  <h5 class="page-header">
                          <a class="btn btn-success" href="<?php echo url('general_character_assessment/assess') ?>">
                              <i class="fa fa-plus"></i> 
                    assess_title') ?>
                          </a>
                      </h5> -->
                <?php } ?>
                <div class="col-sm-6 col-sm-offset-3 list-group">
                    <div class="list-group-item list-group-item-warning">
                        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                            <?php
                            if (form_error($errors, 'classlevel'))
                                echo "<div class='form-group has-error' >";
                            else
                                echo "<div class='form-group' >";
                            ?>
                            
                            <div class="col-sm-8 col-sm-offset-2">
                                <?php
                                $array = array("0" => $data->lang->line("classlevel"));
                                if (!empty($classlevels)) {
                                    foreach ($classlevels as $level) {
                                        $array[$level->classlevel_id] = $level->name;
                                    }
                                }
                                echo form_dropdown("class_level_id", $array, old("class_level_id"), "id='class_level_id' class='form-control'");
                                ?>
                            </div>
                            <span class="col-sm-2 control-label">
                                <?php echo form_error($errors, 'classlevel'); ?>
                            </span>
                    </div>

                    <?php
                    if (form_error($errors, 'class'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                   
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="select2-wrapper">
                            <select class="form-control" id="classes_id" name="classes_id">
                                <option value="">select_class") ?> </option>
                            </select>
                        </div>
                    </div>
                    <span class="col-sm-2 control-label">
                        <?php echo form_error($errors, 'class'); ?>
                    </span>
                </div>



                <?php
                if (form_error($errors, 'academic_year'))
                    echo "<div class='form-group has-error' >";
                else
                    echo "<div class='form-group' >";
                ?>
               
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="select2-wrapper">
                        <select class="form-control" id="academic_year_id" name="academic_year_id">
                            <option value="">select_academic_year") ?> </option>
                        </select>
                    </div>
                </div>
                <span class="col-sm-2 control-label">
                    <?php echo form_error($errors, 'academic_year'); ?>
                </span>
            </div>

            <?php
            if (form_error($errors, 'semester'))
                echo "<div class='form-group has-error' >";
            else
                echo "<div class='form-group' >";
            ?>
            
            <div class="col-sm-8 col-sm-offset-2">
                <div class="select2-wrapper">
                    <select class="form-control" id="semester_id" name="semester_id">
                        <option value="">select_semester") ?> </option>
                    </select>
                </div>
            </div>
            <span class="col-sm-2 control-label">
                <?php echo form_error($errors, 'semester'); ?>
            </span>
        </div>
        <?php
        if (form_error($errors, 'examID'))
            echo "<div class='form-group has-error' >";
        else
            echo "<div class='form-group' >";
        ?>
      
        <div class="col-sm-8 col-sm-offset-2 ">
            <?php
            $arraye = array("0" => $data->lang->line("menu_exam"));
//                                foreach ($exams as $exam) {
//                                    $array[$exam->examID] = $exam->exam;
//                                }
            echo form_dropdown("examID", $arraye, set_value("examID"), "id='examID' class='form-control'");
            ?>
            <span id="exam_warning"></span>
        </div>
    </div>


    <?= csrf_field() ?>
</form>
</div>
</div>
<?php
if (!empty($students)) {
    if (!empty($exam)) {
        ?>

        <div class="col-sm-12">
            <div class="col-sm-6 col-sm-offset-3 list-group">
                <div class="list-group-item">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Class Name</th>
                                <th>Number of Students</th>
                                <th>Exam Name</th>
                                <th>Semester Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?= $student_info->section->classes->classes ?></td>
                                <td><?= sizeof($students) ?></td>
                                <td><?= $exam->exam ?></td>
                                <td><?= $semester->name ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    <?php } ?>

    <div class="col-sm-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#all" aria-expanded="true">
                        menu_student") ?> (<?= sizeof($students) ?>)</a>
                </li>
                <?php
                foreach ($sections as $key => $section) {
                    echo '<li class=""><a data-toggle="tab" href="#' . $section->sectionID . '" aria-expanded="false">' . $section->section . " ( " . $section->category . " )" . '</a></li>';
                }
                ?>                             

            </ul>


            <div class="tab-content">
                <div id="all" class="tab-pane active">
                    <div id="hide-table">
                        <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                            <thead>
                                <tr>
                                    <th class="col-lg-1">slno') ?></th>
                                    <th class="col-lg-2">student_name') ?></th>
                                    <th class="col-lg-3">class_teacher_comment') ?></th>
                                    <th class="col-lg-3">head_teacher_comment') ?>/Character Assessment</th>
                                    <th class="col-lg-1">Average</th>
                                    <th class="col-lg-1">Exam Rank</th>
                                    <th class="col-lg-1">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                //dd($characters);
                                foreach ($students as $student) {
                                    ?>
                                    <tr>
                                        <td data-title="slno') ?>">
                                            <?php echo $i; ?>
                                        </td>

                                        <td data-title="student_name') ?>">
                                            <?php echo $student->name; ?>
                                        </td>

                                        <td data-title="class_teacher_comment') ?>">
                                            <textarea   class="form-control col-md-12 col-lg-12 col-xs-12" style="width: 100%;" id="comment_add<?php echo $student->student_id; ?>" placeholder="Write exam comment"><?php echo isset($characters[$student->student_id]) ? trim($characters[$student->student_id][0]->class_teacher_comment) : ''; ?></textarea>
                                            <button class="btn btn-xs btn-success" onclick="SaveComment('<?php echo $student->student_id; ?>')" >Save</button>
                                            <span id="stat<?php echo $student->student_id; ?>"></span>

                                        </td>

                                        <td data-title="head_teacher_comment') ?>" class="">

                                            <textarea  class="form-control col-md-12 col-lg-12 col-xs-12 head_teacher_comment_add" style="width: 100%;" id="comment_head_add<?php echo $student->student_id; ?>" placeholder="Write exam comment"><?php echo isset($characters[$student->student_id]) ? trim($characters[$student->student_id][0]->head_teacher_comment) : ''; ?></textarea>
                                            <button class="btn btn-xs btn-success" onclick="SaveHeadComment('<?php echo $student->student_id; ?>')" >Save</button>
                                            <span id="head_stat<?php echo $student->student_id; ?>"></span>



                                        </td>
                                        <td data-title="average') ?>"><?= $student->average ?></td>
                                        <td data-title="rank') ?>"><?= $student->rank ?></td>
                                        <td data-title="action') ?>">
                                            <div class="modal fade bs-example-modal-lg<?= $student->student_id ?>" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">

                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                            </button>
                                                            <h4 class="modal-title" id="myModalLabel">
                                                                Exam Report for <?= $student->name ?></h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h2><?= $exam->exam ?> Report</h2>
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Name</th>
                                                                        <?php
                                                                        if (isset($subjects)) {
                                                                            foreach ($subjects as $subject) {
                                                                                $subj = strtolower($subject->subject);
                                                                                $subject_sum["$subj"] = 0;
                                                                                echo !empty($subject) ?
                                                                                        '<th class="col-sm-1 verticalTableHeader">'
                                                                                        . '<p>' . substr(strtoupper($subject->subject), 0, 4) . ''
                                                                                        . '</p></th>' : '<th></th>';
                                                                            }
                                                                        }
                                                                        ?>
                                                                        <th>Total</th>
                                                                        <th>Average</th>
                                                                        <th>Rank per Class</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td><?= $student->name ?></td>
                                                                        <?php
                                                                        if (isset($subjects)) {
                                                                            foreach ($subjects as $subject) {

                                                                                $subject_name = strtolower($subject->subject);
                                                                                $stud = is_object($student) ? (array) $student : $student;

                                                                                if (isset($stud["$subject_name"])) {
                                                                                    $color = ($stud["$subject_name"] < $siteinfos->pass_mark && $stud["$subject_name"] != NULL) ? "pink" : "";

                                                                                    echo '<td  data-title="' . $subject_name . '" style="background: ' . $color . ';">' . $stud["$subject_name"] . '</td>';
                                                                                }else {
                                                                                        echo '<td></td>';
                                                                                }
                                                                            }
                                                                        }
                                                                        ?>
                                                                        <td><?= $student->total ?></td>
                                                                        <td><?= $student->average ?></td>
                                                                        <td><?= $student->rank ?></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg<?= $student->student_id ?>">View Full Report</a>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                </div>

                <?php foreach ($sections as $key => $section) { ?>
                    <div id="<?= $section->sectionID ?>" class="tab-pane">
                        <div id="hide-table">
                            <table class="table table-striped table-bordered table-hover dataTable no-footer">
                                <thead>
                                    <tr>
                                        <th class="col-lg-1">slno') ?></th>
                                        <th class="col-lg-2">student_name') ?></th>
                                        <th class="col-lg-3">class_teacher_comment') ?></th>
                                        <th class="col-lg-3">head_teacher_comment') ?></th>
                                        <th class="col-lg-1">Average</th>
                                        <th class="col-lg-1">Exam Rank</th>
                                        <th class="col-lg-1">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($allsection[$section->section] as $student) {
                                        ?>
                                        <tr>
                                            <td data-title="slno') ?>">
                                                <?php echo $i; ?>
                                            </td>

                                            <td data-title="student_name') ?>">
                                                <?php echo $student->name; ?>
                                            </td>

                                            <td data-title="class_teacher_comment') ?>">

                                                <textarea   class="form-control col-md-12 col-lg-12 col-xs-12" style="width: 100%;" id="comment_add<?php echo $student->student_id; ?>" placeholder="Write exam comment"><?php echo isset($characters[$student->student_id]) ? trim($characters[$student->student_id][0]->class_teacher_comment) : ''; ?></textarea>
                                                <button class="btn btn-xs btn-success" onclick="SaveCommentSection('<?php echo $student->student_id; ?>')" >Save</button>
                                                <span id="stat_sec<?php echo $student->student_id; ?>"></span>

                                            </td>

                                            <td data-title="head_teacher_comment') ?>" >


                                                <textarea  class="form-control col-md-12 col-lg-12 col-xs-12 head_teacher_comment_add" style="width: 100%;" id="comment_head_add<?php echo $student->student_id; ?>" placeholder="Write exam comment"><?php echo isset($characters[$student->student_id]) ? trim($characters[$student->student_id][0]->head_teacher_comment) : ''; ?></textarea>
                                                <button class="btn btn-xs btn-success" onclick="SaveHeadCommentSec('<?php echo $student->student_id; ?>')" >Save</button>
                                                <span id="head_stat_sec<?php echo $student->student_id; ?>"></span>

                                            </td>
                                            <td data-title="average') ?>"><?= $student->average ?></td>
                                            <td data-title="rank') ?>"><?= $student->rank ?></td>
                                            <td data-title="action') ?>">
                                                <div class="modal fade bs-example-modal-lg<?= $student->student_id ?>" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">

                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                                </button>
                                                                <h4 class="modal-title" id="myModalLabel">
                                                                    Exam Report for <?= $student->name ?></h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <h2><?= $exam->exam ?> Report</h2>
                                                                <table class="table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Name</th>
                                                                            <?php
                                                                            if (isset($subjects)) {
                                                                                foreach ($subjects as $subject) {
                                                                                    $subj = strtolower($subject->subject);
                                                                                    $subject_sum["$subj"] = 0;
                                                                                    echo !empty($subject) ?
                                                                                            '<th class="col-sm-1 verticalTableHeader">'
                                                                                            . '<p>' . substr(strtoupper($subject->subject), 0, 4) . ''
                                                                                            . '</p></th>' : '<th></th>';
                                                                                }
                                                                            }
                                                                            ?>
                                                                            <th>Total</th>
                                                                            <th>Average</th>
                                                                            <th>Rank per Stream</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td><?= $student->name ?></td>
                                                                            <?php
                                                                            if (isset($subjects)) {
                                                                                foreach ($subjects as $subject) {

                                                                                    $subject_name = strtolower($subject->subject);
                                                                                    $stud = is_object($student) ? (array) $student : $student;

                                                                                    if (isset($stud["$subject_name"])) {
                                                                                        $color = ($stud["$subject_name"] < $siteinfos->pass_mark && $stud["$subject_name"] != NULL) ? "pink" : "";

                                                                                        echo '<td  data-title="' . $subject_name . '" style="background: ' . $color . ';">' . $stud["$subject_name"] . '</td>';
                                                                                    }else {
                                                                                        echo '<td></td>';
                                                                                }
                                                                                } 
                                                                            }
                                                                            ?>
                                                                            <td><?= $student->total ?></td>
                                                                            <td><?= $student->average ?></td>
                                                                            <td><?= $student->rank ?></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg<?= $student->student_id ?>">View Full Report</a>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php } ?>
            </div>

        </div> <!-- nav-tabs-custom -->
    </div> <!-- col-sm-12 for tab -->
<?php } ?>

<script type="text/javascript">

    function SaveHeadCommentSec(a) {
        return SaveHeadComment(a, '_sec');
    }
    function SaveHeadComment(a, b = null) {
        var content = $('#comment_head_add' + a).val();
        var semester_id = '<?= $semester_id ?>';
        if (content != '') {
            $.ajax({
                type: 'POST',
                url: "<?= url('general_character_assessment/addHeadTeacherComment') ?>",
                data: "comment=" + content + "&sid=" + a+"&semester_id="+semester_id  + "&exam_id=" + <?= (isset($exam) && !empty($exam)) ? $exam->examID : '0' ?>,
                dataType: "html",
                beforeSend: function (xhr) {
                    $('#head_stat' + b + a).html('<a href="#/refresh"><i class="fa fa-spinner"></i> </a>');
                },
                complete: function (xhr, status) {
                    $('#head_stat' + b + a).html('<span class="label label-success">' + status + '</span>');
                },
                success: function (data) {
                    toast(data);
                }
            });
    }
    }
    ; 
    function SaveCommentSection(a) {
        return SaveComment(a, '_sec');
    }
    function SaveComment(a, b = null) {
        var content = $('#comment_add' + a).val();
        var semester_id = '<?= $semester_id ?>';
        if (content != '') {
            $.ajax({
                type: 'POST',
                url: "<?= url('general_character_assessment/addClassTeacherGeneralComment') ?>",
                data: "comment=" + content + "&sid=" + a +"&semester_id="+semester_id + "&exam_id=" + <?= (isset($exam) && !empty($exam)) ? $exam->examID : '0' ?>,
                dataType: "html",
                beforeSend: function (xhr) {
                    $('#stat' + b + a).html('<a href="#/refresh"><i class="fa fa-spinner"></i> </a>');
                },
                complete: function (xhr, status) {
                    $('#stat' + b + a).html('<span class="label label-success">' + status + '</span>');
                },
                success: function (data) {
                    toast(data);
                }
            });
    }
    }

    $('#class_level_id').change(function (event) {
        var class_level_id = $(this).val();
        if (class_level_id === '0') {
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('classes/getClasses') ?>",
                data: "class_level_id=" + class_level_id,
                dataType: "html",
                success: function (data) {
                    $('#classes_id').html(data);
                }
            });
        }
    });



    $('#classes_id').change(function (event) {
        var class_level_id = $('#class_level_id').val();
        if (class_level_id == "") {

        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/get_academic_years_bylevel') ?>",
                data: "class_level_id=" + class_level_id,
                dataType: "html",
                success: function (data) {
                    $('#academic_year_id').html(data);
                }
            });
        }
    });


    $('#academic_year_id').change(function (event) {
        var academic_year_id = $(this).val();
        if (academic_year_id === '0') {
            $('#semester_id').val(0);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('semester/get_semester') ?>",
                data: "academic_year_id=" + academic_year_id,
                dataType: "html",
                success: function (data) {
                    $('#semester_id').html(data);
                }
            });
        }
    });

    $('#semester_id').change(function (event) {
        var academic_year_id = $('#academic_year_id').val();
        var semester_id = $(this).val();
        var classesID = $('#classes_id').val();
        if (academic_year_id === '0') {
            $('#examID').val(0);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('exam/getExamByClass') ?>",
                data: "id=" + classesID + "&academic_year_id=" + academic_year_id + '&semester_id=' + semester_id,
                dataType: "html",
                success: function (data) {
                    $('#exam_warning').html('');
                    if (data === "<option value='0'>Exam Name</option>") {
                        $('#exam_warning').html('<div class="alert alert-danger">No exams defined. Please <a href="<?= url('exam/index') ?>">click here </a> define exam in exam section</div>');
                    } else {
                        $('#examID').html(data);
                    }
                }
            });
        }
    });
    $('#examID').change(function (event) {
        var exam_id = $(this).val();
        var semester_id = $('#semester_id').val();
        var class_id = $('#classes_id').val();
        var academic_year_id = $('#academic_year_id').val();
        if (class_id == "") {

        } else {
            window.location.href = "<?= url('general_character_assessment/index') ?>/" + semester_id + "/" + class_id + '/' + academic_year_id + '/' + exam_id;
        }
    });
</script>
