@extends('layouts.app')
@section('content')

<link href="<?php echo url('public/assets/print.css'); ?>" rel="stylesheet" type="text/css">

<div class="well">
    <div class="row">
        <div class="col-sm-6">

            <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span
                    class="fa fa-print"></span> print') ?> </button>

            <span>&nbsp;</span>
            <?php
            if (isset($allsections) && count($allsections) > 0) {
                foreach ($allsections as $section_info) {
                    ?>

                    <a href="<?= url("exam/printall/" . $exams->examID . '?class_id=' . $student->classesID . '&section_id=' . $section_info->sectionID) . '&year_no=' . request('year_no') ?>"
                       target="_blank">
                        <i class="fa fa-print"></i>print') ?> all (<?= $section_info->section ?>)</a>
                    <span>&nbsp;</span>
                    <?php
                }
            }
            ?>

        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb">
                <li><a href="<?= url("dashboard/index") ?>"><i
                            class="fa fa-laptop"></i> menu_dashboard') ?></a></li>
                <li class="active">view') ?></li>
            </ol>
        </div>
    </div>

</div>
<!-- Begin inline CSS -->
<style type="text/css">

    .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
        font-weight: bolder
    }

    #printablediv {
        color: #000;
    }

    table.table.table-striped td, table.table.table-striped th, table.table {
        font-size: 12px !important;
        margin-left: 10%;
    }

    @page {
        margin: 0
    }

    .sheet {
        margin: 0;
        overflow: hidden;
        position: relative;
        box-sizing: border-box;
        page-break-after: always;
    }

    /** Padding area **/
    .sheet.padding-10mm {
        padding: 10mm
    }

    /** For screen preview **/
    @media screen {

        .sheet {
            background: white;
            box-shadow: 0 .5mm 2mm rgba(0, 0, 0, .3);
            margin: 5mm;
            left:12%;
        }
    }

    table.table.table-striped td, table.table.table-striped th, table.table {
        border: 1px solid #000000;
    }
    @font-face {
        font-family: DejaVuSans_b;
        src: url("../../public/assets/fonts/DejaVuSans_b.woff") format("woff");
    }

    table.table.table-striped td, table.table.table-striped th, table.table {
        border: 1px solid #000000;
        margin: 1px auto !important;
    }

    @media print {
        html, body {
            width: 210mm;
            height: 297mm;
        }

        section.A4 {
            width: 210mm
        }

        @page {
            size: A4;
            margin: 0;

        }

        table {
            width: 100%;
            border-spacing: 0;
        }

        .subpage {
            padding: 1em 1em 0 1em;
            /* border: 3px #f00 solid; */
            font-size: .8em;
        }

        table.table.table-striped td, table.table.table-striped th, table.table {
            border: .1px solid #000;
        }

        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
            padding: 0.2%;
        }

        .table-bordered > thead > tr > th, .table-bordered > tbody > tr > td {
            font-size: 11px;
        }


    }

    h3, h4 {
        page-break-after: avoid;

    }

</style>

<div class="box-body">
    <div class="row">
        <div class="col-sm-12">

            <div id="printablediv" class="page center sheet padding-10mm" >

                <?php
                $array = array(
                    "src" => url('storage/uploads/images/' . $siteinfos->photo),
                    'width' => '126em',
                    'height' => '126em',
                    'class' => 'img-rounded',
                    'style' => 'margin-left:2em'
                );

                $photo = array(
                    "src" => url('storage/uploads/images/' . $student->student->photo),
                    'width' => '128em',
                    'height' => '128em',
                    'class' => 'img-rounded',
                    'style' => 'margin-left:2em'
                );
                ?>

                <table class=" table-striped center" style="margin: 1px 2px 1px 0px;">
                    <thead>
                        <tr>
                            <th class="col-md-2" style="padding-bottom:0px">
                                <?= img($array); ?>
                            </th>
                            <th class="col-md-8 text-center" style="margin: 1% 0 0 16%; padding-top: 2%; color: #000000;">

                                <h2 style="font-weight: bolder; text-transform: uppercase;"><?= $siteinfos->sname ?></h2>
                                <h4>character_report_box") ?><?= $siteinfos->box . ', ' . $siteinfos->address ?>.</h4>

                                <h3>character_report_cell") ?> <?= str_replace(',', ' / ', $siteinfos->phone) ?></h3>
                                <h3> character_report_email") ?><?= $siteinfos->email ?></h3>
                                <h3>character_report_website") ?><?= $siteinfos->website ?></h3>
                            </th>
                            <th class="col-md-2" style="padding-bottom:0px">
                                <?= img($photo); ?>
                            </th>
                        </tr>
                    </thead>
                </table>
                <hr/>

                <div class="">
                    <h1 align='center' style="font-size: 1.6em; font-weight: bolder;padding-top: 0%; text-transform: uppercase; ">character_report_header") . '(' . $studentacademicYear->name . ')' ?></h1>
                </div>
                <?php
                $characterController = new \App\Http\Controllers\General_character_assessment();
                if (isset($character_categories) && count($character_categories) > 0) {

                    echo '<br/><p align="left" style="padding-top: 0%; ">' . $data->lang->line("report_student_name") . '<b>' . $student->student->name . '</b> <span class="right">Class: <b>' . $student->section->classes->classes . '</b></span></p><span>Adm No: <b>' . $student->student->roll . '</b></span>';
                    foreach ($character_categories as $character_category) {

                        echo '<h4 align="left" style="font-size: 1.2em; font-weight: bolder;padding-top: 0%; margin-top: 2%; ">' . ucwords($character_category->character_category) . '</h4>';
                        if ($character_category->based_on == '1') {
                            ?>
                            <table id="example1" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="col-lg-2">code') ?></th>
                                        <th class="col-lg-6">character') ?></th>
                                        <th class="col-lg-2">remark1') ?></th>
                                        <th class="col-lg-2">remark2') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $character_category_id = $character_category->id;
                                    $all_characters = \App\Model\Character::where('character_category_id', $character_category->id)->get();
                                    $n = 0;

                                    foreach ($all_characters as $all_character) {
                                        $character_id = $all_character->id;
                                        ?>
                                        <tr>
                                            <td data-title="code') ?>">
                                                <?php echo $all_character->code; ?>
                                            </td>
                                            <td data-title="character') ?>">
                                                <?php echo $all_character->description; ?>
                                            </td>
                                            <?php
                                            $remark1 = $characterController->getRemark($student_id, $section_id, $semester_id, $character_category_id, $character_id);

                                            if (count($remark1) > 0) {
                                                ?>
                                                <td data-title="remark1') ?>">
                                                    <?php echo $remark1->remark1; ?>
                                                </td>
                                                <td data-title="remark2') ?>">
                                                    <?php
                                                    $remark2 = $characterController->getRemark($student_id, $section_id, $semester_id, $character_category_id, $character_id);
                                                    echo count($remark2) > 0 ? $remark2->remark2 : '';
                                                    ?>
                                                </td>
                                            <?php } else { ?>

                                                <td data-title="">

                                                    <div id="alert<?= $n ?>"></div>
                                                    <form method="POST">
                                                        <div id="checkbox_div" style="display: block;">
                                                            <input type="checkbox" name="show_assess_select" id="show_assess_select" onclick ="showSelect(<?= $n ?>);">
                                                            character_report_assess") ?>
                                                        </div>
                                                        <input type="hidden" id="semester_id" name="semester_id" value="<?php echo $semester_id; ?>">
                                                        <input type="hidden" id="character_id<?= $n ?>" name="character_id" value="<?php echo $character_id; ?>">
                                                        <input type="hidden" id="student_assessed" name="student_assessed" value="<?php echo $student_id; ?>">
                                                        <div style="display: none;" id="grade_div<?= $n ?>">
                                                            <select class="form-control" name ="a_grade" id="a_grade">
                                                                <option value="">character_report_select_a_grade") ?></option>
                                                                <?php foreach ($character_gradings as $grading_system) { ?>
                                                                    <option value="<?php echo $grading_system->id; ?>"><?php echo $grading_system->grade_remark; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                </td>
                                                <td>

                                                    <div style="display: none;" id="grade_div2<?= $n ?>">
                                                        <select class="form-control" name ="e_grade" id="e_grade" onchange="add_remark(<?= $n ?>, 0);">
                                                            <option value="">character_report_select_e_grade") ?></option>
                                                            <?php foreach ($character_gradings as $grading_system) { ?>
                                                                <option value="<?php echo $grading_system->id; ?>"><?php echo $grading_system->grade_remark; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <?= csrf_field() ?>
                                                    </form>
                                                <?php }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                        $n++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        <?php } else { ?>
                            <table id="example1" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="col-lg-2">code') ?></th>
                                        <th class="col-lg-8">character') ?></th>
                                        <?php foreach ($character_gradings as $grade) { ?>
                                            <th class="col-lg-2"><?= $grade->grade ?></th>
                                        <?php } ?>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $all_characters = \App\Model\Character::where('character_category_id', $character_category->id)->get();
                                    $x = 100;
                                    foreach ($all_characters as $all_character) {

                                        $x = $x + $all_character->id;
                                        $character_id = $all_character->id;
                                        $student_character = \App\Model\StudentCharacter::where('character_id', $character_id)->where('student_id', $student_id)->where('semester_id', $semester_id)->first();
                                        ?>
                                        <tr>
                                            <td data-title="code') ?>">
                                                <?php echo $all_character->code; ?>
                                            </td>
                                            <td data-title="description') ?>">
                                                <?php
                                                echo $all_character->description;
                                                //
                                                // echo $student_character->grade;  
                                                ?>
                                            </td>
                                            <?php foreach ($character_gradings as $grade) { ?>
                                                <td data-title="description') ?>">
                                                    <?php
//                                                    $sql = 'SELECT b.* FROM ' . set_schema_name() . ' student_characters a join ' . set_schema_name() . 'refer_character_grading_systems b on b.id=a.grade1 WHERE a.student_id = ' . $student_id . '  AND a.semester_id=' . $semester_id . ' and b.id=' . $grade->id . ' AND a.character_id =' . $character_id;
//                                                    $rem = \collect(DB::select($sql))->first();
//                                                    $grad = $character_gradings->first();
                                                    if (count($student_character) && $student_character->grade == $grade->id) {
                                                        echo '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>';
                                                    } else {
                                                        ?>
                                                        <input type="radio" name="<?= $character_id ?>" id="show_assess_select" value="<?= $grade->id ?>" onclick ="assesCharacter(<?= $grade->id ?>, <?= $character_id ?>,<?php echo $student_id; ?>,<?php echo $semester_id; ?>);">
                                                    </td>
                                                    <?php
                                                }
                                            }
                                            ?>

                                        </tr>
                                        <?php
                                        $x++;
                                    }
                                    ?>
                                </tbody>
                            </table><?php
                        }
                    }
                    ?>
                    <div class="form-group" id="refresh" style="display: none;" >
                        <div class=" col-sm-12">
                            <input type="submit" class="btn btn-success pull-right" value="report_student_refresh") ?>" >
                        </div>
                    </div>
                    <div class="col-sm-12" style="margin-top: 3%;">
                        <div class="row">
                            <div class="col-lg-4"></div>                              
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4">
                                <h3 align='center' style="font-size: 1.5em; font-weight: bolder;padding-top: 0%; margin-bottom: 2%;"><?php echo $data->lang->line("report_grading_system"); ?></h3>
                                <table id="example1" style="width: 80%;" class="table table-striped table-bordered table-hover">
                                    <thead>

                                    <th class="col-lg-4">character_report_remark") ?></th>
                                    <th class="col-lg-2">character_report_grade") ?></th>

                                    </thead>
                                    <tbody>
                                        <?php foreach ($character_gradings as $grading_system) { ?>
                                            <tr>
                                                <td><?php echo $grading_system->grade_remark; ?></td>
                                                <td><?php echo $grading_system->grade; ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <h3 align='left' style="font-size: 1.2em; font-weight: bolder;padding-top: 0%; margin-top: 2%;"><?php echo $data->lang->line("report_class_teacter_comment"); ?></h3>
                        <?php
                        $teachers_comments = $characterController->getTeacherComments($student_id);
                    }
                    ?>
                    <p style="margin-left: 5%; font-size: 1em;"><?php echo count($teachers_comments) > 0 ? $teachers_comments->class_teacher_comment : '' ?></p>
                    <h4 align='right' style="font-size: 1em; font-weight: bolder;padding-top: 0%; margin-top: 2%;"><?php echo $data->lang->line("report_class_teacter_signature"); ?>...................</h4>
                    <?php if (isset($teachers_comments->head_teacher_comment)) { ?>
                        <h3 align='center' style="font-size: 1.2em; font-weight: bolder;padding-top: 0%; margin-top: 2%;"><?php echo $data->lang->line("report_head_teacter_comment"); ?></h3>
                        <p style="margin-left: 5%;"><?php echo $teachers_comments->head_teacher_comment; ?></p>
                        <h4 align='right' style="font-size: 1em; font-weight: bolder;padding-top: 0%; margin-top: 2%;"><?php echo $data->lang->line("report_head_teacter_signature"); ?>...................</h4>
                    <?php } ?>
                    <div style="padding-left:5%;">

                        <div style="z-index: 4000">
                            <div style="float: right; margin-right: 4%; margin-top: 4%;"></div>
                            <div>

                                <?php
                                $path = "../../storage/uploads/images/stamp_" . set_schema_name() . "png?v=1";
                                ?>
<?php
                                                    $array_ = array(
                                                        "src" => $path,
                                                        'width' => '100',
                                                        'height' => '100',
                                                        'class' => '',
                                                        'id' => "",
                                                        'style' => 'position:relative; margin:-17% 15% 0 0; float:right;'
                                                    );
                                                    echo img($array_);
                                                    ?>
                                
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <div class="actionBar"><a href="#" class="buttonFinish buttonDisabled btn btn-default">Previous</a><a href="<?= url('general_character_assessment/report/' . $semester_id . '/' . $section_id . '/' . $next_id) ?>" class="buttonNext btn btn-success">Next</a><a href="#" class="buttonPrevious buttonDisabled btn btn-primary">Finish</a></div>
        </div>
    </div>
</div>
<script type="text/javascript">

    function refresh()
    {
        // window.location.reload();
        $('#refresh').hide();
    }

    function showSelect(a)
    {
        $('#grade_div' + a).show();
        $('#refresh' + a).show();
        $('#grade_div2' + a).show();
        $('#checkbox_div').hide();
    }

    function assesCharacter(a, b, c, d) {
        if (a == "" || b == "") {
            var data = '<h5>Missing data, please select again</h5>';
            alert(data);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('student_characters/assessReport/null') ?>",
                data: "character_id=" + b + "&student_assessed=" + c + "&semester_id=" + d + "&e_grade=check&a_grade=" + a + "&level=" + 1,
                dataType: "html",
                success: function (data) {
                    toastr["success"](data);
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "500",
                        "hideDuration": "500",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }

                }
            });
            $('#grade_div' + a).hide();
            $('#grade_div2' + a).hide();
            $('#checkbox_div').hide();
        }
    }
    function add_remark(a, b) {
        var semester_id = $('#semester_id').val();
        var character_id = $('#character_id' + a).val();
        var e_grade = $('#e_grade' + a).val();
        var a_grade = $('#a_grade' + a).val();
        var student_assessed = $('#student_assessed').val();
        var level = b;
        if (e_grade == "" || a_grade == "") {
            var data = '<h5>character_report_selectall_message") ?></h5>';
            alert(data);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('student_characters/assessReport/null') ?>",
                data: "character_id=" + character_id + "&student_assessed=" + student_assessed + "&semester_id=" + semester_id + "&e_grade=" + e_grade + "&a_grade=" + a_grade + "&level=" + level,
                dataType: "html",
                success: function (data) {
                    $('#alert' + a).html(data);

                }
            });
            $('#grade_div' + a).hide();
            $('#grade_div2' + a).hide();
            $('#checkbox_div').hide();
        }
    }
    function printDiv(divID) {
        window.print();
    }
</script>
@endsection