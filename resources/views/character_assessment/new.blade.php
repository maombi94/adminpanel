@extends('layouts.app')
@section('content')
<link href="<?php echo url('public/assets/print.css'); ?>" rel="stylesheet" type="text/css">

<div class="well">
    <div class="row">
    <div class="col-sm-6">

    <?php   if (!$exam_report) {    ?>
    <button class="btn-cs btn-sm-cs"  data-toggle="modal" data-target="#exampleModal" title="Click here to add  reporting date, send SMS/Email to parents about this report and add a new report title. After this, you can see the option to print reports in all sections"></span> Validate & Create Official Report</button>    
            <a class="btn-info btn-cs btn-sm-cs" id="downloadimage"><span class="fa fa-download"></span> Download Report</a>
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Validate and Create Report</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <h1 class="head">Before you click continue, please ensure</h1>
                <div class="row">
                    <ul class="list-group">
                        <li class="list-group-item justify-content-between">1. All marks have been recorded <span class="badge badge-default badge-pill">important</span></li>
                        <li class="list-group-item justify-content-between">2. School administration have approved that all reports are correctly</li>
                    </ul>
                </div>
                <p class="alert alert-info">
                    If you click "Create Report", you will now be able to print this report. System will also be able to send SMS and Email notification to parents if you click checkboxes below
                </p>
                <form id="demo-form2" action="<?= url('General_character_assessment/create_report/'.$class_id.'/'.$exam_id.'') ?>" method="post" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Report Name 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="first-name" placeholder="E.g Terminal Exam Results" required class="form-control col-md-7 col-xs-12" name="name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">To Parents
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="checkbox" name="email_sent" id="send_email_option">Send Email
                            &nbsp;<input type="checkbox" name="sms_sent" id="send_sms_option">Send SMS
                        </div>
                    </div>
                 
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="reporting_date">Date of reporting 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="reporting_date" placeholder="Which day a school will open" required="required" class="calendar form-control col-md-7 col-xs" name="reporting_date">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                        </label>
                        <div class="col-md-6 text-center col-sm-6 col-xs-12">
                            <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span
                                    class="fa fa-print"></span> print') ?> Preview</button>
                                    <?php
                if ($exam_report) {
                    ?>
                            <a href=""
                               target="_blank"><i class="fa fa-laptop"></i>print') ?> all</a>
                <?php } ?>
                        </div>
                    </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">close') ?></button>
        <input type="submit" name="submit"  class="btn btn-success" value="Create Report"/>
      </div>
    </div>
  </div>
</div>
   

    </div>
    <?php
                }
                else{
                    ?>

<button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span
            class="fa fa-print"></span> print') ?> </button>
                    
                    <?php
                    $rpt = 1;
                    foreach ($allsections as $section_info) {
                        
                         ?>

                        <a href="<?=url('general_character_assessment/printall/'.$exam_id.'/'.$section_info->sectionID.'/'.$academic_year_id)?>"><i class="fa fa-print"></i> print') ?> all (<?= $section_info->section ?>)</a>
                        <span>&nbsp;</span>
                        <?php
                        $rpt++;
                    }
                }
                
                    ?>


</div>

            <ol class="breadcrumb">
                <li><a href="<?= url("dashboard/index") ?>"><i
                            class="fa fa-laptop"></i> menu_dashboard') ?></a></li>
                <li class="active">view') ?></li>
                <button class="btn-default btn-cs btn-sm-cs" data-toggle="modal" data-target="#report_setting_model"><span class="fa fa-gear"></span> Options</button>
            </ol>
        </div>
    </div>
    
<style type="text/css">
    .innertable{
        height: 7em;
    }
    .outertable tr td{
        border: 1px solid black;

    }
    .outertable{
        width: 100%;
        max-width: 100%;
        background-color: transparent;
        border-spacing: 0;
        border-collapse: collapse;
    }


</style>
<div class="box-body">
    <div class="row">
        <div class="col-sm-12">
            <?php
            if (isset($character_categories) && !empty($character_categories)) {
                ?>
                <div id="printablediv" class="page center sheet padding-10mm" >

                    <?php
                    $array = array(
                        "src" => url('storage/uploads/images/' . $siteinfos->photo),
                        'width' => '126em',
                        'height' => '126em',
                        'class' => 'img-rounded',
                        'style' => 'margin-left:2em'
                    );

                    $photo = array(
                        "src" => url('storage/uploads/images/' . $student->student->photo),
                        'width' => '128em',
                        'height' => '128em',
                        'class' => 'img-rounded',
                        'style' => 'margin-left:2em'
                    );
                    ?>

                    <table class=" table-striped center " style="margin: 1px 2px 1px 0px;">
                        <thead>
                            <tr>
                                <th class="col-md-2" style="padding-bottom:0px">
                                    <?= img($array); ?>
                                </th>
                                <th class="col-md-8 text-center" style="margin: 1% 0 0 16%; padding-top: 2%; color: #000000;">

                                    <h2 style="font-weight: bolder; text-transform: uppercase;"><?= $siteinfos->sname ?></h2>
                                    <h4>character_report_box") ?><?= $siteinfos->box . ', ' . $siteinfos->address ?>.</h4>

                                    <h3>character_report_cell") ?> <?= str_replace(',', ' / ', $siteinfos->phone) ?></h3>
                                    <h3> character_report_email") ?><?= $siteinfos->email ?></h3>
                                    <h3>character_report_website") ?><?= $siteinfos->website ?></h3>
                                </th>

                                <th class="col-md-2" style="padding-bottom:0px">
                                    <?= img($photo); ?>
                                </th>

                            </tr>
                        </thead>
                    </table>
                    <hr/>

                    <div class="">
                        <h1 align='center' style="font-size: 1.6em; font-weight: bolder;padding-top: 0%; text-transform: uppercase; ">character_report_header") . '(' . $student->academicYear->name . ')' ?></h1>
                    </div>
                    <div class="">
                        <?php echo '<br/><p align="left" style="padding-top: 0%; ">' . $data->lang->line("report_student_name") . '<b>' . $student->student->name . '</b> <span class="right">Class: <b>' . $student->section->classes->classes . ', ' . $section->section . '</b></span></p><span>Adm No: <b>' . $student->student->roll . '</b></span>'; ?>
                    </div>

                    <h5 align='center' style="font-weight: bolder;padding-top: 0%; text-transform: uppercase; ">DEVELOPMENT OF LEARNING COMPETENCE</h5>
                    <?php
                    $tt = 1;
                    foreach ($character_categories as $character_category) {
                        ?>
                        <?php echo '<h4 align="left" style="font-size: 13px; font-weight: bolder;padding-top: 0%; margin-top: 2%; margin-bottom:1%; color:black ">Competence: ' . ucwords($character_category->character_category) . '</h4>'; ?>

                        <?php
                        $all_characters = \App\Model\Character::where('character_category_id', $character_category->id)->join('character_classes', 'character_classes.character_id', '=', 'characters.id')->where('class_id', $class_id)->orderBy('position', 'asc')->get();
                        $x = 1;
                        ?>
                        <table class="outertable table table-bordered">
                           
                            <tbody>
                                <tr>
                                    <?php
                                    foreach ($all_characters as $all_character) {


                                        $character_id = $all_character->character_id;
                                        $student_character = \App\Model\StudentCharacter::where('character_id', $character_id)->where('student_id', $student_id)->where('exam_id', $exam_id)->first();
                                        
                                        ?>
                                        <td style="padding:0; border:none">
                                            <table class="innertable table table-bordered" style="padding-bottom:0;margin-bottom: 0;border:none">
                                                <thead>
                                                    <tr>
                                                        <th class="col-md-2" colspan="10" style="height:3em !important;"> <?= $all_character->description; ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <tr>
                                                        <?php foreach ($character_gradings as $grade) { ?>
                                                            <td style="height:2em !important; text-align: center"><?= $grade->grade ?></td>
                                                        <?php } ?>
                                                    </tr>
                                                    <tr>
                                                        <?php foreach ($character_gradings as $grade) { ?>
                                                            <td style="height:2em !important; text-align: center"> <?php
                                                            if ($student_character && $student_character->grade == $grade->id) {
                                                                echo '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>';
                                                            } else {
                                                                if (can_access('assess_character')) {
                                                                    ?>
                                                                        <input type="radio" name="<?= $all_character->character_id ?>" id="show_assess_select" value="<?= $grade->id ?>" class="radio_inputs" onclick ="assesCharacter(<?= $grade->id ?>, <?= $all_character->character_id ?>,<?php echo $student_id; ?>,<?php echo $exam_id; ?>);">
                                                                        <span id="<?= $grade->id ?><?= $all_character->character_id ?><?php echo $student_id; ?><?php echo $exam_id; ?>"></span>

                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </td>
                                                        <?php } ?>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </td>
                                    <?php } ?>
                                </tr>
                            </tbody>
                        </table>
                        <?php
                    }
                    ?>
                    <table class="innertable table table-bordered" style="padding-bottom:0;margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th class="col-md-2" colspan="<?=count($character_gradings)?>" style="text-align: center;">CHARACTER GRADING</th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <?php foreach ($character_gradings as $grade) { ?>
                                    <td style=" text-align: center"><?= $grade->grade ?></td>
                                <?php } ?>
                            </tr>
                            
                            <tr>
                                <?php foreach ($character_gradings as $grade) { ?>
                                    <td style=" text-align: center"><?= $grade->grade_remark ?></td>
                                <?php } ?>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table  table-bordered" style=" margin: 1% 0 0 0;">
                        <thead>
                            <tr>
                                <th style="font-weight: bolder; width:56%; text-align: left;"><strong style="vertical-align: top;"><?php echo $data->lang->line("report_class_teacter_comment"); ?>:</strong><br>
                                    <?php if (can_access('assess_character')) { ?>
                                        <textarea class="bolder form-control add_comment" type='1' style="border-bottom: 1px solid black"><?= $assessment ? $assessment->class_teacher_comment : '' ?></textarea>
                                        <?php
                                    } else {
                                        echo $assessment ? $assessment->class_teacher_comment : '';
                                    }
                                    ?>
                                </th>
                                <th style="text-align: left;"> Class Teacher: <?= $section->teacher->name ?><br>
                                    <img src="<?= $section->teacher->signature ?>" width="54" height="32">
                                </th>
                            </tr>
                        </thead>
                    </table>
                    <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                        <thead>
                            <tr>
                                <th style="font-weight: bolder; width: 56%; text-align: left; "><strong style="vertical-align: top;"><?php echo $data->lang->line("report_head_teacter_comment"); ?>:</strong><br>
                                  <?php if (can_access('assess_character')) { ?>
                                        <textarea class="bolder form-control add_comment" type='2' style="border-bottom: 1px solid black"><?= !empty($assessment) ? $assessment->head_teacher_comment : '' ?></textarea>
                                        <?php
                                    } else {
                                    
                                        echo !empty($assessment)  ? $assessment->head_teacher_comment : '';
                                    }
                                    ?>
                                </th>
                                <th style="width: 50%;"> Headmaster's<br> Signature<br></th>
                            </tr>
                        </thead>
                    </table>
                    <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                        <thead>
                            <tr>
                                <th style="text-align: center;">
                                <?php
                                    $path = "storage/uploads/images/" . $level->stamp . "?v=1";
                                    ?>
                                    <?php
                                    $array = array(
                                        "src" => $path,
                                        'width' => '100',
                                        'height' => '100',
                                        'class' => '',
                                        'id' => "",
                                        'style' => 'position:relative; margin:-12% 10% 0 0; float:right;'
                                    );
                                    echo img($array);
                                    ?>

                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>

<?php } ?>
            <?php if (session('table') != 'parent' || session('table') != 'student') { ?>
                <div class="actionBar"><a href="#" class="buttonFinish buttonDisabled btn btn-default">Previous</a><a href="<?= url('general_character_assessment/report/' . $exam_id . '/' . $section_id . '/' . $next_id . '/' . $student->academic_year_id) ?>" class="buttonNext btn btn-success">Next</a><a href="#" class="buttonPrevious buttonDisabled btn btn-primary">Finish</a></div>
            <?php } ?>
        </div>

    </div>
</div>
</div>

<!-- Modal content start here -->
<div class="modal fade" id="report_setting_model">
    <div class="modal-dialog">
        <form action="#" method="post" class="form-horizontal" role="form">
            <input type="hidden" name="id" value=""/>
            <div class="modal-content">

                <div class="modal-header">
                    Single Report Settings
                </div>
<?php
// $vars = get_object_vars($exams_report_setting);
?>
                <div class="modal-body" >
                    <table class="table table-hover">


                        <tr style="border-bottom:1px solid whitesmoke">
                            <td style="padding-left:5px;">
                                <h4>Overall Semester Average Name</h4>
                            </td>
                            <td>
                                <input type="text" name="overall_semester_avg_name" value=""/>
                            </td>
                        </tr>
                    </table>
                </div>


                <div class="modal-footer">
                    <button type="button" style="margin-bottom:0px;" class="btn btn-default" data-dismiss="modal" onclick="javascript:closeWindow()">close') ?></button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
<?= csrf_field() ?>
        </form>
    </div>
</div>
</div>
<!-- Modal content End here -->
<script type="text/javascript">

    function refresh()
    {
        // window.location.reload();
        $('#refresh').hide();
    }

    function showSelect(a)
    {
        $('#grade_div' + a).show();
        $('#refresh' + a).show();
        $('#grade_div2' + a).show();
        $('#checkbox_div').hide();
    }
    $('.add_comment').blur(function () {
        var content = $(this).val();
        var sid =<?= $student->student_id ?>;
        var exam_id =<?= $exam_id ?>;
        var semester_id = '<?= $exam->semester_id ?>';
        var class_teacher_id = '<?= $section->teacher->teacherID ?>';
        var type = $(this).attr('type');
        if (content != '') {
            $.ajax({
                type: 'POST',
                url: "<?= url('general_character_assessment/addComment') ?>",
                data: {comment: content, sid: sid, semester_id: semester_id, exam_id: exam_id, class_teacher_id: class_teacher_id, type: type},
                dataType: "html",
                success: function (data) {
                    toast(data);
                }
            });
        }
    });

    function assesCharacter(a, b, c, d) {
        if (a == "" || b == "") {
            var data = '<h5>Missing data, please select again</h5>';
            alert(data);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('student_characters/assessReport/null') ?>",
                data: {character_id: b, student_assessed: c, exam_id: d, e_grade: 'check', a_grade: a, level: 1},
                dataType: "html",
                beforeSend: function (xhr) {
                    //jQuery('#loader-content').show();
                    $('#' + a + b + c + d).html('<a href="#/refresh"><i class="fa fa-spinner"></i> </a>');
                },
                complete: function (xhr, status) {
                    //jQuery('#loader-content').fadeOut('slow');
                    $('#' + a + b + c + d).html('<span class="label label-success">' + status + '</span>');
                },
                success: function (data) {
                    toast(data);
                }
            });
            $('#grade_div' + a).hide();
            $('#grade_div2' + a).hide();
            $('#checkbox_div').hide();
        }
    }
    function add_remark(a, b) {
        var exam_id = $('#exam_id').val();
        var character_id = $('#character_id' + a).val();
        var e_grade = $('#e_grade' + a).val();
        var a_grade = $('#a_grade' + a).val();
        var student_assessed = $('#student_assessed').val();
        var level = b;
        if (e_grade == "" || a_grade == "") {
            var data = '<h5>character_report_selectall_message") ?></h5>';
            alert(data);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('student_characters/assessReport/null') ?>",
                data: "character_id=" + character_id + "&student_assessed=" + student_assessed + "&exam_id=" + exam_id + "&e_grade=" + e_grade + "&a_grade=" + a_grade + "&level=" + level,
                dataType: "html",
                success: function (data) {
                    $('#alert' + a).html(data);

                }
            });
            $('#grade_div' + a).hide();
            $('#grade_div2' + a).hide();
            $('#checkbox_div').hide();
        }
    }
    function printDiv(divID) {
        $('.radio_inputs').hide();
        $('.well,#topnav').hide();
        window.print();
        $('.radio_inputs').show();
        $('.well,#topnav').show();
    }
   
</script>

@endsection
