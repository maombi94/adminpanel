@extends('layouts.app')
@section('content')


<link href="<?php echo url('public/assets/print.css'); ?>" rel="stylesheet" type="text/css">

<div class="well">
    <div class="row">
        <div class="col-sm-6">

            <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span
                    class="fa fa-print"></span> print') ?> </button>

            <span>&nbsp;</span>
            <?php
           
            if (isset($allsections) && count($allsections) > 0) {
                foreach ($allsections as $section_info) {
                    ?>

                    <a href="<?= url("exam/printall/" . $exams->examID . '?class_id=' . $student->classesID . '&section_id=' . $section_info->sectionID) . '&year_no=' . request('year_no') ?>"
                       target="_blank">
                        <i class="fa fa-print"></i>print') ?> all (<?= $section_info->section ?>)</a>
                    <span>&nbsp;</span>
                    <?php
                }
            }
            ?>

        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb">
                <li><a href="<?= url("dashboard/index") ?>"><i
                            class="fa fa-laptop"></i> menu_dashboard') ?></a></li>
                <li class="active">view') ?></li>
                  <button class="btn-default btn-cs btn-sm-cs" data-toggle="modal" data-target="#report_setting_model"><span class="fa fa-gear"></span> Options</button>
            </ol>
        </div>
    </div>

</div>
<!-- Begin inline CSS -->
<style type="text/css">

    .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
        font-weight: bolder
    }

    #printablediv {
        color: #000;
    }

    table.table.table-striped td, table.table.table-striped th, table.table {
        font-size: 12px !important;
        margin-left: 10%;
    }

    @page {
        margin: 0
    }

    .sheet {
        margin: 0;
        overflow: hidden;
        position: relative;
        box-sizing: border-box;
        page-break-after: always;
    }

    /** Padding area **/
    .sheet.padding-10mm {
        padding: 10mm
    }

    /** For screen preview **/
    @media screen {

        .sheet {
            background: white;
            box-shadow: 0 .5mm 2mm rgba(0, 0, 0, .3);
            margin: 5mm;
            left:12%;
        }
    }

    table.table.table-striped td, table.table.table-striped th, table.table {
        border: 1px solid #000000;
    }
    @font-face {
        font-family: DejaVuSans_b;
        src: url("../../public/assets/fonts/DejaVuSans_b.woff") format("woff");
    }

    table.table.table-striped td, table.table.table-striped th, table.table {
        border: 1px solid #000000;
        margin: 1px auto !important;
    }

    @media print {
        html, body {
            width: 95%;
            height: 90%;
            margin-left: 6%;
        }

        section.A4 {
            width: 210mm
        }

        @page {
            size: A4;
            margin: 0;

        }

        table {
            width: 100%;
            border-spacing: 0;
        }

        .subpage {
            padding: 1em 1em 0 1em;
            /* border: 3px #f00 solid; */
            font-size: .8em;
        }

        table.table.table-striped td, table.table.table-striped th, table.table {
            border: .1px solid #000;
        }

        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
            padding: 0.2%;
        }

        .table-bordered > thead > tr > th, .table-bordered > tbody > tr > td {
            font-size: 11px;
        }


    }

    h3, h4 {
        page-break-after: avoid;

    }

</style>

<div class="box-body">
    <div class="row">
        <div class="col-sm-12">

            <div id="printablediv" class="page center sheet padding-10mm" >

                <?php 
                $array = array(
                    "src" => url('storage/uploads/images/' . $siteinfos->photo),
                    'width' => '126em',
                    'height' => '126em',
                    'class' => 'img-rounded',
                    'style' => 'margin-left:2em'
                );

                $photo = array(
                    "src" => url('storage/uploads/images/' . $student->student->photo),
                    'width' => '128em',
                    'height' => '128em',
                    'class' => 'img-rounded',
                    'style' => 'margin-left:2em'
                );
                
                ?>

                <table class=" table-striped center" style="margin: 1px 2px 1px 0px;">
                    <thead>
                        <tr>
                            <th class="col-md-2" style="padding-bottom:0px">
                                <?= img($array); ?>
                            </th>
                            <th class="col-md-8 text-center" style="margin: 1% 0 0 16%; padding-top: 2%; color: #000000;">

                                <h2 style="font-weight: bolder; text-transform: uppercase;"><?= $siteinfos->sname ?></h2>
                                <h4>character_report_box") ?><?= $siteinfos->box . ', ' . $siteinfos->address ?>.</h4>

                                <h3>character_report_cell") ?> <?= str_replace(',', ' / ', $siteinfos->phone) ?></h3>
                                <h3> character_report_email") ?><?= $siteinfos->email ?></h3>
                                <h3>character_report_website") ?><?= $siteinfos->website ?></h3>
                            </th>
                           
                            <th class="col-md-2" style="padding-bottom:0px">
                                <?= img($photo); ?>
                            </th>
                            
                        </tr>
                    </thead>
                </table>
                <hr/>

                <div class="">
                    <h1 align='center' style="font-size: 1.6em; font-weight: bolder;padding-top: 0%; text-transform: uppercase; ">character_report_header") . '(' . $student->academicYear->name . ')' ?></h1>
                </div>
                <table id="example1" class="table table-bordered">
                    <thead>
                        <?php 
                        if (isset($character_categories) && count($character_categories) > 0) {

                            echo '<br/><p align="left" style="padding-top: 0%; ">' . $data->lang->line("report_student_name") . '<b>' . $student->student->name . '</b> <span class="right">Class: <b>' . $student->section->classes->classes . ', ' . $section->section . '</b></span></p><span>Adm No: <b>' . $student->student->roll . '</b></span>';
                            $tt = 1;
                            foreach ($character_categories as $character_category) {
                                ?>


                                <tr>
                                    <th colspan="<?= count($character_gradings) + 2 ?>"><?php echo '<h4 align="left" style="font-size: 12px; font-weight: bolder;padding-top: 0%; margin-top: 2%; margin-bottom:1%; color:black ">' . ucwords($character_category->character_category) . '</h4>'; ?></th>
                                </tr>
                                <?php
                                if ($tt == 1) {
                                    ?>
                                    <tr>
                                        <th class="col-lg-1">#') ?></th>
                                        <th class="col-lg-6">character') ?></th>
                                        <?php foreach ($character_gradings as $grade) { ?>
                                            <th class="col-lg-2"><?= $grade->grade ?></th>
                                        <?php } ?>

                                    </tr>
                                <?php } ?>
                            </thead>
                            <tbody>
                                <?php
                                $all_characters = \App\Model\Character::where('character_category_id', $character_category->id)->join('character_classes', 'character_classes.character_id', '=', 'characters.id')->where('class_id', $class_id)->orderBy('position', 'asc')->get();

                                $x = 1;

                                foreach ($all_characters as $all_character) {


                                    $character_id = $all_character->character_id;
                                    $student_character = \App\Model\StudentCharacter::where('character_id', $character_id)->where('student_id', $student_id)->where('exam_id', $exam_id)->first();
                                    ?>
                                    <tr>
                                        <td data-title="code') ?>">
                                            <?php echo $x; ?>
                                        </td>
                                        <td data-title="description') ?>">
                                            <?php
                                            echo $all_character->description;
                                            //
                                            // echo $student_character->grade;  
                                            ?>
                                        </td>
                                        <?php foreach ($character_gradings as $grade) { ?>
                                            <td data-title="description') ?>">
                                                <?php
//                                                    $sql = 'SELECT b.* FROM ' . set_schema_name() . ' student_characters a join ' . set_schema_name() . 'refer_character_grading_systems b on b.id=a.grade1 WHERE a.student_id = ' . $student_id . '  AND a.exam_id=' . $exam_id . ' and b.id=' . $grade->id . ' AND a.character_id =' . $character_id;
//                                                    $rem = \collect(DB::select($sql))->first();
//                                                    $grad = $character_gradings->first();
                                                if (count($student_character) && $student_character->grade == $grade->id) {
                                                    echo '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>';
                                                } else {
                                                    if (can_access('assess_character')) {
                                                        ?>
                                                        <input type="radio" name="<?= $all_character->character_id ?>" id="show_assess_select" value="<?= $grade->id ?>" class="radio_inputs" onclick ="assesCharacter(<?= $grade->id ?>, <?= $all_character->character_id ?>,<?php echo $student_id; ?>,<?php echo $exam_id; ?>);">
                                                        <span id="<?= $grade->id ?><?= $all_character->character_id ?><?php echo $student_id; ?><?php echo $exam_id; ?>"></span>
                                                    </td>
                                                    <?php
                                                }
                                            }
                                        }
                                        ?>

                                    </tr>
                                    <?php
                                    $x++;
                                }
                                $tt++;
                            }
                            ?>
                        </tbody>
                    </table><?php ?>
                    <div class="form-group" id="refresh" style="display: none;" >
                        <div class=" col-sm-12">
                            <input type="submit" class="btn btn-success pull-right" value="report_student_refresh") ?>" >
                        </div>
                    </div>
                    <!--                    <div class="col-sm-12" style="margin-top: 3%;">
                                            <div class="row">
                                                <div class="col-lg-4"></div>                              
                                                <div class="col-lg-4"></div>
                                                <div class="col-lg-4">
                                                    <h3 align='center' style="font-size: 1.5em; font-weight: bolder;padding-top: 0%; margin-bottom: 2%;"><?php echo $data->lang->line("report_grading_system"); ?></h3>
                                                    <table id="example1" style="width: 80%;" class="table table-striped table-bordered table-hover">
                                                        <thead>
                    
                                                        <th class="col-lg-4">character_report_remark") ?></th>
                                                        <th class="col-lg-2">character_report_grade") ?></th>
                    
                                                        </thead>
                                                        <tbody>
                    <?php foreach ($character_gradings as $grading_system) { ?>
                                                                                                                    <tr>
                                                                                                                        <td><?php echo $grading_system->grade_remark; ?></td>
                                                                                                                        <td><?php echo $grading_system->grade; ?></td>
                                                                                                                    </tr>
                    <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>-->
                    <div >
                        <br/>

                        <table class="table  table-bordered" style=" margin: 1% 0 0 0;">
                            <thead>
                                <tr>
                                    <th style="font-weight: bolder; width:56%; text-align: left;"><strong style="vertical-align: top;"><?php echo $data->lang->line("report_class_teacter_comment"); ?>:</strong><br> 
                                        <?php if (can_access('assess_character')) { ?>
                                            <textarea class="bolder form-control add_comment" type='1' style="border-bottom: 1px solid black"><?= count($assessment) == 1 ? $assessment->class_teacher_comment : '' ?></textarea>
                                            <?php
                                        } else {
                                            echo count($assessment) == 1 ? $assessment->class_teacher_comment : '';
                                        }
                                        ?>
                                    </th>
                                    <th style="text-align: left;">                                                               Class Teacher: <?= $section->teacher->name ?><br>
                                        <img src="<?= $section->teacher->signature ?>" width="54" height="32">
                                    </th>
                                </tr>
                            </thead>
                        </table>
                        <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                            <thead>
                                <tr>
                                    <th style="font-weight: bolder; width: 56%; text-align: left; "><strong style="vertical-align: top;"><?php echo $data->lang->line("report_head_teacter_comment"); ?>:</strong><br>
                                        <?php if (can_access('assess_character')) { ?>
                                            <textarea class="bolder form-control add_comment" type='2' style="border-bottom: 1px solid black"><?= count($assessment) == 1 ? $assessment->head_teacher_comment : '' ?></textarea>
                                            <?php
                                        } else {
                                            echo count($assessment) == 1 ? $assessment->head_teacher_comment : '';
                                        }
                                        ?>
                                    </th>
                                    <th style="width: 50%;"> Headmaster's<br> Signature<br></th>
                                </tr> 
                            </thead>
                        </table>
                        <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">

                                        <?php
                                        $path = "storage/uploads/images/stamp_" . set_schema_name() . "png";
                                        ?>
<?php
                                                    $array_ = array(
                                                        "src" => $path,
                                                        'width' => '100',
                                                        'height' => '100',
                                                        'class' => '',
                                                        'id' => "",
                                                        'style' => 'position:relative; margin:-11% 15% 0 0; float:right;'
                                                    );
                                                    echo img($array_);
                                                    ?>
                                       
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="col-lg-12">

                        <?php
                    }
                    ?>


                </div>

            </div>
            <?php if (strtolower(session('usertype')) != 'parent') { ?>
                <div class="actionBar"><a href="#" class="buttonFinish buttonDisabled btn btn-default">Previous</a><a href="<?= url('general_character_assessment/report/' . $exam_id . '/' . $section_id . '/' . $next_id . '/' . $student->academic_year_id) ?>" class="buttonNext btn btn-success">Next</a><a href="#" class="buttonPrevious buttonDisabled btn btn-primary">Finish</a></div>
            <?php } ?>  
        </div>
    </div>
</div>

 <!-- Modal content start here -->
    <div class="modal fade" id="report_setting_model">
        <div class="modal-dialog">
            <form action="#" method="post" class="form-horizontal" role="form">
                <input type="hidden" name="id" value=""/>
                <div class="modal-content">

                    <div class="modal-header">
                        Single Report Settings
                    </div>
                    <?php
                   // $vars = get_object_vars($exams_report_setting);
                    ?>
                    <div class="modal-body" > 
                        <table class="table table-hover">
                         
   
                            <tr style="border-bottom:1px solid whitesmoke">
                                <td style="padding-left:5px;">
                                    <h4>Overall Semester Average Name</h4>
                                </td>
                                <td>
                                    <input type="text" name="overall_semester_avg_name" value=""/>
                                </td>
                            </tr>
                        </table>   
                    </div>


                    <div class="modal-footer">
                        <button type="button" style="margin-bottom:0px;" class="btn btn-default" data-dismiss="modal" onclick="javascript:closeWindow()">close') ?></button>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                    <?= csrf_field() ?>
            </form>
        </div>
    </div>
    </div>
    <!-- Modal content End here -->
<script type="text/javascript">

    function refresh()
    {
        // window.location.reload();
        $('#refresh').hide();
    }

    function showSelect(a)
    {
        $('#grade_div' + a).show();
        $('#refresh' + a).show();
        $('#grade_div2' + a).show();
        $('#checkbox_div').hide();
    }
    $('.add_comment').blur(function () {
        var content = $(this).val();
        var sid =<?= $student->student_id ?>;
        var exam_id =<?= $exam_id ?>;
        var semester_id = '<?= $exam->semester_id ?>';
        var class_teacher_id = '<?= $section->teacher->teacherID ?>';
        var type = $(this).attr('type');
        if (content != '') {
            $.ajax({
                type: 'POST',
                url: "<?= url('general_character_assessment/addComment') ?>",
                data: {comment: content, sid: sid, semester_id: semester_id, exam_id: exam_id, class_teacher_id: class_teacher_id, type: type},
                dataType: "html",
                success: function (data) {
                    toast(data);
                }
            });
        }
    });

    function assesCharacter(a, b, c, d) {
        if (a == "" || b == "") {
            var data = '<h5>Missing data, please select again</h5>';
            alert(data);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('student_characters/assessReport/null') ?>",
                data: {character_id: b, student_assessed: c, exam_id: d, e_grade: 'check', a_grade: a, level: 1},
                dataType: "html",
                beforeSend: function (xhr) {
                    //jQuery('#loader-content').show();
                    $('#' + a + b + c + d).html('<a href="#/refresh"><i class="fa fa-spinner"></i> </a>');
                },
                complete: function (xhr, status) {
                    //jQuery('#loader-content').fadeOut('slow');
                    $('#' + a + b + c + d).html('<span class="label label-success">' + status + '</span>');
                },
                success: function (data) {
                    toast(data);
                }
            });
            $('#grade_div' + a).hide();
            $('#grade_div2' + a).hide();
            $('#checkbox_div').hide();
        }
    }
    function add_remark(a, b) {
        var exam_id = $('#exam_id').val();
        var character_id = $('#character_id' + a).val();
        var e_grade = $('#e_grade' + a).val();
        var a_grade = $('#a_grade' + a).val();
        var student_assessed = $('#student_assessed').val();
        var level = b;
        if (e_grade == "" || a_grade == "") {
            var data = '<h5>character_report_selectall_message") ?></h5>';
            alert(data);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('student_characters/assessReport/null') ?>",
                data: "character_id=" + character_id + "&student_assessed=" + student_assessed + "&exam_id=" + exam_id + "&e_grade=" + e_grade + "&a_grade=" + a_grade + "&level=" + level,
                dataType: "html",
                success: function (data) {
                    $('#alert' + a).html(data);

                }
            });
            $('#grade_div' + a).hide();
            $('#grade_div2' + a).hide();
            $('#checkbox_div').hide();
        }
    }
    function printDiv(divID) {
        $('.radio_inputs').hide();
        $('.well,#topnav').hide();
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
                    '<html><head><title></title></head><body>' +
                    divElements + '</body>';

            //Print Page
            window.print();
            //Restore orignal HTML
            document.body.innerHTML = oldPage;

        $('.radio_inputs').show();
        $('.well,#topnav').show();
    }
</script>
@endsection