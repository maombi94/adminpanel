
<style type="text/css">
    .innertable{
        height: 7em;
    }
    .outertable tr td{
        border: 1px solid black;

    }
    .outertable{
        width: 100%;
        max-width: 100%;
        background-color: transparent;
        border-spacing: 0;
        border-collapse: collapse;
    }


</style>
    <?php
        ini_set('max_execution_time', 360);
        /**
         * Description of print
         *
         *  -----------------------------------------------------
         *  Copyright: INETS COMPANY LIMITED
         *  Website: www.inetstz.com
         *  Email: info@inetstz.com
         *  -----------------------------------------------------
         * @author Ephraim Swilla */
    ?>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
        <title>Exam Report</title>

        <link href="<?php echo url('public/assets/print.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo url('public/assets/bootstrap/3.3.2/bootstrap.min.css'); ?>" rel="stylesheet">

        <link rel="shortcut" href="<?= url(); ?>public/assets/images/favicon.ico">

   <!-- Begin inline CSS -->
      <style type="text/css">

      .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
        font-weight: bolder
      }

      #printablediv {
        color: #000;
      }

      table.table.table-striped td, table.table.table-striped th, table.table {
        font-size: 12px !important;
        margin-left: 5%;
      }

      @page {
        margin: 0;
      }

      .sheet {
        margin: 0;
        overflow: hidden;
        position: relative;
        box-sizing: border-box;
        page-break-after: always;
      }

      /** Padding area **/
      .sheet.padding-10mm {
        padding: 10mm
      }

      /** For screen preview **/
      @media screen {

        .sheet {
          background: white;
          box-shadow: 0 .5mm 2mm rgba(0, 0, 0, .3);
          margin: 5mm;
          left: 5%;
        }
      }

      table.table.table-striped td, table.table.table-striped th, table.table {
        border: 1px solid #000000;
      }
      @font-face {
        font-family: DejaVuSans_b;
        src: url("../../public/assets/fonts/DejaVuSans_b.woff") format("woff");
      }

      table.table.table-striped td, table.table.table-striped th, table.table {
        border: 1px solid #000000;
        margin: 1px auto !important;
      }

      @media print {
        html, body {
          width: 95%;
          height: 90%;
        }

        section.A4 {
          width: 210mm;
        }

        @page {
          size: A4;
          margin: 0;
        }

        table {
          width: 100%;
          border-spacing: 0;
        }

        .subpage {
          padding: 1em 1em 0 1em;
          /* border: 3px #f00 solid; */
          font-size: .8em;
        }

        table.table.table-striped td, table.table.table-striped th, table.table {
          border: .1px solid #000;
        }

        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
          padding: 0.2%;
        }

        .table-bordered > thead > tr > th, .table-bordered > tbody > tr > td {
          font-size: 11px;
        }
      }
    </style>
</head>
    <body onload="window.print()">
  
            <?php
            if (isset($character_categories) && !empty($character_categories)) {
                foreach($students as $student){
                    $obj = [
                        'student_id' => $student->student_id, 'exam_id' => $exam_id
                    ];
                    $assessment = DB::table('general_character_assessment')->where($obj)->first();
                ?>
            <div class="row">
                <div class="col-md-12">
                <div class="page center sheet padding-10mm" >
                    <?php
                    $array = array(
                        "src" => url('storage/uploads/images/' . $siteinfos->photo),
                        'width' => '126em',
                        'height' => '126em',
                        'class' => 'img-rounded',
                        'style' => 'margin-left:2em'
                    );

                    $photo = array(
                        "src" => url('storage/uploads/images/' . $student->student->photo),
                        'width' => '128em',
                        'height' => '128em',
                        'class' => 'img-rounded',
                        'style' => 'margin-left:2em'
                    );
                    ?>

                    <table class=" table-striped center " style="margin: 1px 2px 1px 0px;">
                        <thead>
                            <tr>
                                <th class="col-md-2" style="padding-bottom:0px">
                                    <?= img($array); ?>
                                </th>
                                <th class="col-md-8 text-center" style="margin: 1% 0 0 16%; padding-top: 2%; color: #000000;">

                                    <h2 style="font-weight: bolder; text-transform: uppercase;"><?= $siteinfos->sname ?></h2>
                                    <h4>character_report_box") ?><?= $siteinfos->box . ', ' . $siteinfos->address ?>.<br>
                                     character_report_cell") ?> <?= str_replace(',', ' / ', $siteinfos->phone) ?><br>
                                     character_report_email") ?><?= $siteinfos->email ?><br>
                                     character_report_website") ?><?= $siteinfos->website ?></h4>
                                </th>

                                <th class="col-md-2" style="padding-bottom:0px">
                                    <?= img($photo); ?>
                                </th>

                            </tr>
                        </thead>
                    </table>
                    <hr/>

                    <div class="">
                        <h1 align='center' style="font-size: 1.6em; font-weight: bolder;padding-top: 0%; text-transform: uppercase; ">character_report_header") . '(' . $student->academicYear->name . ')' ?></h1>
                    </div>
                    <div class="">
                        <?php echo '<p align="left" style="padding-top: 0%; ">' . $data->lang->line("report_student_name") . '<b>' . $student->student->name . '</b> <span style="float: right">Class: <b>' . $student->section->classes->classes . ', ' . $section->section . '</b></span></p><span>Adm No: <b>' . $student->student->roll . '</b></span>'; ?>
                    </div>

                    <h5 align='center' style="font-weight: bolder;padding-top: 0%; text-transform: uppercase; ">DEVELOPMENT OF LEARNING COMPETENCE</h5>
                    <?php
                
                    foreach ($character_categories as $character_category) {
                        ?>
                        <?php echo '<h4 align="left" style="font-size: 13px; font-weight: bolder;padding-top: 0%; margin-top: 2%; margin-bottom:1%; color:black ">Competence: ' . ucwords($character_category->character_category) . '</h4>'; ?>

                        <?php
                        $all_characters = \App\Model\Character::where('character_category_id', $character_category->id)->join('character_classes', 'character_classes.character_id', '=', 'characters.id')->where('class_id', $class_id)->orderBy('position', 'asc')->get();
                        $x = 1;
                       
                        ?>
                        <table class="outertable table table-bordered">
                                <tr>
                                    <?php
                                    foreach ($all_characters as $all_character) {

                                        $character_id = $all_character->character_id;
                                        $student_character = \App\Model\StudentCharacter::where('character_id', $character_id)->where('student_id', $student->student_id)->where('exam_id', $exam_id)->first();
                                        
                                        ?>
                                        <td style="padding:0; border:none">
                                            <table class="innertable table table-bordered" style="padding-bottom:0;margin-bottom: 0;border:none">
                                                <thead>
                                                    <tr>
                                                        <th class="col-md-2" colspan="10" style="height:3em !important;"> <?= $all_character->description; ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <tr>
                                                        <?php foreach ($character_gradings as $grade) { ?>
                                                            <td style="height:2em !important; text-align: center"><?= $grade->grade ?></td>
                                                        <?php } ?>
                                                    </tr>
                                                    <tr>
                                                        <?php foreach ($character_gradings as $grade) { ?>
                                                            <td style="height:2em !important; text-align: center"> <?php
                                                            if ($student_character && $student_character->grade == $grade->id) {
                                                                echo '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>';
                                                            } else {
                                                                if (can_access('assess_character')) {
                                                                    ?>
                                                                        <input type="radio" name="<?= $all_character->character_id ?>" id="show_assess_select" value="<?= $grade->id ?>" class="radio_inputs" onclick ="assesCharacter(<?= $grade->id ?>, <?= $all_character->character_id ?>,<?php echo $student->student_id; ?>,<?php echo $exam_id; ?>);">
                                                                        <span id="<?= $grade->id ?><?= $all_character->character_id ?><?php echo $student->student_id; ?><?php echo $exam_id; ?>"></span>

                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </td>
                                                        <?php } ?>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>

                                    <?php } ?>
                                </tr>
                            
                        </table>
                        <?php
                        }
                    
                        ?>

                    <table class="table  table-bordered" style=" margin: 1% 0 0 0;">
                        <thead>
                            <tr>
                                <th style="font-weight: bolder; width:56%; text-align: left;"><strong style="vertical-align: top;"><?php echo $data->lang->line("report_class_teacter_comment"); ?>:</strong><br>
                                    <?php
                                        echo $assessment ? $assessment->class_teacher_comment : '';
                                    ?>
                                </th>
                                <th style="text-align: left;">
                                Class Teacher: <?= $section->teacher->name ?><br>
                                    <img src="<?= $section->teacher->signature ?>" width="54" height="32">
                                </th>
                            </tr>
                        </thead>
                    </table>
                    <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                        <thead>
                            <tr>
                                <th style="font-weight: bolder; width: 56%; text-align: left; "><strong style="vertical-align: top;"><?php echo $data->lang->line("report_head_teacter_comment"); ?>:</strong><br>
                                    <?php if (can_access('assess_character')) { ?>
                                        <?= !empty($assessment) ? $assessment->head_teacher_comment : '' ?>
                                        <?php
                                    } else {
                                        echo !empty($assessment)  ? $assessment->head_teacher_comment : '';
                                    }
                                    ?>
                                </th>
                                <th style="width: 50%;"> Headmaster's<br> Signature<br></th>
                            </tr>
                        </thead>
                    </table>
                    <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
                        <thead>
                            <tr>
                                <th style="text-align: center;">

                                <?php
                                    $path = "storage/uploads/images/" . $level->stamp . "?v=1";
                                    ?>
                                    <?php
                                    $array = array(
                                        "src" => $path,
                                        'width' => '100',
                                        'height' => '100',
                                        'class' => '',
                                        'id' => "",
                                        'style' => 'position:relative; margin:-12% 10% 0 0; float:right;'
                                    );
                                    echo img($array);
                                    ?>


                                </th>
                            </tr>
                        </thead>
                    </table>
                    </div>
                </div>
            </div>
            </div>
        <?php 
            } 
        }

    ?>
    