@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>

<div class="page-body">
<div class="card">
                <div class="card-header">
                    <a href="{{ url('users/create') }}" class="btn btn-success">Add User</a>
                </div>
                <div class="card-block">

                <?php 
                    $usertype = session("usertype");
                    if(can_access('add_character')) {
                ?>
                    <h5 class="page-header">
                        <a class="btn btn-success" href="<?php echo url('character_categories/add') ?>">
                            <i class="fa fa-plus"></i> 
                           Add
                        </a>
                    </h5>
                <?php } ?>

                <center>
        <div class="row">
            <div class="col-sm-12">
             <div class="col-sm-8 col-sm-offset-2 list-group">
               <div class="list-group-item list-group-item-warning">
                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                 <?php
                    if (form_error($errors,'classlevel'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                    <label for="class_level_id" class="col-sm-2 control-label">
                        classlevel') ?>*
                    </label>
                    <div class="col-sm-8">
                        <?php
                        $array = array("0" => $data->lang->line("classlevel"));
                        if (count($classlevels)) {
                        foreach ($classlevels as $level) {
                            $array[$level->classlevel_id] = $level->name;
                        }
                        }
                        echo form_dropdown("class_level_id", $array, old("class_level_id"), "id='class_level_id' class='form-control'");
                        ?>
                    </div>
                    <span class="col-sm-2 control-label">
                         <?php echo form_error($errors,'classlevel'); ?>
                    </span>
                    </div>

                    <?php
                    if (form_error($errors,'academic_year'))
                    echo "<div class='form-group has-error' >";
                    else
                    echo "<div class='form-group' >";
                    ?>
                    <label for="academic_year_id" class="col-sm-2 control-label">
                    academic_year') ?>*
                    </label>
                    <div class="col-sm-8">
                    <div class="select2-wrapper">
                            <select class="form-control" id="academic_year_id" name="academic_year_id">
                              <option value="">Select Academic Year</option>
                            </select>
                        </div>
                    </div>
                    <span class="col-sm-2 control-label">
                         <?php echo form_error($errors,'academic_year'); ?>
                    </span>
                    </div>


                   <?php
                    if (form_error($errors,'semester'))
                    echo "<div class='form-group has-error' >";
                    else
                    echo "<div class='form-group' >";
                    ?>
                    <label for="semester_id" class="col-sm-2 control-label">
                    semester') ?>*
                    </label>
                    <div class="col-sm-8">
                       <div class="select2-wrapper">
                            <select class="form-control" id="semester_id" name="semester_id">
                              <option value="">Select Semester</option>
                            </select>
                        </div>
                    </div>
                    <span class="col-sm-2 control-label">
                         <?php echo form_error($errors,'semester'); ?>
                    </span>
                    </div>


                        <?php
                        if (form_error($errors,'class'))
                            echo "<div class='form-group has-error' >";
                        else
                            echo "<div class='form-group' >";
                        ?>
                        <label for="classes_id" class="col-sm-2 control-label">
                            class") ?> *
                        </label>
                        <div class="col-sm-8">
                        <div class="select2-wrapper">
                            <select class="form-control" id="classes_id" name="classes_id">
                              <option value="">Select Class</option>
                            </select>
                        </div>
                     </div>
            	    <span class="col-sm-2 control-label">
            		     <?php echo form_error($errors,'class'); ?>
            	    </span>
                    </div>

                    <?php
                        if (form_error($errors,'section'))
                            echo "<div class='form-group has-error' >";
                        else
                            echo "<div class='form-group' >";
                        ?>
                        <label for="section_id" class="col-sm-2 control-label">
                            section") ?> 
                        </label>
                        <div class="col-sm-8">
                            <div class="select2-wrapper">
                            <select class="form-control" id="section_id" name="section_id">
                            </select>
                        </div>
                     </div>
                    <span class="col-sm-2 control-label">

                    <?php echo form_error($errors,'section'); ?>
                    </span>
                    </div>


                    <?php
                        if(form_error($errors,'student_assessed')) 
                                        echo "<div class='form-group has-error' >";

                                        else
                                        echo "<div class='form-group' >";
                                ?>
                     <label for="select student" class="col-sm-2 control-label">
                     student_assessed") ?> *
                     </label>
                      <div class="col-sm-8">
                          <select class="select2_multiple form-control" multiple="multiple" name="student_assessed[]" id="student_assessed">
                           
                          </select>
                        </div>
                        <span class="col-sm-2 control-label">
                            <?php echo form_error($errors,'student_assessed'); ?>
                        </span>
                      </div>


                      <?php
                        if (form_error($errors,'general_comment'))
                            echo "<div class='form-group has-error' >";
                        else
                            echo "<div class='form-group' >";
                        ?>
                        <label for="comment" class="col-sm-2 control-label">
                            general_comment") ?> *
                        </label>
                        <div class="col-sm-8">
                            <textarea class="form-control" id="comment" name="comment">
                            </textarea>
                     </div>
                    <span class="col-sm-2 control-label">
                         <?php echo form_error($errors,'general_comment'); ?>
                    </span>
                    </div>

                    <div class="row" id="message">
                          
                      </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$data->lang->line("send_comment")?>" >
                        </div>
                    </div>
                <?= csrf_field() ?>
</form>
                </div>
             </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $('#semester_id').change(function(event) {
    var academic_year_id = $('#academic_year_id').val();
    var class_level_id = $('#class_level_id').val();
    if (semester_id=='') {
        //
    }else{
        $.ajax({
            type: 'POST',
            url: "<?=url('general_character_assessment/classesGeneralAssess')?>",
            data: "academic_year_id=" + academic_year_id+"&class_level_id="+class_level_id,
            dataType: "html",
            success: function(data) {
               $('#classes_id').html(data);
               //alert(data);
            }
        });
    }
});

     $('#classes_id').change(function (event) {
    var classes_id = $(this).val();
    if (classes_id == '') {
        //
    } else {
        $.ajax({
        type: 'POST',
        url: "<?= url('general_character_assessment/getSectionByClass') ?>",
        data: "classes_id=" + classes_id,
        dataType: "html",
        success: function (data) {
            $('#section_id').html(data);
        }
        });
    }
    })

$('#section_id').change(function (event) {
    var classes_id = $('#classes_id').val();
    var semester_id = $('#semester_id').val();
    var academic_year_id = $('#academic_year_id').val();
    if (class_level_id == "") {

    } else{
        $.ajax({
            type: 'POST',
            url: "<?=url('general_character_assessment/generalCallStudentsToAssess')?>",
            data: "classes_id=" + classes_id +"&semester_id=" + semester_id,
            dataType: "html",
            success: function(data) {
               $('#student_assessed').html(data);
               //alert(data);
            }
        });
    }
}); 

$('#classes_id').change(function(event) {
    var classes_id = $(this).val();
    var semester_id = $('#semester_id').val();
    var academic_year_id = $('#academic_year_id').val();
    if (semester_id =='' && classes_id =='') {
        //
    }else{
        $.ajax({
            type: 'POST',
            url: "<?=url('general_character_assessment/countStudentGeneralAssessment')?>",
            data: "classes_id=" + classes_id +"&semester_id=" + semester_id,
            dataType: "html",
            success: function(data) {
               $('#message').html(data);
               //alert(data);
            }
        });
    }
});


$('#class_level_id').change(function (event) {
    var class_level_id = $(this).val();
    if (class_level_id === '0') {
        $('#academic_year_id').val(0);
    } else {
        $.ajax({
        type: 'POST',
        url: "<?= url('exam/get_academic_years_bylevel') ?>",
        data: "class_level_id=" + class_level_id,
        dataType: "html",
        success: function (data) {
            $('#academic_year_id').html(data);
        }
        });
        $('.class'+class_level_id).show();
    }
    });

 $('#academic_year_id').change(function (event) {
    var academic_year_id = $(this).val();
    if (academic_year_id === '0') {
        $('#semester_id').val(0);
    } else {
        $.ajax({
        type: 'POST',
        url: "<?= url('semester/get_semester') ?>",
        data: "academic_year_id=" + academic_year_id,
        dataType: "html",
        success: function (data) {
            $('#semester_id').html(data);
        }
        });
    }
    });

</script>


@endsection